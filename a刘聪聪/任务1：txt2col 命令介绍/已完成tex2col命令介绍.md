> ## 文本分析相关之 tex2col命令

`tex2col` 命令旨在将一个Stata单元格的内容分成不同的列，主要适用于从 PDF 文件中提取数据并以简单的方式管理。使用此命令可将选定的数据转换为表格，将文本拆分为不同的列。

该命令可以将以逗号、空格分隔的表格 (无法直接粘贴到 Excel 中) 转换成以 **Tab** 分隔的表格，同时采用 `ignore("chars") ` 选项可达到去掉 % 、千分位符号的效果。

## 1.tex2col  命令简介

```stata
help tex2col  //Split Text into Columns

tex2col [if] [in] [,
               columns(#)
               data(string)
               cname(stub)
               rname(newvarname)
               dpcomma
               ignore("chars")]
* columns(#)                         Number of columns with data
* data(string)                       Name of the variable that contains the data
* cname(stub)                        Column names
* rname(newvarname)                  Row name
* dpcomma                            Convert data with commas as decimals to period-decimal format
* ignore("chars")                    Remove specified non-numeric characters
```
## 2.Stata 范例

### Example 1: 逗号分隔的文本文件 &rarr; 表格

```stata
    clear
    input str60 (data)
        "Chaco Hamedo 16,6 25,1 21,5 881,5 58 73 66"
        "Chaco Seco 16,2 24,3 21,3 736,1 60 79 71"
        "Valles Centrales 14,7 18,3 16,9 721,4 50 74 62"
        "Valles del Sur 12,2 18,1 16 351,3 40 70 53"
    end

    tex2col, data(data) col(7) dpcomma  // dpcomma: 把逗号转换成句点
    list row col*, clean 
```

结果如下：

```stata
. list row col*, clean 

                    row   col_1   col_2   col_3   col_4   col_5   col_6   col_7  
  1.       Chaco Hamedo    16.6    25.1    21.5   881.5      58      73      66  
  2.         Chaco Seco    16.2    24.3    21.3   736.1      60      79      71  
  3.   Valles Centrales    14.7    18.3    16.9   721.4      50      74      62  
  4.     Valles del Sur    12.2    18.1      16   351.3      40      70      53 
```

**注意事项：**
- 使用 `input str##` 输入数据时，由于变量都是字符串类型，所以每一行观察值都要用半角双引号包围。
- `tex2col` 命令中的 `col(#)` 选项是必填项，**#** 需要根据数据的列数自行指定。

### Example 2: 空格分隔的文本文件 &rarr; 表格  

 ```stata 
    clear
    input str60 (data)
        "Argentina 2011 18.7%"
        "Bolivia 2011 0.4%"
        "Brasil 2011 3.6%"
        "Chile 2011 1.7%"
        "Colombia 2011 5.2%"
        "Costa Rica 2010 8.0%"
        "Ecuador 2011 3.1%"
        "El Salvador 2010 0.3%"
        "Honduras 2011 3.0%"
        "Mexico 2010 3.1%"
        "Panam 2011 0.6%"
        "Paraguay 2010 0.9%"
        "Per 2011 24.1%"
        "Uruguay 2011 4.3%"
        "Venezuela 2011 10.3%"
    end

    tex2col, data(data) col(2) ignore(%)
    list
```

结果如下：

```stata
.list				
     +-----------------------------------------------------+
     |         row   col_1   col_2                    data |
     |-----------------------------------------------------|
  1. |   Argentina    2011    18.7    Argentina 2011 18.7% |
  2. |     Bolivia    2011      .4       Bolivia 2011 0.4% |
  3. |      Brasil    2011     3.6        Brasil 2011 3.6% |
  4. |       Chile    2011     1.7         Chile 2011 1.7% |
  5. |    Colombia    2011     5.2      Colombia 2011 5.2% |
     |-----------------------------------------------------|
  6. |  Costa Rica    2010       8    Costa Rica 2010 8.0% |
  7. |     Ecuador    2011     3.1       Ecuador 2011 3.1% |
  8. | El Salvador    2010      .3   El Salvador 2010 0.3% |
  9. |    Honduras    2011       3      Honduras 2011 3.0% |
 10. |      Mexico    2010     3.1        Mexico 2010 3.1% |
     |-----------------------------------------------------|
 11. |       Panam    2011      .6         Panam 2011 0.6% |
 12. |    Paraguay    2010      .9      Paraguay 2010 0.9% |
 13. |         Per    2011    24.1          Per 2011 24.1% |
 14. |     Uruguay    2011     4.3       Uruguay 2011 4.3% |
 15. |   Venezuela    2011    10.3    Venezuela 2011 10.3% |
     +-----------------------------------------------------+
```

**注意事项:**	
- `tex2col`命令中的 `ignore("chars") ` 选项是选填项，**chars** 根据数据的特征自行指定。

### Example 3: 去掉千分位符号  

```stata
  clear
  input str200 (data)	
    "738 0.333 . . . . ."
    "738 0.802 0.802 4,527 0.708 5.33 0.000"
    "738 0.560 0.560 4,527 0.398 8.28 0.000"
    "738 0.420 0.420 4,527 0.114 22.16 0.000"
    "738 0.225 0.225 4,527 0.068 14.10 0.000"
    "738 0.234 0.234 4,527 0.101 10.44 0.000"
  end
  compress

  tex2col, data(data) col(7) cname(v) ignore(,)
  list row v*, clean noobs	
```  

结果如下:

```stata
 list row v*, clean noobs
    row    v1     v2     v3     v4     v5      v6   v7  
          738   .333      .      .      .       .    .  
          738   .802   .802   4527   .708    5.33    0  
          738    .56    .56   4527   .398    8.28    0  
          738    .42    .42   4527   .114   22.16    0  
          738   .225   .225   4527   .068    14.1    0  
          738   .234   .234   4527   .101   10.44    0  
```

**注意事项:**
- `tex2col` 命令中的`cname(stub)`选项是选填项，定义拆分后的列名。
- 须保持原始数据中某一列数据类型一致，否则 `tex2col` 命令不能奏效。详细说明如下：

```stata
  clear
  input str200 (data)	
    "738 0.333 . d . . ."
    "738 0.802 0.802 4,527 0.708 5.33 0.000"
    "738 0.560 0.560 4,527 0.398 8.28 0.000"
    "738 0.420 0.420 4,527 0.114 22.16 0.000"
    "738 0.225 0.225 4,527 0.068 14.10 0.000"
    "738 0.234 0.234 4,527 0.101 10.44 0.000"
  end
  compress

  tex2col, data(data) col(7) cname(v) ignore(,)
  list row v*, clean noobs
```

当将第一行、第四列的数据  `.`  改为 `d` ，这个时候第四列的数据类型就有字符型和数值型两种类型。而经过 `tex2col` 命令处理后，可以发现第四列的数据类型全部变成了字符型了。这也就说明，`tex2col` 在执行的时候会判断某一列的数据类型，如果这一列中有字符型，那么 `ignore(#)` 选项自然就失去作用了。

![image.png](https://gitee.com/uploads/images/2019/0501/233159_224a4c42_4951405.png)

#### **其他文本分析的一些命令**

```stata
help screening // 文字变量的清理 , Stata Journal 10-3
help txttool // 文字变量的清理 , Stata Journal 14-4
help tex_equal // 多个文本的对比
help fren // 修改文件名称
help fdta // 替换文字变量的内容
```