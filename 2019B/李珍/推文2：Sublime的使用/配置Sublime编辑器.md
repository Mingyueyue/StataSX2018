> 作者：李珍  (厦门大学)     
> &emsp;
> Stata 连享会： [知乎](https://zhuanlan.zhihu.com/arlion) | [简书](http://www.jianshu.com/u/69a30474ef33) | [码云](https://gitee.com/arlionn) | [CSDN](https://blog.csdn.net/arlionn)

> Stata连享会 [计量专题](https://gitee.com/arlionn/Course/blob/master/README.md)  || [精品课程](https://active.clewm.net/B9EOq8?qrurl=https://c3.clewm.net/B9EOq8&gtype=1&key=4cedd150575bd900c651021e9795edbac41ce34075) || [推文集锦](https://www.jianshu.com/p/de82fdc2c18a) 

[![点击查看完整推文列表](https://images.gitee.com/uploads/images/2019/0828/100126_fbd87a3e_1522177.png)](https://mp.weixin.qq.com/s/RwkuPpLS7bI5C5OhRqjkOw)



&emsp;
> #### [连享会#金秋十月 @ 空间计量专题 (2019.10.24-27,成都)](https://gitee.com/arlionn/Course/blob/master/2019Spatial.md)
[![](https://images.gitee.com/uploads/images/2019/0828/100126_a5038cf8_1522177.png)](https://mp.weixin.qq.com/s/aU6B9HZaf2BSHF7lSLe6LA)




&emsp;


首先我们来看看 Sublime Text 的界面，显然右边 Sublime 的代码呈现比 Stata 自带的  do-file 酷炫、美观很多：

![111.png](https://images.gitee.com/uploads/images/2019/0828/100126_101b259b_1522177.png)


如何做到呢？本文基于 [Use Sublime!]( https://acarril.github.io/posts/use-st3#stata)，介绍如何配置 **subline** 以便能编写和执行 Stata 命令。

## 1、什么是 Sublime ？

[Sublime Text]([https://www.sublimetext.com/) 是一个免费的跨平台源代码编辑器。我们可以轻松添加多个插件来扩展它的功能，它不仅可以编辑 Stata ， Python ， R ， LaTeX ， Julia 等各种软件的代码，还可以自定义其他方面，像是多列布局或快捷键等等。

你可能会问，这和目前在 Python 或者 LaTeX 中运行使用的像 PyCharms 这样的 Python IDE 编辑器和像 Texmaker 的 LaTeX 编辑器有什么不同吗？

事实上，如果我们遇到大型复杂的项目时，需要处理各种各样的文件，这个时候如果拥有一个可以处理所有文件的轻量级的、可自定义的编辑器，我们能够更容易地掌握全局，并快速深入了解任何类型的代码。

## 2、Sublime的安装

前往[Sublime Text](sublimetext.com)，选择适合电脑操作系统的 Sublime Text 3（ST3） 下载并安装。

安装 ST3 后，还必须安装 **Package Control** ，以便轻松添加和删除其他插件。

### 方法一：通过 Sublime Text

这是最简单容易、推荐使用的方法。我们只需要打开已经安装的 ST3 界面，使用 ``ctrl +`` 快捷方式或通过菜单 `View > Show Console` 访问控制台，将下面的 ST3 的 Python 代码粘贴到控制台中。（你也可以查看 [PackageControl](https://packagecontrol.io/installation#st3)，使用适用于 Sublime Text 2 的安装代码。）

```
import urllib.request,os,hashlib; h = '6f4c264a24d933ce70df5dedcf1dcaee' + 'ebe013ee18cced0ef93d5f746d80ef60'; pf = 'Package Control.sublime-package'; ipp = sublime.installed_packages_path(); urllib.request.install_opener( urllib.request.build_opener( urllib.request.ProxyHandler()) ); by = urllib.request.urlopen( 'http://packagecontrol.io/' + pf.replace(' ', '%20')).read(); dh = hashlib.sha256(by).hexdigest(); print('Error validating download (got %s instead of %s), please try manual install' % (dh, h)) if dh != h else open(os.path.join( ipp, pf), 'wb' ).write(by)
```

### 方法二：手动安装

首先，单击 `Preferences > Browse Packages… menu`；

然后，浏览文件夹，找到 `Installed Packages` 文件夹；

接着，下载 [Package Control.sublime-package](https://packagecontrol.io/Package%20Control.sublime-package)，并将它拷贝至 `Installed Packages` 文件夹中，即存放于 `Installed Packages/` 路径下；

最后，重新启动 `Sublime Text` 就完成安装了。


## 3、Sublime的界面优化

在菜单栏中 `Tools` 的下拉选项中选择 `Command Palette` ，输入 `install` ，按 `回车键` , ST3 会出现对话:

![image.png](https://images.gitee.com/uploads/images/2019/0828/100126_026d9e25_1522177.png)

在对话框中搜索 **Monokai Pro** ，点击 enter 进行安装。**Monokai Pro** 还有一系列“彩色滤镜”，通过 ` Tools ‣ Command Palette ‣ Monokai Pro: select theme`可以查看。

![image.png](https://images.gitee.com/uploads/images/2019/0828/100126_7f73bfb8_1522177.png)


## 4、准备工作
为了能够使用 Sublime Text 对 Stata 文件进行编辑，首先要给 ST3 添加插件。

在 Windows 中有两个用 Sublime Text 的 Stata 插件 ：StataEditor 和 Stata Enhanced 。 此处以 StataEditor 进行说明。

### 安装步骤:

#### 1、安装 StataEditor ：

在ST3中，使用 `Ctrl + Shift + P` 启动命令面板，输入 **install** 并按 Enter 键 (也可以在菜单栏中 `Tools` 的下拉选项中选择 `Command Palette` ，输入 `install` ，按 `回车键` ），搜索 **StataEditor** ，然后单击它即可安装。

#### 2、根据提示，安装 Pywin32 ：

重复上述过程，安装 Pywin32 插件；


#### 3、配置 StataEditor ：

根据提示，在 ST3 中，利用 `Preferences -> Package Settings -> StataEditor -> Settings - Default` 。我们可以复制默认设置内容，然后打开 `Preferences -> Package Settings -> StataEditor -> Settings - User` 根据自己电脑的设置更改 Stata 的版本的路径。 

例如，我的电脑显示 Windows 中的配置文件如下所示（注意每条设置之间需要用逗号隔开）
	
	*为了使 Stata 正常工作，需要提供 Stata 的安装位置。（使用正斜杠而不是反斜杠）*
```
	"stata_path": "C:/FILES/本地磁盘F/StataSE12.0/StataSE.exe",
```
	* 为确保 Sublime Text 可以正常使用 Stata 编码的代码，需要提供 Stata 的版本。 Windows-1252 用于 Stata 13 及更早版本，而 utf-8 用于 Stata 14 。*

```
	"stata_version": 12,
```


#### 4、注册Stata Register Stata Automation library: 

根据 ST3 给出的路径，创建 ** StataSE.exe ** 的快捷方式，然后右击快捷方式，选择属性，在目标中，路径在末尾添加  ` /Register` （**注意：/ 前有空格**）。例如将目标改为 `"C:\Program Files\Stata14\StataSE.exe"  /Register`，然后点击应用并确定更改。之后再次右键单击快捷方式，选择“以管理员身份运行”（注意：此项运行什么都不会发生）。


## 5、和 Stata 配合使用（适用于 Stata 10.0 以上版本）

重新启动 ST3 并尝试打开现有的 do 文件（或创建一个扩展名为 .do 的新文件），可以发现 ST3 的右下角为 **Stata** ，此时使用 `Ctrl + D` 会弹出 **Stata** 的对话框，并执行刚才打开的这个 do 文件， 即外部文本软件直接执行 do-file 。


 ST3 中还有自动补全的功能，非常方便。当输入 re ，就自动出现下拉列表，展示所有 re 开头的命令 `replace` 、`recode` 以及 `rename` , 在输入 c 时，会自动提示数据中所有 c 开头的变量名 。这在多次输入某个命令或者变量名的时候会很方便，避免输入错误。

![image.png](https://images.gitee.com/uploads/images/2019/0828/100127_1b33a4a7_1522177.png)


![image.png](https://images.gitee.com/uploads/images/2019/0828/100126_333e0d3a_1522177.png)

而且，在ST3中 使用 暂元 local ,可以用快捷键 `alt + L` ，或者直接按下 ![微信图片_20190722215154.png](https://images.gitee.com/uploads/images/2019/0828/100127_b833e1f5_1522177.png) 键，即可得到 `` `' ``。

此外，通过菜单 `Preferences -> Package Settings -> StataEditor -> Key Bingdings - User`  参照 `key Bindings- defult` 中的设置复制修改快捷键内容。

![image.png](https://images.gitee.com/uploads/images/2019/0828/100127_8b45c34f_1522177.png)


**ATTENTION**：目前遇到的问题是，当 ST3 配合 Stata 12.0 的 do 文件使用时， 会出现中文乱码的问题： 所有中文的标签、注释、路径都无法正确显示。

![Stata 12.0.png](https://images.gitee.com/uploads/images/2019/0828/100127_62cd630f_1522177.png)

这一问题对 Stata 15.0 的 do 文件似乎不存在。

![Stata15.png](https://images.gitee.com/uploads/images/2019/0828/100128_a87163cc_1522177.png)


## 6、结语

1、在安装之前，要确认电脑的操作系统是 32 位或 64 位，这对拓展 ST3 的操作环境非常重要。

2、可以通过菜单 `View ‣ Side Bar ‣ Show Side Bar` ，显示 ST3 的侧边栏。侧边栏的好处是不仅列出了所有打开过的文件，还可以新建、打开、关闭项目及文件。 ST3 还提供了一个叫 `SideBarEnhancements` 的插件可以强化侧边栏的功能。


![image.png](https://images.gitee.com/uploads/images/2019/0828/100128_5f04ead8_1522177.png)

3、ST3 、ultraedit 等许多文本软件的功能要比 Stata 自带的 dofile  editor 更为强大，在处理复杂命令时，这些外部文本软件的优势更为明显，更可况 Sublime Text 目前被认为是最优秀的文本外部助推器。


&emsp;

&emsp;
> #### [连享会计量方法专题……](https://gitee.com/arlionn/Course/blob/master/README.md)


>#### 关于我们

- 【**Stata 连享会(公众号：StataChina)**】由中山大学连玉君老师团队创办，旨在定期与大家分享 Stata 应用的各种经验和技巧。
- 公众号推文同步发布于 [CSDN-Stata连享会](https://blog.csdn.net/arlionn) 、[简书-Stata连享会](http://www.jianshu.com/u/69a30474ef33) 和 [知乎-连玉君Stata专栏](https://www.zhihu.com/people/arlionn)。可以在上述网站中搜索关键词`Stata`或`Stata连享会`后关注我们。
- 点击推文底部【阅读原文】可以查看推文中的链接并下载相关资料。
- Stata连享会 [精品专题](https://gitee.com/arlionn/stata_training/blob/master/README.md)  || [精彩推文](https://github.com/arlionn/stata/blob/master/README.md)

>#### 联系我们

- **欢迎赐稿：** 欢迎将您的文章或笔记投稿至`Stata连享会(公众号: StataChina)`，我们会保留您的署名；录用稿件达`五篇`以上，即可**免费**获得 Stata 现场培训 (初级或高级选其一) 资格。您也可以从 [连享会选题平台](https://gitee.com/Stata002/StataSX2018/wikis/Home) &rarr; [002_备选主题] 中选择感兴趣的题目来撰写推文。
- **意见和资料：** 欢迎您的宝贵意见，您也可以来信索取推文中提及的程序和数据。
- **招募英才：** 欢迎加入我们的团队，一起学习 Stata。合作编辑或撰写稿件五篇以上，即可**免费**获得 Stata 现场培训 (初级或高级选其一) 资格。
- **联系邮件：** StataChina@163.com

>#### 往期精彩推文
- [Stata连享会推文列表](https://www.jianshu.com/p/de82fdc2c18a) 
- Stata连享会 [精品专题](https://gitee.com/arlionn/stata_training/blob/master/README.md)  || [精彩推文](https://github.com/arlionn/stata/blob/master/README.md)

[![点击此处-查看完整推文列表](https://images.gitee.com/uploads/images/2019/0828/100128_c7b0546b_1522177.png "连享会(公众号: StataChina)推文列表")](https://gitee.com/arlionn/Course/blob/master/README.md)


---
![欢迎加入Stata连享会(公众号: StataChina)](https://images.gitee.com/uploads/images/2019/0828/100128_2c1510ec_1522177.jpeg "扫码关注 Stata 连享会")
