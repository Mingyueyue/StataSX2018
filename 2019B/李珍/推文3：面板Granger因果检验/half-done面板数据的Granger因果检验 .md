&emsp;

> 作者：李珍 (厦门大学)      
>     
> Stata 连享会： [知乎](https://zhuanlan.zhihu.com/arlion) | [简书](http://www.jianshu.com/u/69a30474ef33) | [码云](https://gitee.com/arlionn) | [CSDN](https://blog.csdn.net/arlionn)

> **Source：** Luciano Lopez,  Sylvain Weber, 2017, Testing for Granger Causality in Panel Data, Stata Journal, 17(4): 972–984. [[pdf]](https://sci-hub.tw/10.1177/1536867X1801700412)

  
- [连享会推文集锦](https://www.jianshu.com/p/de82fdc2c18a) 
- Stata连享会 [精品专题](https://gitee.com/arlionn/stata_training/blob/master/README.md)  || [精彩推文](https://github.com/arlionn/stata/blob/master/README.md)
[![点击此处-查看完整推文列表](https://images.gitee.com/uploads/images/2019/0804/103448_349eb192_1522177.png)](https://gitee.com/arlionn/Course/blob/master/README.md)

&emsp;
> #### [连享会计量方法专题……](https://gitee.com/arlionn/Course/blob/master/README.md)

&emsp;
  


随着面板数据库规模的扩大，围绕面板数据因果关系的理论也迅速发展。面板数据正从具有大样本量（ N ）和较短时间维度（ T ）的微观面板数据转变为到具有大样本量（ N ）和长的时间维度（T）的宏观面板数据。在这种情况下，就需要注意时间序列计量经济学的经典问题，即（非）平稳性和（非）因果关系。

在本文中，我们介绍了社区贡献的外部命令  `xtgcause` ，它实现了 [Dumitrescu 和 Hurlin (2012)](http://www.univ-orleans.fr/deg/masters/ESA/CH/Causality_20111.pdf) 提出的面板数据中 Granger 因果关系的检验过程。 

`xtgcause` 通过最小化 Akaike 信息准则 (**AIC**)、贝叶斯信息准则 (**BIC**) 和 Hannan-Quinn 信息准则 (**HQIC**) 来选择模型中的滞后阶数，同时，它提供了 bootstrap 方法来计算 p 值和临界值。

&emsp;

>## 1. Dumitrescu-Hurlin 检验  （DH 检验）

### 1.1 Granger 因果检验的基本思想
[Granger (1969)](https://www.jstor.org/stable/1912791?origin=crossref&seq=1#metadata_info_tab_contents) 开创性地提出一种分析时间序列数据因果关系的方法。 

假设 $x_{t}$ 和 $y_{t}$ 是两个平稳的序列，我们可以用如下模型来检验 $x$ 是不是导致 $y$ 变动的原因：

$$y_{t}=\alpha+\sum_{k=1}^{K} \gamma_{k} y_{t-k}+\sum_{k=1}^{K} \beta_{k} x_{t-k}+\varepsilon_{t}，t=1,……，T \quad (1)$$

其基本思想在于，在控制 $y$ 的滞后项 (过去值) 的情况下，如果 $x$ 的滞后项仍然有助于解释 $y$ 的当期值的变动，则认为 $x$ 对 $y$ 产生因果影响。

检验的原假设为：

$$H_{0} : \beta_{1} = \cdots =\beta_{K}=0$$

这可以通过构造 F 统计量进行检验。如果 F 检验拒绝 $H_{0}$ ，则认为存在因果关系，即 $x$ 是 $y$ 的 Granger 因。显然，我们可以互换 $x$ 和 $y$ 的位置，以便检验 $y$ 是否为 $x$ 的 Granger 因。在很多情况下都会出现模型中所有变量之间都存在双向因果关系。

### 1.2 Dumitrescu-Hurlin 的拓展
**[Dumitrescu-Hurlin (2012)](http://www.univ-orleans.fr/deg/masters/ESA/CH/Causality_20111.pdf)** 在此基础上进行了拓展，提供了一个检验面板数据因果关系的方法。潜在的回归模型是：

$$y_{i, t}=\alpha_{i}+\sum_{k=1}^{K} \gamma_{i k} y_{i, t-k}+\sum_{k=1}^{K} \beta_{i k} x_{i, t-k}+\varepsilon_{i, t} \quad (2)$$

其中，$i=1,……,N$；$t=1,……，T$。  $x_{i, t}$ 和 $y_{i, t}$ 是两个平稳序列在个体 $i$ 和时间 $t$ 上的观测值。 

DH 的面板因果检验允许每个截面单元的回归系数是可变的（即在同一时间上，系数在个体之间不同）。假设滞后阶数 $k$ 对于所有个体是相同的，并且面板必须是平稳的。

类似于 Granger 因果检验，DH 检验也是通过 $x$ 的过去值对 $y$ 的现值的影响来判断因果关系。


#### 检验统计量

原假设认为面板中的所有个体都不存在因果关系，即：

$$H_{0} : \beta_{i 1}=\cdots=\beta_{i K}=0 \quad \forall i=1, \ldots, N_{1} \quad (3)$$ 

备择假设为部分（不是所有的）个体存在因果关系：

$$\begin{array}{cc}{H_{1} : \beta_{i 1}=\cdots=\beta_{i K}=0} & {\forall i=1, \ldots, N_{1}} \\ {\beta_{i 1} \neq 0 \text { or } \ldots \text { or } \beta_{i K} \neq 0} & {\forall i=N_{1}+1, \ldots, N}\end{array}$$

其中，$N_{1} \in[0, N-1]$ 是未知的。 如果 $N_{1} = 0$ ，则面板中的所有个体都存在因果关系。 $N_{1}$ 必须严格小于 $N$ 。

#### 实现方法

在实际操作方面，DH 提出运行包含在 (1) 式中的 N 个独立回归，执行 $k$ 个线性假设的 F 检验来获得 Wald 统计量 $W_{i}$ ，最后计算 Wald 统计量的平均值 

$$\overline{W}=\frac{1}{N} \sum_{i=1}^{N} W_{i}$$

DH 检验的目的在探索面板数据的因果关系，拒绝 $H_{0}$ 并不排除一些个体的非因果关系。使用蒙特卡罗模拟（Monte Carlo Simulation）, DH 发现 $W$ 的渐进表现良好，可以用来检测面板因果关系。


在 Wald 统计量 $W_{i}$ 是独立同分布的假设下，当 $T \rightarrow \infty$ 和 $N \rightarrow \infty$ ，标准化的统计 $\widetilde{Z} $ 服从下面的正态分布：

$$\overline{Z}=\sqrt{\frac{N}{2 K}} \times(\overline{W}-K) \quad \frac{d}{T, N \rightarrow \infty}\!\!{\rightarrow} \quad \mathcal{N}(0,1) \quad (4)$$  

而且，对一个 $T>5+3K$ 的固定的 $T$ 维度来说，最大标准统计量 $Z$ 服从以下正态分布：

$$\widetilde{Z}=\sqrt{\frac{N}{2 K} \times \frac{T-3 K-5}{T-2 K-3}} \times\left(\frac{T-3 K-3}{T-3 K-1} \times \overline{W}-K\right)\frac{d}{T, N \rightarrow \infty}\!\!{\rightarrow}  \quad \mathcal{N}(0,1) \quad (5)$$

原假设 (3) 的检验也是基于 $\overline{Z}$ 和 $\widetilde{Z}$ 。如果这些值大于标准化值，则拒绝原假设 $H_{0}$，认为 Granger 因果存在。对 $N$ 和 $T$ 足够大的面板数据来说，使用 $Z$ 统计量是合理的。对 $N$ 足够大但 $T$ 相对较小的面板数据而言，也可以使用 $Z$ 统计量。对于 $N$ 和 $T$ 都很小的样本数据而言， DH 用蒙特卡洛推断说明了检验具有良好的样本性。

#### 滞后阶数的选取

滞后阶数 $k$ 的选择是一个经验问题，但是  [Dumitrescu 和 Hurlin ( 2012 )](http://www.univ-orleans.fr/deg/masters/ESA/CH/Causality_20111.pdf) 并没有说明。解决这个问题的一个方法就是根据信息准则（ AIC / BIC / HQIC ）选择滞后阶数。在这个过程中，所有估计都要被嵌套在公共样本中执行、进而可以进行比较。事实上，这意味着 $K_{\max }^{3}$ 时间序列在整个滞后选择过程中被忽略掉了。另一个需要考虑的现实问题是面板数据中的横截面依赖性。为此，在 [Dumitrescu 和 Hurlin ( 2012 )](http://www.univ-orleans.fr/deg/masters/ESA/CH/Causality_20111.pdf) 在 6.2 节中提出了一个计算 bootstrapped 临界值而不是渐进临界值：具体步骤为：
1.  拟合模型 (2)，根据（4）和（5）中的定义获得 $\overline{Z}$ 和 $\widetilde{Z}$ ；
2.  在 $H_{0} : y_{i, t}=\alpha_{i}^{0}+\sum_{k=1}^{K} \gamma_{i k}^{0} y_{i, t-k}+\varepsilon_{i, t}$ 假设下拟合模型，得到残差矩阵 $\widehat{\boldsymbol{\varepsilon}}(T-K) \times N$ ；
3.  重新抽取矩阵 $\widehat{\varepsilon}$ ，形成 $\widehat{\boldsymbol{\varepsilon}}(T-K) \times N$ ；
4.  随机抽取 $\left(\mathbf{y}_{\mathbf{1}}^{\star}, \ldots, \mathbf{y}_{\mathbf{K}}^{\star}\right)^{\prime}$ , 令 $\mathbf{y}_{\mathrm{t}}^{\star}=\left(y_{1, t}^{\star}, y_{2, t}^{\star}, \cdots, y_{N, t}^{\star}\right)$ ，其中 $k$ 为可重复的连续时间序列 ；
5.  根据 $k$ 期随机抽取，构建 $y_{i, t}^{\star}=\widehat{\alpha}_{i}^{0}+\sum_{k=1}^{K} \widehat{\beta}_{i k}^{0} y_{i, t-k}^{\star}+\varepsilon_{i, t}^{\star}$ 重抽样序列；
6.  拟合模型 $y_{i, t}^{\star}=\alpha_{i}^{b}+\sum_{k=1}^{K} \gamma_{i k}^{b} y_{i, t-k}^{\star}+\sum_{k=1}^{K} \beta_{i k}^{b} x_{i, t-k}+\varepsilon_{i, t}$ ，计算 $\overline{Z}^{b}$ 和 $\widetilde{Z}^{b}$ ；
7.  重复步骤 3-6 ；
8.  根据 $\overline{Z}^{b}$ 和 $\widetilde{Z}^{b}$ 的分布，计算 P 值、和 $\overline{Z}$ 和 $\widetilde{Z}$ 的标准值。

DH 的面板因果是允许每个截面单元的回归系数可变的，因此 $Z$ 统计量也是多个 $Z$ 的平均值，叫做 $z-bar$  统计量。检验结果主要看最后一列相伴概率，可知所有变量都是双向因果的。


&emsp;

> #### [连享会计量方法专题……](https://gitee.com/arlionn/Course/blob/master/README.md)

>## 2. xtgcasue 命令

### 2.1 语法结构
```stata
xtgcause depvar indepvar [if]  [in]  ///
    [, lags (# | aic [#] | [#] | hqic [#])  ///
       regress bootstrap breps (#)         ///
       blevel(#) blength (#) seed(#) nodot ]
```

### 2.2 选项
* `lags(#laic [#] | bic [#] | hqic [#])` 指出之后结构以执行回归计算统计量。默认滞后一阶，即 lags(1) 。
* `lags(#)` 指定序列滞后阶数 # 需要在回归中应用。最大滞后阶数为 T>5+3*# 。
* `lags(#laic [#] | bic [#] | hqic [#])` 要求序列滞后阶数选择应根据回归设定最小的 AIC / BIC / HQIC 的平均。执行 1 阶至 # 阶滞后回归，将所有估计样本量限制为 T-#，使模型具有嵌套并具有可比性。显示的统计量来源于 AIC / BIC/ HQIC 平均值最小回归集。
* `regress` 可用于显示基于的 N 个的回归的检验的结果。此选项有助于查看单个回归的系数。当面板中的样本数量足够大时，此选项的输出非常长。
* `bootstrap` 按照 [Dumitrescu 和 Hurlin（2012，sec.6.2）]((http://www.univ-orleans.fr/deg/masters/ESA/CH/Causality_20111.pdf) ) 的方法计算 P 值和临界值。 使用 bootstrap 能够克服数据中横截面存在依赖性。
    - `breps (#) ` 表示 bootstrap 重复的次数，默认为 breps(1000)。
    - `blevel (#)` 表示 bootstrap 计算的临界值的显著性水平，默认为 blevel(95) 。
    - `blength(#) ` 表示 bootstrap 的模块长度大小。默认每一时间段抽样独立且替代 blength(1) 。`blength()` 允许将样本划分为 # 时间段的重复块，并从中非替代独立抽样来完成 bootstrap 。如果怀疑存在自相关，则使用超过一个时间段是有用的。
    - `seed(#)` 用来设定随机数种子，默认为不设置。
    - `nodots` 抑制重复点，默认为每次重复就打印一个点，以指示 bootstrap 的演变。

`breps ()`, `blevel ()` , `blength ()`, `seed ()` 以及 `nodots` 都是 `bootstrap` 的子选项。只有在 `bootstrap` 设定的情况下才能使用。

### 2.3 结果的存储
  +  `xtgcause` 存储在下述的 `r()` 标量中
  +  `r(wbar)`	表示 Wald 统计量的平均值
  +  `r(zbart)`	表示 Z-bar tilde 统计量
  +  `r(lags)`	表示 检验的滞后阶数
  +  `z(bart_pv)`	表示 Z-bar tilde 统计量的 P 值
  +  `r(zbar)`	表示 Z-bar统计量
  +  `r(zbart_pv)`	表示 Z-bar tilde 的 P 值
**Matrices**
`r(Wi)` 表示个体的 Wald 统计量，`r(PVi)` 表示个体的 Wald 统计量对应的 P 值
<br/>使用 `bootstrap` 的 `xtgcause` 也存储在下列 `r()` 标量中
  +  `r(zbarb_cv)` 表示 Z-bar 统计量的临界值
  +  `r(blevel)`  表示 bootstrap 临界值的显著性水平
  +  `r (zbartb_cv)`   表示 Z-bar tilde 统计量的临界值
  +  `r(blength)`	表示 block 长度的大小
  +  `r(breps)` 	表示 bootstrap的重复次数
  +  `r(ZBARb)`  表示 bootstrap 产生的 Z-bar 统计量
  +  `r(ZBARTb)` 表示 bootstrap 产生的 Z-bar tilde 统计量

&emsp;

> ## 3. Stata 范例

`xtgcause` 命令假定所有的变量都是平稳的。用 `xtunitroot`，可以提供多种面板稳定性检验。我们也可以用来进行二代单位根检验，例如 Pesaran 提出的控制横断面非独立性。

### 3.1 基于模拟数据的例子

为了解释说明 `xtgcause`，首先使用 Dumitrescu 和 Hurlin (2012) 提供的 [**[数据]**](http://www.execandshare.org/execandshare/htdocs/data/MetaSite/upload/companionSite51/data/data-demo.csv) 。可以直接从网站将数据导入 Stata。在原始 CSV 表格中，数据是矩阵形式，每一个样本的所有观测值都在同一个单元格中。在这个单元格中，变量 x 的 10 个值之间用空格分开， x 的最后一个值和 y 的第一个值用逗号隔开，然后变量 y 的 10 个值之间仍旧使用空格间隔。因此，下面几行命令以让 Stata 的了解数据结构，以进行转换。


```stata
. insheet using "http://www.execandshare.org/execandshare/htdocs/data/MetaSite/upload/companionSite51/data/data-demo.csv", ///
   delimiter(",")  clear   //导入数据，生成 x 和 y ,每一个变量各包括 10 个值。
   **注意：对于 Stata 13.0 以上版本运行，还可以使用 import delimited  

. split x, parse(`=char(9)') destring
. split y, parse(`=char(9)') destring
. drop x y
. gen t = _n
. reshape long x y, i(t) j(id)

. xtset id t    //设定为面板数据

. list id t x y in 2/7

     +----------------------------------+
     | id   t            x            y |
     |----------------------------------|
  2. |  2   1   -1.4703536    1.2586422 |
  3. |  3   1   -.38944513   -.90265296 |
  4. |  4   1    1.5032091    .09225216 |
  5. |  5   1   -1.2006193    .12203134 |
  6. |  6   1   -2.0245606    .93400482 |
     |----------------------------------|
  7. |  7   1    -.5433985   -.22942822 |
     +----------------------------------+
```

#### xtgcause 的基本语法

首先，我们使用 `xtgcause` 的默认情况进行操作（即使用滞后一阶，不设定更长的滞后阶数），在这种情况下，检验的结果为接受原假设。结果包括 w(w-bar) , z(z-bar tilde) 。对后两个统计量来说，还提供了就标准正态分布的 P 值。

```stata
. xtgcause y x    //检验 x 是否导致了 y

Dumitrescu & Hurlin (2012) Granger non-causality test results:
--------------------------------------------------------------
Lag order: 1
W-bar =          1.2909
Z-bar =          0.6504   (p-value = 0.5155)
Z-bar tilde =    0.2590   (p-value = 0.7956)
--------------------------------------------------------------
H0: x does not Granger-cause y.
H1: x does Granger-cause y for at least one panelvar (id).

```

#### Wald 统计量

也可以使用 Stata 的返回值 `r(Wi)` 和 `r(PVi)` 展示 Wald 统计量和相关值（首先将其整合成一个简单矩阵以节省空间）：

```
matrix Wi_PVi = r(Wi), r(PVi)

matrix list Wi_PVi

Wi_PVi[10,2]
             Wi        PVi
 id1  .56655945  .46256089
 id2  .11648998  .73731411
 id3  .09081952  .76701924
 id4  8.1263612  .01156476
 id5  .18687517  .67129995
 id6  .80060395  .38417583
 id7  .53075859  .47681675
 id8  .00158371  .96874825
 id9  .43635413   .5182858
id10  2.0521113  .17124367

```

#### 设定滞后阶数

使用 `lags()` 选项，可以进行 x 和 y 的二阶滞后检验，检验结果和之前类似。

```stata
. xtgcause y x, lags(2)

Dumitrescu & Hurlin (2012) Granger non-causality test results:
--------------------------------------------------------------
Lag order: 2
W-bar =          1.7302
Z-bar =         -0.4266   (p-value = 0.6696)
Z-bar tilde =   -0.7052   (p-value = 0.4807)
--------------------------------------------------------------
H0: x does not Granger-cause y.
H1: x does Granger-cause y for at least one panelvar (id).
````

#### 基于 Bootstrap 的标准误

我们也可以使用 **bootstrapped** 计算 P 值和临界值，在这种情况下，**bootstrapped** 的 P 值和第一次检验中的渐进 P 值相近。

```stata
. xtgcause y x, bootstrap lags(1) breps(100) seed(20190802)

----------------------------
Bootstrap replications (100)
----------------------------
..................................................    50
..................................................   100

Dumitrescu & Hurlin (2012) Granger non-causality test results:
--------------------------------------------------------------
Lag order: 1
W-bar =          1.2909
Z-bar =          0.6504   (p-value* = 0.5800, 95% critical value = 2.7526)
Z-bar tilde =    0.2590   (p-value* = 0.8400, 95% critical value = 1.9042)
--------------------------------------------------------------
H0: x does not Granger-cause y.
H1: x does Granger-cause y for at least one panelvar (id).
*p-values computed using 100 bootstrap replications.
```

### 3.2 现实数据的例子

以 [Paramati, Ummalla 和 Apergis (2016)](https://www.sciencedirect.com/science/article/pii/S014098831630305X/pdfft?md5=7f5c483c1dfd2e23e05d44d61df41020&pid=1-s2.0-S014098831630305X-main.pdf) 论文中使用的[清洁能源的外国直接投资和股票市场增长率数据](https://ars.els-cdn.com/content/image/1-s2.0-S014098831630305X-mmc1.zip) 为例，重现文章的结果分析。

首先，将网站的数据下载，并导入Stata ，进行设定。需要说明的是，在使用 `import excel` 命令时，
还可以增加 "case(preserve|lower|upper)" 选项来设置变量名的大小写。

```stata
. import excel "Data.xlsx", clear first  ///
   cellrange(A1:J461) sheet(LN-EU) 

. xtset ID Year
       panel variable:  ID (strongly balanced)
        time variable:  Year, 1993 to 2012
                delta:  1 unit
```

使用 `xtgcause` 来检验因果关系，和文中表 8 的结果一致。我们使用滞后 2 阶的数据和 Paramati, Ummalla 和 Apergis ( 2016 ) 中附录的数据匹配。和他们的结果相比，Eviews 中 Zbar-Stat 和 Z-bar tilde 统计量是一致的（ Eviews 中不提供 Zbar ）

此外， `xtgcause` 可以自行设置滞后阶数，同时保证 AIC , BI , HQIC 最小化。Dumitrescu 和 Hurlin ( 2012 ) 并没有提供滞后阶数的选择，这就需要根据使用者经验判断。举例来说，我们可以通过 `output fdi` 设定滞后选项 `lags(bic)`

事实上，`xtgcause` 可以进行滞后 1 阶至最高阶 T>5+3K 或者使用者自行设定限制值以下的回归。此外，如果 5 阶滞后都被考虑，面板中最初的 5 个观测值就不在估计中，即使滞后阶数可以比 5 少。 这确保了嵌套模型，然后可以使用 AIC ，BIC 或 HQIC 对其进行适当比较。 在这一系列估计之后，`xtgcause` 选择最佳结果（即，使得 N 个个体估计的平均 AIC / BIC / HQIC 最低）并且使用最佳滞后数并使用所有可用观察值重新运行所有估计。 对后者的统计数据报告为输出。

在上面的例子中，使用 BIC 的最优滞后阶数 1 ，这与 Paramati , Ummalla 和 Apergis ( 2016 ) 为此检验选择的滞后顺序不同。这种差异可能会造成检验的结果是相反的。更确切地说，原假设不会因为最佳选择的单一滞后所拒绝，但 Paramati , Ummalla 和 Apergis ( 2016 ) 使用了两个滞后，因此拒绝零假设。 考虑到经济学中的实证研究被用于制定政策建议，这种不准确的结论可能是有害的。 因此，我们考虑使用 `xtgcause` 的选项，允许用户根据 AIC / BIC / HQIC 选择滞后数。 它将允许研究人员依赖这些广泛接受的标准清楚地选择。

最后，`xtgcause` 通过 `bootstrap` 过程使得计算与 z-bar  和 z-bar tilde 相关的 P 值和临界值。计算 bootstrapped 临界值（而不是相似估计）可以非常有用当面板非独立。基于 Paramati, Ummalla 和 Apergis ( 2016 ) 数据，我们检验了因果关系从 output 到 fdi ，通过添加 bootstrap 选项（我们也为了replicability 使用 seed，节省空间使用nodots）。

这里 `xtgcause` 首先使用前一系列估计中的最优滞后数来计算 Z-bar 和 Z-bar tilde 统计量; 然后，它计算了 bootstrapped 的 P 值和临界值。 默认情况下，每次形成 1000 次 bootstrap 重复。 我们观察到，与之前获得的渐近 P 值（从 0.34 到 0.45 ）相比， Z-bar 的 bootstrapped的 P 值显着增加，而 Z-bar tilde的 P 值保持更接近。 这应该被解释为估计遭受小样本偏差的信号，因此低估了渐近 P 值。 Bootstrapped P 值表明零假设远未被拒绝，这加强了对 Paramati ， Ummalla 和 Apergis （ 2016 ） 基于渐近 P 值并以两个滞后获得的结论的担忧。

&emsp;

>## 4. 结论

在本文中，我们介绍了社区贡献的外部命令 `xtgcause`，该命令实现了 Dumitrescu 和 Hurlin（2012） 提出的面板数据 Granger 因果关系的检验。  `xtgcause` 命令的一个重要贡献是允许用户根据 AIC ，BIC 或 HQIC 选择滞后阶数。`xtgcause` 还可以计算 bootstrapped 临界值，以克服横截面的非独立。

>## 5. 参考文献

- Berkowitz, J., and L. Kilian. 2000. Recent developments in bootstrapping time series. _Econometric Reviews_ 19: 1- 48.
- Breitung, J. 2000. The  local  power  of some unit  root  tests for  panel data.  In Advances in Econometrics: _Vol. 15——Nonstationary Panels, Panel Cointegration, and Dynamic Panels_, ed. B. H. Baltagi, 161-178. New York: Elsevier.
- Dumitrescu, E.-I., and C. Hurlin. 2012. Testing for Granger non-causality in heteroge­ neous panels. _Economic Modelling_ 29: 1450-1460. [[PDF]](http://www.univ-orleans.fr/deg/masters/ESA/CH/Causality_20111.pdf)
- Granger, C. W. J. 1969. Investigating causal relations by econometric models and cross-spectral methods. _Econometrica_ 37: 424-438.
- Hadri, K. 2000. Testing for stationarity in heterogeneous panel data. _Econometrics Journal_ 3: 148-161. [[PDF]]([https://sci-hub.tw/10.1111/1368-423X.00043](https://sci-hub.tw/10.1111/1368-423X.00043)
)
- Harris, R. D. F., and E. Tzavalis. 1999. Inference for unit roots in dynamic  panels where the time dimension is fixed. _Journal of Econometrics_ 91: 201-226.
- Im, K. S., M. H. Pesaran, and Y. Shin. 2003. Testing for unit roots in heterogeneous panels. _Journal of Econometrics_ 115: 53-74.
- Levin, A., C.-F. Lin, and C.-S. J. Chu. 2002. Unit  root  tests in  panel data:  Asymptotic and finite-sample properties. _Journal of Econometrics_ 108: 1- 24.
- Luciano Lopez,  Sylvain Weber, 2017, Testing for Granger Causality in Panel Data, Stata Journal, 17(4): 972–984. [[pdf]](https://sci-hub.tw/10.1177/1536867X1801700412)
- Paramati, S. R., N. Apergis, and M. Ummalla. 2017. Financing clean energy projects through domestic and foreign capital: The  role  of  political  cooperation  among  the EU, the G20 and OECD countries. _Energy Economics_ 61: 62- 71.
- Paramati, S. R., M. Ummalla, and N. Apergis. 2016. The effect of foreign direct invest­ ment and stock market growth on clean energy use across a panel of emerging market economies. _Energy Economics_ 56: 29- 41.
- Pesaran, M. H. 2007. A simple panel unit root test in the presence of cross-section dependence. _Journal of Applied Econometrics_ 22: 265- 312.
- Salahuddin, M., K. Alam , and I. Ozturk. 2016. The effects of Internet usage and eco­ nomic growth on CO2 emissions in OECD countries: A panel investigation. _Renewable and Sustainable Energy Reviews_ 62: 1226-1235.
- Stine, R. A. 1987. Estimating properties of autoregressive forecasts. _Journal of the American Statistical Association_ 82: 1072-1078.


&emsp;
> #### [连享会计量方法专题……](https://gitee.com/arlionn/Course/blob/master/README.md)

&emsp;


>#### 关于我们

- 【**Stata 连享会(公众号：StataChina)**】由中山大学连玉君老师团队创办，旨在定期与大家分享 Stata 应用的各种经验和技巧。
- 公众号推文同步发布于 [CSDN-Stata连享会](https://blog.csdn.net/arlionn) 、[简书-Stata连享会](http://www.jianshu.com/u/69a30474ef33) 和 [知乎-连玉君Stata专栏](https://www.zhihu.com/people/arlionn)。可以在上述网站中搜索关键词`Stata`或`Stata连享会`后关注我们。
- 点击推文底部【阅读原文】可以查看推文中的链接并下载相关资料。
- Stata连享会 [精品专题](https://gitee.com/arlionn/stata_training/blob/master/README.md)  || [精彩推文](https://github.com/arlionn/stata/blob/master/README.md)

>#### 联系我们

- **欢迎赐稿：** 欢迎将您的文章或笔记投稿至`Stata连享会(公众号: StataChina)`，我们会保留您的署名；录用稿件达`五篇`以上，即可**免费**获得 Stata 现场培训 (初级或高级选其一) 资格。您也可以从 [连享会选题平台](https://gitee.com/Stata002/StataSX2018/wikis/Home) &rarr; [002_备选主题] 中选择感兴趣的题目来撰写推文。
- **意见和资料：** 欢迎您的宝贵意见，您也可以来信索取推文中提及的程序和数据。
- **招募英才：** 欢迎加入我们的团队，一起学习 Stata。合作编辑或撰写稿件五篇以上，即可**免费**获得 Stata 现场培训 (初级或高级选其一) 资格。
- **联系邮件：** StataChina@163.com

>#### 往期精彩推文
- [Stata连享会推文列表](https://www.jianshu.com/p/de82fdc2c18a) 
- Stata连享会 [精品专题](https://gitee.com/arlionn/stata_training/blob/master/README.md)  || [精彩推文](https://github.com/arlionn/stata/blob/master/README.md)

[![点击此处-查看完整推文列表](https://images.gitee.com/uploads/images/2019/0804/103448_44d39a34_1522177.png "连享会(公众号: StataChina)推文列表")](https://gitee.com/arlionn/Course/blob/master/README.md)





---
![欢迎加入Stata连享会(公众号: StataChina)](https://images.gitee.com/uploads/images/2019/0804/103448_899f9b82_1522177.jpeg "扫码关注 Stata 连享会")



 