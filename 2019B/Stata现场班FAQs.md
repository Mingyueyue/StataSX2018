
&emsp;

> 作者：连享会助教团队      
>     
> Stata 连享会： [知乎](https://zhuanlan.zhihu.com/arlion) | [简书](http://www.jianshu.com/u/69a30474ef33) | [码云](https://gitee.com/arlionn) | [CSDN](https://blog.csdn.net/arlionn)


&emsp;


> #### [2019暑期Stata现场班，7.17-26日，北京，连玉君+刘瑞明 主讲](https://gitee.com/arlionn/Course/blob/master/2019%E6%9A%91%E6%9C%9FStata%E7%8E%B0%E5%9C%BA%E7%8F%AD.md)


&emsp;


(注：文档中的链接可直接点击，一些链接也已经插入词句中~)

> ### 安装及资料下载

1. **如何安装Stata15?**  
    请查看“A-课件下载和使用说明”文件，下载地址为：  
    百度云盘链接：<https://pan.baidu.com/share/init?surl=rZ5l16jmcz1-gkdj8pkKKg>  
    密码：k3qw  

1. **Mac系统如何安装?给出的链接中只有exe文件?**  
    （注：Windows系统请使用问题1中的链接安装）  
    百度云盘链接：<https://pan.baidu.com/s/1xQygOIHDWsYcw8R6bEIduw#list/path=%2F>  
    密码：2dtb

1. **本次培训的软件及数据资料多久可以下载好？下载太慢怎么办？**  
    正常为一小时左右，具体时间和网速有关系，可以使用下方两种加速器的任意一个。  
    下载地址：① <http://pandownload.com/>   
    ②  <https://faq.speedpan.com/chapter2/install.html>

1. **Mac系统安装后中的stata/ado里面没有personal这个文件夹怎么办？**  
    自己新建一个名为“personal”的文件夹，然后把PX A 2019a放进去就可以了。

1. **如果Stata15软件装在C盘可以使用吗？**  
    可以使用的，只是老师给得do文档中部分路径需要简单修改。

注：安装和下载有问题可以找助教~

&nbsp;
>    ### 命令执行

6. **在Stata15主界面的执行某一命令行，如第一个命令`global...`提示invalid syntax错误？**  
    将语令后的中文注释删除后再在命令窗口执行即可。
    推荐在do文件中选中命令行后执行，具体可见老师给的课件（页码标号第5页,PDF的第7页）。

1. **如何操作do文档中的命令？**  
    选中之后点击do文档中的`Execute（do）`按钮执行选中部分，也可以选中后用快捷键`Ctrl+D`或`Ctrl+R`（静默执行），注意不进行选择直接点击do文档中的`Execute（do）`按钮会执行整个do文件，中途停止执行可以点击结果窗口上面的`叉号×（Break）`，执行中为红色。

1. **语句执行报错，文件无法打开？比如`shellout "Stata2019.xlsx"`报告“Cannot find...”**   
    多半是文档不在现有的执行路径内所导致的，可以先执行`pwd`查看现在的工作路径，如果文件不在路径内那么Stata也就无法找到文件了。  
    可以通过`cd`来修改命令。
    如果路径没有问题，可能是文件名输错，再核对一下命令~

1. **命令无法识别或无法执行？**  
    &emsp;外部命令统一存放于“D:\stata15\ado\plus”文件夹中，正确设定 Stata 的文件路径就不会出现命令无法识别的问题。  
    &emsp;连老师自己编写的命令统一存放于 “D:\stata15\ado\personal\PX_A_2019a\adofiles”文件夹中，要使这些命令正确执行，需要使用` adopath + `命令将上述路径加入 stata15 的搜索范围内。  
    &emsp;对于外部命令，使用`ssc install (命令)` 就可以将命令下载于路径中，如
    `ssc install esttab`。之后命令就可正常运行（只需执行一次命令）。

1. **如何设定Stata打开时自动执行的 profile.do 的文件？**  
    [Stata: 聊聊 profile.do 文档](https://www.jianshu.com/p/387118287099)  

        ------------以下增加于2019.1.18------------
        
1. **`shellout` 命令无法使用？**  
    - Mac用户：[Stata: 苹果(MAC)用户无法使用 shellout 命令？](https://www.jianshu.com/p/dfd034d1f00e)
    - Windows用户如果无法使用有两种情况：① 路径设置错误，文件不在当前路径内 ② 可以在`shellout`后加`using`之后进行尝试，例如`shellout using "Acock_2014.pdf"`

1. **执行命令+ `if code = 111`无法识别？**  
    Stata中一个=代表赋值，两个=表示判断，用`if`等条件语句时都是使用`==`，命令应该修改为`if code == 111`。

1. **日期形式的变量如何比大小？**  
    比如设定变量 **ymdate** 为`%ym`形式，运行`if ymdate > 2001m1`后程序报错，应该将以上命令改为`if ymdate > ym(2001m1)`后即可运行。

&nbsp;
> ### 具体命令解答
14. **如何更好理解`merge`命令同维度和不同维度数据的合并？**  
    - [具体图解](http://blog.sina.com.cn/s/blog_629bb758010123av.html)  
    - [连老师简书-数据拆分与合并](https://www.jianshu.com/p/8d193ffdde05)
    - 多对多合并可用`joinby`  [组内交叉合并——joinby](https://mp.weixin.qq.com/s/G4qJrUkCRMTwk_8Eld9-Cg)
    - [A004. 为什么Stata中merge m:m容易出问题](https://www.jianshu.com/p/4509291531e0)

1. **如何更好理解`global`与`local`？**  
    - [认识 Stata 的暂元（Macro）](https://mp.weixin.qq.com/s/GK87N27oBD7eYXTuny3BHQ)
    - 应用如 [Stata绘图: 用暂元统一改变图形中的字号]( https://www.jianshu.com/p/fdbc6b3074f6)
    - 最好的方式依然是官方文档~ 可以执行`help macro`及`help quotes`（里面有说到single quotes和double quotes）进行深入的理解
    
1. **Stata中`destring`与`encode`将字符型转换成数值型直接的区别？**   
    [stata文本变量的处理](https://www.jianshu.com/p/d8045fde6c15)

1. **`logout` 和 `esttab` 输出作用的区别？**  
    `logout`可用于输出所有命令结果；`esttab`一般用于输出回归表格，也可以输出描述统计。  
    可见连老师的推文 [君生我未生！Stata - 论文四表一键出](https://www.jianshu.com/p/97c4f291ee1e)  
    另外推荐一个功能强大的输出结果命令：`asdoc` (附[使用指南](https://mp.weixin.qq.com/s/gPc26DJTu8PAEvbIPBGLEg))  

        ------------以下增加于2019.1.18------------
            
1. **`tabstat wage hours married, by(race) s(mean) f(%3.2f)`中命令选项的`s()`是什么意思？**    
    &emsp;s即statistics，表示统计量，如最大值、最小值、平均数等。  
    &emsp;通过`s()`可以输出特定的统计信息，如`s(mean count n sum max min rang sd cv p1 p5 p10)`等等，具体可以通过执行`help tabstat`语句后在对应的帮助文档中进行详细的查看。

1. **什么时候需要使用`adopath`命令？**  
    &emsp;ado文件的创建是用户基于现有的Stata命令，根据自己的需求编写脚本和程序来添加一些新的特性或功能以自动实现可重复分析。  
    &emsp;相应的adopath就是执行这些文件的路径，当用户将自己写的ado文件保存到除Stata自动设定搜索的路径之外时就需要在这些路径中加入自己保存文件的路径。命令如`adopath + "E:\"`  
    &emsp;可见推送[Stata编程——我的第一个Stata程序](https://mp.weixin.qq.com/s/8ehxJMxbWwLcwZKa9DH4Tg)

1. **日期从Excel导入后的格式为字符串格式（str7），如2013-08，应该如何进行设定为Stata中的日期格式？**  
   &emsp;以下的解决方法仅供参考~ 大家可以多参考一些官方文档（如[Working with dates and times]( https://www.stata.com/manuals13/u24.pdf)）  

    &emsp;一种解决方案：
    ```stata
    split time,p(-)
    destring time1 time2,replace force
    gen ymdata = ym(time1,time2)
    format ymdata %tm
    ```
    &emsp;注：如有报错可能是内存中存在time1或time2变量，可以先删去或者`rename`变量；也可以在Excel中先进行分列后分为年份和月份变量后导入Stata再执行后两行语句。
    
&nbsp;
> ### 计量与模型

        ------------以下增加于2019.1.18------------
 

21. **Stata如何检验分组回归后的组间系数差异？**  
    - [Stata: 如何检验分组回归后的组间系数差异？](https://www.jianshu.com/p/38315707ef6c)
    - [[旧版]Stata: 如何检验分组回归后的组间系数差异？](https://www.jianshu.com/p/f27f8716dd2d)

1. **如何理解内生变量和外生变量？**  
    &emsp;在模型中，如果一个变量能够被该模型中的其他变量所决定或被影响，那么就称这个变量为**内生变量**。如果一个变量独立于系统中其他所有变量，其他变量的变化不对该变量造成影响，那么就称该变量为**外生变量**。  
    &emsp;通常我们将外生变量作为自变量，内生变量作为因变量，如果自变量中存在内生变量，就会产生共线性问题，随机干扰项对自变量X的条件期望为0也是为了保证随机扰动项是外生的。
    
    &emsp;变量一般分为外生变量，前定变量和内生变量。可以根据模型去说明，比如连老师的解释为：
    ```
    模型： y[it] = a0*y[it-1] + a1*x[it] + a2*w[it] + u_i + e[it]
    
    特征：解释变量中包含了被解释变量的一阶滞后项
            x[it]  —— 严格外生变量   E[x_it,e_is] =0  for all t and s
                        即，所有干扰项与x都不相关
            w[it]  —— 先决变量       E[w_it,e_is]!=0  for s<t, but E[x_it,v_is]=0 for all s>=t
                        即，前期干扰项与当期x相关，但当期和未来期干扰项与x不相关。
            y[it-1]—— 内生变量       E[x_it,e_is]!=0  for s<=t
                        即，前期和当期，尤其是当期干扰项与x相关
    
    动态面板里面的y_i,t-1就是前定变量
    ```
    
    &emsp;另附上有关内生性的一些讨论链接：[Stata - 内生性问题：处理方法与进展](https://www.jianshu.com/p/27352f4d89ef)

1. **过度投资如何计算？**  
    [盈余管理、过度投资怎么算？分组回归获取残差](https://mp.weixin.qq.com/s/J2rRp7N4QGIC09h4lenH7A)


&nbsp;
> ### 其他

24. **`%v.df`的含义?**  
    该语句用来规定输出形式。可以通过`help format`进行查看，其中v为输出结果的总位数，d为小数点后的输出位数。

        ------------以下增加于2019.1.18------------
 

1. **如何打开连老师推荐的Baum和Acock两本电子书？**  
    - 在Stata15中执行下述程序：  
  
   ```Stata
    global path "`c(sysdir_personal)'\PX_A_2019a\A1_intro"
    global R "$path\refs" 
    shellout using "$R\Baum_2006.pdf" 
    shellout using "$R\Acock_2014.pdf" 
   ```
    &emsp;注：如果路径正确而文件无法打开可能是系统的问题，可以在`sheelout`后加`using`再尝试
    - 可以直接在连老师发的资料("D:\stata15\ado\personal\PX_A_2019a\A1_intro\refs")中查找。
    
1. **屏幕涂鸦与缩放软件如何使用？**  
   [屏幕涂鸦和缩放：ZoomIt (教师利器)](https://www.jianshu.com/p/76b78af068f5)

1. **正则表达式是什么？如何利用stata对文本进行分析？**  
    连老师简书 - [Stata: 正则表达式和文本分析](https://www.jianshu.com/p/38315707ef6c)


&nbsp;
> ### 有问题我们会去哪里找答案？  
- 先`help`一下~最好查Stata的manual（用户手册）
- [Stata连享会推文列表](https://www.jianshu.com/p/de82fdc2c18a)
- [经管之家：Stata专区 | 连玉君老师Stata-VIP专区 | Stata常见问题](https://bbs.pinggu.org/thread-272681-1-1.html)
- 微信“搜一搜”

&nbsp;
---
> **初级班助教 共同整理**  
展金泳（对外经贸）、袁雪倩 (辽宁大学)、王宇桐 （北师大）

> **更新时间**  
2019年6月19日


&emsp;

&emsp;

>#### 关于我们

- 【**Stata 连享会(公众号：StataChina)**】由中山大学连玉君老师团队创办，旨在定期与大家分享 Stata 应用的各种经验和技巧。
- 公众号推文同步发布于 [CSDN-Stata连享会](https://blog.csdn.net/arlionn) 、[简书-Stata连享会](http://www.jianshu.com/u/69a30474ef33) 和 [知乎-连玉君Stata专栏](https://www.zhihu.com/people/arlionn)。可以在上述网站中搜索关键词`Stata`或`Stata连享会`后关注我们。
- 点击推文底部【阅读原文】可以查看推文中的链接并下载相关资料。
- Stata连享会 [精品专题](https://gitee.com/arlionn/stata_training/blob/master/README.md)  || [精彩推文](https://github.com/arlionn/stata/blob/master/README.md)

>#### 联系我们

- **欢迎赐稿：** 欢迎将您的文章或笔记投稿至`Stata连享会(公众号: StataChina)`，我们会保留您的署名；录用稿件达`五篇`以上，即可**免费**获得 Stata 现场培训 (初级或高级选其一) 资格。您也可以从 [连享会选题平台](https://gitee.com/Stata002/StataSX2018/wikis/Home) &rarr; [002_备选主题] 中选择感兴趣的题目来撰写推文。
- **意见和资料：** 欢迎您的宝贵意见，您也可以来信索取推文中提及的程序和数据。
- **招募英才：** 欢迎加入我们的团队，一起学习 Stata。合作编辑或撰写稿件五篇以上，即可**免费**获得 Stata 现场培训 (初级或高级选其一) 资格。
- **联系邮件：** StataChina@163.com

>#### 往期精彩推文
- [Stata连享会推文列表](https://www.jianshu.com/p/de82fdc2c18a) 
- Stata连享会 [精品专题](https://gitee.com/arlionn/stata_training/blob/master/README.md)  || [精彩推文](https://github.com/arlionn/stata/blob/master/README.md)

[![点击此处-查看完整推文列表](https://upload-images.jianshu.io/upload_images/7692714-8b1fb0b5068487af.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240 "连享会(公众号: StataChina)推文列表")](https://gitee.com/arlionn/Course/blob/master/README.md)



---
[![欢迎加入Stata连享会(公众号: StataChina)](https://upload-images.jianshu.io/upload_images/7692714-fbec0770ffb974d8.jpg?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240 "扫码关注 Stata 连享会")](https://gitee.com/arlionn/Course/blob/master/README.md)



