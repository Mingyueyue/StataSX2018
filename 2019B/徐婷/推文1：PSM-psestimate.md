「Source：Understanding the cardinality of psestimate https://acarril.github.io/posts/cardinality-psestimate」

# 对psestimate基数性的理解

我写过一个 Stata 程序`psestimate`,它实现了由 Imbens 和 Rubin 提出的有助于判定估计倾向得分时使用的协变量的一阶或二阶多项式的算法。从本质上讲，这个算法在运算中非常缓慢，而且频繁出现。因此，在给定初始参数的情况下，我检查了程序所需要的模型的数量。

本文补充了对`psestimate`的主要解释。如果你对这个命令并不熟悉，我建议首先阅读对其的主要解释，或者自 SSC 处下载并阅读这个命令的帮助文件。当然，我也推荐阅读 Imbens 和 Rubin （2015）的原文。

## 数据集和基础设置

我们使用关注DehejiaWahba的样本数据（Lalonde，1986），点击此处可以下载[http://economics.mit.edu/faculty/angrist/data1/mhe/dehejia]（文件名是nswre74.dta）。这个也包括了`psestimate`的辅助文件。

作为一个运行程序的例子，我们将使用`psestimate`来确定能够更好地确定实验变量（treat）的协变量的多项式。我们将教育变量`ed`作为主要变量，即算法将自动把它包含在内。此外，我们指定候选变量为年龄、是否是黑人虚拟变量、是否为西班牙人虚拟变量以及是否有学位虚拟变量。下面的命令可以满足以上设定：
```stata
.psestimate treat ed,totry(age black hisp nodeg)
``` 
也就是说，首先拟合的主模型为`logit treat ed`。

## 1.第一阶段（线性变量）
在程序的第一阶段，将通过几个迭代逐步完成对包含在主模型中的其他变量的选择。程序将在`totry()`选项中的四个变量中进行选择。因此，它将首先运行主要的logit模型`logit treat ed`，然后对4种模型进行拟合。
```stata
.logit tread ed age
.logit tread ed black
.logit tread ed hisp
.logit tread ed nodeg
``` 
在每个估计之后，它将对基本模型进行相似比检验。选择4个模型中 LR 检验的值最高的模型，除非4个模型的 LR 值都低于`clin()`中报告的临界值。

在第一次迭代之后（假设已经选定了一项），程序将会在主模型中加入这一项，然后为每个剩余的变量匹配一个额外的 logit 模型。所以，如果选择了`nodeg` 选项，主模型将为`logit treat ed nodeg`。
```stata
.logit tread ed nodeg age
.logit tread ed nodeg black
.logit tread ed nodeg hisp
``` 
总之，如果将被尝试的变量的个数为 C ，则在第一阶段中，在理论上程序将会拟合 ∑C 个 logit 模型。在我们的例子中，有4个备选项，也就是说最多会拟合10个 logit 模型。如下是我们在第一阶段中的结果：
```stata
. psestimate treat ed, totry(age black hisp nodeg)
Selecting first order covariates... (10)
----+--- 1 ---+--- 2 ---+--- 3 ---+--- 4 ---+--- 5
...s..s..
Selected first order covariates are: nodeg hisp
``` 
最后，我们发现，在4个候选项中，拟合了9个 logit 模型后，只选择了 `nodeg`和 `hisp` 两个变量。为什么不是10个呢？因为，一旦 `nodeg `和 `hisp` 被包含在主模型中（经过2次全迭代，拟合了7个 logit 模型后），加入 `nodeg` 或 `hisp` 都无法提高 LR 检验的值，所以不需要检验更多的模型。

## 2.第二阶段（二次项）

由于将被尝试的项数不是非常明显的，所以我们需要将更多的注意力放到第二阶段上。首先，我们将探讨许多术语，包含二次项和二者相互作用，程序将需要在第二阶段中检查。

### 2.1二次项的数目和二者相互作用

如果只选择了一个协变量` a` ，在第二阶段中程序仅需检查 a2 次。如果同时选择了`a `和` b `两个协变量，程序将需要检查的次数为  ：
```
a,b :    (a^2 + b^2) + (a*b)
``` 
按照这个思路，
```
a,b,c   : (a^2, b^2, c^2), (a*b, a*c, b*c)
a,b,c,d : (a^2, b^2, c^2, d^2), (a*b, a*c, a*d, b*c, b*d, c*d)
...
``` 
总之，如果线性项的数目是 L （包括主要变量），在第二阶段中将被尝试的二次项的数目很明显也是 L 次，双向相互作用的数目为 $$∑L-1$$ 个。所以，如果二次项的总数为 Q ,则 Q 等于 $$L（L+1）/2 $$。在我们的例子中， L=3 ，所以将被尝试的二次项总数为6个。

### 3.2第二阶段中迭代的次数.

第二阶段中将拟合的 Logit 模型的数量与第一阶段相同。然而，我们需要考虑要尝试的项的实际数量，我们知道 Q 是6。实际的项包括：
```
ed^2, nodeg^2, hisp^2, ed*nodeg, ed*hisp, nodeg*hisp
```

在这一阶段，理论上， logit 模型的最大数量等于 Q 的和，即21。再一次，以下是第二阶段的结果：
```stata
. psestimate treat ed, totry(age black hisp nodeg)
Selecting first order covariates... (10)
----+--- 1 ---+--- 2 ---+--- 3 ---+--- 4 ---+--- 5
...s..s..
Selected first order covariates are: nodeg hisp
Selecting second order covariates... (21)
----+--- 1 ---+--- 2 ---+--- 3 ---+--- 4 ---+--- 5
.....s.....
Selected second order covariates are: c.nodeg#c.ed
Final model is: ed nodeg hisp c.nodeg#c.ed
```
在第一次迭代中（6个 logit 模型），程序选择了`c.nodeg#c.ed`。在第二次迭代中（5个 logit 模型），其他二次项没有提高拟合程度，所以程序终止运行。