*------------------------------------------------------------
* 
* Stata 生成可重复性报告系列（一）：运用动态文档命令 (dyn*)
*
*-Author: 万莉（北京航空航天大学）
*-Date: 2019/09/02
*------------------------------------------------------------

* 设置工作路径
cd "D:\2019年Stata连享会\推文5：Stata 生成可重复性报告系列（一）：运用动态文档命令 (dyn)\Files"

*=============
*-0 写在前面
*=============

* 可重复性报告 Reproducible and automated reporting

* 生成重复性报告（Reproducible and automated reporting）的命令可分为两类：

*- 第一类是带 dyn 前缀的动态文档命令 (dyntext | dyndocx)。
*  该类命令可自定义生成 text 文本、HTML 和 Word 文档。更重要的是，该命令支持 Markdown 文本格式语言。

*- 第二类是带 put 前缀的导出命令 (putdocx | putpdf | putexcel )。
*  该类命令可自定义生成 Word、PDF 和 Excel 文件。

*=====================
*-1 准备工作
*=====================
/*
在学习动态文档命令前，我们先做一些准备工作。
由于该类命令支持 Markdown 文本格式语言，且需要包含 Stata dynamic tags（动态标签），
我们先大致了解下 [Markdown](https://sspai.com/post/47199) 和 Stata dynamic tags（动态标签）。
*/

*-1.1 什么是 Markdown ？

/*
Makdown 是一种轻量级且易使用的标记语言，通过对标题、正文、加粗、链接等主要文本格式的预设编码， 帮用户在写作中有效避免频繁的格式调整，获得更加流畅沉浸的写作体验。
本质上，Markdown 是一种标记语法，它的格式更接近于纯文本。

- 语法的入门教程：
  - [连玉君 Markdown 笔记](https://www.jianshu.com/p/db1d26af109d)
  - [Markdown 语法说明](http://www.appinn.com/markdown)

- 推荐的在线编辑器
  - [Editor.md](https://pandao.github.io/editor.md)
  - [Cmd Markdown 编辑阅读器](https://www.zybuluo.com/mdeditor)
  - [有道云笔记](http://note.youdao.com)
  - [简书](https://www.jianshu.com)
  - [码云](https://gitee.com)
*/

*-1.2 什么是 Stata dynamic tags（动态标签）?

/*
在动态文档命令 (dyndoc, dyntext) 中使用动态标签，用来定义执行某种操作，
比如运行某个区域块，在文字中插入 Stata 命令的运行结果，导出图片等。
*/

help dynamic tags // 动态标签说明

/*
可用的动态标签，如下：

- <<dd_version>> 
	定义执行命令所需的最低版本；使用时需放在单行，推荐放在 dofile 的首行。
-------------------------------------------------------------------------	
<<dd_version: version_number>>
* version_number 不是指 Stata 版本，而是命令的版本。
比如，当前 `dyndoc` 版本为 2, Stata 15 引入该命令， Stata 16 完善该命令。
你可输入 `dis c(dyndoc_version)` 查看你所安装的 Stata 中该命令的相应版本。
--------------------------------------------------------------------------	

- <<dd_do>> <</dd_do>>
	执行 Stata 代码块，并可选择性地将结果显示在文档中；使用时需放在单行。
---------------------------------
<<dd_do: attribute>>
block of Stata code ...
<</dd_do>>
---------------------------------

- <<dd_display>> 
	在文档中显示某个 Stata 表达式的值；使用时可放在行内。
------------------------------------------------------------------
<<dd_display: display_directive>>

// 例子
2*1*<<dd_display:%4.2f c(pi)>> = <<dd_display:%4.2f 2*1*c(pi)>>
* 输出为 2*1*3.14 = 6.28
------------------------------------------------------------------	

- <<dd_docx_display>>
	只能用于 `putdocx textblock` 命令，在文档中显示某个 Stata 表达式的值及带格式的文本；
	使用时可放在行内。
------------------------------------------------------------------
putdocx textblock begin
... text <<dd_docx_display text_options: display_directive>> text ...
putdocx textblock end

// 例子
putdocx textblock begin
2*1*<<dd_docx_display bold:%4.2f c(pi)>> = <<dd_docx_display bold:%4.2f 2*1*c(pi)>>
putdocx textblock end
* 输出为 2*1*3.14 = 6.28 （其中 3.14 和 6.28 为粗体）。
------------------------------------------------------------------


- <<dd_graph>> 
	插入图片；使用时可放在行内。
---------------------------------
<<dd_graph: attribute>>
---------------------------------
	
- <<dd_include>>
	将外部文件的内容插入到文档中；使用时需放在单行。
---------------------------------
<<dd_include: filename>>	
---------------------------------

- <<dd_ignore>> <</dd_ignore>>
	处理时忽略这两个标签内的代码；使用时需放在单行。

- <<dd_skip_if>> <<dd_skip_else>> <<dd_skip_end>>
	按某条件显示文本；使用时需放在单行。
---------------------------------
<<dd_skip_if: Stata expression>>
lines of text ...
<<dd_skip_end>>
* 或者
<<dd_skip_if: Stata expression>>
lines of text ...
<<dd_skip_else>>
lines of text ...
<<dd_skip_end>>
--------------------------------- 
	
- <<dd_remove>> <</dd_remove>> 
	删掉这两个标签内的内容；使用时可放在行内。
---------------------------------------  
... <<dd_remove>>text to remove ...
lines of text to remove ...
text to remove ... <</dd_remove>> ...
---------------------------------------   

注意： 
1.带 / 的标签需成对出现。
2. 还可进一步定义标签，在后面附加属性 (attributes)；
形如：<<dd_tags: attributes>>。
*/


*==================
*-2 命令一：dyntext
*==================

help dyntext //输出格式为 text 文本(.txt, .html, .do)

*-2.1 语法结构

* ----------------------------------------------------------
* dyntext srcfile [arguments], saving(targetfile) [options]
* ----------------------------------------------------------

/* 其中 **srcfile** 是包含 **dynamic tags** 的纯文本格式，
即我们要转换的文档；**targetfile** 是输出的文档。
srcfile 和 targetfile 的格式均为 text 文本(.txt, .html, .do)。

- 选项说明 (options)
	- saving(targetfile): 指定要保存的目标文件，saving( )是必需的。
	- replace: 如果目标文件已经存在，替换目标文件。
	- noremove：处理时忽略<<dd_remove>> 和 <</dd_remove>>。
	- nostop：运行过程中有错误时，不中断。
*/


*-2.2. 范例 

* 我们先新建一个 do 文件（也可以是 .txt 或 .html），命名为 dyntext_input1。

*--------------------------begin dyntext_input1.do------------------------
In this analysis, we are going to discover 
   the mean miles per gallon of cars in 
   1978!

<<dd_do>>
sysuse auto.dta, clear
quietly summarize mpg
dis r(mean)
<</dd_do>>
*--------------------------end dyntext_input1.do---------------------------

dyntext dyntext_input1.do, saving(dyntext_output1.txt) replace

ssc install outreg2 // 安装此命令，调用shellout命令
shellout "dyntext_output1.txt"

* 输出结果如下：

*--------------------------begin dyntext_output1.txt------------------------
In this analysis, we are going to discover 
   the mean miles per gallon of cars in 
   1978!

. sysuse auto.dta, clear
(1978 Automobile Data)

. quietly summarize mpg

. dis r(mean)
21.297297
*--------------------------end dyntext_output1.txt---------------------------


*==================
*-3 命令二：dyndoc
*==================

help dyndoc //输出格式为 text 文本(.html, .docx)

*-2.1 语法结构

* ----------------------------------------------------------
* dyndoc srcfile [arguments] [, options]
* ----------------------------------------------------------

/* 其中 **srcfile** 是包含 **dynamic tags** 的 Markdown 文档(.txt, .md, .do)，
   即我们要转换的文档。

- 选项说明 (options)
	- saving(targetfile): 指定要保存的目标文件，saving( )不是必需的。
	- replace: 如果目标文件已经存在，替换目标文件。
	- hardwrap：用 HTML 换行符的标签 <br> 替换 Markdown 文档中的实际换行符。
	- nomsg：Stata 结果输出界面不再显示指向目标文件的链接信息。
	- nostop：运行过程中有错误时，不中断。
	- *embedimage：输出的 HTML 文件中插入的图像文件为 Base64 编码。
	- *docx：输出 Word 文档，而不是 HTML 文件。
	- 继 Stata 15 后，Stata 16 对该命令进行了完善，带*选项不适用于 Stata 15。
*/


*-2.2. 范例 1：输出 HTML 文件

* 我们先新建一个 do 文件（也可以是 .txt 或 .md），命名为 dyndoc_example。

*--------------------------begin dyndoc_example.do------------------------
<<dd_version: 2>>
<<dd_include: header.txt>>


Blood pressure report
===============================================================

We use data from the Second National Health and Nutrition Examination Survey
to study the incidence of high blood pressure.

<<dd_do:quietly>>
webuse nhanes2, clear
<</dd_do>>

##Logistic regression results

We fit a logistic regression model of high blood pressure on
 weight, age group, and the interaction between age group and sex.
 
~~~
<<dd_do>>
logistic highbp weight agegrp##sex, nopvalues vsquish
<</dd_do>>
~~~

##Interaction plot

<<dd_do:quietly>>
margins agegrp#sex
marginsplot, title(Age Group and Sex Interaction) ///
        ytitle(Expected Probability of High Blood Pressure) ///
        legend(ring(0) bplacement(seast) col(1))
<</dd_do>>

<<dd_graph: saving("interaction.png") replace height(400)>>
*--------------------------end dyndoc_example.do---------------------------

/*
其中，
header.txt 文件是 HTML 代码，用来定义输出文件样式。
你可以调整里面的 stmarkdown.css 来改变样式。
当前工作路径下必须同时包含 header.txt 和 stmarkdown.css。
你可以删掉 `<<dd_include: header.txt>>` 对比下输出结果。
=== 定义报告的标题。
## 定义章节标题
<<dd_do:quietly>> 执行代码时，不显示代码和结果。
若用 Stata 15 运行，则需改成 <<dd_version: 1>>。
*/


dyndoc dyndoc_example.do, replace

ssc install outreg2 // 安装此命令，调用shellout命令
shellout "dyndoc_example.html"


* 用不同浏览器打开，展示的结果略有差异；还可在浏览器页面查看源代码（一般单击右键可见）。



*-2.3. 范例 2：输出 Word 文档

* 利用范例 1 中的输入文件，我们添加 `docx` 选项，便可将输出结果保存为 Word 文档。
* 范例 2 需使用 Stata 16 或以上版本。

dyndoc dyndoc_example.do, docx replace

* 也可以用下述命令
 
html2docx dyndoc.html, replace


ssc install outreg2 // 安装此命令，调用shellout命令
shellout "dyndoc_example.docx"


*==================
*-4 结语
*==================
/*
可通过 `help dyntext` 和 `help dyndoc` 参考更多官方范例，相关数据和文件下载地址如下：
- https://www.stata-press.com/data/r15/markdown/
- https://www.stata-press.com/data/r16/reporting/

如何重复性分析、规范地报告结果是实证研究中的难点之一。 
我们可以用 `dyntext` 和 `dyndoc` 直接写 dofile，并将报告正文和图表等整合，
自定义输出文本，HTML 或 Word 格式的报告，即所谓的一种输入，多种输出。
这在很大程度上能减少我们实证研究中的工作量。
&ensp;
### 相关链接
- 官方帮助文件
  - [Reproducible and automated reporting](https://www.stata.com/new-in-stata/truly-reproducible-reporting/)
  - [Stata Reporting Reference Manual](https://www.stata.com/manuals/rpt.pdf)
  - [RPT Dynamic documents intro](https://www.stata.com/manuals/rptdynamicdocumentsintro.pdf)
  - [dyntext](https://www.stata.com/manuals/rptdyntext.pdf)
  - [dyndoc](https://www.stata.com/manuals/rptdyndoc.pdf)
- 其他参考资料
  - [Stata Tips #7 - dyntext, dyndoc and user-written commands](https://www.stata-uk.com/news/stata-tips-7-dyntext-dyndoc-user-written-commands/)
  - [【STATA自动化报告与可重复研究】【陈堰平】](https://bbs.pinggu.org/linkto.php?url=https%3A%2F%2Fwww.stata.com%2Fmeeting%2Fchina17%2Fslides%2Fchina17_Weiping.pdf)（Automated report and reproducible research in Stata）
  - [【putdocx：输出结果 so easy】【李春涛】【中南财经政法大学金融学院】](https://bbs.pinggu.org/linkto.php?url=https%3A%2F%2Fwww.stata.com%2Fmeeting%2Fchina17%2Fslides%2Fchina17_Chuntao.pdf)（putdocx makes it easy: exporting results to Word document）
  - [君生我未生！Stata - 论文四表一键出](https://www.jianshu.com/p/97c4f291ee1e)
  - [Stata 15之Markdown——没有做不到，只有想不到！](http://m.sohu.com/a/154784310_697896)
*/






