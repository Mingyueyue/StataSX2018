<<dd_version: 2>>
<<dd_include: header.txt>>


Blood pressure report
===============================================================

We use data from the Second National Health and Nutrition Examination Survey
to study the incidence of high blood pressure.

<<dd_do:quietly>>
webuse nhanes2, clear
<</dd_do>>

##Logistic regression results

We fit a logistic regression model of high blood pressure on
 weight, age group, and the interaction between age group and sex.
 
~~~
<<dd_do>>
logistic highbp weight agegrp##sex, nopvalues vsquish
<</dd_do>>
~~~

##Interaction plot

<<dd_do:quietly>>
margins agegrp#sex
marginsplot, title(Age Group and Sex Interaction) ///
        ytitle(Expected Probability of High Blood Pressure) ///
        legend(ring(0) bplacement(seast) col(1))
<</dd_do>>

<<dd_graph: saving("interaction.png") replace height(400)>>
