## Q&A

- **Q1:** 老师，总是弄不成功，就连那个 stata 的快捷方式的那一点就搞不定，请问 Win 10 上能成功吗？

   **A1:** 亲测 Win 10 可以。请参考推文 [「Stata+Atom: 强强联手打造编辑利器」](https://www.jianshu.com/p/ba887a77b6a8)中 **Link the Stata Automation library** 部分。有几点需要注意，如下：
   1. 在修改路径时，需要用**英文**引号，不能用**中文**引号。
   2. 在修改路径时，Register 前面是一道斜杠，而且斜杠之前还有一个空格。
   
  还可参考文章[「如何用Sublime Text 3改善Stata命令编写环境？」](https://www.jianshu.com/p/1255eee18a08) 中 **4、安装Stata Automation object 并注册 Stata Automation type library** 部分；[「珠联璧合：Jupyter Notebook 与 Stata 之融合」](https://www.jianshu.com/p/2f52064865c4)中 **2. 将 stata 添加到命令行注册** 部分。
&ensp;
- **Q2:** 弄了一天还是安装失败了，每次运行代码的时候都不能执行，总报 hydrogen 的错，感觉是不是没能成功关联上 stata？

   **A2:** 能否提供报错内容？点击 `File > Preferences/Settings > Install > Search packages`, 搜索到 `language-stata` 后点击 `Install`；或者打开 cmd 命令行，输入并执行 `apm install language-stata`。若仍报错，您可以按照[「珠联璧合：Jupyter Notebook 与 Stata 之融合」](https://www.jianshu.com/p/2f52064865c4) 中 **4. 打开 Jupyter Notebook，新建 stata 语法格式的 notebook，执行 stata 代码，检验是否关联成功** 部分，运行 Jupyter Notebook。若能成功运行，则应该成功关联上 Stata，是包 `hydrogen` 或 Atom 的问题。建议重新卸载安装 `hydrogen`，或在  [`hydrogen` 的 GitHub 页面](https://github.com/nteract/hydrogen) 搜索相关问题。
&ensp;
- **Q3:** 听说atom启动慢。启动后很牛。

   **A3:**  亲测，相比 Sublime Text3，Atom 的启动速度是要慢一些。Atom 的很多操作与 Sublime Text3 基本一致，Sublime Text3 的用户应该能轻松过渡到 Atom。Atom 的最大优势就是：由 GitHub（目前全球范围内影响力最大的代码仓库/开源社区） 打造，有着很大的发展潜力。开源社区非常非常活跃，能提供大量丰富的插件，也能及时解决各种问题。
&ensp;
- **Q4:** 老师，请问 mac 版本可以实现这个功能吗？

   **A4:** 可以，请参考插件开发者的官方说明：
  - [language-stata](https://github.com/kylebarron/language-stata)
  - [stata-exec](https://github.com/kylebarron/stata-exec)
  - [stata_kernel](https://kylebarron.dev/stata_kernel/getting_started/) 

- **Q5:** 其他问题……

   **A5:** 若在安装中遇到问题，可以参考插件开发者的官方说明或搜索 Atom 社区中的相关问题。
  - [language-stata](https://github.com/kylebarron/language-stata)
  - [stata-exec](https://github.com/kylebarron/stata-exec)
  - [stata_kernel](https://kylebarron.dev/stata_kernel/getting_started/) 
  - [hydrogen](https://github.com/nteract/hydrogen) 
  - [Atom 中文社区](https://atom-china.org/)
  - [Atom GitHub 社区](https://github.com/atom/atom/issues)
  - [Atom Discuss](https://discuss.atom.io/)

