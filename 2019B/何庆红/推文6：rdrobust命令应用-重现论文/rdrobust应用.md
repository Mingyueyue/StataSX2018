> **任务：** 根据作者提供的数据和程序，重现论文中的质证结果。这篇推文主要介绍 `rdrobust` 和 `rdbwselect` 命令的应用。
- 这篇文章主要采用图形的方式来呈现结果，值得学习；
- 请把作者提供的所有 dofiles 整合成一个 dofiles，类似于我暑假论文班 **Faulkender2006.do** 那个文档的模式。


- Song, B., 2018, Estimating incumbency effects using regression discontinuity design, Research & Politics, 5 (4): 2053168018813446. [[PDF]](https://sci-hub.tw/10.1177/2053168018813446)，[[论文重现程序和数据]](https://dataverse.harvard.edu/dataset.xhtml?persistentId=doi%3A10.7910%2FDVN%2FJSOWUR&version=&q=&fileTypeGroupFacet=&fileAccess=Public&fileTag=&fileSortField=&fileSortOrder=)