*======================================
*======================================
*========合成控制法的案例分析==========
*======================================
*======================================
cap log close
log     using SCM190515.log,text replace

clear
use 数据.dta,replace

*（一）生成变量

//生成工业相对产值变量
bysort 年份: egen 工业产值均值= mean(工业总产值万元)
gen 工业相对产值= 工业总产值万元/ 工业产值均值
//生成工业相对就业率变量
bysort 年份: egen 工业年平均从业人员数均值= mean(工业年平均从业人员数)
gen 工业相对就业率= 工业年平均从业人员数万人/工业年平均从业人员数均值
//生成第三产业相对就业率变量
bysort 年份: egen 第三产业从业人员数均值= mean(第三产业从业人员数万人)
gen 第三产业相对就业率= 第三产业从业人员数万人/第三产业从业人员数均值
//生成第三产业相对产值变量
bysort 年份: egen 第三产业产值均值= mean(第三产业增加值亿元)
gen 第三产业相对产值=第三产业增加值亿元/第三产业产值均值
//生成相对工资变量
bysort 年份: egen 职工平均工资均值= mean(职工平均工资元)
gen 相对工资= 职工平均工资元/职工平均工资均值
//生成相对房价变量
bysort 年份: egen 房价均值= mean(房价元平方米)
gen 相对房价= 房价元平方米/房价均值
gen ln人均GDP=log(人均GDP元)
gen ln人口密度人平方公里=log(人口密度人平方公里)
gen ln年末金融机构存款余额万元=log(年末金融机构存款余额万元)
gen ln医院卫生院床位数张=log(医院卫生院床位数张)
gen ln国际互联网用户数户=log(国际互联网用户数户)
gen 上海工业相对产值=工业相对产值 if 序号==35
gen 重庆工业相对产值=工业相对产值 if 序号==26
gen 上海第三产业相对产值=第三产业相对产值 if 序号==35
gen 重庆第三产业相对产值=第三产业相对产值 if 序号==26

tab 年份, gen(年份)

*（二）生成面板数据
tsset 序号 年份  //设置面板数据
****

*（三）基本结果

// 图1(a) 真实重庆与合成重庆的相对工业总产值
preserve
drop if 城市=="上海"
synth 工业相对产值 工业相对产值(2006(1)2010) 相对工资 ln人均GDP 财政支出占GDP比重 ln人口密度人平方公里  ln年末金融机构存款余额万元 ln医院卫生院床位数张 ln国际互联网用户数户  工业相对产值(2006) 工业相对产值(2008) 工业相对产值(2010), trunit(26) trperiod(2011) nested fig
restore

****----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

****--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
****稳健性检验*****************************
//有效性检验（仅展示重庆房产税对工业相对产值影响的稳健性检验程序）
//图3　各城市工业相对产值差值分布
************************************稳健性检验一(工业相对产值为目标变量)************************************
//政策实施前均方预测误差的平方根
tempname resmat
        forvalues i = 1/35 {
        synth 工业相对产值 相对工资 ln人均GDP 财政支出占GDP比重 ln人口密度人平方公里  ln年末金融机构存款余额万元 ln医院卫生院床位数张 ln国际互联网用户数户  工业相对产值(2006) 工业相对产值(2008) 工业相对产值(2010), trunit(`i') trperiod(2011) xperiod(2006(1)2010) mspeperiod
        matrix `resmat' = nullmat(`resmat') \ e(RMSPE)
        local names `"`names' `"`i'"'"'
        }
        mat colnames `resmat' = "RMSPE"
        mat rownames `resmat' = `names'
        matlist `resmat' , row("Treated Unit")
		** loop through units

//各城市预测误差分布图
forval i=1/35{
qui synth 工业相对产值 相对工资 ln人均GDP 财政支出占GDP比重 ln人口密度人平方公里  ln年末金融机构存款余额万元 ln医院卫生院床位数张 ln国际互联网用户数户  工业相对产值(2006) 工业相对产值(2008) 工业相对产值(2010), xperiod(2006(1)2010) trunit(`i') trperiod(2011) keep(synth_`i', replace)
}

forval i=1/35{
use synth_`i', clear
rename _time years
gen tr_effect_`i' = _Y_treated - _Y_synthetic
keep years tr_effect_`i'
drop if missing(years)
save synth_`i', replace
}
**

use synth_1, clear
forval i=2/35{
qui merge 1:1 years using synth_`i', nogenerate
}

**
**删除拟合不好的城市及上海市（干预组）
drop tr_effect_2     //删除天津
drop tr_effect_20    //删除武汉
drop tr_effect_35    //删除上海


local lp1
forval i=1/1 {
   local lp1 `lp1' line tr_effect_`i' years, lpattern(dash) lcolor(gs8) ||
}
** 
local lp2
forval i=3/19 {
   local lp2 `lp2' line tr_effect_`i' years, lpattern(dash) lcolor(gs8) ||
}
 
 local lp3
forval i=21/34 {
   local lp3 `lp3' line tr_effect_`i' years, lpattern(dash) lcolor(gs8) ||
}
 
 **create plot
twoway `lp1' `lp2' `lp3'  || line tr_effect_26 years, ///
lcolor(black) legend(off) xline(2011, lpattern(dash))






