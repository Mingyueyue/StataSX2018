
&emsp;


&emsp;

> 作者：何庆红（北京大学中国卫生经济研究中心）
>
>连享会：([知乎](https://zhuanlan.zhihu.com/arlion) | [简书](http://www.jianshu.com/u/69a30474ef33) | [码云](https://gitee.com/arlionn) | [CSDN](https://blog.csdn.net/arlionn))  


本推文介绍合成控制法高效实现命令`synth_runner`。合成控制方法（Synthetic Control Method）由Abadie and Gardeazabal (2003)提出。目前，该方法已被广泛使用。

#  1.背景介绍
>上一篇推文已经介绍了当实验组只有一个对象（A市）时，Abadie and Gardeazabal (2003)提出“合成控制法”（Synthetic Control Method）构造反事实框架。其基本思想是，虽然无法找到A市的最佳控制地区，但通常可对若干大城市进行适当的线性组合，以构造一个更为优秀的“合成控制地区”（synthetic control region），并将“真实A市”与“合成A市”进行对比，故名“合成控制法”。合成控制法的一大优势是，可以根据数据（data-driven）来选择线性组合的最优权重，避免了研究者主观选择控制组的随意性。
但当实验组中有多个试点对象在不同的时间受到政策冲击时，上文的`synth`命令将无法实现。此时，`synth_runner`命令是解决该问题的不二选择。该命令的优点包括：
- 首先，它可以直接进行安慰剂检验，并提供统计推断的P value来比较安慰剂检验的效果；
- 其次，允许多个试点对象在不同的时间受到冲击；
- 第三，提供拟合优度和估计结果的可视化呈现。

##  2. 合成控制法原理
原理介绍请看以下链接：
[Stata: 合成控制法程序](https://www.jianshu.com/p/1bba82b65a78)
[合成控制法：一组文献](https://www.jianshu.com/p/fbdebf3283d0)
[合成控制法简介及代码](https://wenku.baidu.com/view/aeae141bd1d233d4b14e852458fb770bf78a3b9f.html)

##  3. 合成控制高效实现`synth_runner` 
### 3.1 命令安装
在 Stata 命令窗口中输入如下命令即可自动安装 `synth_runner` 命令：
```stata
ssc install synth_runner, replace
```

 ### 3.2 语法格式

`synth_runner` 的基本语法格式如下：
```stata
 synth_runner depvar predictorvars, {trunit(#) trperiod(#)|d(varname)}
                    [trends pre_limit_mult(real) training_propr(real) gen_vars
                    noenforce_const_pre_length ci max_lead(int)
                    n_pl_avgs(string) pred_prog(string) deterministicoutput
                    parallel pvals1s drop_units_prog(string)
                    xperiod_prog(string) mspeperiod_prog(string)
                    synthsettings]
```
- 其中，**depvar**为因变量（outcome variable）
- **predictorvars**为预测变量（predictors）。
- 当试点对象只有一个时，必选项**trunit**用于指定唯一的试点对象（trunit表示 treated unit），必选项**trperiod**用于指定试点时期（trperiod表示 treated period）。
- 当试点对象有多个时，选项**d(varname)**为0-1变量，当试点对象在试点时期之后取值为1，否则取值为0。
- 选项**trends**是指根据结果变量的时间顺序来匹配。具体而言，将研究对象的结果变量按照时间顺序排列，这样试点前一期的取值为1。
 - 选项**pre limit mult**作用是在进行安慰剂检验时，需要删除均方误差特别大的控制组对象，而这个选项提供了删除的倍数。
- 选项**training propr**计算因变量的哪些时间作为预测变量。
- 选项**gen vars**表示只有一个试点时间时，可以产生下面想这些估计量：
   - 选项**lead** 表示试点后的动态时间，若为1，表示试点后第1期。
   - 选项**depvar synth**表示因变量的合成
   - 选项**effect**表示真实因变量与合成因变量之差。
   - 选项**pre rmspe**表示试点前预测变量的均方误差。
   - 选择项**post rmspe**表示试点后预测变量的均方误差。
   - 选项**depvar scaled** 表示标准化的因变量。
   - 选项**depvar scaled synth(if the match was done on trends)**表示标准化的合成因变量。  
   - 选项**effect scaled (if the match was done on trends)**表示标准化的因变量与合成因变量之差。

### 3.3 [加州控烟案例](http://xueshu.baidu.com/usercenter/paper/show?paperid=1s3s0p50mm780cn0km0n0gb08w261726&site=xueshu_se)

>背景：1988年11月美国加州通过了当代美国最大规模的控烟法（anti-tobacco legislation），并于1989年1月开始生效。该法将加州的香烟消费税（cigarette excise tax）提高了每包25美分，将所得收入专项用于控烟的教育与媒体宣传，并引发了一系列关于室内清洁空气的地方立法（local clean indoor-air ordinances），比如在餐馆、封闭工作场所等禁烟。Abadie et al. (2010)根据美国1970-2000年的州际面板数据，采用合成控制法研究美国加州1988年第99号控烟法（Proposition 99）的效果。
```stata
. sysuse smoking      （打开数据集）
. xtset state year       （设为面板数据）
       panel variable:  state (strongly balanced)
        time variable:  year, 1970 to 2000
                delta:  1 unit
```
#### 3.3.1 例子1：只有一个实验对象和实验期
>首先，运用`synth_runner `命令重现上一篇推文`synth`命令的结果
```
. synth_runner cigsale beer(1984(1)1988) lnincome(1972(1)1988)  ///
retprice age15to24 cigsale(1988) cigsale(1980) cigsale(1975),  ///
trunit(3)  trperiod(1989) gen_vars

Estimating the treatment effects
Estimating the possible placebo effects (one set for each of the 1 treatment periods)
|                                    | Total: 38
.....................................| 26.00s elapsed. 

Conducting inference: 5 steps, and 38 placebo averages
Step 1... Finished
Step 2... Finished
Step 3... Finished
Step 4... Finished
Step 5... Finished

Post-treatment results: Effects, p-values, standardized p-values

             | estimates      pvals  pvals_std 
-------------+---------------------------------
          c1 | -7.887098   .1315789          0 
          c2 | -9.693599   .1842105          0 
          c3 |  -13.8027   .2105263          0 
          c4 |   -13.344   .1315789          0 
          c5 |  -17.0624   .1052632          0 
          c6 |  -20.8943   .0789474          0 
          c7 |  -19.8568   .1315789   .0263158 
          c8 |  -21.0405   .1578947          0 
          c9 |  -21.4914   .1052632   .0263158 
         c10 |  -19.1642   .1842105   .0263158 
         c11 |   -24.554   .1052632          0 
         c12 |  -24.2687   .1052632   .0263158 

```
- 其中，**cigsale(1975) cigsale(1980) cigsale(1988)**分别表示人均香烟消费在1975、1980与1988年的取值。
- 必选项**trunit(3)**表示第3个州（即加州）为处理地区；
- 必选项**trperiod(1989)**表示控烟法在1989年开始实施。

>上表显示，加州控烟法对于人均香烟消费量有很大的负效应，而且此效应随着时间推移而变大，且统计上显著。具体来说，在1989-2000年（C1-C12）期间，加州的人均年香烟消费逐年减少。


> 其次，绘制图形查看合成效果，并绘制加州与合成加州人均香烟消费之差（即处理效应）。
```stata
. single_treatment_graphs, trlinediff(-1) effects_ylabels(-30(10)30)  ///
effects_ymax(35) effects_ymin(-35)
(13 real changes made)
(3 real changes made)

. effect_graphs, trlinediff(-1)

```
![真实加州与合成加州](https://images.gitee.com/uploads/images/2019/0917/213453_42681258_5087133.png)
![控烟的政策效应](https://images.gitee.com/uploads/images/2019/0917/213453_ac91911a_5087133.png)

>最后，进行统计推断：
```stata
. pval_graphs
```
![原始P值](https://images.gitee.com/uploads/images/2019/0917/213453_f2fe52a7_5087133.png)
![标准化P值](https://images.gitee.com/uploads/images/2019/0917/213453_e93ad2b6_5087133.png)

>上面两幅图显示，加州控烟法对于人均香烟消费量的负效应是显著的。
#### 3.3.2 例子2：有多个实验对象和实验期
```
. capture drop D

. program my_pred, rclass
  1. args tyear
  2. return local predictors "beer(`=`tyear'-4'(1)`=`tyear'-1') lnincome(`=`tyear'-4'(1)`=`tyear'-1')" 
  3. return local predictors "beer(`=`tyear'-4'(1)`=`tyear'-1') lnincome(`=`tyear'-4'(1)`=`tyear'-1')" 
  4. end

. program my_drop_units
  1. args tunit
  2. if `tunit'==39 qui drop if inlist(state,21,38)
  3. if `tunit'==3 qui drop if state==21
  4. end

. program my_xperiod, rclass
  1. args tyear
  2. return local xperiod "`=`tyear'-12'(1)`=`tyear'-1'"
  3. end

. program my_mspeperiod, rclass
  1. args tyear
  2. return local mspeperiod "`=`tyear'-12'(1)`=`tyear'-1'"
  3. end
* 除了加州，假定佐治亚州在1988年也实施了烟草法案
. generate byte D = (state==3 & year>=1989) | (state==7 & year>=1988)

. synth_runner cigsale retprice age15to24, d(D) pred_prog(my_pred)  ///
trends training_propr(`=13/18') drop_units_prog(my_drop_units)) xperi
> od_prog(my_xperiod) mspeperiod_prog(my_mspeperiod)
Estimating the treatment effects
Estimating the possible placebo effects (one set for each of the 2 treatment periods)
|                                   | Total: 37
....................................| 45.00s elapsed. 
|                                   | Total: 37
....................................| 44.00s elapsed. 

Conducting inference: 6 steps, and 1369 placebo averages
Step 1... Finished
Step 2... Finished
Step 3... Finished
Step 4... Finished
Step 5... Finished
Step 6... Finished

Post-treatment results: Effects, p-values, standardized p-values

             | estimates      pvals  pvals_std 
-------------+---------------------------------
          c1 |  -.027493   .3002191   .0021914 
          c2 | -.0485773   .1775018   .0043828 
          c3 | -.0921521   .0394449          0 
          c4 | -.1017043   .0409058          0 
          c5 | -.1270111   .0241052          0 
          c6 | -.1352273   .0219138          0 
          c7 |  -.141674   .0262966          0 
          c8 |  -.196867   .0051132          0 
          c9 | -.1754307   .0124178          0 
         c10 | -.1833944   .0197224          0 
         c11 | -.1910038   .0233747          0 
         c12 | -.1889059   .0219138          0 
```
>上表显示，加州控烟法对于人均香烟消费量的负效应随着时间逐渐变大，且统计上显著。

> 其次，绘制图形查看合成效果，并绘制实验组与合成实验组人均香烟消费之差（即处理效应）。

![真实实验组和合成对照组](https://images.gitee.com/uploads/images/2019/0917/213453_d4a77580_5087133.png)
![法案控烟效果](https://images.gitee.com/uploads/images/2019/0917/213453_7e5cae8f_5087133.png)

>最后，进行统计推断：

![原始P值](https://images.gitee.com/uploads/images/2019/0917/213454_e5bb472d_5087133.png)
![标准化P值](https://images.gitee.com/uploads/images/2019/0917/213454_e2602013_5087133.png)
>上面两幅图显示，加州控烟法对于人均香烟消费量的负效应是显著的。
### 4. 参考资料
- [Abadie, A., Diamond, A., and J. Hainmueller. 2014. Comparative Politics and the Synthetic Control Method. American Journal of Political Science](http://xueshu.baidu.com/usercenter/paper/show?paperid=5b788ba1d4c288a4afea8e62b21c9154&site=xueshu_se&sc_from=pku)

- [Abadie, A., Diamond, A., and J. Hainmueller. 2010. Synthetic Control Methods for Comparative Case Studies: Estimating the Effect of California's Tobacco Control Program.  Journal of the American Statistical Association](http://xueshu.baidu.com/usercenter/paper/show?paperid=1s3s0p50mm780cn0km0n0gb08w261726&site=xueshu_se&sc_from=pku)

- [Abadie, A. and Gardeazabal, J. 2003. Economic Costs of Conflict:  A Case Study of the Basque Country. American Economic Review](http://xueshu.baidu.com/usercenter/paper/show?paperid=b9b8a6f807b94ecb62740fef1f4ff6c7&site=xueshu_se&hitarticle=1&sc_from=pku)

- Vanderbei, R.J. 1999. LOQO: An interior point code for quadratic programming.  Optimization Methods and Software 11: 451-484.
-  Cavallo, E., S. Galiani, I. Noy, and J. Pantano. 2013. Catastrophic
        natural disasters and economic growth.  Review of Economics and
        Statistics 95: 1549-1561.


