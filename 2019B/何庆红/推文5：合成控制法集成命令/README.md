### Synth_Runner命令：合成控制法高效实现

> **任务：** 根据如下文章写一篇推文，适当介绍理论部分，重点在于 Stata 操作。

- Sebastian Galiani,  Brian Quistorff, 2017, The Synth_Runner Package: Utilities to Automate Synthetic Control Estimation Using Synth, Stata Journal, 17(4): 834–849. [[pdf]](https://sci-hub.tw/10.1177/1536867X1801700404)