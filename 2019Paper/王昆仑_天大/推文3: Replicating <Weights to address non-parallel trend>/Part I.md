*++++++++++++++++++++++++++++++++++++++++++++

*Authors: Beck, T., Levine, R., & Levkov, A. (2010). 

*Paper: Weights to Address Non-parallel Trends in Panel Difference-in-differences Models

*Journal: CESifo Economic Studies, 64(2), 216-240.

```
*The main content: Two algorithms to figure out the sampling weights

*Part I: The explanation of  simulating process citing two algorithms

*part II: The introduction of the first algorithms to obtain the sampling weights, Gird Search Method

*part III: The presentation of the second algorithms to get the sampling weights, Iteration Method

```
*++++++++++++++++++++++++++++++++++++++++++++

```	
* Simulation Code From This Paper
	
	* LOAD MONTE CARLO DATA SET,打开预设得MCDATA
	u "DATA/INPUT/MCDATA.dta", replace

	
	* GEN ATTRIBUTE VARIABLES，生成三个属性变量(原文中M=3)， 无法观测得特征变量，如距离CBD的距离
		qui gen h1 = runiform(0,1) 
		qui gen h2 = runiform(0,1)   
		qui gen h3 = runiform(0,1)  
	* GEN HETEROGENEITY VARIABLE (IN EQUATION 2)根据原文中H = -M/2+SIGMA(h)，这是为了保证均值为零，三个均匀分布均值在0.5*3，因此加上-3/2	
		qui gen H =  (-1.5+h1+h2+h3)	
	
	* GENERATE TREATMENT VARIABLES,生成两个处理变量，处理效应的大小于此无关，类似于连续性DD？
		qui gen T1 = runiform(0,1) 
		qui gen T2 = runiform(0,1)  

	* ADD TREATMENT x H CORRELATION TO TREND COMPONENT TO GENERATE OMEGA，生成与处理变量相关的因子载荷(w,factor loading in equaltion 2)
		qui replace TREND = TREND + (0.5 * T1 + 0.5 * T2) * H
		
	* GENERATE SAMPLING WEIGHTS AND SAMPLING FRACTIONS, 生成样本权重和样本得分，二者互为倒数
		* RANDOM DISTRIBUTION (CONTAINS SCALARS r1, r2, r3)，生成01均匀分布RW
			gen RW = runiform(0,1)
		* GEN SAMPLING WEIGHTS, CORRESPONDS TO S,根据S定义，样本权重为h与RW的前三行的乘积(可以为任意三行)，这样生成的权重由于与H相关，因此此时为非随机样本，会导致估计有偏
			qui gen MODELW =  (RW[1]*h1+RW[2]*h2+(RW[3])*h3)
			* NORMALIZE 对样本权重进行标准化
			qui sum MODELW 
			qui replace MODELW = MODELW /r(mean)
	* GEN SAMPLING FRACTIONS F AS INVERSE OF SAMPLING WEIGHTS S，由样本权重取倒数得到样本得分
		qui gen IVW = 1/MODELW

	* USE SAMPLING FRACTIONS TO DRAW THE OBSERVED SAMPLE i = 1,...,I FROM THE POPULATION j = 1,...,J，根据样本得分确实进行回归的样本，非随机抽取	
		qui gsample `obs' [w=IVW]	
	 
	* RESHAPE DATA SET INTO LONG FORMAT 将十个文件夹合并，样本量为1000*10，并且每个文件夹随机生成，因此形成panel data frame
		qui foreach num of numlist 1/10 {
		preserve
		keep ID LE YE Y`num' h*  TREND T1 T2 MODELW IVW 
		gen PERIOD = `num'
		ren Y`num' Y
		gen panelID = _n
		save "TEMP/re_temp`num'.dta", replace
		restore
		}
		qui u "TEMP/re_temp1.dta", clear
		qui erase "TEMP/re_temp1.dta"
		qui foreach num of numlist 2/10 {
		append using "TEMP/re_temp`num'.dta"
		qui erase "TEMP/re_temp`num'.dta"
		}	
		* RESHAPE DONE 增加样本量,形成Panel Data Frame
	
	* DEFINE PANEL DIMENSION
		qui xtset panelID PERIOD	
		
	* ADD THE f(T) x OMEGA INTERACTION TO OUTCOME
		qui replace Y = Y + TREND*PERIOD

	* ADD TREATMENT TO OUTCOME FOR POST PERIOD，处理效应为相加的系数，因此默认为1,我将其设置为2和4
		qui replace  Y = Y + 3*T1 if PERIOD >=6
		qui replace  Y = Y + 6*T2 if PERIOD >=6

	* GEN TREATMENT x POST DIFFERENCE-IN-DIFFERENCES VARIABLES，生成交互项
		qui gen T1_POST = T1 * (PERIOD >=6)
		qui gen T2_POST = T2 * (PERIOD >=6)

	* ADD RANDOM ERROR，添加随机误差项
		qui replace Y = Y + rnormal(0,0.1)
	
	* ESTIMATION****** *************************************************************
	
	* ESTIMATE OLS D,OLS DID 估计
		 reg Y T1_POST T2_POST i.PERIOD, abs(panelID)
		 areg Y T1_POST T2_POST i.PERIOD, abs(panelID) cluster(panelID)
		 areg Y T1_POST T2_POST i.PERIOD  [pw=MODELW], abs(panelID) cluster(panelID)
		 xtreg Y T1_POST T2_POST i.PERIOD [pw=IVW], cluster(panelID) fe
			* RECOVER ESTIMATED TREATMENT EFFECTS
			scalar bT1_OLS_`it' = _b[T1_POST]
			scalar bT2_OLS_`it' = _b[T2_POST]
	
	* ESTIMATE WPT DD USING SAMPLING WEIGHTS,加权的OLS估计，由于存在H和W相关，因此此时的估计也存在问题？
		 reg Y T1_POST T2_POST i.PERIOD [w=MODELW], abs(panelID) 
			* RECOVER ESTIMATED TREATMENT EFFECTS
			scalar bT1_WLS_`it' = _b[T1_POST]
			scalar bT2_WLS_`it' = _b[T2_POST]
	
	* ESTIMATE WPT DD USING WEIGHTS FROM GRID SEARCH ALGORITHM，利用网格搜索算法得到权重，详细说明见ALG_GRID文档
	
		* SAVE DATA FOR ALGORITHM
			qui save "TEMP/temp", replace
		* RUN ALGORITHM	
			qui do "DOS/ALG_GRID.do"
		* RECOVER DATA SET AND MERGE WPT WEIGHTS	
			qui u "TEMP/temp", clear
			qui merge m:m ID using "TEMP/WA_MC.dta"	
			erase "TEMP/WA_MC.dta"	
			qui tab _m
			qui drop _m 
		* ESTIMATE WPT DD
		  reg Y T1_POST T2_POST i.PERIOD [w=WA_MC], abs(panelID) 
				* RECOVER ESTIMATED TREATMENT EFFECTS
				qui scalar bT1_WLS_grid_`it' = _b[T1_POST]
				qui scalar bT2_WLS_grid_`it' = _b[T2_POST]
				* DROP WEIGHTS
				qui drop WA_MC  
				
	* ESTIMATE WPT DD USING WEIGHTS FROM ITERATIVE ALGORITHM，利用网格搜索算法得到权重，详细说明见ALG_GRID文档
	
		* SAVE DATA FOR ALGORITHM
			qui save "TEMP/temp", replace
		* RUN ALGORITHM	
			qui do "DOS/ALG_ITER.do"
		* RECOVER DATA SET AND MERGE WPT WEIGHTS	
			qui u "TEMP/temp", clear
			qui merge m:m ID using "TEMP/WA_MC.dta"	
			erase  "TEMP/WA_MC.dta"	
			qui tab _m
			qui drop _m 
		* ESTIMATE WPT DD
		 reg Y T1_POST T2_POST i.PERIOD [w=WA_MC], abs(panelID) 
				* RECOVER ESTIMATED TREATMENT EFFECTS
				qui scalar bT1_WLS_alg_`it' = _b[T1_POST]
				qui scalar bT2_WLS_alg_`it' = _b[T2_POST]
				* SAVE TREATMENT PRE-TREND CORRELATION BY TREATMENT
				qui scalar T1CORR_`it' = T1CORR
				qui scalar T2CORR_`it' = T2CORR	 
				* DROP WEIGHTS
				qui drop WA_MC
				erase "TEMP/temp.dta"
```