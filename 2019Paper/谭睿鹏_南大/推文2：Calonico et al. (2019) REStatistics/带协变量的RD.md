> &emsp;          
> 作者：谭睿鹏   (南京大学)      
> &emsp;          
> Stata 连享会： [知乎](https://zhuanlan.zhihu.com/arlion) | [简书](http://www.jianshu.com/u/69a30474ef33) | [码云](https://gitee.com/arlionn) | [CSDN](https://blog.csdn.net/arlionn)
> &emsp;  




> Stata连享会 [计量专题](https://gitee.com/arlionn/Course/blob/master/README.md)  || [精品课程](https://active.clewm.net/B9EOq8?qrurl=https://c3.clewm.net/B9EOq8&gtype=1&key=4cedd150575bd900c651021e9795edbac41ce34075) || [推文集锦](https://www.jianshu.com/p/de82fdc2c18a) 





> 连享会 RDD 专题文章：
- [RDD: 断点回归的非参数估计及Stata实现](http://www.jianshu.com/p/2aa62edc148b)
- [Stata: 断点回归分析 (RDD) 文献和命令](http://www.jianshu.com/p/405eb1dff559)
- [Stata: 两本断点回归分析 (RDD) 易懂教程](http://www.jianshu.com/p/538ed1805004)
- [Abadie 新作: 简明 IV, DID, RDD 教程和综述](http://www.jianshu.com/p/2849f03a3843)


&emsp;


## 一. 引言
断点回归（RD）方法在经济学、政治学和其他社会科学，以及行为科学、生物医学和统计科学中应用广泛。在因果推断框架中，断点回归被认为是一种最可信的非实验估计策略，因为其依赖的假设较弱，这可以为局部政策效应提供比较稳健的估计和统计推断。
在断点回归中，如果使用加权最小二乘法估计政策效应，研究者通常会加入一些事前已经确定的不受政策影响的协变量，如个体的人口统计学和经济社会学特征；但是，如果使用非参数局部多项式方法（如局部线性回归）来估计该政策效应，大多数研究却并没有考虑加入协变量对该估计的影响，这可能导致不一致的估计量。
在Calonico et al. (2019) 的文章中，作者们提出了将协变量纳入考虑后，在RD框架下使用非参数多项式回归方法估计政策效应的方法，并对相应的统计推断进行了数理论证。


## 二. Calonico et al. (2019) 方法的优点
Calonico et al. (2019) 推导了在包括协变量以后，断点回归估计量的一致性和大样本性质，该方法的主要优点有：
1.允许多种形式的协变量进入待估模型，协变量可以是连续型变量，非连续型变量或者两种都有。如可以加入年龄（连续型）、性别（非连续型），或将两者均加入；

2.并不需要其他平滑方法，如用其他方法对协变量选择窗宽。也就是说，可以使用同样的方法对关键解释变量和协变量选择窗宽；

3.无需假设协变量进行模型的形式，协变量进入模型的方式可以是参数形式，也可以是非参数形式。如果是以参数形式，也不需要进行任何形式的函数假设。

## 三. 实例
使用Calonico et al. (2019)的方法估计美国"启智计划"政策对美国儿童死亡率的影响 （Ludwig and Miller，2007）。"启智计划"政策是美国联邦政府为贫困家庭设立的联邦幼儿园资金项目，该政策于1965年向美国300个最贫困的县提供了技术援助来制定"启智计划"。作为向贫困宣战的一种策略，"启智计划"每年为 90 多万名 3 至 5 岁的贫困儿童及其家庭提供学前教育、健康和其他方面的社会服务。
在这一计划的实施过程中，如何选择实施对象是根据美国县级1960年的贫困指数，当贫困指数在59.1984以上时，美国政府就在该县实施"启智计划"，在59.1984 以下时，则不实施"启智计划"。这就提供了做研究的一个断点，即贫困指数在59.1984以上的县为实验组，其他为控制组，可以使用断点回归（RDD）来估计这一政策效应。
但是影响儿童死亡率的变量很多，除了"启智计划"以外，还应包括总人口，黑人人数占比，城市人口占比，3-5 岁的儿童人口数及占比，14-17 岁的人口数及占比，25 岁以上的人口数及占比。因此，在估计"启智计划"对儿童死亡率的影响时，应该控制这些协变量。
这一节将比较标准 RD 政策效应估计量与使用异方差稳健最近邻方差纳入协变量后的 RD 政策效应。使用到的 stata 命令为 rdrobust，主要命令如下：


```stata
. use "headstart.dta", clear  //该数据可以在
. global y mort_age59_related_postHS  // y 为因变量（儿童死亡率）
. global x povrate60                 // x 为分配变量（forcing variable）,美国县级 1960 年贫困指数
. global z census1960_pop census1960_pctsch1417 census1960_pctsch534 ///
     census1960_pctsch25plus census1960_pop1417 census1960_pop534 ///
	 census1960_pop25plus census1960_pcturban census1960_pctblack   // z 为协变量，包括人口总数，黑人和城镇人数占比，3-5 岁儿童人数占比， 14-17 岁人数占比以及年龄大于25岁的人数占比
```
由于对于 RD 的点估计和偏差估计量可以有不同的窗宽，分别用 h 和 b 来表示，在实际操作中可以限制 h=b，也可以不加限制；在进行统计推断时，可以使用 RD 标准的统计推断方法，也可以使用协变量调整的 RD 统计推断方法。因此可以分为以下几种情形进行讨论。


3.1 情形一：对 h 和 b 之间的关系不加限制，选择使得未考虑协变量时的政策估计效应的 MSE 最小化的方法来挑选最优窗宽，使用 RD 标准统计推断方法
 
```stata
. rdrobust $y $x, c(59.1968)
. local  h = e(h_l)
. local  b = e(b_l)
. local  IL =  e(ci_r_rb) - e(ci_l_rb)
```
其中 y 为被解释变量，x 为解释变量，c 括号中输入的为分配变量的值，暂元 h 中存储了 RD 点估计时使用的窗宽，暂元 b 中存储了 RD 偏差估计时使用的窗宽，IL 为 95% 置信区间的区间长度。
```stata
--------------------------------------------------------------------------------
Sharp RD estimates using local polynomial regression
--------------------------------------------------------------------------------    
 Cutoff c = 59.1968 | Left of c Right of c            Number of obs =       2783
-------------------+----------------------            BW type       =      mserd
     Number of obs |      2489         294            Kernel        = Triangular
Eff. Number of obs |       234         180            VCE method    =         NN
    Order est. (p) |         1           1
    Order bias (q) |         2           2
       BW est. (h) |     6.809       6.809
       BW bias (b) |    10.724      10.724
         rho (h/b) |     0.635       0.635

Outcome: mort_age59_related_postHS. Running variable: povrate60.
--------------------------------------------------------------------------------
            Method |   Coef.    Std. Err.    z     P>|z|    [95% Conf. Interval]
-------------------+------------------------------------------------------------
      Conventional | -2.4102     1.2053   -1.9996  0.046   -4.77264     -.047753
            Robust |     -          -     -2.0340  0.042   -5.46284      -.10125
--------------------------------------------------------------------------------
```
可以看出此时政策效应大小为 -2.41，p 值为 0.042，h 值为 6.809，b 值为 10.724，稳健性 95% 置信区间为 [-5.46,-0.10]。 

3.2 情形二：对 h 和 b 之间的关系不加限制，选择使得未考虑协变量时的政策估计效应的 MSE 最小化的方法来挑选最优窗宽，使用协变量调整的 RD 统计推断方法

```stata
. rdrobust $y $x, c(59.1968) covs($z) h(`h') b(`b')
```
其中 y 为被解释变量，x 为解释变量，c 括号中输入的为分配变量的值，z 为协变量，h 为情形一中计算 RD 点估计时使用的窗宽，b 为情形一计算 RD 偏差估计时使用的窗宽。h(`h') 和 b(`b') 这两个选项很重要，只有加了它们，才能保证情形二是在情形一的基础上进行的计算，且使用了协变量调整的RD 统计推断方法。

```stata
--------------------------------------------------------------------------------
Covariate-adjusted sharp RD estimates using local polynomial regression.
--------------------------------------------------------------------------------
 Cutoff c = 59.1968 | Left of c Right of c            Number of obs =       2779
-------------------+----------------------            BW type       =     Manual
     Number of obs |      2485         294            Kernel        = Triangular
Eff. Number of obs |       234         180            VCE method    =         NN
    Order est. (p) |         1           1
    Order bias (q) |         2           2
       BW est. (h) |     6.809       6.809
       BW bias (b) |    10.724      10.724
         rho (h/b) |     0.635       0.635

Outcome: mort_age59_related_postHS. Running variable: povrate60.
--------------------------------------------------------------------------------
            Method |   Coef.    Std. Err.    z     P>|z|    [95% Conf. Interval]
-------------------+------------------------------------------------------------
      Conventional | -2.5073     1.0973   -2.2849  0.022   -4.65808     -.356569
            Robust |     -          -     -2.3160  0.021   -5.36613     -.446789
--------------------------------------------------------------------------------
Covariate-adjusted estimates. Additional covariates included: 9
--------------------------------------------------------------------------------
```
此时政策效应大小为 -2.51，p 值为 0.021，h 值为 6.809，b 值为 10.724，稳健性 95% 置信区间为 [-5.37,-0.45]。 


3.3 情形三：对 h 和 b 之间的关系不加限制，选择使得考虑协变量时的政策估计效应的 MSE 最小化的方法来挑选最优窗宽，使用协变量调整的 RD 统计推断方法

```stata
. rdrobust $y $x, c(59.1968) covs($z)
```
其中 y，x 和 z 的意义与情形一中相同
```stata
--------------------------------------------------------------------------------
Covariate-adjusted sharp RD estimates using local polynomial regression.
--------------------------------------------------------------------------------
 Cutoff c = 59.1968 | Left of c Right of c            Number of obs =       2779
-------------------+----------------------            BW type       =      mserd
     Number of obs |      2485         294            Kernel        = Triangular
Eff. Number of obs |       240         184            VCE method    =         NN
    Order est. (p) |         1           1
    Order bias (q) |         2           2
       BW est. (h) |     6.977       6.977
       BW bias (b) |    11.636      11.636
         rho (h/b) |     0.600       0.600

Outcome: mort_age59_related_postHS. Running variable: povrate60.
--------------------------------------------------------------------------------
            Method |   Coef.    Std. Err.    z     P>|z|    [95% Conf. Interval]
-------------------+------------------------------------------------------------
      Conventional | -2.4746     1.0888   -2.2729  0.023   -4.60858     -.340674
            Robust |     -          -     -2.2586  0.024   -5.20688     -.368638
--------------------------------------------------------------------------------
Covariate-adjusted estimates. Additional covariates included: 9
--------------------------------------------------------------------------------
```
此时，政策效应大小为 -2.4746，p 值为 0.024，h 值为 6.977，b 值为 11.636，稳健性 95% 置信区间为 [-5.21,-0.37]。


3.4 情形四：限制h=b，选择使得不考虑协变量时的政策估计效应的MSE最小化的方法来挑选最优窗宽，不加入协变量，使用RD标准统计推断方法
```stata
. rdrobust $y $x, c(59.1968) rho(1)
. local h = e(h_l)
. local b = e(b_l)
```
其中 y，x，z，h和b的意义与情形一中相同，rho 括号中写 1，意思是限制 h 和 b 相同。

```stata
---------------------------------------------------------------------------------
Sharp RD estimates using local polynomial regression.
---------------------------------------------------------------------------------   
 Cutoff c = 59.1968 | Left of c Right of c            Number of obs =       2783
-------------------+----------------------            BW type       =      mserd
     Number of obs |      2489         294            Kernel        = Triangular
Eff. Number of obs |       234         180            VCE method    =         NN
    Order est. (p) |         1           1
    Order bias (q) |         2           2
       BW est. (h) |     6.809       6.809
       BW bias (b) |     6.809       6.809
         rho (h/b) |     1.000       1.000

Outcome: mort_age59_related_postHS. Running variable: povrate60.
--------------------------------------------------------------------------------
            Method |   Coef.    Std. Err.    z     P>|z|    [95% Conf. Interval]
-------------------+------------------------------------------------------------
      Conventional | -2.4102     1.2053   -1.9996  0.046   -4.77264     -.047753
            Robust |     -          -     -2.7627  0.006   -6.41372     -1.09023
--------------------------------------------------------------------------------
```
如果不加入协变量，但限制 h=b，政策效应大小的估计值为 -2.4102，p 值为 0.006，b 和 h 值均为 6.809，稳健性 95% 置信区间为 [-6.41,-1.09]。

3.5 情形五：限制 h=b，选择使得不考虑协变量时的政策估计效应的 MSE 最小化的方法来挑选最优窗宽，使用协变量调整的 RD 统计推断方法

```stata
. rdrobust $y $x, c(59.1968) covs($z) h(`h') b(`b') rho(1)
```
其中 y，x，c和z的意义与上述情形中相同，h 为情形四中计算 RD 点估计时使用的窗宽，b 为情形四计算 RD 偏差估计时使用的窗宽。h(`h') 和 b(`b')这两个选项很重要，只有加了它们，才能保证情形五是在情形四的基础上进行的计算，且使用了协变量调整的 RD 统计推断方法。

```stata
--------------------------------------------------------------------------------
Covariate-adjusted sharp RD estimates using local polynomial regression.
--------------------------------------------------------------------------------
 Cutoff c = 59.1968 | Left of c Right of c            Number of obs =       2779
-------------------+----------------------            BW type       =     Manual
     Number of obs |      2485         294            Kernel        = Triangular
Eff. Number of obs |       234         180            VCE method    =         NN
    Order est. (p) |         1           1
    Order bias (q) |         2           2
       BW est. (h) |     6.809       6.809
       BW bias (b) |     6.809       6.809
         rho (h/b) |     1.000       1.000

Outcome: mort_age59_related_postHS. Running variable: povrate60.
--------------------------------------------------------------------------------
            Method |   Coef.    Std. Err.    z     P>|z|    [95% Conf. Interval]
-------------------+------------------------------------------------------------
      Conventional | -2.5073     1.0973   -2.2849  0.022   -4.65808     -.356569
            Robust |     -          -     -3.0701  0.002   -6.63546     -1.46441
--------------------------------------------------------------------------------
Covariate-adjusted estimates. Additional covariates included: 9
--------------------------------------------------------------------------------
```
限制 h=b 后，政策效应大小的估计值为 -2.5073，p 值为 0.002，b 和 h 值均为 6.809，稳健性 95% 置信区间为 [-6.64,-1.46]。


3.6 情形六：限制 h=b，选择使得考虑协变量的政策估计效应的MSE最小化的方法来挑选最优窗宽，使用协变量调整的RD统计推断

```stata
.rdrobust $y $x, c(59.1968) covs($z) rho(1)
```
其中y，x，c，z和rho的意义与上述情形中相同
```stata
--------------------------------------------------------------------------------
Covariate-adjusted sharp RD estimates using local polynomial regression.
--------------------------------------------------------------------------------
 Cutoff c = 59.1968 | Left of c Right of c            Number of obs =       2779
-------------------+----------------------            BW type       =      mserd
     Number of obs |      2485         294            Kernel        = Triangular
Eff. Number of obs |       240         184            VCE method    =         NN
    Order est. (p) |         1           1
    Order bias (q) |         2           2
       BW est. (h) |     6.977       6.977
       BW bias (b) |     6.977       6.977
         rho (h/b) |     1.000       1.000

Outcome: mort_age59_related_postHS. Running variable: povrate60.
--------------------------------------------------------------------------------
            Method |   Coef.    Std. Err.    z     P>|z|    [95% Conf. Interval]
-------------------+------------------------------------------------------------
      Conventional | -2.4746     1.0888   -2.2729  0.023   -4.60858     -.340674
            Robust |     -          -     -3.0182  0.003    -6.5421     -1.39074
--------------------------------------------------------------------------------
Covariate-adjusted estimates. Additional covariates included: 9
--------------------------------------------------------------------------------

```
此时政策效应大小的估计值为 -2.4746，p 值为 0.003，b 和 h 值均为 6.977，稳健性 95% 置信区间为 [-6.54,-1.39]。


## 四. 结语
Calonico et al. （2019）提出了考虑协变量后的断点回归的非参数局部多项式回归的统计量估计和统计推断，在 stata 中可以使用 rdrobust 命令或在R中安装 “rdrobust” 包后可以方便快捷的调用，“rdrobust” 包下载地址为 “https://sites.google.com/site/rdpackages/”。这一方法对协变量进入模型的方式不加任何限制，研究者可以根据自行需要灵活使用。


## 参考文献
Calonico S, Cattaneo M D, Farrell M H, et al. Regression discontinuity designs using covariates. Review of Economics and Statistics, 2019, 101(3): 442-451.
Ludwig J, Miller D L. Does Head Start improve children's life chances? Evidence from a regression discontinuity design. The Quarterly Journal of Economics, 2007, 122(1): 159-208.




>#### 关于我们

- 【**Stata 连享会(公众号：StataChina)**】由中山大学连玉君老师团队创办，旨在定期与大家分享 Stata 应用的各种经验和技巧。
- 公众号推文同步发布于 [CSDN-Stata连享会](https://blog.csdn.net/arlionn) 、[简书-Stata连享会](http://www.jianshu.com/u/69a30474ef33) 和 [知乎-连玉君Stata专栏](https://www.zhihu.com/people/arlionn)。可以在上述网站中搜索关键词`Stata`或`Stata连享会`后关注我们。
- 点击推文底部【阅读原文】可以查看推文中的链接并下载相关资料。
- Stata连享会 [计量专题](https://gitee.com/arlionn/stata_training/blob/master/README.md)  || [精彩推文](https://github.com/arlionn/stata/blob/master/README.md)

>#### 联系我们

- **欢迎赐稿：** 欢迎将您的文章或笔记投稿至 StataChina@163.com，我们会保留您的署名；录用稿件达`五篇`以上，即可**免费**获得 Stata 现场培训 (初级或高级选其一) 资格。您也可以从 [连享会选题平台](https://gitee.com/Stata002/StataSX2018/wikis/Home) &rarr; [002_备选主题] 中选择感兴趣的题目来撰写推文，网址为：[https://gitee.com/Stata002/StataSX2018/wikis/Home](https://gitee.com/Stata002/StataSX2018/wikis/Home)。
- **招募英才：** 欢迎加入我们的团队，一起学习 Stata。合作编辑或撰写稿件五篇以上，即可**免费**获得 Stata 现场培训 (初级或高级选其一) 资格。
- **联系邮件：** StataChina@163.com

>#### 往期精彩推文
> Stata连享会 &emsp; [计量专题](https://gitee.com/arlionn/Course/blob/master/README.md)  || [精品课程](https://mp.weixin.qq.com/s/hWtncj56PeFNL4yg2-va0Q) || [简书推文](https://www.jianshu.com/p/de82fdc2c18a)  || [公众号合集](https://mp.weixin.qq.com/s/RwkuPpLS7bI5C5OhRqjkOw) 

[![点击查看完整推文列表](https://images.gitee.com/uploads/images/2019/0824/112542_4f81948e_1522177.png)](https://gitee.com/arlionn/Course/blob/master/README.md)







---
![欢迎加入Stata连享会(公众号: StataChina)](https://images.gitee.com/uploads/images/2019/0824/112542_8928c91d_1522177.jpeg "扫码关注 Stata 连享会")
