
> &emsp;          
> 作者：李鑫   (云南大学)      
> &emsp;          
> Stata 连享会： [知乎](https://zhuanlan.zhihu.com/arlion) | [简书](http://www.jianshu.com/u/69a30474ef33) | [码云](https://gitee.com/arlionn) | [CSDN](https://blog.csdn.net/arlionn)
> &emsp;  

[![](https://images.gitee.com/uploads/images/2019/0824/112542_5ea01a9b_1522177.png)](https://mp.weixin.qq.com/s/aU6B9HZaf2BSHF7lSLe6LA)


> Stata连享会 [计量专题](https://gitee.com/arlionn/Course/blob/master/README.md)  || [精品课程](https://active.clewm.net/B9EOq8?qrurl=https://c3.clewm.net/B9EOq8&gtype=1&key=4cedd150575bd900c651021e9795edbac41ce34075) || [推文集锦](https://www.jianshu.com/p/de82fdc2c18a) 

[![点击查看完整推文列表](https://images.gitee.com/uploads/images/2019/0824/112542_4c8caecb_1522177.png)](https://mp.weixin.qq.com/s/RwkuPpLS7bI5C5OhRqjkOw)



> 连享会 RDD 专题文章：
- [RDD: 断点回归的非参数估计及Stata实现](http://www.jianshu.com/p/2aa62edc148b)
- [Stata: 断点回归分析 (RDD) 文献和命令](http://www.jianshu.com/p/405eb1dff559)
- [Stata: 两本断点回归分析 (RDD) 易懂教程](http://www.jianshu.com/p/538ed1805004)
- [Abadie 新作: 简明 IV, DID, RDD 教程和综述](http://www.jianshu.com/p/2849f03a3843)


&emsp;


## 1. 背景

断点回归 ( **RDD** ) 已经成为当前微观计量经济学分析政策效果的重要手段，而样本的随机分配被认为是该项工作的黄金律。因为只有样本在临界值 ( cut-point ) 的领域范围内是随机分布的，那么模型估计在临界值处的平均因果效应 ( ATE ) 才是政策实施对于处理组和控制组之间的差异的无偏估计值。

然而，在现实经验研究中，样本的随机分配往往难以得到满足，经常出现样本分配机制人为干预现象。

例如，学校开展对于期末考试分数低于某一值的学生进行强制的暑期培训，如果学生们提前知道这个分数值 ( 如 60 分 )，那么原本考试成绩在这个分数值附近的学生就会加倍努力或与老师套近乎 (寻租) 而通过考试，避免参加暑期培训项目。这意味着通过考试的学生数量会在断点 ( 60 分 ) 右侧有一个数量上的明显上升，致使评价参加暑期培训是否能够提高学生的考试成绩的平均因果效应 ( ATE ) 存在偏差。

下面，我们通过模拟数据的形式来展现样本在临界值处有人为干预的现象。

[![Figure1：RDD - 图示有 (无) 人为干预的情形](https://images.gitee.com/uploads/images/2019/0824/112542_e6d000e2_1522177.png "Figure1：RDD - 图示有 (无) 人为干预的情形")](https://mp.weixin.qq.com/s/RwkuPpLS7bI5C5OhRqjkOw)


在 figure1(a) 中，样本分配不存在人为干预，即学生事前并不知道低于 60 分将参加学校组织的暑期培训班项目，那么在分配变量 (考试分数) 的直方图中不存在明显的样本数量在临界值 (60 分) 处任意一侧样本数量的出现巨大的波动。

在 figure1(b) 中，我们通过人为改变样本数，模拟样本的认为干预现象，即学生事前知道低
于 60 分将必须参加学校组织的暑期培训项目，那么分配变量 （考试分数） 的直方图中在临界值 (60 分) 右侧将出现较高的学生数，而在临界值 (60 分) 左侧的学生数大幅度下降，出现明显的跳跃现象。


## 2. 检验工具

因此，在断点回归中，检验样本是否随机分配，即分配变量密度函数的连续性，将有助于判断政策效应估计的有效性。

下面，我们通过使用 David S. Lee (2007，[[PDF]](https://escholarship.org/content/qt6nm6j3zv/qt6nm6j3zv.pdf)) 关于参议院选举的样本数据，分析三种不同的工具检验样本是否存在人为干预的现象。其中，该数据中民主党获胜的票数差 **margin** 为分配变量，临界值为 **margin=0**。

> Note: 后面分析需要使用 `rdrobust` 命令对应的相关程序和数据，请执行如下两条命令下载外部命令 (默认存储于 **..\ado\plus** 文件夹下) 和相关数据 (默认存储于当前工作路径下，下载完成后输入 `cd` 可以查看当前路径地址；输入 `dir` 可以查看当前工作路径下的文件)： 

```stata
. net install st0366_1.pkg, replace //rdrobust外部命令
. net get     st0366_1.pkg, replace //rdrobust相关数据
```

### 2.1 使用 histogram 命令

最简单直接的方法便是使用 `histogram` 命令绘制分配变量的直方图，以图示观测断点处的频数变动和断点两侧的变动情况。  
- **优点：** 其操作简单、直观。  
- **缺点：** 直方图在不同组间距中的样本数量的不同，使得很难直观上观测到连续性。

```stata
*----------使用直方图显示分配变量的连续性 figure2 ------
*-使用 David S.Lee (2007) 参议院选举的数据
 use "https://gitee.com/arlionn/data/raw/master/data01/rdrobust_rdsenate.dta", clear
 *use "rdrobust_senate.dta", clear  
 #d ;
  histogram margin, 
    lcolor(brown) fcolor(gs16) 
    title("Senate_selection")  
    xtitle("margin") 
    note("figure2");
 #d cr
```

![Senate_figure2](https://images.gitee.com/uploads/images/2019/0824/112542_fae9fc23_1522177.png "figure6.png")

从 figure2 中可以看出，分配变量 (民主党获胜票数差) 在临界值 (**margin**=0) 处两侧没有明显的数量波动，表明分配变量密度函数在临界值处是连续函数。

### 2.2 **使用 McGrary 方法**

 `DCdensity` 命令是由 **McCrary** (2006) 提出来用于检验分配变量在临界值处是否连续，即样本在临界值处是否存在人为干预现象。主要方法如下：
-  (1) 生成分配变量的密度直方图，确保临界值不会被直方图所覆盖；
-  (2) 在临界值左右两侧，分别进行局部的线性回归。其中因变量是组内频数，自变量是组间距的中间值；
-  (3) 检验临界值两侧的拟合值的差的对数值是否显著不为零。

**优点：**
- 该命令相对于绘制直方图，绘制出较为平滑的核密度曲线图以及置信区间，能够直观的现实分配变量在断点处的连续性。

**缺点：** 
- (1) 该命令所使用的核密度函数的带宽 (**bandwith**) 并不是使用交互验证或者是插入法确定的最优带宽选择，而是由其编写的默认公式所确定，因此在整体断点回归中使用不同带宽无法有效地证明其分配变量的连续性。
- (2) 如果存在某种干预模式使得高于临界值点和低于临界值点处的样本数量相同，那么该命令无法有效识别该类分配变量的跳跃。
- (3) 图形无法进行注释修饰。

 **McGrary** (2006) 分配变量密度函数连续检验的基本命令为 `DCdensity` , 其基本语法格式如下：

```stata
 DCdensity varlist [if] [in] [weight] [, options]
```

其中，主要选项如下

-  `varlist`: 分配变量
-  `breakpoint()`: 分配变量临界值处
-  `b()`： 指定特定带宽值
-  `generate()`: 生成新的参数变量
-  `graphname()`: 保存图形  

举例

```stata
*----------使用 McGray (2006) 命令 figure3 ----------------
. DCdensity margin, breakpoint(0) gen(Xj Yj r0 fhat se_fhat)
  Discontinuity estimate (log difference in height): -.100745626
                                                   (.117145041)    
. return list
  scalars:
          r(bandwidth) =  25.84938346120713
            r(binsize) =  1.841330210610218
                 r(se) =  .117145040900551
              r(theta) =  -.1007456257891786
```

![DCdensity_figure3](https://images.gitee.com/uploads/images/2019/0824/112542_cf562046_1522177.png "figure3.png")

从 figure3 中可以看到，尽管在临界值两侧的密度函数存在跳跃，但置信区间在此处重叠，表明在临界值两侧的分配变量 (民主党获胜票数差) 密度函数是连续函数。并且临界值处估计的对数样本数差 [*r(theta)*] 不显著，证明不存分配变量 (民主党获胜票数差) 的人为干预。

#### 2.3 使用 rdcont 命令

相对于前文介绍的命令，`rdcont` 能够有效地克服由于断点处局部小样本的因素所导致对于密度函数连续性检验的影响。 `rdcont` 通过构造一个 g 阶的统计量来实现检验，其易于实现，较其他方法使用能够在更弱的条件下渐近有效，在比其渐近有效性所需的条件更强的条件下显示出有效样本。

**优点：** (1) 操作简单，对于样本数量没有限制；(2) 在估计过程中，不涉及核密度函数、局部多项式、偏差修正、密度函数最优带宽的选择等问题。
     
 `rdcont` 命令的基本语法格式如下：

```stata
 rdcont running_var [if] [in], [options]
```

其中：

- `running_var`: 分配变量
- `alpha()`: 指定用于计算最佳带宽的临界值
- `threshold()`: 指定测试的临界值
- `qband()`: 指定特定的带宽值

**Stata 范例：**

```stata
*----------使用 rdcont 命令 table1 ----------------------
. rdcont margin, threshold(0)
RDD non-randomized approximate sign test
Running variable: margin
Cutoff c =       0 | Left of c  Right of c    Number of obverse = 1390
-------------------+----------------------                    q =   94
     Number of obs |       640         750
Eff. number of obs |        46          48
 Eff. neighborhood |    -1.743       1.824
-------------------+----------------------
           p-value |     0.918
```


> 原假设 $H_0$：分配变量密度函数在临界值处是连续函数。

如上所示，断点回归在临界值处的样本非随机检验的 p-value 为 0.918 ，不能拒绝原假设，表明分配变量 (民主党获胜票数差) 密度函数在临界值处是连续函数，不存在人为干预。

&emsp;

## 3. 结语

这篇推文主要介绍了如何在 **RDD** 实证分析中进行平滑性检验，主要介绍了三种不同的检验方法。三种方法各有利弊，因此，在实际操作中分别使用进而实现相互验证。

## 4. 参考资料

(1) Lee D S. Randomized experiments from non-random selection in US House elections[J]. Journal of Econometrics, 2008, 142(2): 675-697.[[pdf]](https://escholarship.org/content/qt6nm6j3zv/qt6nm6j3zv.pdf)

(2) McCrary J. Manipulation of the running variable in the regression discontinuity design: A density test[J]. Journal of econometrics, 2008, 142(2): 698-714.[[pdf]](https://www.nber.org/papers/t0334.pdf)

(3) Bugni F A, Canay I A. Testing Continuity of a Density via g-order statistics in the Regression Discontinuity Design[J]. arXiv preprint arXiv:1803.07951, 2018.[[pdf]](https://arxiv.org/pdf/1803.07951.pdf)


&emsp;

## 附：本文涉及的 Stata 代码

```stata
*-------驱动变量不连续图形figure1--------
*----生成模拟数据---
 clear
 set obs 5000
 set seed 123
 gen z = rnormal()*0.5       
 save simu1.dta, replace
 
*----生成干预数据----   
 gen z0 = z if (z>=-0.15)&(z<0)
 gen z1 = z if (z>=0)&(z<0.15)    
 gen z2 = z
 replace z2 = . if z2 == z0
 stack z2 z1, into(z3) clear
 drop if z3 == . 
                       
 *-stack命令会将z变量删除，该命令将z变量添加进来
 merge 1:1 _n using simu1.dta, nogenerate
 drop if z3 == .

*-为了图形显示更加直观，进行轻微调整
 gen zNo  = z3 + 0.1
 gen zYes =  z - 0.1 	 
 
 #d ;
  twoway (histogram zYes,
	    frequency lcolor(gs12) fcolor(gs12)) 
	 (histogram zNo, 
	    frequency lcolor(black) fcolor(none)),
	  xline(0, lpattern(dash) lcolor(green) lwidth(*1.5))
	  xtitle("Score")
	  xlabel(-2 "40" -1 "50" 0 "60" 1 "80" 2 "90")
	  legend(label(1 "figure1(a) 无人为干预")
	         label(2 "figure1(b) 人为干预"))
	  note("figure1")
	  scheme(burd);
 #d cr
 
*----------使用直方图显示分配变量的连续性figure2------
 * use "rdrobust_senate.dta", clear
 use https://gitee.com/arlionn/data/raw/master/data01/rdrobust_rdsenate.dta, clear
 #d ;
  histogram margin, 
     lcolor(brown) fcolor(gs16) 
     title("Senate_selection")  
  	xtitle("margin") 
 	note("figure2");
 #d cr
 
*----------使用McGray(2006)命令figure3----------------
 DCdensity margin, breakpoint(0) gen(Xj Yj r0 fhat se_fhat)
 return list
	
*----------使用rdcont命令table1----------------------
 rdcont margin, threshold(0)
```

&emsp;
> #### [连享会计量方法专题……](https://gitee.com/arlionn/Course/blob/master/README.md)

&emsp;



>#### 关于我们

- 【**Stata 连享会(公众号：StataChina)**】由中山大学连玉君老师团队创办，旨在定期与大家分享 Stata 应用的各种经验和技巧。
- 公众号推文同步发布于 [CSDN-Stata连享会](https://blog.csdn.net/arlionn) 、[简书-Stata连享会](http://www.jianshu.com/u/69a30474ef33) 和 [知乎-连玉君Stata专栏](https://www.zhihu.com/people/arlionn)。可以在上述网站中搜索关键词`Stata`或`Stata连享会`后关注我们。
- 点击推文底部【阅读原文】可以查看推文中的链接并下载相关资料。
- Stata连享会 [计量专题](https://gitee.com/arlionn/stata_training/blob/master/README.md)  || [精彩推文](https://github.com/arlionn/stata/blob/master/README.md)

>#### 联系我们

- **欢迎赐稿：** 欢迎将您的文章或笔记投稿至 StataChina@163.com，我们会保留您的署名；录用稿件达`五篇`以上，即可**免费**获得 Stata 现场培训 (初级或高级选其一) 资格。您也可以从 [连享会选题平台](https://gitee.com/Stata002/StataSX2018/wikis/Home) &rarr; [002_备选主题] 中选择感兴趣的题目来撰写推文，网址为：[https://gitee.com/Stata002/StataSX2018/wikis/Home](https://gitee.com/Stata002/StataSX2018/wikis/Home)。
- **招募英才：** 欢迎加入我们的团队，一起学习 Stata。合作编辑或撰写稿件五篇以上，即可**免费**获得 Stata 现场培训 (初级或高级选其一) 资格。
- **联系邮件：** StataChina@163.com

>#### 往期精彩推文
> Stata连享会 &emsp; [计量专题](https://gitee.com/arlionn/Course/blob/master/README.md)  || [精品课程](https://mp.weixin.qq.com/s/hWtncj56PeFNL4yg2-va0Q) || [简书推文](https://www.jianshu.com/p/de82fdc2c18a)  || [公众号合集](https://mp.weixin.qq.com/s/RwkuPpLS7bI5C5OhRqjkOw) 

[![点击查看完整推文列表](https://images.gitee.com/uploads/images/2019/0824/112542_4f81948e_1522177.png)](https://gitee.com/arlionn/Course/blob/master/README.md)







---
![欢迎加入Stata连享会(公众号: StataChina)](https://images.gitee.com/uploads/images/2019/0824/112542_8928c91d_1522177.jpeg "扫码关注 Stata 连享会")



