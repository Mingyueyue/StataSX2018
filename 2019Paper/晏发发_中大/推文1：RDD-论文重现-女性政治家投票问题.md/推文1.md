 &emsp;

> 作者：晏发发 (中山大学)，yanff@mail2.sysu.edu.cn    

> &emsp;         

> Stata 连享会： [知乎](https://zhuanlan.zhihu.com/arlion) | [简书](http://www.jianshu.com/u/69a30474ef33) | [码云](https://gitee.com/arlionn) | [CSDN](https://blog.csdn.net/arlionn)

> &emsp; 

[![](https://images.gitee.com/uploads/images/2019/0824/112542_5ea01a9b_1522177.png)](https://mp.weixin.qq.com/s/aU6B9HZaf2BSHF7lSLe6LA)

> Stata连享会 [计量专题](https://gitee.com/arlionn/Course/blob/master/README.md)  || [精品课程](https://active.clewm.net/B9EOq8?qrurl=https://c3.clewm.net/B9EOq8&gtype=1&key=4cedd150575bd900c651021e9795edbac41ce34075) || [推文集锦](https://www.jianshu.com/p/de82fdc2c18a)

[![点击查看完整推文列表](https://images.gitee.com/uploads/images/2019/0824/112542_4c8caecb_1522177.png)](https://mp.weixin.qq.com/s/RwkuPpLS7bI5C5OhRqjkOw)

> 连享会 RDD 专题文章：

- [RDD: 断点回归的非参数估计及Stata实现](http://www.jianshu.com/p/2aa62edc148b)

- [Stata: 断点回归分析 (RDD) 文献和命令](http://www.jianshu.com/p/405eb1dff559)

- [Stata: 两本断点回归分析 (RDD) 易懂教程](http://www.jianshu.com/p/538ed1805004)

- [Abadie 新作: 简明 IV, DID, RDD 教程和综述](http://www.jianshu.com/p/2849f03a3843)

&emsp;

**Do female politicians empower women to vote or run for office?A regression discontinuity approach**

## 1. 核心思想

**•评估关于女性候选人鼓舞潜力的长期理论争议。**

**•采用断点回归设计来做出令人信服的因果关系。**

**•与普遍预期相反，女性公职人员对美国的女性选民投票率或其他女性候选人资格没有影响。**

**•与印度的调查结果形成对比，表明存在强大的影响。**

本文的核心问题是识别女性政治家鼓舞对女性投票和竞选公职的因果效应。尽管鼓舞对女性担任公职的影响非常重要，但学术一直存在争论，很少研究能够有说服力地识别出美国社会中女性的政治参选或者担任公职对大众和精英政治参与的因果效应。在实证上的挑战主要是因为**数据限制**和**选择偏误**。由于学者尚未完全理解女性在其所在地选举的原因，因此要令人信服地分析出女性在一个地区的候选资格或选举与感兴趣的结果之间的联系（例如，Healy，2013）比较困难是因为我们不能确定是否已经控制了所有可能的混淆因素。尽管学者们已经找到了实验证据证明，在特定案例研究和实验中，女性政治家改变了对女性候选人资格的看法（MacManus，1981；Gitelson和Gitelson，1981），但识别这些纵横交错的现象之间的因果关系的存在困难，这限制了能够产生强烈因果关系的研究的发展。支持这些方法论问题，最近的研究重新验证选举非洲裔美国人对地方和州议院的影响，发现严格的观察设计可能会夸大描述性代表性对参与的因果效应（Hopkins and McCabe，2013；Henderson et al.,2013）。

尽管美国没有随机预约系统可以直接识别女性服务和候选人资格的因果效应（在印度可能），但可以采用准实验方法：断点回归设计。断点回归设计在政治科学和经济学中的使用越来越普遍，并已应用于识别传统方法难以解决的各种情况下的因果效应（例如，Lee，2008，Trounstine，2011；Caughey和Sekhon，2011）。有关RDD的更多信息以及有关其性质的证明，请参阅Lee（2008）。

使用女性在与男性同台选举竞争中获得投票的50%作为断点来识别女性胜利的因果效应。即，比较女性要么在与男性选举中“刚刚赢了”，要么“刚刚失利”的地区和附近的群众和精英参与的后续模式来检验其他女性是否更可能（1）在随后的选举中投票给刚刚赢得了之前选举的女性（群众鼓舞假设）或者（2）在女性刚赢得前一次选举的附近地区参加竞选或硬的公职竞选。

本文识别了两种因果效应：**女性政治家的任期和由此产生的女性候选人资格竞选的可能性**。

为了说明断点回归如何识别女性担任公职的影响，Figure 1总结了如何发现“打破隐形障碍”的现象，即一名女性的胜利使其他女性更有可能在她所在区域的其他地区竞选公职。

下面，我们先介绍一下女性参选人的数量或者女性参选人提名的可能性。


[![Fig. 1. Hypothesized “breaking of the glass ceiling” pattern](https://images.gitee.com/uploads/images/2019/0921/104338_87d9df2a_1963713.jpeg "Fig. 1. Hypothesized “breaking of the glass ceiling” pattern")](https://mp.weixin.qq.com/s/RwkuPpLS7bI5C5OhRqjkOw)

Fig. 1. Hypothesized “breaking of the glass ceiling” pattern

在Figure 1中，横轴是指在第1阶段A地区女性候选人在与男性角逐竞选中的得到的投票。纵轴是指在第2阶段A地区附近的其他地区女性候选人参加公职竞选或者胜出的人数。

如Figure 1所示，垂直虚线的左边是第1阶段的选举中，男性赢得了所有选举，这与女性两名候选人的得票率低于50%的地区相对应；在两名候选人的得票率高于50%的地区，以及在垂直虚线的右边是在第1阶段选举中，女性赢得了所有选举。尽管女性选举往往得胜或失败的地区是内生的，但在断点处，混淆因素不太可能存在系统性差异。这意味着，女性的胜出可以被认为是准随机分配的(Lee，2008年)，因此，在A地区选举一名妇女可归因于跨断点的后续模式的差异。也就是说，在断点处的因变量中的任何‘跳跃’都可以归因于跨断点的效应(即A地区的女性获胜)，因为在女性碰巧“刚刚获胜”或“刚刚输掉”的地区之间不应该存在系统性差异。

总之，该设计比较了两组地区—— 一组是在下一次选举时女性都担任公职的地区（ 因此，通常也是在投票中），另一组是在下一次选举时男性担任公职的地区——这些地区被准随机分配到这些情况下 (Lee ，2008 年) 。 因此，所有可能被认为与女性的政治参与相关的因素自然保持不变，不需要额外的控制变量(见 Green 等人（2009），以验证 RDDs 的这种准实验性质) 。

除了能够分离出女性候选人资格和担任公职的因果影响外，断点回归方法还具有一个特点，即它具体地识别出女性胜出的效应(即那些男性几乎获胜的竞选)。虽然在许多情况下，断点回归方法的确存在一个局限，学者们传统上认为，正是在这些最近期的选举中，女性描述性代表的鼓舞作用最大(如Burns等，2001)。首先，选民和精英政治观察员通常最为密切地关注这些选举；因此，Atkeson (2003) 认为，在竞选中，女性候选人的存在与增加女性政治参与率密切相关。因此，人们预期考虑竞选公职的精英和女性可能也会对女性赢得这类选举的能力收集最多的信息，而且由于更多的认知参与、媒体报道、竞选交流等因素，人们预期选民最熟悉谁在竞选(例如，Kam和Utych，2011)。那些关注女性在最近期的选举中所展示出来的能力的精英们也可能通过观察女性在这些竞选中的表现来收集最多的信息(如Martin和Peskowitz，2013)。总之，尽管RD方法的通用性仅限于较小的一组案例，但正是在这些情况下，现有的理论工作通常预测效果最有可能实现。

因此，凭借丰富的数据、在最小假设条件下识别因果效应的能力，以及对极具竞争性和极具吸引力的选举的独特别注，该设计有很好的机会识别女性描述性代表的鼓舞效应，以及其他女性在美国州议会选举中的大众政治参与或竞选公职的候选资格。此外，女性很可能知道她们的州议员是谁，就像她们是国会议员一样(Burns等人，2001，第102页)，这意味着州一级的统计精度也具有较强的外部有效性。选举结果当然不能清晰地说明在其他各级政府或其他选举中选举女性的效应，但对女性选民来说，最近期的议会选举要比人们想象的要突出得多。

## 2. 模型设定

### 2.1 数据来源

#### 2.1.1 女候选人

罗格斯大学的美国妇女和政治中心（the Center on American Women and Politics (CAWP) at Rutgers University，CAWP）自1999年以来收集并提供州议院所有女性候选人的数据。为了在一名女性或一名男性可能赢得公职的样本中进行断点回归，首先剔除了一名女性与另一名女性竞争或一名女性无人反对的情况下的所有情形。还将这些数据限制在偶数年份的竞选中，因为女性选民奇数年参加选举的投票数据并不容易获得。最后剔除了所有有多个成员区的议院，因为在多候选人竞选中设定断点回归模型并不容易。这产生了3813个州议会选举的数据集，其中一名女性在2002年、2004年、2006年和2008年与一名男性竞争（2000年和2010年因2000/2010和2002/2012之间的重新划分而被排除在外。）。

#### 2.1.2 选举结果

将这些数据与从州议会网站收集的州议会选举结果据相匹配。由此产生的数据集刻画了3813个种族中女性候选人和男性候选人所获得的得票额。

#### 2.1.3 因变量I：女性在附近地区的候选资格

为了评估一个地区女性选举对附近地区随后选举中女性候选资格的影响，将选举结果和CAWP数据与美国人口普查中描述州议会地区地理位置和边界的数据相匹配。这些地理边界文件能够识别3813个主要区域中的每一个区域，因此精英和潜在的女性候选人可以了解女性的选举胜利，并最终见证其担任公职。

为了衡量哪些地区在其他区域“附近”，哪些地区在其他地区“内部”，以及精英和女性本身可能合理地观察并与女性公职人员互动的地方，首先创建一个数据集，描述美国的6652个州议会地区中的每两个地区之间的距离（大约66522 / 2, 200万个不同的地区对）。计算两个二维对象之间距离的方法有两种，本文选择用比较常用并且简单的方法，就是计算质心（即几何“中心”或“重心”）之间距离。为了说明哪些州议会地区在加利福尼亚州的旧金山地区50英里范围内，Figure 2 列举了一个例子 。在图中，旧金山的质点用深黑色圆点表示，灰色的大圆圈对应于距该质点50英里范围内的区域。其他十个州首府的质点落在该区域内，对应于阴影区域内十个灰色圆点，因此将它们视为在旧金山“50英里以内”。在阴影区域以外的其它质点因此被视为在旧金山“50英里范围外”。操作由ArcGIS实现。

[![Fig. 2. District distance measurement procedure example](https://images.gitee.com/uploads/images/2019/0921/104337_7b42937c_1963713.jpeg "Fig. 2. District distance measurement procedure example")](https://mp.weixin.qq.com/s/RwkuPpLS7bI5C5OhRqjkOw)

Fig. 2. District distance measurement procedure example

为了做溢出分析，构建了一个最终的因变量，将这些相关但是距离数据与2002年以来的州议会选举数据集以及CAWP的女性候选人数据匹配，产生一个示性变量，即2002年起的州议会选举中女性是候选人还是胜出者。然后计算竞选的总数和每个地区任意距离内的其他地区里的选举中的女性候选人和获胜者的总人数，从而计算出最终的因变量。（见下部一小节。）

#### 2.1.4 因变量II：女性选民的比例

为了测量女性的投票行为，从选民数据清洗中心Catalist购买了数据。Catalist提供了有关大选中投票总数的数据，以及2000年至2010年期间每个偶数选举年度女性从其全面的美国选民档案中投票的数量。这个数据集在每次选举中产生了两个有意思的变量：将女性的投票数除以投票的总数，得出女性在选民中所占的比例；并将女性的投票数除以女性总人口数，得到女性选民的比例。
$$
女性在选民中所占的比例 = \frac{女性的投票数}{投票的总数}
$$

$$
女性选民的比例 = \frac{女性的投票数}{投票的总数}
$$

### 2.2 RD模型设定

采用标准的方法设定断点回归，先估计断点两侧的函数，然后估计这两个函数在断点处的差异（Lee,2008）。一般是断点两侧是采用采用高阶多项式或者局部线性回归（Imebens and Lemiewx,2008）。本文报告了两种方法的回归结果。

模型假设一名女性在一个地区参加选举会导致这个地区的女性参与投票，模型设定如下：

$$
\frac{\Sigma Female Voters_i }{\Sigma Women_i} = \alpha + \beta_1V_i + \beta_2V_i^2 + \beta_3V_i^3 + \beta_4V_i^4 + \gamma F_i  + \beta_5V_iF_i + \beta_6V_i^2F_i + \beta_7V_i^3F_i  + \beta_8V_i^4F_i + \varepsilon _i     (1)
$$
其中，方程左边是在第2阶段（第2阶段定义为地区i发生下一次竞选的时间）i地区中女性选民的比例；$\alpha$为常数项；$V_i$ 是在第1阶段i 地区中女性的两位候选人投票比例；$F_i$ 是哑变量，如果$V_i >\frac{1}{2}$,$F_i =1$（意味着女性在第1阶段i地区赢得竞选），否则$F_i =0$；$\varepsilon_i$是扰动项；$\gamma$是感兴趣的变量的回归系数，捕捉女性在上次竞选中获胜对因变量的因果效应。用多项式模型采用稳健标准误和Imbens和Lemieux(2008)标准误运算公式。

##### 2.2.1 女性赢得上次竞选后是否会更加频繁地参与竞选

正如一些文献将女性描述性代表的影响归因于女性在竞选中的候选资格，而不是女性实际担任公职，本文首先研究女性在第1阶段是否赢得竞选对第2阶段是否是候选人有很强的影响。这样的话，这个断点也可以看成是女性在下一期是否参选的外生的工具变量（Brookman,2009）：因为大部分候选人若是赢得了竞选很可能参加再次竞选，女性（外生地）更有可能在当第1阶段有女性胜出后，在第2阶段参加竞选。Table 1的第1列和第2列以及Figure 3中的panel（C）表明当女性刚赢得选举时，女性在同一地区的后续选举中成为候选人的可能性大约是两倍（即，在大多数情况下，同一名女性竞选连任）。因此，我估计出的效应可以反映出女性任期的综合效应，从而大大增加了候选资格的可能性（前者的增加直接导致了后者的增加）。

[![Table 1.  RD estimate of electing a woman on women’s subsequent political participation.](https://images.gitee.com/uploads/images/2019/0921/104337_84de7ba2_1963713.png "Table 1.  RD estimate of electing a woman on women’s subsequent political participation.")](https://mp.weixin.qq.com/s/RwkuPpLS7bI5C5OhRqjkOw)

Table 1.  RD estimate of electing a woman on women’s subsequent political participation.


[![Fig. 3. Effect of women's victories on women's voter turnout.](https://images.gitee.com/uploads/images/2019/0921/104338_8f42a553_1963713.png "Fig. 3. Effect of women's victories on women's voter turnout.")](https://mp.weixin.qq.com/s/RwkuPpLS7bI5C5OhRqjkOw)

Fig. 3. Effect of women's victories on women's voter turnout. (For interpretation of the references to colour in this figure legend, the reader is referred to the web version of this article.)

```stata
clear
use "main data.dta"
gen midterm = next_cycle_year == 2002 | next_cycle_year == 2006 | next_cycle_year == 2010
*Generate StateXYear FEs
tostring next_cycle_year, gen(next_cycle_year_string)
gen stateyear = State + next_cycle_year_string
encode stateyear, gen(stateyear_fes)
xtset stateyear_fes
*##### 2.2.1 女性赢得上次竞选后是否会更加频繁地参与竞选，见Table1第1、2列
*论文3.2节Table 1的第1列
*Woman on ballot next year
local bws 0.05 0.1 0.15 0.2 0.25
foreach bandwidth in `bws'{
       reg femaleonballotnextyear womanwon - winXfem_v_c_p_4 if absolute_margin < `bandwidth', robust
}
*论文3.2节Table 1的第2列
local bws 0.05 0.1 0.15 0.2 0.25
foreach bandwidth in `bws'{
       xtreg femaleonballotnextyear womanwon - winXfem_v_c_p_4 if absolute_margin < `bandwidth', fe
}
```

#### 2.2.2 女性候选人和政治家是否动员了女性投票

Table 1 的第3列和第4列列出了在下一次选举中选举一个地区的女性对该地区女性选民的比例的影响的估计数（即P（投票\vert 女性）），最后两列估计女性胜出对下一次选举中女性选民的比例的影响（P（女性|投票））。在每种情况下，粗体线代表方程（1）中\gamma 的估计，即女性选举对因变量的因果效应。尽管在所有情况下本文使用了与Lee（2008）一致的四阶多项式，但是为了节约篇幅，多项式的回归系数（方程（1）中的\beta ）没有报告出来。常数项描述了左侧限制的因变量的平均值。回想一下，样本量小于完整数据集，因为该处理仅限于在断点的15个百分点内的观测值；附录中的表A1中显示了更小和更大带宽的类似结果。

Table 1中的第3列和第4列显示，在美国州议会选举中，女性胜出对女性选民的投票率的影响在统计上为零。第4列包括州 - 年固定效应，为女性选民的投票率的估计效果提供更准确的上限。

采用固定效应，该模型结果表明在95％的置信水平下，女性选举对后续选举中女性选民的比例的影响很可能为零，最多不超过2.8个百分点。Imbens和Lemieux（2008）估计（比多项式更有效，因此具有较小的标准误差；参见Table A1）同样估计在95％的置信水平下，女性选举对女性选民的比例的影响不会上升到0.9个百分点。为了将后一种估计置于实际背景中，女性对女性选民的比例的动员效果似乎最多等于，甚至小于收到一封关于“公民义务”GOTV简约明信片的邮件的动员效应（约1个百分点）（Gerber和Green，2000）。这个上限也比被要求面对面投票或被发送一封“社会压力”邮件的估计效果要小一个数量级（Gerber和Green，2000，Gerber等，2008）。总之，结果表明，在美国女性选民的投票比例上，由女州议员或女性州议会候选人所代表的部分的影响在统计上和实质上均为零。

Figure3中的panel（a）以图形方式显示这些结果，以0.5-百分点宽的组距分组，并且在Table 1的第3列中加入估计的模型。X轴是指在选举1中的两个候选人时女性投票中的比例，Y轴是选举2中女性选民的投票比例。深色线显示模型在断点两侧的拟合值。垂直的红线将第1阶段中女性刚刚选举获胜和女性刚刚失败的样本分开。如果选举女性对其他女性的政治参与产生很大的因果影响，那么对垂直红线右侧的样本而言，女性选民的投票比率的应该会增加。从原始数据可以清楚地看出，女性刚刚选举胜出而非刚刚选举失败的地区女性选民的投票比例没有显着增加。

```stata
*#### 2.2.2 女性候选人和政治家是否动员了女性投票，见Table1第3、4列
*论文Table 1 的第3列（3.3节），窗宽0.15
local bws 0.05 0.1 0.15 0.2 0.25
foreach bandwidth in `bws'{
	reg femaleturnout_nextcycle womanwon - winXfem_v_c_p_4 if absolute_margin < `bandwidth', robust	
}
local bws 0.05 0.1 0.15 0.2 0.25
foreach bandwidth in `bws'{
        xtreg femaleturnout_nextcycle  womanwon - winXfem_v_c_p_4 if absolute_margin < `bandwidth', fe
}
```

#### 2.2.3 选民中女性的比例

然而，选民的投票比例可能不是女性鼓舞的最佳代理变量：原始投票率统计数据没有考虑女性胜利对男性投票率的影响，因此了解女性在选民中的比例是否更大对男性也很重要，Table 1最后两列估计了女性胜利对女性选民的比例的影响—— 即女性所有选票的比例。结果再次表明选举女性的影响在统计上为零：此外，估计结果表明在95％的置信水平下，由于女性 在前一次选举中取得胜利，女性在选民中所占比例不超过0.9个百分点。Figure 3的panel（b）中所示的原始数据和拟合模型同样说明了零结果。女性选举似乎无法有意义地增加女性选民。

稳健性检验见Table A1，改变窗宽和估计过程，估计结果本质是仍然是一样的，并不是方法上的统计显著。

总之，结果表明，女性参选和担任公职对女性在美国州议会选举中的政治参与而言是，最小因果关系。虽然根据其性质，没有统计程序可以证明一个变量对另一个变量的影响确实为零，但这里达到的零估计值足够精确，可以排除除了实质上最小的影响之外的所有影响。虽然女性选民的投票比例与女性候选人的表现之间存在极为强烈的相关关系（事实上，在这些数据中，女性选民的投票比例与女性在大选中的表现之间的相关关系具有超过18的t统计量），断点回归的方法表明，在印度并不会像在美国那样，选举女性到当地的州议会任职对这种关系产生因果效应（Deininger等，2013）。女性政治参与的潜在障碍影响了女性担任公职和女性选民的投票比例，更能解释美国的这种关系。
```stata
*#### 2.2.3 选民中女性的比例，见Table1第5、6列
*论文Table 1 的第5列（3.4节），窗宽0.15
local bws 0.05 0.1 0.15 0.2 0.25
foreach bandwidth in `bws'{
	reg female_percentageofelectorate_ne womanwon - winXfem_v_c_p_4 if absolute_margin < `bandwidth', robust	
}
local bws 0.05 0.1 0.15 0.2 0.25
foreach bandwidth in `bws'{
        xtreg  female_percentageofelectorate_newomanwon - winXfem_v_c_p_4 if absolute_margin < `bandwidth', fe
}
```

### 2.3 女性政治家是否为其他女性竞选打破无形障碍

#### 2.3.1 地区距离的临界值：女性选举胜出对哪里的其他女性竞选会产生影响

尽管女性选举似乎对更多女性投票没有因果关系，但是女性的选举是否会导致其它附近的地区女性参加竞选或者担任公职？如果党派的领袖人物或者女性本人从女性胜利者或者后续公职服务中了解到或者受到鼓舞，那么会不会最终在他们区域内的其他地区中有更多女性参加竞选或者担任公职？（正如Beaman et al.2009；Bhavnani,2009）。

为了检验这个假说，设定模型如下：

$$
\frac{\Sigma Female Candidates Nearby_i}{\Sigma Elections Nearby_i} = \alpha + \beta_1V_i + \beta_2V_i^2 + \beta_3V_i^3 + \beta_4V_i^4 + \gamma F_i  + \beta_5V_iF_i + \beta_6V_i^2F_i + \beta_7V_i^3F_i  + \beta_8V_i^4F_i + \varepsilon _i     (2)
$$
其中，等式左边是在第2阶段（第2阶段是指在下次选举在i地区举行）i地区附近的其他地区有女性候选人的选举比例，其他变量与方程（1）中一样：$\alpha$为常数项；$V_i$ 是在第1阶段i 地区中女性的两位候选人投票比例；$F_i$ 是哑变量，如果$V_i >\frac{1}{2}$,$F_i =1$（意味着女性在第1阶段i地区赢得竞选），否则$F_i =0$；$\varepsilon_i$是扰动项；$\gamma$是感兴趣的变量的回归系数，刻画在第1阶段i地区选举女性对第2阶段i地区附近地区其他女性竞选公职的因果效应。用多项式模型采用稳健标准误和Imbens和Lemieux(2008)标准误运算公式。

采用了两种“附近”地区的标准——第一种，如前面Figure 2 所示的质心距离，本文考虑距离每个地区75英里的范围内的其他地区的女性是否是候选人或者在选举中获胜；第二种，估计每个地区最近的10个其他地区中相同的选举数量。第二种方法可以抵消不同区域的空间不规则：有些州比其他州大，有些地区比其他地区大，有些地区太偏远（比如北Alaska）使得距其中心100英里内没有其他地区。在所有的情况中，本文只考虑同一个州的同一个议院的席位选举的影响，尽管当考虑对所有地区的影响是结果是一样的。

#### 2.3.2 结果：成功的女性候选人不会导致该选取附近的女性候选人更多

Table 2的前2列和Figure 4的 panel（a）和 panel（c）显示了女性胜利对女性在附近地区随后的大选投票中出现的可能性的影响的估计。Panel (a) 和Table 2的第1列显示75英里内所有地区范围内的估计值，panel（c）和Table 2的第2列显示每个地区最近的10个区。在每种情况下，回归结果在统计上为零；选举女性对其他女性竞选公职的可能性可以忽略不计。Figure 3和 Figure 4显示了在组距为0.5个百分点宽的原始数据，其中叠加了拟合模型。从Figure 4中可以清楚地看出，选举女性对其他女性候选人的影响基本上为零。

[![Table 2. Effect of women's victories on women's candidacies nearby.](https://images.gitee.com/uploads/images/2019/0921/104337_c64e45e4_1963713.png "Table 1.  RD estimate of electing a woman on women’s subsequent political participation.")](https://mp.weixin.qq.com/s/RwkuPpLS7bI5C5OhRqjkOw)

Table 2. Effect of women's victories on women's candidacies nearby.

实质上，与其他因素对妇女候选人资格的影响相比，这一无效结果尤其明显。虽然来自Imbens和Lemieux (2008) 的95%信任区间 (Table A1) 估计，由于女性获胜，10个最近的地区的女性竞选公职的可能性最多高出2个百分点，但Fox and Lawless (2010) 从对可能的候选人进行的调查中发现，女性在政治把关人要求下认真考虑竞选公职的可能性要高出约37个百分点。同样，Fox and Lawless (2005) 估计，如果女性认为自己更有资格参选，那么女性更有可能认真考虑参选，这一估计再次远远超过了对见证其他女性胜利所产生影响的最乐观估计。

```stata
*#### 2.3.2 结果：成功的女性候选人不会导致该选取附近的女性候选人更多
*Table2第1列和第2列
local bws 0.05 0.1 0.15 0.2 0.25
foreach bandwidth in `bws'{
	reg femcands_over_contests_next_75m   womanwon - winXfem_v_c_p_4 if absolute_margin < `bandwidth', robust
}
local bws 0.05 0.1 0.15 0.2 0.25
foreach bandwidth in `bws'{
	reg femcands_over_contests_next_10r   womanwon - winXfem_v_c_p_4 if absolute_margin < `bandwidth', robust
}
```

#### 2.3.3 结果：女性胜利

Table 2的第3列和第4列表明选择女性对附近后续选举中女性当选的比例的影响的估计在统计上也是零。这表明，先前的无效结果并不掩盖更多才华横溢的女性可能参选的质量效应，尽管候选率保持不变，但增加了女性胜利。相反，在附近的其他地区，由于在附近的前一次选举中获胜，女性当选的可能性最多高出几个百分点；Figure 4 中 panel (b)和panel (d)以图形方式显示此无效结果。

仅考虑上议院选举的结果是相同的，上议院选举对下议院选举、仅公开席位选举，以及在州议会区平均规模以上和以下的地区都是相同的。附录中的 Table A1显示，结果仍然适用于不同的距离标准，包括更小或者更大的地理半径和更多或者更少的附近地区。Table A1还表明，当使用Imbens和Lemieux (2008) 估计方法时，这些估计结果对不同的带宽选择仍然是稳健的，并且是相同的。

[![Table A1.](https://images.gitee.com/uploads/images/2019/0921/104338_9b36ea7a_1963713.png "Table A1.")](https://mp.weixin.qq.com/s/RwkuPpLS7bI5C5OhRqjkOw)

Table A1

总之，调查结果一致认为，在一个地区选举一名女性，将使女性在附近地区竞选公职的可能性增加最多几个百分点，而且很可能根本就没有增加。在美国，女性面临巨大障碍，无法担任公职；然而，其他妇女担任公职本身似乎不太可能削弱她们的地位。
```stata
*#### 2.3.3 结果：女性胜利
*Table2第3列和第4列
local bws 0.05 0.1 0.15 0.2 0.25
foreach bandwidth in `bws'{
		reg femwins_over_contests_next_75m   womanwon - winXfem_v_c_p_4 if absolute_margin < `bandwidth', robust
}
local bws 0.05 0.1 0.15 0.2 0.25
foreach bandwidth in `bws'{
		reg femwins_over_contests_next_10r   womanwon - winXfem_v_c_p_4 if absolute_margin < `bandwidth', robust
}
```

## 3.评估影响内部和外部有效性的潜在障碍

### 3.1.排序能解释无效结果吗？

前几节中提出的估计是基于这样的假设，即女性在断点处“几乎是随机地”赢得和输掉选举。虽然设计的假设并不要求总体上女性输掉和赢得选举的地区之间没有差异，但它们确实要求在理论限制上没有差异；也就是说，政党和候选人不太擅长操纵选举，以至于接近赢家往往比接近输家更有系统性的优势。如果政党和团体能够以足够精确地预测竞选活动的预期选票率，那么更有权力的行为者可能会将他们的资源集中在可能接近输掉的比赛上(或者在事后的法律争论中扭转选举结果)，从而导致一种系统性的偏误，即更有权力的政党和团体正好在断点的获胜侧被过度代表。

Caughey和Sekhon (2011) 提供的证据表明，这种偏误存在于美国众议院选举中，接近赢得这些竞选的政党比接近输掉的政党更有可能获得丰富的财政和其他资源。在本论文的案例中，如果在美国州议会选举中存在这样一种偏误，可能会使选举结果倾向于揭露虚假的正向影响，因此不太可能引起关注。然而，正如这些作者所指出的，RD设计的一个优点是可以检测这个识别假设，本文这样做是为了进一步确保结果的稳健性。为此，我进行了一项安慰剂检验，估计女性在选举中获胜对女性投票率的“影响”，以及同时女性被提名或在附近其他选举中获胜的可能性。我们知道，女性胜利对同时其他女性的提名和胜利没有产生因果效应；因此，如果这种安慰剂方法能够找到显著的效果，那么它就会引起极大的关注。

[![Fig. 5. Placebo tests for sorting.](https://images.gitee.com/uploads/images/2019/0921/104338_abb4b78d_1963713.jpeg "Fig. 5. Placebo tests for sorting.")](https://mp.weixin.qq.com/s/RwkuPpLS7bI5C5OhRqjkOw)

Fig. 5. Placebo tests for sorting. (For interpretation of the references to colour in this figure legend, the reader is referred to the web version of this article.)


Figure 5显示了安慰剂检验的结果，使用与Table 1和Table 2中相同的设定，但是替换因变量，因变量采用相同的衡量方式，并且用相同的年度来计算。再次肯定的是，检验正确地估计，女性性候选人的胜利对同时发生的其他事件没有因果影响：估计结果没有达到统计上的显著，并且在大小上与这些变量在第2期中的估计系数的差异基本相同。设计的假设似乎保持良好，为无效结果给予了进一步的信心。

### 3.2.外部有效性

与其他潜在方法相比，虽然断点回归方法有很大的优势，但必须谨慎地解释结果的更广泛含义。本文的RDD从两方面估计了当地的平均处理效果——这种估计针对在断点附近发生的竞选类型，而本文的样本是针对在大选中女性与男性竞争的地方。这两点都值得认真考虑。

首先，设计估计了针对断点附近的竞选类型的因果效应——在这种情况下，选举非常有竞争力。然而，正如第2节所述，学者们通常认为，正是在这些非常接近的选举之后，假设的影响是最大的。然而，正如所指出的，在很少选择女性参加竞争性选举的地方，例如在印度，仍然存在着积极的影响。

此外，将分析限定于候选人性别相反的选举，在某种程度上限制了结果对女性已经被提名为她们可能赢得公职的地域的一般性(即，其中，它们不仅仅是“牺牲品”）。一方面，关注竞争强调理论观点，即，在许多女性已经担任公职的政治(像美国)中，在选举额外的女性公职人员时，存在有限的参与性外部性——对这些地域的女性参与的现有偏见超出了她们的选举所能削弱的。因此，结果并不表明女性的选举不能鼓舞女性——这种影响在印度显然存在 ——但仅仅是在像美国这样的政体中不太可能这样做，因为女性候选人和公职并不少见。

有了这个说明，根据这些限制来评估更具一般性的结果的方法是仅通过附近少数其他女性担任公职的地域数据的子集来估计断点。令人欣慰的是，这样做会产生相同的结果—— 即使最少量的现有女性描述性代表的子样本，效果仍然在统计上为零。

最后，虽然关于州议会的这些结果本身就很重要，但也有一些问题是，它们是否会推广到诸如国会竞选之类的环境中。然而，Burns等(2001，第102页)发现，女性知道谁是州议员一样，知道谁是国会议员，这表明，州级的统计精度也具有较强的外部有效性。虽然需要做更多的工作来了解这一效应在多大程度上推广到其他类型的选举，但现有数据并不支持这个问题，即可能受到鼓舞的女性根本不知道她们在其州议会机构中被女性代表。

总之，这些州立法选举在很大程度上与文献所确定的关键案件相似，无论是在女性对这些选举的认识上还是在她们的竞争力方面。此外，这一结果也适用于美国目前很少有女性担任公职的地域。虽然使用来自某种情形的数据的研究永远无法确定其在其他方面的推广，但该情形的大多数方面与其他方面有很大的相似之处。然而，未来的学者应该寻求在其他情况下复制这些结果。本文描述的设计提供了一种以更高的精度和更少的偏差机会来做到这一点的方法。

## 4. 结论
许多学者、政策制定者和活动家对女性描述性代表的因果效应提出了类似的观点，最近印度的新发现支持这一观点，该发现表明，选举女性进入地方议会可以对其他女性参与政治产生许多鼓舞效应。

本文用断点回归分析女性政治家和候选人在美国的男性与女性竞争的州议会选举中否有这样的影响。

第一，**在美国选举更多女性对其他女性的投票率没有任何有意义的因果影响。**这些估计在统计上是零的，并且精确到足以排除比收到简单的GOTV明信片所产生的影响更小的影响(Gerber和Green，2000)。

第二，**在随后的选举中，一个地区女性胜利对附近其他地区其他女性竞选或当选的可能性没有任何有意义的因果关系。**这些无效的估计在统计上也是精确的，这意味着选举女性对其他女性在其地域的候选资格的影响比其他学者对政党领导人要求女性竞选公职的影响的估计不止小一个数量级(Fox和Unidly，2010)。尽管这些结果与女性的候选资格和胜利之间存在相关关系，但女性的选举似乎没有在其中发挥重要的因果效应。

第三，**这些结果对许多替代设定、解释和稳健性检验都具有很强的稳健性**。安慰剂检验验证了设计的识别假设，而所描述的竞争性州议会选举与女性候选人的竞争性选举（现有文献认为应该找到最强烈的影响）具有很强的相似性。即使在美国目前服务的女性很少的地区，这些结果也成立，尽管女性很可能知道在国会中谁代表他们的就像她们也很可能知道在州议会中谁代表她们一样。

第四，**女性公职人员何时可以“打破无形障碍”并为其他女性竞选并赢得公职铺平道路的背景依赖性**。来自印度的令人兴奋的结果表明，女性政治家的选举显著地降低了选民、精英和女性对女性候选人的偏见，并使得更多的女性担任公职和更大的女性文化角色（Beaman et al.，2009，Bhalotra etal.，2013，Bhavnani，2009，Beaman et al.，2012）。本文认为在美国似乎并非如此。相反，结果表明，不让美国女性继续担任公职的偏见令人失望地持久，并且对反例不敏感。在此之前很少有女性担任过职务的情况下，选择女性榜样对于改善其他女性参与政府最为重要（Gilardi，2013）。

这些结果强调，**关注女性平等参与政治的根本障碍至关重要**。由于在美国选举更多女性担任职务似乎本身不可能有意义地减少女性面临的担任公职和政治参与的其余障碍，因此必须继续关注这些潜在的障碍来补足选举个别女性的努力（Duflo，2012，Kanthak and Woon，2013）。为实现平等要求，必须认真关注能够和不能解决这些挑战的因素。

## 5. 前沿研究

在Google学术查阅引用这篇文章的文献有87篇，2019年有27篇，主要集中在政治参与方面。

### 5.1  [The politics of heroes through the prism of popular heroism](https://link.springer.com/article/10.1057/s41293-019-00105-8)

在当今的英国，政治家、教育家和文化产业专业人士经常使用关于国家英雄主义的讨论，同时也是一个流行的概念，用来形容以各种方式为英国社会做出贡献的“行善者”。我们认为，虽然这种英雄主义话语是一种鼓励政治和道德上可取行为的话语手段，但它与当代大众英雄主义在很大程度上探索不足的一面是脱节的。为了弥补这一差距，本文利用英国成年人的调查数据，探讨了公众对英雄的偏好。这一分析表明了对英雄主义的理解的概念延伸，并允许识别影响pu的年龄和性别关联的动态。对性别的关注表明，大众英雄主义的景观再现了一种男性主导的偏见，这种偏见存在于更广泛的政治和文化英雄主义话语中。同时，我们的研究表明，如果英国的国家英雄主义话语仍然以男性为中心，那么大众英雄主义的特点是，英雄私有化的趋势在女性中尤为突出。在结语中，本文主张对国家英雄主义话语进行概念上的修正和重新塑造，以此作为走向男女主人公基于经验的、对年龄和性别敏感的政治的一步。

本文研究了美国初选与大选的相互作用。通过初选中的断点回归设计，研究了1980-2010年美国众议院选举结果和立法行为中极端主义者的提名是如何变化的。当一个极端主义者——用初选竞选收据模式来衡量——比一个更温和的候选人赢得一场“掷硬币”选举时，该党的大选得票率平均下降约9-13个百分点，而该党赢得席位的可能性降低了35-54个百分点。这种选举惩罚是如此之大，以至于提名更极端的初选候选人会导致选区随后的点名选举代表在提名一名极端共和党人时变得更加自由，而当一名极端民主党人被提名时则会变得更加保守。总的来说，调查结果显示了大选选民是如何作为对初选提名作出反应的温和派过滤器的。

### 5.2 [Gender-Based Differences in Information Use and Processing among State Legislators](https://papers.ssrn.com/sol3/papers.cfm?abstract_id=3445835)

根据对美国612名州议员的调查数据，提出问题：男性和女性议员是否以不同的方式使用和回应信息？研究结果表明：(1)女性议员比男性议员关注更多的信息；(2)女性议员比男性议员更少依赖自己的思想和经验来指导她们；(3)女性议员比男性议员更有可能关注利益集团、研究、国家机构和部门以及地方政治家的信息；(4)女性议员在做出议会决定时比男性议员考虑更多的争论类型；(5)女性议员比男性议员更有可能认真考虑密集、可信、政策和经济分析性的论点以及其他导向的论点。总之，这些结果支持信息处理的选择性模式，该模型认为，与男性不同，女性是综合信息处理者，她们不是在一个或几个选择机会出现时将注意力集中在一个或几个信息源上，而是试图吸收和处理他们掌握的所有信息。

### 5.3 [The impact of women above the political glass ceiling: Evidence from a Norwegian executive gender quota reform](https://www.sciencedirect.com/science/article/pii/S0261379419300022)

女性在民主议会中的代表性历来不足，特别是在具有行政权力的最高职位上。大多数性别配额改革都是通过在选举名单上规定更平等的性别代表性来解决这一问题的。相反，挪威1992年的立法改革要求地方执行委员会的政党候选人名单至少包括40%的男女政治家。这一法律变革不仅是由更高级别的政府外部强加的，而且在挪威各城市也产生了明显的配额限制。利用“配额冲击”的差异，采用DID设计来确定配额对妇女政治代表性和地方公共政策的影响。发现，在改革后，更多的女性进入了执行委员会，尽管女性在地方议会中的代表性和女性市长或最高管理者的可能性都很弱。还发现没有一致的证据表明公共政策的转变是由于女性在具有行政权力的职位上所占比例增加。


### 5.4 [The Effect of Electing Women on Future Female Candidate Selection Patterns: Findings from a Regression Discontinuity Design](https://www.cambridge.org/core/journals/politics-and-gender/article/effect-of-electing-women-on-future-female-candidate-selection-patterns-findings-from-a-regression-discontinuity-design/58A2A45E9A3447E91A95C271D82CD417)

讨论了如何选举女性到国家或国家以下各级议会的问题，利用波兰的例子，在一个公开的比例代表制中影响未来的女性候选人选择。认为选举一名女性有三个潜在的影响。首先，根据现有的在职利益理论，当选的女性在今后的选举中应该有更高的重新选择和重新选举的机会(在职情况)。其次，由于在其党内变得更加强大，当选的女性可能对未来的名单组成有更强的影响，因此，更多的女性应在这些名单上竞选公职(增强权能)。最后，认为，其他政党可以根据其他政党名单上女性的选举情况(传染效应)调整其候选人选择模式。发现有很强的证据证明了占位效应，也有一些支持传染效应的证据。然而，鼓舞假说没有发现经验性的支持。

### 5.5 [Descriptive Representation and the Political Engagement of Women](https://www.cambridge.org/core/journals/politics-and-gender/article/descriptive-representation-and-the-political-engagement-of-women/29E8A04DC32173A5D73D66404356D0AD)

当女性在竞选活动中和当选的职位上都有代表时，选民中的女性被证明更多地参与政治。然而，说明代表性对女性鼓舞的影响的大多数证据来自1980年代和1990年代的调查。更新这些研究报告，以考虑女性候选人和官员如何影响其他女性在选民中的政治知识、兴趣和参与。根据2006年至2014年合作国会选举研究的回应，发现，当女性在国会和州政府中有女性代表时，男性和女性在政治上都更有见识。考虑到政治参与，发现几乎没有证据表明女性在政治上更感兴趣或更有参与性。

## 参考文献

1. [A. Healy, Does Prosperity under Female Leadership Help Other Women Win Elections?,Working Paper,Loyola Marymount University (2013)

2. [S. MacManus,A city's first female officeholder: coattails for future female officeholders,West. Polit. Q., 34 (1981), pp. 88-99](https://www2.scopus.com/record/display.uri?eid=2-s2.0-0040595995&origin=inward&txGid=4d483675e7f9d90d423d8dd00210207f)

3. [I. Gitelson, A. Gitelson,Adolescent attitudes toward male and female political candidates,Women Polit., 1 (1981), pp. 53-64](https://scholar.google.com/scholar_lookup?title=Adolescent%20attitudes%20toward%20male%20and%20female%20political%20candidates&publication_year=1981&author=I.%20Gitelson&author=A.%20Gitelson)

4. D. Hopkins, K.T. McCabe,After It's Too Late: Estimating the Policy Impacts of Black Mayors in US Cities,Working Paper,Georgetown University (2013)

5. J.A. Henderson, J.S. Sekhon, R. Titiunik,Cause of Effect? Turnout in Hispanic Majority-Minority Districts,Working Paper,University of California, Berkeley (2013)

6. [S. MacManus, A city's first female officeholder: coattails for future female officeholders,West. Polit. Q., 34 (1981), pp. 88-99](https://www2.scopus.com/record/display.uri?eid=2-s2.0-0040595995&origin=inward&txGid=2cd66c501478324db235f67edf57403c)

7. [J. Trounstine,Evidence of a local incumbency advantage,Legis. Stud. Q., 36 (2) (2011), pp. 255-280](https://onlinelibrary.wiley.com/doi/full/10.1111/j.1939-9162.2011.00013.x)

8. [D.M. Caughey, J.S. Sekhon,Elections and the regression discontinuity design: lessons from close US house races, 1942-2008,Polit. Anal., 19 (2011), pp. 385-408](https://www.cambridge.org/core/journals/political-analysis/article/elections-and-the-regression-discontinuity-design-lessons-from-close-us-house-races-19422008/E5A69927D29BE682E012CAE9BFD8AEB7)

9.[D.S. Lee,Randomized experiments from non-random selection in US House elections,J. Econ., 142 (2008), pp. 675-697](https://id.elsevier.com/as/w4kBQ/resume/as/authorization.ping)

10.[D.P. Green, T.Y. Leong, H.L. Kern, A.S. Gerber, C.W. Larimer,Testing the accuracy of regression discontinuity analysis using experimental benchmarks,Polit. Anal., 17 (4) (2009), pp. 400-417](https://www.cambridge.org/core/journals/political-analysis/article/testing-the-accuracy-of-regression-discontinuity-analysis-using-experimental-benchmarks/D98C4804E30844880C72A76BE9AACB58)

11.[L.R. Atkeson,Not all cues are created equal: the conditional impact of female candidates on political engagement,J. Polit., 65 (4) (2003), pp. 1040-1061](https://www.journals.uchicago.edu/doi/10.1111/1468-2508.t01-1-00124)

12.[C. Kam, S. Utych,Close elections and cognitive engagement,J. Polit., 73 (4) (2011), pp. 1251-1266](https://www.journals.uchicago.edu/doi/10.1017/S0022381611000922)

13.[G.W. Imbens, T.W. Lemieux,Regression discontinuity designs: a guide to practice,J. Econ. (2008), pp. 615-635](https://www.sciencedirect.com/science/article/pii/S0304407607001091)