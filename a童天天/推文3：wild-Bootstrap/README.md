> **任务：** 重现这篇论文的表 1，主要目的在于介绍 wild Bootstrap 以及对应的 Stata 命令 `boottest` 的用法。

### Wild Bootstrap 简介

对于线性回归模型：
$$
y_{t}=X_{t} \beta+u_{t}, \quad \mathrm{E}\left(u_{t} | X_{t}\right)=0, \quad \mathrm{E}\left(u_{s} u_{t}=0\right) \forall s \neq t
$$
可以通过多种方法来建立 bootstrap DGPs 。有些方法要求对 $u_{t}$ 施加强约束，有些方法则对  $u_{t}$ 施加的约束较弱。一般来说，如果满足了更强的假设，就会得到更好的性能，但是如果不满足，就会导致渐进无效的推论。

wild bootstrap 是处理异方差更好的方法，最早由Liu(1988) 提出，Mammen(1993) 在 Liu 的基础上进行了进一步发展。wild bootstrap 通过独立于模型误差产生随机权重并乘以残差的方式来模拟实际模型误差的分布。对于无限制模型，wild bootstrap 的 DGP 为
$$y_{i}^{*}=\boldsymbol{X}_{i} \hat{\boldsymbol{\beta}}+f\left(\hat{u}_{i}\right) v_{i}^{*}$$
其中，$f\left(\hat{u}_{i}\right)$ 表示为第 $i$个$\widehat{u}_{i}$ 的函数形式，$v_{i}^{*}$ 是均值为 0 方差为 1 的随机变量。

对于 $f(\cdot)$，一般有 w1、w2、w3 三种形式。分别表示为
$$
w1:f\left(\hat{u}_{i}\right)=\sqrt{(n /(n-k+1)} \tilde{u}_{i}
$$
$$
w2:f\left(\hat{u}_{i}\right)=\frac{\hat{u}_{i}}{\left(1-h_{i}\right)^{1 / 2}}
$$
$$
w3:f\left(\hat{u}_{i}\right)=\frac{\hat{u}_{i}}{\left(1-h_{i}\right)}
$$
对于随机变量  $v_{i}^{*}$ ，，一般指定为两点分布，主要有$F_{1}$ 、$F_{2}$  两种分布情况。
![image.png](https://images.gitee.com/uploads/images/2019/0813/180307_e9d6b72f_4769669.png)
$F_{1}$ 这种分布的理论优点是 bootstrap 误差项的偏度与扰动项的偏度相同。另一种更为简单的分布被称为 Rademacher 分布，具体形式为：
$$
F_{2} : \quad v_{i}^{*}=\left\{\begin{aligned}-1 & \text { with probability } \frac{1}{2} \\ 1 & \text { with probability } \frac{1}{2} \end{aligned}\right.
$$
如果误差项本身具有对称性，则最好采用这一分布。

当使用像 $F_{1}$  或 $F_{2}$  这样的两点分布时，bootstrap 误差项对于每个观察值只能取两个可能的值，这与真实模型中的扰动项可能并不相同。尽管如此，wild-boostrap 仍然很好地模拟了真正的 DGP 的基本特性，在许多情况下都很有用。在有限样本中，即使真实的扰动项分布是适度偏倚的，wild-boostrap 基于 $F_{1}$  分布得到的结果仍优于基于 $F_{2}$  分布的结果。

对于任何模型 boostrap 方法，都满足：
$$
\begin{aligned} \hat{\boldsymbol{\beta}}_{j}^{*}-\overline{\boldsymbol{\beta}}^{*} &=\left(\boldsymbol{X}^{\top} \boldsymbol{X}\right)^{-1} \boldsymbol{X}^{\top} \boldsymbol{y}_{j}^{*}-\overline{\boldsymbol{\beta}}^{*} \\ &=\left(\boldsymbol{X}^{\top} \boldsymbol{X}\right)^{-1} \boldsymbol{X}^{\top}\left(\boldsymbol{X} \hat{\boldsymbol{\beta}}+\boldsymbol{u}_{j}^{*}\right)-\overline{\boldsymbol{\beta}}^{*} \\ &=\left(\boldsymbol{X}^{\top} \boldsymbol{X}\right)^{-1} \boldsymbol{X}^{\top} \boldsymbol{u}_{j}^{*}+\left(\hat{\boldsymbol{\beta}}-\overline{\boldsymbol{\beta}}^{*}\right) \end{aligned}
$$
通过上式，如果 wild-boostrap DGP 得到的 OLS 估计量是无偏的，那么 boostrap 估计量 $\hat{\beta}_{j}^{*}$ 的期望等于估计值$\hat{\beta}$。

通过 Bootstrap 方法得到的 P 值为：
$$
\hat{p}^{*}(\hat{\tau})=2 \min \left(\frac{1}{B} \sum_{j=1}^{B} \mathrm{I}\left(\tau_{j}^{*} \leq \hat{\tau}\right), \frac{1}{B} \sum_{j=1}^{B} \mathrm{I}\left(\tau_{j}^{*}>\hat{\tau}\right)\right)
$$
这被称为是 equal-tail bootstrap P 值，表示在 $\alpha$ 水平下，处于拒绝域内的任一个 $\hat{\tau}$ 的取值要么小于 $\alpha / 2$ 分位数上的经验分布取值 $\tau_{j}^{*}$，要么大于 $1-\alpha / 2$ 分位数上的经验分布取值 $\tau_{j}^{*}$。

一般来说，Bootstrap 方法比 t 检验表现的更好，但也可能产生错误的结论。特别是在回归因子为处理虚拟变量时，此时得到的聚类稳健标准误可能会非常小，Bootstrap 方法可以得到更为稳健的推论。然而当处理集群数量非常少时，wild-boostrap 可能会发生严重的拒绝不足问题；在这种情况下，其他 boostrap 方法则可能发生严重的过度拒绝问题。

计算基于 $\beta_{k}=0$ 假设的 Bootstrap P 值 的算法如下：

第一步：通过线性回归模型 $\boldsymbol{y}=\boldsymbol{X} \boldsymbol{\beta}+\boldsymbol{u}, \quad \mathrm{E}\left(\boldsymbol{u} \boldsymbol{u}^{\prime}\right)=\boldsymbol{\Omega}$，得到$\hat{\beta}$和$\widehat{\operatorname{Var}}(\hat{\boldsymbol{\beta}})$ 的 OLS 估计量，用来计算聚类稳健 t 统计量：
$$t_{k}=\hat{\beta}_{k} / \mathrm{s.e.}\left(\hat{\beta}_{k}\right)$$
其中，$\mathrm{S.e.}\left(\hat{\beta}_{k}\right)$ 表示$\widehat{\operatorname{Var}}(\hat{\boldsymbol{\beta}})$第 k 个对角元素的平方根。

第二步：生成 boostrap 样本，基于 boostrap 样本重复第一步的
 OLS 回归，得到 $\hat{\boldsymbol{\beta}}^{* b}$ 和$\widehat{\operatorname{Var}}\left(\hat{\boldsymbol{\beta}}^{* b}\right)$。

第三步：对每一个 boostrap 样本，计算 boostrap t 统计量$t_{k}^{* b}$，具体有两种形式：
$$t_{k r}^{* b}=\frac{\hat{\beta}_{k}^{* b}}{\operatorname{s.e.}\left(\hat{\beta}_{k}^{* b}\right)}$$
或者
$$t_{k u}^{* b}=\frac{\hat{\beta}_{k}^{* b}-\hat{\beta}_{k}}{\operatorname{s.e.}\left(\hat{\beta}_{k}^{* b}\right)}$$

第四步：计算 bootstrap P 值：
$$\hat{P}_{\mathrm{S}}^{*}=\frac{1}{B} \sum_{b=1}^{B} \mathbb{I}\left(\left|t_{k}^{*}\right|>\left|t_{k}\right|\right)$$
或者
$$
\hat{P}_{\mathrm{ET}}^{*}=\frac{1}{B} \min \left(\sum_{b=1}^{B} \mathbb{I}\left(t_{k}^{* b}<t_{k}\right), \sum_{b=1}^{B} \mathbb{I}\left(t_{k}^{* b} \geq t_{k}\right)\right)
$$

### Wild Bootstrap 的 手动操作

下文将以 88 年美国妇女工资数据为例，选择 W1F2 的 wild bootstrap DGP 形式，具体操作如下：
```stata
clear

sysuse "nlsw88.dta", clear


regress wage  married collgrad industry occupation union south age 

ereturn list
mat SE=e(V)
gen t=_b[married]/sqrt(SE[1,1]) //全样本的t值
gen t_abs=abs(t)
gen t1=.

cap  forvalues i = 1/1000 {
regress wage  married collgrad industry occupation union south age 
cap drop u y d du yhat
predict u,res
predict y

cap drop random_num
set seed 54651
gen random_num=runiform()
gen  d=1 if random_num>0.5
replace d=-1 if random_num<0.5
gen du=u*d
gen yhat=y+du

preserve

sample 399,count

regress yhat  married collgrad industry occupation union south age 
restore

ereturn list

 mat SE=e(V)
replace t1=_b[married]/sqrt(SE[1,1]) in `i'
}

gen t1_abs=abs(t1)
gen x=1 if t1_abs>t_abs
keep if _n<1001
replace x=0 if x==.
cap drop p
sum x
dis r(sum)/1000

```
首先初始 OLS 回归中，married 的 P 值为 0.115，具体结果如下图所示：
```stata

      Source |       SS           df       MS      Number of obs   =     1,861
-------------+----------------------------------   F(7, 1853)      =     81.76
       Model |  7637.29497         7  1091.04214   Prob > F        =    0.0000
    Residual |  24728.6092     1,853   13.345175   R-squared       =    0.2360
-------------+----------------------------------   Adj R-squared   =    0.2331
       Total |  32365.9042     1,860  17.4010238   Root MSE        =    3.6531

------------------------------------------------------------------------------
        wage |      Coef.   Std. Err.      t    P>|t|     [95% Conf. Interval]
-------------+----------------------------------------------------------------
     married |  -.2821825   .1789425    -1.58   0.115    -.6331327    .0687677
    collgrad |   4.131068   .2096327    19.71   0.000     3.719927    4.542209
    industry |  -.0779166   .0292965    -2.66   0.008    -.1353743   -.0204589
  occupation |  -.3024011   .0254679   -11.87   0.000    -.3523499   -.2524522
       union |   1.318596   .2036496     6.47   0.000     .9191893    1.718003
       south |  -1.052283   .1745676    -6.03   0.000    -1.394653   -.7099131
         age |   .0099445    .027916     0.36   0.722    -.0448057    .0646947
       _cons |   8.520637   1.129818     7.54   0.000     6.304788    10.73649
------------------------------------------------------------------------------
```
通过上述操作，得到 wild bootstrap 的 P 值为 0，married 显著影响美国妇女工资水平。

### Wild Bootstrap 的 stata 实现

下面将通过一个例子展示 wild bootstrap 的 stata 操作与结果，首先介绍 `boottest` 的用法。
```stata
ssc install boottest
help boottest
```
`boottest` 是一个 post-estimation 命令，通过使用 wild、score bootstrap、Rao 或 Wald 检验来检验线性假设，适用于 OLS、2SLS、LIML、Fuller、k-class、linear GMM 等估计。它提供了几种用于生成模拟数据集的 bootstrap 算法，并基于数据集进行的几个检验。
`boottest` 基于 bootstrap 的检验通常运行比较快，不过运行 wild restricted efficient bootstrap （简称为 WRE）则比其他的要慢很多。下面将通过`boottest`来分别进行 wild cluster  restricted bootstrap（WCR）和 wild cluster  unrestricted bootstrap （WCU）。

```stata

********* column one **********
/*BCMM treatment variable*/

 /*robust standard error*/
regress demdiff_2012to2008 early_diff_2012to2008 exfelon_diff_2012to2008 id_diff_2012to2008 $xcovs ///
[aweight=popestres], vce(robust)

/*cluster by ctyfips*/
regress demdiff_2012to2008 early_diff_2012to2008 exfelon_diff_2012to2008 id_diff_2012to2008 $xcovs ///
[aweight=popestres], cluster(ctyfips)

/*cluster by state*/
regress demdiff_2012to2008 early_diff_2012to2008 exfelon_diff_2012to2008 id_diff_2012to2008 $xcovs ///
[aweight=popestres], cluster(state) 

/*WCU*/
boottest early_diff_2012to2008, boottype(wild) nonull cluster(state) seed(96) reps(99999) nograph

/*WCR*/
boottest early_diff_2012to2008, boottype(wild) cluster(state) seed(96) reps(99999) nograph


********* column two *********
/*fixed symmetric treatment variable*/

/*robust standard error*/
regress demdiff_2012to2008 fixed_symm exfelon_diff_2012to2008 id_diff_2012to2008 $xcovs ///
[aweight=popestres], vce(robust)

/*cluster by ctyfips*/
regress demdiff_2012to2008 fixed_symm exfelon_diff_2012to2008 id_diff_2012to2008 $xcovs ///
[aweight=popestres], cluster(ctyfips)

/*cluster by state*/
regress demdiff_2012to2008 fixed_symm exfelon_diff_2012to2008 id_diff_2012to2008 $xcovs ///
[aweight=popestres], cluster(state) 

/*WCU*/
boottest fixed_symm, boottype(wild) cluster(state) nonull seed(96) reps(99999) nograph

/*WCR*/
boottest fixed_symm, boottype(wild) cluster(state) seed(96) reps(99999) nograph

********** column three and four ********
/*column 3 - adopted*/
/*column 4 - repeal*/
/*robust standard error*/
regress demdiff_2012to2008 adopt repeal exfelon_diff_2012to2008 id_diff_2012to2008 $xcovs ///
[aweight=popestres], vce(robust)

/*cluster by ctyfips*/
regress demdiff_2012to2008 adopt repeal exfelon_diff_2012to2008 id_diff_2012to2008 $xcovs ///
[aweight=popestres], cluster(ctyfips)
 
/*cluster by state*/
regress demdiff_2012to2008 adopt repeal exfelon_diff_2012to2008 id_diff_2012to2008 $xcovs ///
[aweight=popestres], cluster(state)

/*column 3 - bootstraps*/

/*WCU*/
boottest adopt, boottype(wild) cluster(state) nonull seed(96) reps(99999) nograph

/*WCR*/
boottest adopt, boottype(wild) cluster(state) seed(96) reps(99999) nograph

/*column 4 - bootstraps*/

/*WCU*/
boottest repeal, boottype(wild) cluster(state) nonull seed(96) reps(99999) nograph


/*WCR*/
boottest repeal, boottype(wild) cluster(state) seed(96) reps(99999) nograph

```
通过上述操作，整理得到表1。
![d4a32969e591fb950a0a85e80942aee.png](https://images.gitee.com/uploads/images/2019/0813/180307_8691b914_4769669.png)
表 1 中分别报告了参数估计值的 Robust、基于 Ctyfips 的 CRVE、基于 State 的 CRVE 以及 WCU 和 WCR 下的 P 值。以第一列为例，Robust 和 CRVEfips 的  P 值小于 0.05，非常显著；而当以stata聚类后得到的 CRVE(State) 的 P 值大于 0.05，并不显著；可见聚类的方法不同，其 P 值结果具有明显差异。而 wild cluster bootstrap 的 P 值大于 0.05，并不显著。

第四列反映的是较少处理集群情况下的回归结果，可以发现在这个模型中，Robust、CRVE 和 WCU 都非常显著，而 WCR 并不显著。对于处理集群较少情况，WCU 仍然有效，而 WCR 在处理集群较多的情况下，表现则较 WCU 更为显著。

通过上述例子，可以发现当使用回归模型来估计处理效果时，当集群大小相似时，即使处理集群非常小，普通的 wild bootstrap 仍然有效，此时聚类 wild bootstrap 则一般会存在过度拒绝问题。。检验模型是否存在问题的一个简单方法是通过使用两种 wild cluster bootstrap 的变型分别计算 bootstrap P 值。如果 WCR (restricted) 和 WCU (unrestricted) bootstraps 得到完全不同的推论，那么模型肯定存在问题。通常，在这种情况下，WCU 会拒绝，WCR 也不会拒绝。

### 参考文献

- MacKinnon, J. G. 2013, Thirty years of heteroskedasticity-robust inference[C], Recent advances and future directions in causality, prediction, and specification analysis, Springer,  437-461. [[PDF]](https://www.researchgate.net/profile/James_Mackinnon/publication/254447243_Thirty_Years_of_Heteroskedasticity-Robust_Inference/links/00b495304078c3ba2c000000/Thirty-Years-of-Heteroskedasticity-Robust-Inference.pdf)

- Efron, B., R. Tibshirani. An introduction to the bootstrap[M]. Chapman & Hall, 1997.

&emsp;

&emsp;
