# Stata: 数据包络分析 (DEA) 简介
&emsp;

> 作者：彭诗敏 (中山大学)，pengshm7@mail2.sysu.edu.cn
>     
> Stata 连享会：  [知乎](https://zhuanlan.zhihu.com/arlion) | [简书](http://www.jianshu.com/u/69a30474ef33) | [码云](https://gitee.com/arlionn) | [CSDN](https://blog.csdn.net/arlionn) | [StataChina公众号](https://mp.weixin.qq.com/s/RwkuPpLS7bI5C5OhRqjkOw) 




## 1. 数据包络分析（DEA）简介

数据包络分析 (DEA) 是由美国著名运筹学家 A.Charnes（查恩斯）、W.W.Cooper（库铂）、E.Rhodes（罗兹）于 1978 年首先提出，在相对效率评价概念基础上发展起来的一种非参数检验方法。

在 DEA 中，受评估的单位或组织被称为决策单元（简称 DMU ）。DEA 通过选取决策单元的多项投入和产出数据，利用线性规划，以最优投入与产出作为生产前沿，构建数据包络曲线。其中，有效点会位于前沿面上，效率值标定为 1；无效点则会位于前沿面外， 并被赋予一个大于 0 但小于 1 的相对的效率值指标。

DEA 模型具体又可细分为三种类型：（1）**CCR 模型**：该模型假定规模报酬不变，主要用来测量技术效率；（2） **BCC 模型**：该模型假定规模报酬可变，主要测算纯技术效率，即技术效率与规模效率的比值；（3）**DEA-Malmquist 指数模型**：该模型可以测算出决策单元 (DMUs) 的生产效率在不同时期的动态变化情况。


### 1.1 CCR 模型

**CCR 模型**由 Charnes, Cooper 和 Rhodes 于 1978 年提出，它可以计算规模报酬不变情况下的资源配置效率。

首先，我们简单推倒一下 CCR 模型，以第 $j_{0}$ 个决策单元的效率指数为目标，以所有决策单元的效率为约束，我们可以得到以下模型：

$$
\max h_{j_{0}}=\frac{\sum_{r=1}^{s} u_{r} y_{rj_{0}}}{\sum_{i=1}^{m} v_{i} x_{i j_{0}}}
$$

$$
\text { s.t. } \ \frac{\sum_{r=1}^{s} u_{r} y_{r j}}{\sum_{i=1}^{m} v_{i} x_{i j}} \leq 1, j=1,2, \ldots n
$$

$$
u \geq 0, v \geq 0
$$

其中，$x_{ij}$ 表示第 $j$ 个决策单元对第 $i$ 种投入要素的投放总量，而 $y_{rj}$  则表示第 $j$ 个决策单元中第 $r$ 种产品的产出总量，$v_{i}$ 和 $u_{r}$ 分别指第 $i$ 种类型投入与第 $r$ 种类型产出的权重系数。

令 $ w=\frac{1}{v^{T} x_{0}} v, \mu=\frac{1}{v^{T} x_{0}} u$，经 **Charnes-Cooper** 变换，可变为如下线性规划模型：

$$
\max \  h_{j_{0}}=\mu^{T} y_{0}
$$

$$
\text {s.t. }\  w^{T} x_{j}-\mu^{T} y_{j} \geq 0, \ j=1,2, . . . n
$$

$$
w^{T} x_{0}=1
$$

$$
w \geq 0, \mu \geq 0
$$

在上述规划的对偶规划中我们引入松弛变量 $s^{+}$ 和剩余变量 $s^{-}$，松弛变量表示达到最优配置需要减少的投入量，剩余变量表示达到最优配置需要增加的产出量。由此，不等式约束会变为等式约束，模型可以简化为：

$$
\min \theta
$$

$$
\text { s.t. }\sum_{j=1}^{n} \lambda_{j} y_{j}+s^{+}=\theta x_{0}
$$

$$
\sum_{j=1}^{n} \lambda_{j} y_{j}-s^{-}=\theta y_{0}
$$

$$
\lambda_{j} \geq 0, j=1,2, . . . n
$$

$$
s^{+} \geq 0, s^{-} \leq 0
$$

我们能够用 CCR 模型判定技术有效和规模有效是否同时成立：

1. 若满足 $\theta^{*}=1$且$s^{*+}=0, s^{*-}=0$，则决策单元为 DEA 有效，决策单元的经济活动同时为技术有效和规模有效；
2. 若满足 $\theta^{*}=1$，但至少某个投入或者产出大于 0，则决策单元为弱 DEA 有效，决策单元的经济活动不是同时为技术有效和规模有效；
3. 若满足 $\theta^{*}<1$，决策单元不是 DEA 有效，经济活动既不是技术有效，也不是规模有效。

### 1.2 BCC 模型

CCR 模型是在规模报酬不变的前提下所得到的，但是技术创新的规模报酬是不固定的，现实中存在的不平等竞争也会导致某些决策单元不能以最佳规模运行，于是 Banker，Charnes 和 Cooper 在 1984 年对之前仅讨论固定规模效益的 DEA 分析进行了扩展，提出了 BCC 模型。


BCC 模型考虑到在可变规模收益 (VRS) 情况，即当有的决策单元不是以最佳的规模运行时，技术效益 (Technology efficiency,TE) 的测度会受到规模效率 (Scale efficiency,SE) 的影响。

![](https://upload-images.jianshu.io/upload_images/19701550-8565306246b24c7f.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

以上图为例，位于生产函数曲线 f(x) 上的点 A 与点 C 都是技术有效，位于 f(x) 曲线内的点 B 则不是技术有效。由于点 A 还位于生产函数曲线的拐点，A 还是规模有效点。然而点 C 位于规模收益递减区域，因此它不是规模有效。BCC 模型正是要讨论位于这种生产状况的决策单元。

因此，在构建 BCC 模型时，我们需要假设规模报酬可变，对 CCR 模型的约束条件进行简单的改进，增加凸性假设条件：$
\sum{\lambda_{j}}=1, \ j=1,2, . . . n
$，即可得：

$$
\min \theta
$$

$$
\text { s.t. }\sum_{j=1}^{n} \lambda_{j} y_{j}+s^{+}=\theta x_{0}
$$

$$
\sum_{j=1}^{n} \lambda_{j} y_{j}-s^{-}=\theta y_{0}
$$

$$
\sum{\lambda_{j}}=1, \ j=1,2, . . . n
$$

$$
s^{+} \geq 0, s^{-} \leq 0
$$

我们可以对数据同时做 CCR 模型和 BCC 模型的 DEA 分析来评判决策单元的规模效率 (SE)。如果决策单元 CCR 和 BCC 的技术效益存在差异，则表明此决策单元规模无效，并且规模无效效率可以由 BCC 模型的技术效益和 CCR 模型的技术效益之间的差异计算出来。

### 1.3 DEA-Malmquist 指数模型

传统的 CCR 和 BCC 模型只能横向比较决策单元在同一时间点的生产效率，DEA-Malmquist 指数模型则可以测度决策单元在不同时期间效率的动态变化，因此它可以分析面板数据，具有较广泛的应用性。

Malmquist 指数利用距离函数 (E) 进行运算，表示为以下数学表现形式：

$$M P I_{I}^{t}=\frac{E_{I}^{t}\left(x^{t+1}, y^{t+1}\right)}{E_{I}^{t}\left(x^{t}, y^{t}\right)}, M P I_{I}^{t+1}=\frac{E_{I}^{t+1}\left(x^{t+1}, y^{t+1}\right)}{E_{I}^{t+1}\left(x^{t}, y^{t}\right)}$$

为了把两个时期的技术水平都纳入考虑，我们取它们的几何平均值：

$$M P I_{I}^{G}=\left(MPI_{I}^{\mathrm{t}} M P I_{I}^{t+1}\right)^{1 / 2}=\left[\left(\frac{E_{I}^{t}\left(x^{t+1}, y^{t+1}\right)}{E_{I}^{t}\left(x^{t}, y^{t}\right)}\right) \cdot\left(\frac{E_{I}^{t+1}\left(x^{t+1}, y^{t+1}\right)}{E_{I}^{t+1}\left(x^{t}, y^{t}\right)}\right)\right]^{1 / 2}$$

该生产率指数又可以分解为面向输入的效率变化 (EFFCH) 和技术效率(TECHCH) ，技术效率又可以分解为规模效率 (SECH) 和纯技术效率 (PECH) 两部分：

$$MPI_{I}^{G}=\left(EFFCH_{I}\right) \cdot \left(TECHCH_{I}^{G}\right)=\left(\frac{E_{I}^{t+1}\left(x^{t+1}, y^{t+1}\right)}{E_{I}^{t}\left(x^{t}, y^{t}\right)}\right) \cdot \left[\left(\frac{E_{I}^{t}\left(x^{t}, y^{t}\right)}{E_{I}^{t+1}\left(x^{t}, y^{t}\right)}\right) \cdot\left(\frac{E_{I}^{t}\left(x^{t+1}, y^{t+1}\right)}{E_{I}^{t+1}\left(x^{t+1}, y^{t+1}\right)}\right)\right]^{1/2}$$

$$SECH=\left[\frac{E_{v r s}^{t+1}\left(x^{t+1}, y^{t+1}\right) / E_{c r s}^{t+1}\left(x^{t+1}, y^{t+1}\right)}{E_{v r s}^{t+1}\left(x^{t}, y^{t}\right) / E_{c r s}^{t+1}\left(x^{t}, y^{t}\right)} \cdot \frac{E_{v r s}^{t}\left(x^{t+1}, y^{t+1}\right) / E_{c r s}^{t}\left(x^{t+1}, y^{t+1}\right)}{E_{v r s}^{t}\left(x^{t}, y^{t}\right) / E_{c r s}^{t}\left(x^{t}, y^{t}\right)}\right]^{1 / 2}$$

$$PECH=\frac{E_{\mathrm{vrs}}^{t+1}\left(x^{t+1}, y^{t+1}\right)}{E_{c r s}^{t}\left(x^{t}, y^{t}\right)}$$


## 2. DEA 和 MPI 的 Stata 实现

### 2.1 dea 命令

`dea` 命令允许用户从数据文件中选择输入和输出变量，并通过指定的选项在 Stata 求解 dea 模型，该命令由 Yong-bae Ji 和 Choonjo Lee 于 2010 年编写。

#### 2.1.1 dea 命令语法

在 Stata 命令窗口中输入 `ssc install dea, replace` 可以下载最新版的 `dea` 命令，进而输入 `help dea` 可以查看其帮助文件。

该命令的语法详情如下：

```stata
dea ivars = ovars [if] [in] [using/filename ][,  ///
    rts(string) ort(string) stage(#)  ///
	trace saving(filename) ]
```


其中，  

- `ivars` 表示投入变量  
- `ovars` 表示产出变量  
- `rts(string)` 可选择不同规模报酬的相应模型：默认值是 `rts(crs)` ，即规模报酬不变（对应 CCR 模型）， `rts(vrs)` 、 `rts(drs)` 和 `rts(nirs)` 分别表示规模报酬可变（对应 BCC 模型），规模报酬递减和规模报酬不增长
- `ort(string)` 指定方向：默认值是 `ort(i)` ，表示面向投入的 DEA 模型； `ort(o)` 表示面向产出的 DEA 模型；面向投入的 DEA 模型是指在至少满足已有的产出水平的情况下最小化投入，而面向产出的 DEA 模型则是指在不需要更多的投入的情况下最大化产出
- `stage(#)` 默认值是 `stage(2)` ，即两阶段 DEA 模型；`stage(1)` 为单阶段 DEA 模型
- `trace` 允许所有序列显示在结果窗口中，并保存在 “ dea.log ” 文件中 

注意：需要导入决策单元变量 **dmu** 。

#### 2.1.2 dea 的使用方法

举例：

```stata
*-导入例子所用的数据
use https://gitee.com/arlionn/data/raw/master/data01/data_dea.dta, clear

*-采用dea命令
dea area employee = sales profit
```

其中， **area** **employee** 为 csv 表中的投入变量，**sales** **profit** 为产出变量；**data_dea.csv** 数据仅用来举例，大家可以导入自己实际需要处理的数据。

#### 2.1.3 dea 的结果解读

根据输出的结果，我们可以得到 5 个决策单元的生产效率：B 、D 、E 公司为 DMU 有效，得分皆为 1；A 、C 公司为 DMU 低效率，得分皆为 0.889 。

```
options: RTS(CRS) ORT(IN) STAGE(2)
CRS-INPUT Oriented DEA Efficiency Results:
                        ref: ref: ref:     ref:     ref:  islack:   islack:  oslack:  oslack:
       rank     theta     A    B    C        D        E     area  employee    sales   profit
dmu:A     5   .888889     0    0    0        0  .666667        0   1.55556        0  .666667
dmu:B     1         1     0    1    0        0        0        0         0        0        0
dmu:C     4   .888889     0    0    0  .333333  .333333        0   .333333        0        0
dmu:D     1         1     0    0    0        1        0        0         0        0        0
dmu:E     1         1     0    0    0        0        1        0         0        0        0
```

对于公司 A，减少 1.556 单位 的 **employee** 投入与 0.667 单位的 **profit** 产出将使得生产更有效率。对于公司 C ，减少 0.333 单位的 **employee** 投入将使得生产更有效率。


### 2.2 malmq 命令

`malmq` 命令允许用户从数据文件中选择输入和输出变量，并通过指定的选项在 Stata 解决 Malmquist 生产率指数模型，该命令由 Kyong-Rock Lee 和 Byung-Ihn Leem 于 2011 年编写。

#### 2.2.1 malmq 命令语法

```stata
malmq ivars = ovars [if] [in] [using/filename] [, ///
      ort(string) period(string) ///
	  trace saving(filename) ]
```


其中，  

- `ivars` 表示投入变量
- `ovars` 表示产出变量
- `ort(string)` 指定方向：默认值是 `ort(i)` ，表示面向投入的 DEA 模型；`ort(o)`表示面向产出的 DEA 模型
- `period(string)` 指定时间
- `trace` 允许所有序列显示在结果窗口中，并保存在 “ malmq.log ” 文件中  

注意：决策单元变量 (**dmu**) 需要是字符型


#### 2.2.2 malmq 的使用方法

举例：

```stata    
*-导入例子所用的数据
use https://gitee.com/arlionn/data/raw/master/data01/data_mpi.dta, clear
 
*-使用malmq命令
 malmq i_x = o_q,ort(i) period(year) 
```

其中， **i_x** 为 csv 表中的投入变量，**o_q** 为产出变量，时间间隔为一年 (year)；**data_mpi.csv** 数据仅用来举例，大家可以导入自己实际需要处理的数据。

#### 2.2.3 malmq 的结果解读

我们可以得到以下输出结果：

```
Cross CRS-DEA Result:
          from     thru        t       t1
dmu:A        1        2       .5     .375
dmu:B        1        2      .75     .375
dmu:C        1        2  1.33333      .75
dmu:D        1        2       .6       .6
dmu:E        1        2        1     .625
dmu:A        2        3     .375     .375
dmu:B        2        3    .5625    .5625
dmu:C        2        3        1        1
dmu:D        2        3      .45      .45
dmu:E        2        3      .75      .75

Malmquist efficiency INPUT Oriented DEA Results:

     +--------------------------------+
     | year   dmu   CRS_eff   VRS_eff |
     |--------------------------------|
  1. |    1     A        .5         1 |
  2. |    1     B        .5      .625 |
  3. |    1     C         1         1 |
  4. |    1     D        .8        .9 |
  5. |    1     E   .833333         1 |
     |--------------------------------|
  6. |    2     A      .375         1 |
  7. |    2     B     .5625   .666667 |
  8. |    2     C         1         1 |
  9. |    2     D       .45   .533333 |
 10. |    2     E       .75         1 |
     |--------------------------------|
 11. |    3     A      .375         1 |
 12. |    3     B     .5625   .666667 |
 13. |    3     C         1         1 |
 14. |    3     D       .45   .533333 |
 15. |    3     E       .75         1 |
     +--------------------------------+

Malmquist productvity index INPUT Oriented DEA Results:

     +--------------------------------------------------------------+
     | period   dmu     tfpch   effch    techch      pech      sech |
     |--------------------------------------------------------------|
  1. |    1~2     A         1     .75   1.33333         1       .75 |
  2. |    1~2     B       1.5   1.125   1.33333   1.06667   1.05469 |
  3. |    1~2     C   1.33333       1   1.33333         1         1 |
  4. |    1~2     D       .75   .5625   1.33333   .592593   .949219 |
  5. |    1~2     E       1.2      .9   1.33333         1        .9 |
     |--------------------------------------------------------------|
  6. |    2~3     A         1       1         1         1         1 |
  7. |    2~3     B         1       1         1         1         1 |
  8. |    2~3     C         1       1         1         1         1 |
  9. |    2~3     D         1       1         1         1         1 |
 10. |    2~3     E         1       1         1         1         1 |
     +--------------------------------------------------------------+
```

由表二，我们可以得到决策单元每年的 CRS 效率与 VRS 效率。由表三，我们可以得到决策单元在每个阶段的全要素生产率变化 (tfpch) ，相对于 CRS 技术的技术效率变化 (effch)，技术变革 (techch)，相对于VRS技术的纯技术效率变化 (pech) 和规模效率变化 (sech) 这五个指标。

## 结语

现如今 DEA 作为评估组织绩效的管理工具已经得到了相当大的关注，它被广泛用于评估银行、航空公司、医院、大学和制造业等公共部门和私营部门的效率中。而 `dea` 命令和 `malmq` 命令使得决策单元的效率可以在 Stata 中直接进行评估，不再需要同时使用统计分析软件与数据包络分析软件，大大简便了操作。我们可以看到当下很多评估组织效率的论文都使用了 Stata 求解 DEA 模型。然而，这两条命令只能指定上文提及的这些相对基础的模型，要指定 FG 模型、ST 模型、CCGSS 加法模型和具有无穷多个 DMU 的 CCW 模型，还需要对命令作进一步的优化和改进。


### 文献来源


> [1]Charnes, A., W. W. Cooper, and E. Rhodes. (1978) Measuring the Efficiency of Decision Making Units. European Journal of Operational Research 2: 429–444.[[PDF]](https://gitee.com/Stata002/StataSX2018/raw/master/2019C/%E5%BD%AD%E8%AF%97%E6%95%8F-%E4%B8%AD%E5%A4%A7/Measuring_the_efficiency_of_decision%EF%BC%8Dmaking_units.pdf)   
> [2]Banker, R. D., A. Charnes, and W. W. Cooper. (1984) Some Models for Estimating Technical and Scale Inefficiencies in Data Envelopment Analysis. Management Science 30: 1078–1092.[[PDF]](https://gitee.com/Stata002/StataSX2018/raw/master/2019C/%E5%BD%AD%E8%AF%97%E6%95%8F-%E4%B8%AD%E5%A4%A7/Some%20Models%20for%20Estimating%20Technical%20and%20Scale%20Inefficiencies.pdf)  
> [3]Yong-bae Ji, and Choonjo Lee. (2010) Data Envelopment Analysis. Stata Journal, 10(2): 267–280. [[PDF]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1001000207)  
> [4]Kyong-Rock Lee, and Byung-Ihn Leem. (2011) Malmquist Productivity Analysis Index using DEA frontier in Stata. The Stata Journal.[[PDF]](https://gitee.com/Stata002/StataSX2018/raw/f36f42ca3f2e4f1c21842e8d6f8e20d1189405ef/2019C/%E5%BD%AD%E8%AF%97%E6%95%8F-%E4%B8%AD%E5%A4%A7/Malmquist_Productivity_Index_using_DEA_frontier_in_Stata.pdf)