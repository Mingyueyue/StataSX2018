
&emsp;

> 作者：胡国恒 (武汉大学社会保障中心)
>     
> Stata 连享会：  [知乎](https://zhuanlan.zhihu.com/arlion) | [简书](http://www.jianshu.com/u/69a30474ef33) | [码云](https://gitee.com/arlionn) | [CSDN](https://blog.csdn.net/arlionn) | [StataChina公众号](https://mp.weixin.qq.com/s/RwkuPpLS7bI5C5OhRqjkOw) 
  

> Stata连享会 &emsp; [计量专题](https://gitee.com/arlionn/Course/blob/master/README.md)  || [精品课程](https://mp.weixin.qq.com/s/hWtncj56PeFNL4yg2-va0Q) || [简书推文](https://www.jianshu.com/p/de82fdc2c18a)  || [公众号合集](https://mp.weixin.qq.com/s/RwkuPpLS7bI5C5OhRqjkOw)

[![点击查看完整推文列表](https://images.gitee.com/uploads/images/2019/1017/160259_f1d8cae5_5343056.png)](https://mp.weixin.qq.com/s/RwkuPpLS7bI5C5OhRqjkOw)


#### 摘要：数据处理过程中，研究者常会遇到收集的数据与预期采用的工具不匹配问题。目前，常用的方式是 Datatransfer 等数据格式转换器，通过导入及导出实现数据格式的转换，缺点是软件收费以及当代转换的数据多时，耗时耗力。鉴于此，本文借助于 python 以及 python 与 Stata 相结合的方法提出两种可实现批量数据格式快速转换的方案。

## 方案一：python 批量转换数据格式

#### 思路 1
首先借助于 python 中 os 库实现对电脑存储下的文件进行调用和处理，生成文件的路径。其次，借助于 python 中强大的 pandas 库读取文件路径（ pandas 支持 Stata、SAS、CSV、JSON、SQL 等 15 中常见的数据格式）。最后，借助 pandas 将读取的文件存储为研究者想要的格式即可。

#### 具体介绍 1
python 中 os 是常见的路径操作的模块，可实现对系统下文件的读取、写入以及创建文件等多种操作。本文用到的是 `os.listdir(path='.')` ,该函数可实现提取路径 （**path**） 下所有文件的文件名（此处的文件名包括数据格式），并将其作为列表储存。获得路径下所有文件的文件名后，借助 `for` 循环列表将文件名依次取出并与路径相结合形成具体文件的路径，以便后续 pandas 的读取。文件路径的合并采用的函数是 `os.path.join(path,*paths)` ,该函数可智能的连接一个或多个路径组件。形成具体文件路径后，借助 pandas 读取，可依据文件格式选择 pandas 的 os tool 中的读存方式即可（多种格式数据的读存方式见下图 1 ）。
![图1：pandas支持的文件格式以及读存取命令](https://images.gitee.com/uploads/images/2019/1017/160332_e601a60c_5343056.png "屏幕截图.png")
**图片来源：** https://pandas.pydata.org/pandas-docs/stable/user_guide/io.html

#### 案例 1
作者收集数据为 CHNS ，该数据原版数据为 SAS 格式。笔者长期采用的数据分析软件为 Stata ，需将 SAS 数据转换为Stata数据，格式由 sas7bdat 转换为 dta 。按照具体介绍，作者预先设置 **FindPath** 为原SAS数据存储的路径，然后借助 `listdir()` 查询路径下所有文件名，并将其返回列表 **FileNames** 存储。其次，遍历 **FileNames** ，提取 **FindPath** 路径下每一文件名与 **FindPath** 路径连接。合成具体文件的路径**fullfilename** 后，开始借助 **pandas** 进行读取，因原文件是 sas7bdat 格式，故采用 `read_sas()` ，预转换格式为 dta ，所有存储方式为 `.to_stata()` 。在存储过程中本文用到了切片，原因在于 `listdir()` 提取的 **filename** 是包括文件名的格式的，因此在存储时要把原文件名的后缀格式去除，因 .sas7bdat 是 9 位数，所以切片为 `[:-9]` 。另外在保存数据过程中，如不设置，默认保存，如需设置，可在保存时加入保存路径即可。

上述方案的优点是执行速度快，免去手动拖拽的麻烦与时间消耗。作者实践 49 个 SAS 转 Stata 文件仅需数秒。缺点是原文件中对数据名的标签会丢失，针对比较熟悉的数据，可忽略此缺陷。针对此缺点，目前作者尚未寻找 python 的解决办法。

具体代码如下：
```Python 
import pandas as pd 
import os
FindPath = 'C:/Users/nuonu/Desktop/abc/'  #python中路径表示与常规路径不同
FileNames = os.listdir(FindPath)   #返回指定路径下的文件和文件夹列表
for file_name in FileNames:
    fullfilename=os.path.join(FindPath,file_name)    
    df=pd.read_sas(fullfilename)
    df.to_stata(file_name[:-9]+'.dta') #切片是为了去除原格式（.sas7bdat）
```  
**借鉴链接：** [@ 小_小_杨_](https://blog.csdn.net/u012235274/article/details/51315214) 、[ os 库](https://docs.python.org/3.9/library/os.path.html)、[pandas库](https://pandas.pydata.org/pandas-docs/stable/)  
**借鉴内容：** @小小杨：某文件夹下所有文件名的提取。 OS 库与 pandas 库的命令操作语法。

**缺陷：原文件中的 label 信息会丢失**

保存路径也可自行设置，如：

```
df.to_stata('C:/Users/nuonu/Desktop/Stata/'+file_name[:-9]+'.dta')
```

## 方案二： python 和 Stata 联合转换数据格式
#### 思路 2
鉴于方法 1 数据转换中无法保存数据名 label 的现实问题，作者发现 Stata16 中可导入 Excel、SPSS、SAS、CSV 等多种格式文件，并且能有效保存数据的 label 。因此，笔者想到可借助 python 批量产生 Stata 导入和导出数据的命令，然后放入 Stata 的 do_file 中 `ctrl+D` 执行，以期实现数据的转换与 label 的保存。

#### 具体介绍 2
因具体介绍 1 中已介绍 OS 模块中 `listdir()` 的功能，故不在赘述。`import` 与 `save` 是 Stata 中导入和导出的命令，常规的命令规范是 `import sas [using] filename` ，其中 `sas` 表示的是数据文件，如果导入是 `excel` 则替换为 `excel`，**filename** 是要导入的文件名。导入后，存储为 Stata 数据时，可直接 `save，replace` 或指定路径进行存储。如果需存储为其他数据格式，可使用 `export` 格式，目前支持  SAS、CSV 等格式。导出命令举例： `export delimited using "H:\a.csv", replace` ； `delimited` 是需要导出的数据文件类型 `H：\` 为存储路径，**a** 为文件名称。

#### 案例 2
与案例 1 目的一致，仍是 SAS 转 Stata 数据。因需导入 Stata 进行数据转换，考虑到 Stata 与 python 绝对路径表达方式的不同，故采用转义符 `\` 表示，`\\` 生成结果时，会只保留一个 `\` 。如案例 1 ，先设置文件夹路径 **FindPath** 。其次，借助 `listdir()` 查询文件夹中文件名，返回列表。接着，将未添加文件绝对路径的导入命令 `import sas using "C:\Users\nuonu\Desktop\\SAS\` 赋值为 **a** ，将未添加文件保存路径`save "C:\\Users\\nuonu\\Desktop\\Stata\\` 赋值为 **b** 。最后，遍历循环文件名并分别与 **a**，并加闭合双引号和 `,clear` 形成文件导入文件绝对路径；将 **b** 与去除文件格式后的遍历文件名相加，并加预保存的文件格式名与闭合双引号。可运行程序，即可形成可放入 Stata 中执行的命令，如图 3 。

具体操作如下：
```
import os
FindPath = 'C:\\Users\\nuonu\\Desktop\\SAS\\'
FileNames = os.listdir(FindPath)   #返回指定路径下的文件和文件夹列表
for file_name in FileNames:
    a='import sas using "C:\\Users\\nuonu\\Desktop\\SAS\\'
    b='save "C:\\Users\\nuonu\\Desktop\\Stata\\'
    print(a+file_name+'"',',clear') 
    print(b+file_name[:-9]+'.dta'+'"')
```   
**借鉴链接：** [@ 小_小_杨_](https://blog.csdn.net/u012235274/article/details/51315214) 、[ os 库](https://docs.python.org/3.9/library/os.path.html)、[pandas库](https://pandas.pydata.org/pandas-docs/stable/)  
**借鉴内容：** @小小杨：某文件夹下所有文件名的提取。OS 库与 pandas 库的命令操作语法。

执行后结果如图：

![图3：执行结果](https://images.gitee.com/uploads/images/2019/1017/160259_c51cac5c_5343056.png)

将执行结果 copy 后，放入 do 文档；注意：在执行 do 文档时，先导入一次 SAS 文件，目的是让 Stata 设置 `import` 的默认路径；否则会报错。

![在这里插入图片描述](https://images.gitee.com/uploads/images/2019/1017/160259_b6c1fea6_5343056.png)

`Ctrl+D` 执行；即可；结果如下图：
![输入图片说明](https://images.gitee.com/uploads/images/2019/1019/193831_019e15bb_5343056.png "bfbe222857e4095fd5194f553619e35.png")

## 总结：
上述两种办法皆是基于 python 实现的数据转换。方案一是以 python 为基础，借助 pandas 对数据的读存进行转换。其优点是读存速度快，几行代码一站式解决数据转换；缺点是数据原有 label 会消失，导致数据分析时存在变量识别障碍。方案二是以 python+ Stata 为基础，借助 python 生成 Stata 命令，在 Stata 中执行。其优点是数据保存完整；缺点是耗时长，须命令腾挪。二者的共同之处是不同数据的转换只需变化 python 中的数据格式即可。


**参考资料：**
>1、https://pandas.pydata.org/pandas-docs/stable/user_guide/io.html  
>2、https://docs.python.org/3/library/os.html?highlight=listdir#os.listdir  
>3、https://blog.csdn.net/u012235274/article/details/51315214
