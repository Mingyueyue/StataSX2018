> #### [连享会：内生性问题及估计方法专题](https://mp.weixin.qq.com/s/FWpF5tE68lAtCEvYUOnNgw)
>[![连享会-内生性专题现场班-2019.11.14-17](https://images.gitee.com/uploads/images/2019/0918/162119_290eadd0_1522177.png)](https://gitee.com/arlionn/Course/blob/master/2019Spatial.md)


&emsp;

> 作者：王美庭 (中南民族大学经济学院)    
>     &emsp;     

> **本文的目的：** 当 Stata 官方提供的 `unicode *` 命令组和 连老师提供的  `ua` 命令执行完后仍然存在乱码时，可以试试本文的解决方案。本文的主要代码来自于[「爬虫俱乐部 精通Stata之数据整理」](https://ke.qq.com/course/286526)，在其基础上进行了一些改进 (详情参见本文第三部分)，主要包括两个方面：其一，可以对「数字-文字对应表」中的中文乱码进行自动转码。其二，支持批量多文件(夹)转码。

&emsp;
> #### [2019金秋十月-空间计量专题班，杨海生主讲，成都](https://mp.weixin.qq.com/s/aU6B9HZaf2BSHF7lSLe6LA)
>[![](https://images.gitee.com/uploads/images/2019/0918/162119_5346fb67_1522177.jpeg)](https://mp.weixin.qq.com/s/aU6B9HZaf2BSHF7lSLe6LA)

&emsp;

## 1. 问题阐述

时至今日，Stata 已经进入 16 时代代，各项功能日益完善。然而，对于广大中文老用户而言，仍然存在一个历史性问题——**转码**。
 
 这一切来源于 Stata 14 跨时代地全面启用了适用性更广的 **UTF-8** 编码格式，从而保证我们的 **dofile**，**.dta**, **.hlp** 等文件中可以支持各种语言和字符，非英语用户再也不用一定使用英语字母作为变量名了。

历史遗留问题在于，对于国内用户，使用 Stata 13 及早期版本保存的 do 文件和 .dta 文件一般为 **gb2312** 或 **gbk** 或 **gb18030** 编码，而 Stata 14 及高级版本采用的是 **UTF-8** 编码。就像英文单词 `he`，在英语里的意思是`他、男性，男子；雄性动物`，而在汉语拼音中，可能被认为是`和、何、禾`等，所以同样的文字，在不同的编码下，将有不同的含义。于是，国内用户早期版本 Stata 保存下来的 do 文件和 .dta 文件在高级版本 Stata 下那些非英文字符出现乱码也是自然而然的事。

Stata 官方和用户们都给出了一些解决方案。比如，针对国内用户，官方提供了 `unicode encoding set gb18030` 和 `unicode translate *` 命令组，以便实现转码 (从编码 **gb18030** 转码至 **UTF-8** ) 命令，以及连玉君老师编写的 `ua` 命令对其的扩展（这里要注意的是，由于 **gb18030** 编码包含 **gbk** ，而 **gbk** 又包括 **gb2312**，所以针对国内用户，转码前设置编码 **gb18030** 即可）。

然而，当文件中包含了不可转换字符，则会导致以上命令无法奏效。当然，我们也可以加上 `invalid` 选项以保证上述命令被强制执行，但这样很可能会导致数据的部分位置被修改（而我们又不知道哪里被修改了），从而对后期的数据分析产生影响。

## 2. 解决方案
对于以上问题，在这里我们是怎么处理的呢？

对于 do 文件，处理还是比较简单的，我们在后期版本中打开时软件会自动提示选择何种编码打开，此时我们国内一般选择 **gb18030** 即可。本文重点在于解决 .dta 文件的转码。我们知道，.dta 文件的乱码一般会出现在**数据标签、变量名、变量内容、变量标签、值标签**。因此，对于每一个 .dta 文件，我们只需对这些部分利用相关的转码函数进行转码即可。这里看似与官方命令重复了，其实重要区别是如果 .dta 存在不可转换的字符，利用本文的做法，你就能发现不可转换的字符存在何处，然后通过本身对于数据的现有信息，手动更正即可（而对于官方命令，你不知道这里无法转换的字符在何处）。
      
我们最终的目的在于对于当前文件夹、子文件夹、子子文件夹中的所有 .dta 文件进行转码（由 **gb18030** 转至 **UTF-8** ），如果刚开始就按照这个思维去写程序，这是比较困难的。我们先从单个文件入手，再进入单个文件夹，最后再对三层嵌套文件夹进行操作。

> Stata连享会 &emsp; [计量专题](https://gitee.com/arlionn/Course/blob/master/README.md)  || [精品课程](https://mp.weixin.qq.com/s/hWtncj56PeFNL4yg2-va0Q) || [简书推文](https://www.jianshu.com/p/de82fdc2c18a)  || [公众号合集](https://mp.weixin.qq.com/s/RwkuPpLS7bI5C5OhRqjkOw)
[![点击查看完整推文列表](https://images.gitee.com/uploads/images/2019/0918/162119_4105880b_1522177.jpeg)](https://mp.weixin.qq.com/s/RwkuPpLS7bI5C5OhRqjkOw)

### 2.1 转码程序——单个 .dta 文件

#### 使用说明

这里的**目的**在于将单个 .dta 文件由 **gb18030** 编码转换至  **UTF-8** 编码。前面已经说过，.dta 文件可能出现乱码的地方有：数据标签、变量名、变量内容、变量标签、值标签。因此我们用 Stata 对这些可能出现乱码的地方依次进行转码，最后再将转码后的数据替换原来的数据，以防万一，以下程序还备份了原来的 .dta 文件。

具体的注意事项如下：

1. 在当前目录下生成了 **backup files** 子文件夹。
2. 将原 .dta 数据保存至了 **backup files** 子文件夹中。
3. 转码后 **UTF-8** 格式的 .dta 数据，覆盖了当前文件夹的原数据。
4. 此程序 **只能转一遍，只能转一遍，只能转一遍**！否则原数据将丢失，且数据重新变回乱码。
5. 转换后若还存在乱码（一般只有少数），则这些乱码就是所谓的利用官方命令（`unicode translate *`）无法转换的字符，需要加 `invalid` 选项才能继续运行命令，但这样将造成数据被更改和丢失。而在这里，运行以下程序后数据本身并没有发生变化（只是编码发生了变化），而且我们还能定位到发生乱码的具体位置，接着我们只需要根据数据本身的信息（如调查问卷的阐述）对其中存在的少量的乱码进行手动修改即可，以**保证数据的原始性**。
6. 程序有“用户修改部分”和“无需变更部分”，顾名思义，用户只需在“用户修改部分”修改少量的程序，即可把以下代码用在自己的数据中。

```stata
*---------------------------用户修改部分---------------------------------------
cd "D:\stata15\ado\personal\Net_course\"
local dta_file "gdpChina.dta"


*----------------------------无需变更部分--------------------------------------
use `dta_file', clear 

*将原dta文件备份至"backup files"文件夹中
cap mkdir "backup files"
copy `dta_file' "backup files\", replace

*对数据标签进行转码
local data_lbl: data label
local data_lbl = ustrfrom("`data_lbl'", "gb18030", 1)
label data "`data_lbl'"

*对变量名、变量标签、字符型变量取值转码
foreach v of varlist _all {
	* 对字符型变量取值进行转码
	local type: type `v'   //将变量的类型放入宏type中
	if strpos("`type'", "str") {
		replace `v' = ustrfrom(`v', "gb18030", 1)   //如果变量是字符型变量，使用ustrfrom()函数进行转码
	}

	* 对变量标签进行转码
	local lbl: var label `v'   //将变量的标签放入宏lbl中
	local lbl = ustrfrom("`lbl'", "gb18030", 1)   //使用ustrfrom()函数对`lbl'转码
	label var `v' `"`lbl'"'   //将转码后的字符串作为变量的标签

	* 对变量名进行转码
	local newname = ustrfrom(`"`v'"', "gb18030", 1)   //使用ustrfrom()函数将变量名字符串进行转码
	qui rename `v' `newname'   //将转码后的字符串重命名为变量名
}

*下面对数值标签进行转码
qui label save using label.do, replace   //将数值标签的程序保存到label.do中（如果原来dta数据是gb18030编码，则这里导出的do文件也是gb18030编码）
preserve //将do文件的内容用txt的方式导入一个变量之中
qui import delimited using label.do, ///
  varnames(nonames) delimiters("asf:d5f14d5d",asstring) encoding(gb18030) clear
qui describe
if r(N) == 0 {
		restore
		save `dta_file', replace
	}
	else {
		qui levelsof v1, local(v1_lev)
		restore
		foreach label_modify of local v1_lev {    //这个的foreach与前面的local(v_lev)对应，细节可查看帮助文件
			`label_modify'      //依次对原数据执行值标签替换操作
		} 
		save `dta_file', replace  //将转码好的dta文件替换原来的dta文件
	}
	
* 删除中间临时文件
erase label.do

```

### 2.2 转码程序——单个文件夹中的所有 .dta 文件

#### 使用说明
这里的**目的**在于，将单个文件夹的所有 .dta 文件由 **gb18030** 编码转换至 **UTF-8** 编码。

具体的注意事项同 **2.1 节**一样。

```stata

*------------------------用户修改部分--------------------------------
// cd "D:\stata15\ado\personal\Net_course"


*-------------------------无需变更部分--------------------------------
local dir_dta: dir . files "*.dta", respectcase //windows系统需要加respectcase选项以区分大小写

cap mkdir "backup files"  //建立备份文件夹
foreach dta_file of local dir_dta {
	qui copy `dta_file' "backup files\", replace //将当前文件夹的dta数据备份至"backup files"文件夹中
	qui use `dta_file', clear
	
	*对数据标签进行转码
	local data_lbl: data label
	local data_lbl = ustrfrom("`data_lbl'", "gb18030", 1)
	label data "`data_lbl'"
	
	*对变量名、变量标签、字符型变量取值转码
	foreach v of varlist _all {
			* 对字符型变量取值进行转码
			local type: type `v'   //将变量的类型放入宏type中
			if strpos("`type'", "str") {
				replace `v' = ustrfrom(`v', "gb18030", 1)   //如果变量是字符型变量，使用ustrfrom()函数进行转码
			}
		* 对变量标签进行转码
		local lbl: var label `v'   //将变量的标签放入宏lbl中
		local lbl = ustrfrom("`lbl'", "gb18030", 1)   //使用ustrfrom()函数对`lbl'转码
		label var `v' `"`lbl'"'   //将转码后的字符串作为变量的标签
		* 对变量名进行转码
		local newname = ustrfrom(`"`v'"', "gb18030", 1)   //使用ustrfrom()函数将变量名字符串进行转码
		qui rename `v' `newname'   //将转码后的字符串重命名为变量名
	}

	*下面对数值标签进行转码
	qui label save using label.do, replace   //将数值标签的程序保存到label.do中
	preserve
	*- 将do文件的内容用txt的方式导入一个变量之中，然后再对该变量进行拉直
	qui import delimited using label.do, ///
	  varnames(nonames) delimiters("asf:d5f15g4dsf9qw8d4d5d",asstring) encoding(gb18030) clear
	qui describe
	if r(N) == 0 {
		restore
		save `dta_file', replace
	}
	else {
		qui levelsof v1, local(v1_lev)
		restore
		foreach label_modify of local v1_lev {    //这个的foreach与前面的local(v_lev)对应，细节可查看帮助文件
		`label_modify' //依次对原数据执行值标签替换操作
		} 
		save `dta_file', replace  //将转码好的dta文件替换原来的dta文件
	}
		
	* 删除中间临时文件
	erase label.do
}

```

### 2.3 转码程序——三层嵌套文件夹下的所有 .dta 文件

#### 使用说明
这里的**目的**在于，将当前文件夹、所有子文件夹、以及所有子子文件夹下的所有 .dta 文件由 **gb18030** 编码转换至 **UTF-8** 编码。

具体的注意事项除了 **2.1 节**的以外，其他的还有：
1. 这部分程序嵌套了**转码\_单个文件夹.do**文件（为了程序结构的易读性），于是用户在使用这部分程序之前，需要将 **2.2 节**的代码部分打包成**转码\_单个文件夹.do**文件（内容无需更改，直接复制粘贴即可），然后将其放在D盘根目录下。
2. 在“用户修改部分”将路径修改为自己要转码的路径即可。

```stata
*------------------------用户修改部分--------------------------------
cd "D:\stata15\ado\personal\Net_course"


*-------------------------无需变更部分--------------------------------
*将当前文件夹下的所有dta文件进行转码（第一层）
do "D:\转码_单个文件夹.do"

*将当前子文件夹下的所有dta文件进行转码（第二层）
local dir_list : dir . dirs "*", respectcase
foreach subdir of local dir_list {
	if "`subdir'" == "backup files" {
		continue //保留备份的原文件不动
	}
	cd "`subdir'"
	do "D:\转码_单个文件夹.do"
	
	*将当前子子文件夹下的所有dta文件进行转码（第三层）
	local dir_list2 : dir . dirs "*", respectcase
	foreach subdir2 of local dir_list2 {
		if "`subdir2'" == "backup files" {
			continue //保留备份的原文件不动
		}
		cd "`subdir2'"
		do "D:\转码_单个文件夹.do"
		qui cd ..
	}
	qui cd ..	
}
```

## 3. 引用说明

**这里先表示一下谢意，文中引用了部分代码却没有做一个说明。同时在这里也先感谢原作者（爬虫俱乐部），正是你们的代码，给了本文以灵感。**

下面进入正题：以上程序中部分代码借鉴了 [「爬虫俱乐部-精通Stata之数据整理」](https://ke.qq.com/course/286526) 腾讯课程。这里将其原代码列示如下：
```stata
* 构造数据
clear
cd "E:\直播课程\转码\ustrfrom"
set obs 2
gen _变量1 = "爬虫俱乐部" in 1
replace _变量1 = "将爬虫进行到底" in 2   //变量名与字符型变量的取值
gen v = _n
label var v "标签变量"   //变量标签
label define vlab 1 "是" 2 "否"   //值标签
label values v vlab
* 构造的数据中，变量名、字符型变量的取值、变量标签、值标签中都出现了中文，保存成gb18030编码后，都会成为乱码
saveascii 转码.dta, replace encoding(gb18030) version(13)

* 转码
use 转码.dta, clear
foreach v of varlist _all {
	* 对字符型变量取值进行转码
	local type: type `v'   //将变量的类型放入宏type中
	if index("`type'", "str") {
		replace `v' = ustrfrom(`v', "gb18030", 1)   //如果变量是字符型变量，使用ustrfrom()函数进行转码
	}

	* 对变量标签进行转码
	local lbl: var label `v'   //将变量的标签放入宏lbl中
	local lbl = ustrfrom("`lbl'", "gb18030", 1)   //使用ustrfrom()函数对`lbl'转码
	label var `v' `"`lbl'"'   //将转码后的字符串作为变量的标签

	* 对变量名进行转码
	local newname = ustrfrom(`"`v'"', "gb18030", 1)   //使用ustrfrom()函数将变量名字符串进行转码
	rename `v' `newname'   //将转码后的字符串重命名为变量名
}
* 变量名、变量标签、字符型变量的取值都转码完成，乱码还剩下数值标签
label save using label.do, replace   //将数值标签的程序保存到label.do中
*****将label.do打开，将程序复制过来*****
label define vlab 1 `"是"', modify
label define vlab 2 `"否"', modify
********这段程序将对数值标签转码********
****************转码完成****************
```

对于以上代码，本文的改进之处如下：
- **可以支持对「数字-文字对应表」中的中文乱码进行转码。**  爬虫俱乐部提供的代码重点解决了 **.dta** 数据文件中 **变量名、变量内容、变量标签、数字-文字对应表** 可能存在的乱码，事实上，**数据标签** (Stata 中的定义方式为 `label data "xxx"`) 也可能存在乱码，在使用一份数据之前，能够提前知道其大致的内容是很重要的，而 **数据标签** 就起着这样的功能。
- **自动对 .dta 文件中「数字-文字对应表」中的中文标签进行转码**：原代码需要手动复制粘贴，操作上繁复且易出错。经本文测试，**.do** 文件可以用 `import delimited` 导入，再通过一系列的判断语句和循环语句，真正实现自动化一次性转码完成。
- **支持批量多文件(夹)转码。** 原代码只针对单个文件进行转码，本文方法不仅可以针对单个文件，而且可以对当前目录的所有 .dta 文件、当前目前目录下子文件夹下的所有 .dta 文件以及子子文件夹（三层嵌套）下的所有 .dta 文件进行一次性转码**。

### 附录

以上三部分代码的Stata do文件如下：

- [Stata转码_单个文件](https://gitee.com/Stata002/StataSX2018/raw/master/2019C/%E7%8E%8B%E7%BE%8E%E5%BA%AD_%E4%B8%AD%E5%8D%97%E6%B0%91%E6%97%8F/%E6%8E%A8%E6%96%871%EF%BC%9AStata%E8%BD%AC%E7%A0%81-%E9%99%84%E4%BB%B6/Stata%E8%BD%AC%E7%A0%81_%E5%8D%95%E4%B8%AA%E6%96%87%E4%BB%B6.do)
- [Stata转码_单个文件夹](https://gitee.com/Stata002/StataSX2018/raw/master/2019C/%E7%8E%8B%E7%BE%8E%E5%BA%AD_%E4%B8%AD%E5%8D%97%E6%B0%91%E6%97%8F/%E6%8E%A8%E6%96%871%EF%BC%9AStata%E8%BD%AC%E7%A0%81-%E9%99%84%E4%BB%B6/Stata%E8%BD%AC%E7%A0%81_%E5%8D%95%E4%B8%AA%E6%96%87%E4%BB%B6%E5%A4%B9.do)
- [Stata转码_三层嵌套文件夹](https://gitee.com/Stata002/StataSX2018/raw/master/2019C/%E7%8E%8B%E7%BE%8E%E5%BA%AD_%E4%B8%AD%E5%8D%97%E6%B0%91%E6%97%8F/%E6%8E%A8%E6%96%871%EF%BC%9AStata%E8%BD%AC%E7%A0%81-%E9%99%84%E4%BB%B6/Stata%E8%BD%AC%E7%A0%81_%E4%B8%89%E5%B1%82%E5%B5%8C%E5%A5%97%E6%96%87%E4%BB%B6%E5%A4%B9.do)

### 主要参考资料

- 爬虫俱乐部 [精通Stata之数据整理](https://ke.qq.com/course/286526)

&emsp;

>#### 关于我们

- **「Stata 连享会」** 由中山大学连玉君老师团队创办，定期分享实证分析经验， 公众号：**StataChina**。
- 公众号推文同步发布于 [CSDN](https://blog.csdn.net/arlionn) 、[简书](http://www.jianshu.com/u/69a30474ef33) 和 [知乎Stata专栏](https://www.zhihu.com/people/arlionn)。可在百度中搜索关键词 「Stata连享会」查看往期推文。
- 点击推文底部【阅读原文】可以查看推文中的链接并下载相关资料。
- **欢迎赐稿：** 欢迎赐稿。录用稿件达 **三篇** 以上，即可 **免费** 获得一期 Stata 现场培训资格。
- **E-mail：** StataChina@163.com
- 往期推文：[计量专题](https://gitee.com/arlionn/Course/blob/master/README.md)  || [精品课程](https://mp.weixin.qq.com/s/hWtncj56PeFNL4yg2-va0Q) || [简书推文](https://www.jianshu.com/p/de82fdc2c18a)  || [公众号合集](https://mp.weixin.qq.com/s/AWxDPvTuIrBdf6TMUzAhWw) 

[![点击此处-查看完整推文列表](https://images.gitee.com/uploads/images/2019/0918/162119_4116da55_1522177.png)](https://mp.weixin.qq.com/s/RwkuPpLS7bI5C5OhRqjkOw)


---
![欢迎加入Stata连享会(公众号: StataChina)](https://images.gitee.com/uploads/images/2019/0918/162119_d40bf61d_1522177.jpeg)
