
&emsp;

> 作者：王美庭 (中南民族大学经济学院)  

> Stata连享会 &emsp; [计量专题](https://gitee.com/arlionn/Course/blob/master/README.md)  || [精品课程](https://mp.weixin.qq.com/s/hWtncj56PeFNL4yg2-va0Q) || [简书推文](https://www.jianshu.com/p/de82fdc2c18a)  || [公众号合集](https://mp.weixin.qq.com/s/RwkuPpLS7bI5C5OhRqjkOw)

[![点击查看完整推文列表](https://images.gitee.com/uploads/images/2019/0924/231607_b8788e7f_1522177.png)](https://mp.weixin.qq.com/s/RwkuPpLS7bI5C5OhRqjkOw)



> #### [连享会：内生性问题及估计方法专题](https://mp.weixin.qq.com/s/FWpF5tE68lAtCEvYUOnNgw)

[![连享会-内生性专题现场班-2019.11.14-17](https://images.gitee.com/uploads/images/2019/0924/231607_ddb4a24e_1522177.png)](https://gitee.com/arlionn/Course/blob/master/Done/2019Endog.md)







|              &ensp;      	| asdoc 	| xxx2docx 	| out_log 	| esttab 	|
|:-------------------	|:-----	|:--------	|:-------	|:------	|
|    描述性统计输出   	|  ★★★★ 	|   ★★★★★  	|   ★★★★  	|  ★★★★★ 	|
| 分组 T 均值检验输出 	|   ★   	|    ★★★   	|   ★★★   	|  ★★★★★ 	|
|   相关系数矩阵输出  	|   ★★  	|   ★★★★   	|   ★★★   	|  ★★★★★ 	|
|     回归结果输出    	|   ★★  	|   ★★★★   	|   ★★★★  	|  ★★★★★ 	|
|     Word 整体性     	| ★★★★★ 	|   ★★★★★  	|   ★★★   	|  ★★★★★ 	|
|    LaTeX 代码输出   	|   -   	|     -    	|   ★★★   	|  ★★★★★ 	|
|       综合评价      	|   ★★  	|   ★★★★   	|   ★★★   	|  ★★★★★ 	|

&emsp;

## 1. 本文目的
目前 Stata 有着众多的实证结果输出命令，连享会对于 [asdoc](https://blog.csdn.net/arlionn/article/details/83500601)、[xxx2docx系列](https://www.jianshu.com/p/97c4f291ee1e)、[outreg2](http://www.peixun.net/author/3.html)、[logout](http://www.peixun.net/author/3.html) 和 [esttab](http://www.peixun.net/author/3.html) 都有讲解，不熟悉的同学可以先去了解。然而，这么多的命令，各有其特点，往往让我们眼花缭乱。到底哪一种真正适合自己？这也许是很多同学都会提出的问题。

我们的测评结果呈现于文首的表格中，下文展开详细分析。

一般而言，我们将实证结果导入到 **Word** 的最多，如果要做学术报告或者发 SSCI，那可能还需要用到 **LaTeX**，所以本文**以 Word 和 LaTeX 为基础**，**目的在于**：从我们实证论文常用到的**描述性统计**、**分组 T 均值检验**、**相关系数矩阵**和**回归结果**四个方面出发，去评判以上几种常见输出命令的优劣，最后再给出一个总结。

> **整体说明和注意事项：**
> 
> 由于以上所有命令都支持 **Word** 输出，而只有部分命令支持 **LaTeX** 输出，且  **LaTeX** 输出命令中只有少量与 **Word** 输出的命令不一致，所以我们在 3 - 6 节均以输出到 **Word** 的角度去阐述和评价，而在第 7 节再统一以 **LaTeX** 的角度去阐述和评价。
>
> 另外，`outreg2` 只支持描述性统计输出和回归结果输出，而 `logout` 支持分组 T 均值检验和相关系数矩阵的输出，且两者命令结构相似，且都来自同一个作者（Roy Wada），于是本文将这两个命令合并到一起看作一个命令进行讲解（简称 `out_log` 命令）。**于是本文要评价的输出命令为：`asdoc`、`xxx2docx`、`out_log` 和 `esttab` 四个命令**。
>
> 以上命令都可以通过 `ssc install 命令名称, replace` 安装更新，本文在测试时均采用了行文时的最新版本。

## 2. 数据的前期处理

```stata
sysuse "nlsw88.dta", clear 
tabulate race, gen(race_num)
drop race_num1
```

## 3. 描述性统计输出
### 3.1 命令展示与描述
#### 3.1.1 asdoc

```stata
local varlist "wage age race married grade collgrad south union occupation"
asdoc sum `varlist', save(Myfile.doc) replace ///
	stat(N mean sd min p50 max)  dec(3) ///
	title(asdoc_Table: Descriptive statistics)
```
![asdoc_Table: Descriptive statistics](https://images.gitee.com/uploads/images/2019/0924/231607_8ca1d749_1522177.png)


> **命令特点及存在的问题**：
> 1. 在将结果导入到 **Word** 的同时，Stata 界面也能看到相应的结果(下同)。
> 2. 该命令不支持中文。
> 3. 若有字符串变量，命令会报错。

#### 3.1.2 sum2docx

```stata
local varlist "wage age race married grade collgrad south union occupation"
sum2docx `varlist' using Myfile.docx,replace ///
	stats(N mean(%9.2f) sd(%9.3f) min(%9.2f) median(%9.2f) max(%9.2f)) ///
	title(sum2docx_Table: Descriptive statistics)
```
![sum2docx_Table: Descriptive statistics](https://images.gitee.com/uploads/images/2019/0924/231607_48c63079_1522177.png)


> **命令特点及存在的问题**：
> 1. 在将结果导入到 **Word** 的同时，Stata 界面不能看到相应的结果(下同)。
> 2. 该命令支持中文。
> 3. 若有字符串变量，命令会报错。
> 4. 能分别设置每个统计量的小数点位数。

#### 3.1.3 outreg2

```stata
local varlist "wage age race married grade collgrad south union occupation"
outreg2 using Myfile, sum(detail) replace word eqkeep(N mean sd min p50 max) ///
	fmt(f) keep(`varlist') sortvar(wage age grade) ///
	title(outreg2_Table: Descriptive statistics)
```
![outreg2_Table: Descriptive statistics](https://images.gitee.com/uploads/images/2019/0924/231607_5285ca10_1522177.png)

> **命令特点及存在的问题**：
> 1. 在将结果导入到 **Word** 的同时，Stata 界面能看到相应的结果(下同)。
> 2. 该命令不支持中文。
> 3. 若有字符串变量，命令会在窗口说明什么变量是字符型，并在报告列表中自动剔除该变量。
> 4. 支持变量排序。

#### 3.1.4 esttab

```stata
local varlist "wage age race married grade collgrad south union occupation"
estpost summarize `varlist', detail
esttab using Myfile.rtf, ///
	cells("count mean(fmt(2)) sd(fmt(2)) min(fmt(2)) p50(fmt(2)) max(fmt(2))") ///
	noobs compress replace title(esttab_Table: Descriptive statistics)
```
![esttab_Table: Descriptive statistics](https://images.gitee.com/uploads/images/2019/0924/231607_dc35e611_1522177.png)


> **命令特点及存在的问题**：
> 1. 在将结果导入到 **Word** 的同时，Stata 界面能看到相应的结果(下同)。
> 2. 该命令不支持中文(有些教程说可以对中文进行 **gb18030** 转码后导入，但本文试验不成功)。
> 3. 若有字符串变量，命令仍会直接运行，在输出列表中字符型变量名会写上去，但后面的统计量为空白。
> 4. 能分别设置每个统计量的小数点位数。

### 3.2 小结
- 在将结果导入到 **Word** 的同时，命令`asdoc`、`outreg2`、`esttab`能在 Stata 界面能看到相应的结果，而`sum2docx`不行(下同)。
- 仅`sum2docx`支持中文，其余命令不支持。
- 若变量里有字符串变量，`outreg2`命令的处理最智能化。
- `sum2docx`和`esttab`能分别设置每个统计量的小数点位数，其他的命令不行。
- 仅`outreg2`支持变量排序。

&emsp;
> #### [连享会计量方法专题……](https://mp.weixin.qq.com/s/hWtncj56PeFNL4yg2-va0Q)



## 4. 分组 T 均值检验输出
### 4.1 命令展示与描述
#### 4.1.1 asdoc

```stata
local common_exp "save(Myfile.doc) by(south) stat(obs mean p)"
asdoc ttest wage, `common_exp' replace title(asdoc_Table: T_test by group)
asdoc ttest age, `common_exp' rowappend 
asdoc ttest race, `common_exp' rowappend
asdoc ttest married, `common_exp' rowappend
asdoc ttest grade, `common_exp' rowappend
asdoc ttest collgrad, `common_exp' rowappend
asdoc ttest union, `common_exp' rowappend
```
![asdoc_Table: T_test by group](https://images.gitee.com/uploads/images/2019/0924/231607_b1ebbec0_1522177.png)


> **命令特点及存在的问题**：
> 1. 每次只能检验一个变量, 当然它可以通过`rowappend`选项进行叠加，但这样麻烦不少。
> 2. 内容不完整，本身没有也无法添加 **MeanDiff** 一列。

#### 4.1.2 t2docx

```stata
local varlist "wage age race married grade collgrad union"
t2docx `varlist' using Myfile.docx,replace ///
	not by(south) title(t2docx_Table: T_test by group)
```
![t2docx_Table: T_test by group](https://images.gitee.com/uploads/images/2019/0924/231608_7b1ed449_1522177.png)


> **命令特点及存在的问题**：
> 1. 表格报告的内容剔除了所输入变量中包含缺漏值的观测值，且该操作无法被禁止。
> 2. 支持中文。
> 3. 有 **MeanDiff** 一列，并默认以`* p < 0.1 ** p < 0.05 *** p < 0.01`的方式标注星号。

#### 4.1.3 logout

```stata
local varlist "wage age race married grade collgrad union"
logout, save(Myfile) word replace: ttable2 `varlist', by(south)
```
![logout_Table: T_test by group](https://images.gitee.com/uploads/images/2019/0924/231607_e89e5acd_1522177.png)


> **命令特点及存在的问题**：
> 1. 能一次性分组 T 均值检验所有变量
> 2. 有 **MeanDiff** 一列，并默认以`* p < 0.1 ** p < 0.05 *** p < 0.01`的方式标注星号
> 3. 该命令不能书写表格标题。

#### 4.1.4 esttab

```stata
local varlist "wage age race married grade collgrad union"
estpost ttest `varlist', by(south)
esttab using Myfile.rtf, ///
	cells("N_1 mu_1(fmt(3)) N_2 mu_2(fmt(3)) b(star fmt(3))") starlevels(* 0.10 ** 0.05 *** 0.01) ///
	noobs compress replace title(esttab_Table: T_test by group) 
```

![esttab_Table: T_test by group](https://images.gitee.com/uploads/images/2019/0924/231608_d9faffcb_1522177.png)


> **命令特点及存在的问题**：
> 1. 能一次性分组 T 均值检验所有变量。
> 2. 有 **MeanDiff** 一列，可设置以`* p < 0.1 ** p < 0.05 *** p < 0.01` 的方式标注星号
> 3. 能分别设置每一列的小数点位数。

### 4.2 小结
- `t2docx` 表格报告的内容剔除了所输入变量中包含缺漏值的观测值，且该操作无法被禁止，研究者在查看输出结果时需要注意。其他的命令无此问题。
- 在将结果导入到 **Word** 的同时，`asdoc`、`logout`、`esttab`命令能在 Stata 界面能看到相应的结果，而`t2docx`不行。
- `logout`、`esttab` 和 `t2docx` 能一次性针对所有变量进行分组 T 均值检验，而`asdoc`需要多行命令，不太方便。
- `asdoc` 输出的内容不完整，没有 **MeanDiff** 一列，更无法设置相应的星号标注，而其他命令无此问题。
- `logout`  无法书写表格标题，而其他命令无此问题。
- `esttab`  能设置每一列的小数点位数，而其他命令不行。
- `t2docx`  支持中文，而其他的命令不行。


&emsp;
> #### [连享会计量方法专题……](https://mp.weixin.qq.com/s/hWtncj56PeFNL4yg2-va0Q)


## 5. 相关系数矩阵输出
### 5.1 命令展示与描述
#### 5.1.1 asdoc

```stata
local varlist "wage age race married grade collgrad"
asdoc cor `varlist', save(Myfile.doc) replace nonum dec(3) ///
	title(asdoc_Table: correlation coefficient matrix)
```
![asdoc_Table: correlation coefficient matrix](https://images.gitee.com/uploads/images/2019/0924/231608_bb8509c4_1522177.png)


> **命令特点及存在的问题**：
> 1. 无法使用 `asdoc pwcorr` 命令，而简易的 `asdoc cor` 命令不能报告 p 值(显著性)，不能标星号。

#### 5.1.2 corr2docx

```stata
local varlist "wage age race married grade collgrad"
corr2docx `varlist' using Myfile.docx, replace spearman(ignore) pearson(pw) ///
	star title(corr2docx_Table: correlation coefficient matrix)
```
![corr2docx_Table: correlation coefficient matrix](https://images.gitee.com/uploads/images/2019/0924/231608_5f384245_1522177.png)


> **命令特点及存在的问题**：
> 1. 支持中文
> 2. 不能报告 p 值，但能以`* p < 0.1 ** p < 0.05 *** p < 0.01`的方式标注星号
#### 5.1.3 logout

```stata
local varlist "wage age race married grade collgrad"
logout, save(Myfile) word replace : pwcorr_a `varlist', ///
	star1(0.01) star5(0.05) star10(0.1)
```
![logout_Table: correlation coefficient matrix](https://images.gitee.com/uploads/images/2019/0924/231608_58d74453_1522177.png)


> **命令特点及存在的问题**：
> 1. 可设置以`* p < 0.1 ** p < 0.05 *** p < 0.01`的方式标注星号。
> 2. 表格会有串行的问题，需要后期自己手动调整。
> 3. 不能设置表格标题。

#### 5.1.4 esttab

```stata
local varlist "wage age race married grade collgrad"
estpost correlate `varlist', matrix
esttab using Myfile.rtf, ///
	unstack not noobs compress nogaps replace star(* 0.1 ** 0.05 *** 0.01) ///
	b(%8.3f) p(%8.3f) title(esttab_Table: correlation coefficient matrix)
```
![esttab_Table: correlation coefficient matrix](https://images.gitee.com/uploads/images/2019/0924/231608_416bcd19_1522177.png)


> **命令特点及存在的问题**：
> 1. 能报告 p 值，可设置以`* p < 0.1 ** p < 0.05 *** p < 0.01`的方式标注星号。

### 5.2 小结
- `corr2docx`支持中文，而其他命令不行。
- `esttab`既能报告 p 值，也能以一般文献的要求报告星号，`logout`也可以，但有串行的问题；`corr2docx`只能标注星号；`asdoc`都不能做。
- `logout`不能设置表格标题，而其他的命令可以。


&emsp;
> #### [连享会现场课程报名中……](https://gitee.com/arlionn/Course/blob/master/README.md)

## 6. 回归结果输出
### 6.1 命令展示与描述
#### 6.1.1 asdoc

```stata
asdoc reg wage age married occupation, save(Myfile.doc) nest replace ///
	cnames(OLS-1) rep(se) add(race, no)
asdoc reg wage age married collgrad occupation, save(Myfile.doc) nest append ///
	cnames(OLS-2) add(race, no)
asdoc reg wage age married collgrad occupation race_num*, save(Myfile.doc) nest append ///
	add(race, yes) cnames(OLS-3) dec(3) drop(occupation race_num*) ///
	stat(r2_a, F, rmse, rss) title(asdoc_Table: regression result)
```
![asdoc_Table: regression result](https://images.gitee.com/uploads/images/2019/0924/231608_e7b762a7_1522177.png)


> **命令特点及存在的问题**：
> 1. 可添加不同列的列名。
> 2. 没有诸如`indicate("race=race_num*") `的选项，需用`add(race, yes)`代替。
> 3. 不能对变量进行排序。
> 4. 表格布局与一般的文献不同，且不能用命令调整。

#### 6.1.2 reg2docx

```stata
reg wage age married occupation
est store m1
reg wage age married collgrad occupation
est store m2
reg wage age married collgrad occupation race_num*
est store m3
reg2docx m1 m2 m3 using Myfile.docx, replace indicate("race=race_num*") ///
	b(%9.2f) se(%7.2f) scalars(r2(%9.3f) r2_a(%9.2f) N) ///
	drop(occupation) order(married) ///
	title(reg2docx_Table: regression result) ///
	mtitles("OLS-1" "OLS-2" "OLS-3") 
```
![reg2docx_Table: regression result](https://images.gitee.com/uploads/images/2019/0924/231608_ca376f7c_1522177.png)


> **命令特点及存在的问题**：
> 1. 可添加不同列的列名
> 2. 有诸如`indicate("race=race_num*")`的选项。
> 3. 命令运行之后会将所有已经储存的回归结果清除，导致后面无法再继续使用前面的回归结果。
> 4. 支持中文。

#### 6.1.3 outreg2

```
local subexp "nor2 noobs e(r2 r2_a F N) fmt(f) bdec(2) sdec(2) drop(occupation race_num*) sortvar(married)"
reg wage age married occupation
outreg2 using Myfile, word replace title(outreg2_Table: regression result) ///
	ctitle(OLS-1) `subexp' addtext(race, no)
	
reg wage age married collgrad occupation
outreg2 using Myfile, word append ctitle(OLS-2) `subexp' addtext(race, no)

reg wage age married collgrad occupation race_num*
outreg2 using Myfile, word append ctitle(OLS-3) `subexp' addtext(race, yes)
```
![outreg2_Table: regression result](https://images.gitee.com/uploads/images/2019/0924/231608_68cac30f_1522177.png)

> **命令特点及存在的问题**：
> 1. 可添加不同列的列名。
> 2. 没有诸如`indicate("race=race_num*")`的选项，需用`addtext(race, yes)`代替。

#### 6.1.4 esttab

```stata
reg wage age married occupation
est store m1
reg wage age married collgrad occupation
est store m2
reg wage age married collgrad occupation race_num*
est store m3
esttab m1 m2 m3 using Myfile.rtf, ///
	replace star( * 0.10 ** 0.05 *** 0.01 ) nogaps compress ///
	order(married) drop(occupation) b(%20.3f) se(%7.2f)  ///
	r2(%9.3f) ar2 aic bic obslast scalars(F)  ///
	indicate("race=race_num*") mtitles("OLS-1" "OLS-2" "OLS-3") ///
	title(esttab_Table: regression result) 
```
![esttab_Table: regression result](https://images.gitee.com/uploads/images/2019/0924/231608_5ab31dd4_1522177.png)


> **命令特点及存在的问题**：
> 1. 可添加不同列的列名。
> 2. 有诸如`indicate("race=race_num*")`的选项。
> 3. 表格形式最符合一般文献报告表格的格式。

### 6.2 小结
- 都可添加不同列的列名。
- `reg2docx`和`esttab`都有诸如`indicate("race=race_num*")`的选项，而其他命令都需要手动添加。
- `reg2docx`运行之后会将所有已经储存的回归结果清除，导致后面无法再继续使用前面的回归结果。而`esttab`无此问题。
- `reg2docx`支持中文，而其他命令不能。
- 相对于其他命令，`esttab`输出到 **Word** 的表格形式最符合一般文献报告表格的格式。


## 7. LaTeX 代码输出
以上四个命令中，只有`out_log`和`esttab`支持 **LaTeX** 代码输出，所以本部分从这两个命令入手，阐述它们的特点和优劣。

由于 **LaTeX** 代码编译之后，表格与 **Word** 的一致，所以本部分不再展示导出后的表格。

### 7.1 out_log
#### 7.1.1 代码部分
```stata
*描述性统计输出
local varlist "wage age race married grade collgrad south union occupation"
outreg2 using Myfile, sum(detail) replace tex eqkeep(N mean sd min p50 max) ///
	fmt(f) keep(`varlist') sortvar(wage age grade) ///
	title(outreg2_Table: Descriptive statistics)
/*
默认：输出一个完整的可以编译的但格式较为简易 .tex 文件，表格默认居左对齐。
outreg2 的 tex() 的专有选项：
1. fragment：只输出表格本身。
2. pretty：在默认的基础上增加了更为丰富的格式，并将表格居中对齐。
3. landscape：横置页面。
*/


*分组 T 均值检验输出
local varlist "wage age race married grade collgrad union"
logout, save(Myfile) tex replace: ttable2 `varlist', by(south)
/*
1. LaTeX 输出不能增加表格标题。
2. logout 的 tex 没有选项，只有默认形式。
*/


*相关系数矩阵输出
local varlist "wage age race married grade collgrad"
logout, save(Myfile) tex replace : pwcorr `varlist', ///
	star(0.05) listwise
/*
1. 表格会有串行的问题，需要后期自己手动调整。
2. LaTeX 输出不能增加表格标题。
3. logout 的 tex 没有选项，只有默认形式。
*/


*回归结果输出
local subexp "tex nor2 noobs e(r2 r2_a F N) fmt(f) bdec(2) sdec(2) drop(occupation race_num*) sortvar(married)"
reg wage age married occupation
outreg2 using Myfile, replace title(outreg2_Table: regression result) ///
	ctitle(OLS-1) `subexp' addtext(race, no)
reg wage age married collgrad occupation
outreg2 using Myfile, append ctitle(OLS-2) `subexp' addtext(race, no)
reg wage age married collgrad occupation race_num*
outreg2 using Myfile, append ctitle(OLS-3) `subexp' addtext(race, yes)
```

#### 7.1.2 小结
- `out_log`针对每个输出部分都能导出结果，但每个部分前后不能`append`在一起。本文测试过，如果用命令强行`append`，表格内容会乱套。所以，如果要使用该命令导出 **LaTeX** 代码，建议自己先写一个总的 **LaTeX** 框架，然后针对每个导出的部分采用`\input{}`命令即可。
- 表格本身也存在着一些小问题，如采用`logout`命令的部分无法书写表格标题，表格可能会出现串行。


### 7.2 esttab
#### 7.2.1 代码部分
```stata
*描述性统计输出
local varlist "wage age race married grade collgrad south union occupation"
estpost summarize `varlist', detail
esttab using Myfile.tex, ///
	cells("count mean(fmt(2)) sd(fmt(2)) min(fmt(2)) p50(fmt(2)) max(fmt(2))") ///
	noobs compress replace title(esttab_Table: Descriptive statistics) ///
	booktabs  page(array, makecell) alignment(cccccc) width(\hsize)
/*
esttab 的 LaTeX 输出的专有选项：
1. booktabs: 用 booktabs 宏包输出表格(三线表格)。
2. page[(packages)]: 创建完成的 LaTeX 文档以及添加括号里的宏包
3. 如果写了 booktabs 选项，则 page[(packages)] 将自动添加\usepackage{booktabs}。
4. alignment(cccccc)：定义从第二列开始的列对齐方式(默认居中)。
5. width(\hsize)：可以使得表格宽度为延伸至页面宽度
6. fragment：不输出表头表尾，只输出表格本身内容，其不能与 page[(packages)] 选项共存。
*/


*分组 T 均值检验输出
local varlist "wage age race married grade collgrad union"
estpost ttest `varlist', by(south)
esttab using Myfile.tex, ///
	cells("N_1 mu_1(fmt(3)) N_2 mu_2(fmt(3)) b(star fmt(3))") starlevels(* 0.10 ** 0.05 *** 0.01) ///
	noobs compress append title(esttab_Table: T_test by group) ///
	booktabs  page width(\hsize)
/*
1. 这里的 page 选项可以将本部分附着在先前的 document 环境中，从而形成一个整体。
*/


*相关系数矩阵输出
local varlist "wage age race married grade collgrad"
estpost correlate `varlist', matrix listwise
esttab using Myfile.tex, ///
	unstack not noobs compress nogaps append star(* 0.1 ** 0.05 *** 0.01) ///
	b(%8.3f) p(%8.3f) title(esttab_Table: correlation coefficient matrix) ///
	booktabs  page width(\hsize)


*回归结果输出
reg wage age married occupation
est store m1
reg wage age married collgrad occupation
est store m2
reg wage age married collgrad occupation race_num*
est store m3
esttab m1 m2 m3 using Myfile.tex, ///
	append star( * 0.10 ** 0.05 *** 0.01 ) nogaps compress ///
	order(married) drop(occupation) b(%20.3f) se(%7.2f)  ///
	r2(%9.3f) ar2 aic bic obslast scalars(F)  ///
	indicate("race=race_num*") mtitles("OLS-1" "OLS-2" "OLS-3") ///
	title(esttab_Table: regression result) booktabs  page width(\hsize)
```

#### 7.2.2 小结
- 这里所有的表格都可以生成标题，也没有串行的问题。
- 与`out_log`不一样，`esttab`能形成一个整体，所有的表格都能完美的`append`在一起，并且可以设置生成导言区和正文区的`document`环境。所以以上命令生成的 **.tex** 文件可以直接拿去编译，并生成所有表格，一步到位。之后我们再统一做一些微调就可以放在 **LaTeX** 正文中了。

## 8. 总结
上面从各个角度对四个实证结果输出命令，这里做一个总结。
- 从**描述性统计输出**的角度，四个命令的效果差不多，只是`sum2docx`和`esttab`能分别设置每个统计量的小数点位数而占据了上风。
- 从**分组 T 均值检验输出** 的角度，`asdoc`输出内容不完整且操作最麻烦；`logout`和`t2docx`输出表格虽然完整但各有优缺点（`logout`不能设置表格标题；`t2docx`表格报告的内容剔除了所输入变量中包含缺漏值的观测值，且该操作无法被禁止）；`esttab`功能最完整。
- 从**相关系数矩阵输出**的角度，`esttab`既能报告 p 值，也能以一般文献的要求报告星号，`logout`也可以，但有串行的问题；`corr2docx`只能标注星号；`asdoc`都不能做。
-  从**回归结果输出**的角度，`asdoc`的效果最差，不能对变量进行排序，表格布局与一般的文献不同，且不能用命令调整；其他三个命令功能都是完整的，而对于`outreg2`没有 `indicate()`选项使得操作更麻烦，`reg2docx`运行之后会将所有已经储存的回归结果清除，导致后面无法再继续使用前面的回归结果（这应该是该命令近期更新后的一个 **bug**）。
-  从 **Word 整体性的角度**（即将所有表格都`append`到一个 **Word** 里的程度），`asdoc`、`xxx2docx`和`esttab`都能完美的自动实现一个 **Word** 文件装上所有表格，而`out_log`无法实现这一点，主要是由于其中的`logout`命令没有`append`选项。
- 从 **LaTeX 代码输出**的角度，只有`out_log`和`esttab`支持 **LaTeX** 输出。其中`esttab`能自动的形成包含所有表格的单个 **.tex** 文件，且该 **.tex** 能直接进行编译。这使得`esttab`在这方面极大的优于`out_log`。
- 从**综合评价**的角度，功能越完整，自动化程度越高，那么这样的命令往往也越会获得我们的青睐。通过前面的论述，本文认为，`asdoc`的 **Word** 整体性虽然很好，但由于其他的缺陷实在太多，于是将其归为最差的位置；`out_log`既支持 **Word** 输出也支持 **LaTeX** 输出，在复杂的回归结果输出中有着强悍的力量，但是，在一般的应用中，其整体效果还是要差于`xxx2docx`；`xxx2docx `是唯一能够完美支持中文和将表格导出到 **.docx** 文件的命令，命令表达式也通俗易懂，但是其不支持 **LaTeX** 代码的输出和近期更新后出现的一些 **bug**，使得该命令的综合表现要差于`esttab`；`esttab`在某些方面虽然语句复杂一些，但也造就了其功能的完整性，使得其在每一方面都能有很好的表现，因此本文对于该命令的综合评价位于四大输出命令之首。

为了能够更形象的表示出本文对于以上四个实证结果输出命令在各个方面的评价，本文将以**推荐指数**的形式，列于下方的表格中。


|              &ensp;      	| asdoc 	| xxx2docx 	| out_log 	| esttab 	|
|:-------------------:	|:-----:	|:--------:	|:-------:	|:------:	|
|    描述性统计输出   	|  ★★★★ 	|   ★★★★★  	|   ★★★★  	|  ★★★★★ 	|
| 分组 T 均值检验输出 	|   ★   	|    ★★★   	|   ★★★   	|  ★★★★★ 	|
|   相关系数矩阵输出  	|   ★★  	|   ★★★★   	|   ★★★   	|  ★★★★★ 	|
|     回归结果输出    	|   ★★  	|   ★★★★   	|   ★★★★  	|  ★★★★★ 	|
|     Word 整体性     	| ★★★★★ 	|   ★★★★★  	|   ★★★   	|  ★★★★★ 	|
|    LaTeX 代码输出   	|   -   	|     -    	|   ★★★   	|  ★★★★★ 	|
|       综合评价      	|   ★★  	|   ★★★★   	|   ★★★   	|  ★★★★★ 	|


&emsp;

## 9. 相关附件
> 本文使用的所有 Dofiles：[「全体 Stata 实证结果输出命令.do」](https://gitee.com/Stata002/StataSX2018/raw/master/2019C/%E7%8E%8B%E7%BE%8E%E5%BA%AD_%E4%B8%AD%E5%8D%97%E6%B0%91%E6%97%8F/%E6%8E%A8%E6%96%872%EF%BC%9AStata%EF%BC%9A%E5%AE%9E%E8%AF%81%E7%BB%93%E6%9E%9C%E7%9A%84%E5%90%84%E7%A7%8D%E8%BE%93%E5%87%BA%E5%91%BD%E4%BB%A4%EF%BC%8C%E5%AD%B0%E4%BC%98%E5%AD%B0%E5%8A%A3%EF%BC%9F%E2%80%94%E9%99%84%E4%BB%B6/%E5%85%A8%E4%BD%93%20Stata%20%E5%AE%9E%E8%AF%81%E7%BB%93%E6%9E%9C%E8%BE%93%E5%87%BA%E5%91%BD%E4%BB%A4.do)


&emsp;
 
&emsp;
>#### 关于我们

- **「Stata 连享会」** 由中山大学连玉君老师团队创办，定期分享实证分析经验， 公众号：**StataChina**。
- 公众号推文同步发布于 [CSDN](https://blog.csdn.net/arlionn) 、[简书](http://www.jianshu.com/u/69a30474ef33) 和 [知乎Stata专栏](https://www.zhihu.com/people/arlionn)。可在百度中搜索关键词 「Stata连享会」查看往期推文。
- 点击推文底部【阅读原文】可以查看推文中的链接并下载相关资料。
- **欢迎赐稿：** 欢迎赐稿。录用稿件达 **三篇** 以上，即可 **免费** 获得一期 Stata 现场培训资格。
- **E-mail：** StataChina@163.com
- 往期推文：[计量专题](https://gitee.com/arlionn/Course/blob/master/README.md)  || [精品课程](https://mp.weixin.qq.com/s/hWtncj56PeFNL4yg2-va0Q) || [简书推文](https://www.jianshu.com/p/de82fdc2c18a)  || [公众号合集](https://mp.weixin.qq.com/s/AWxDPvTuIrBdf6TMUzAhWw) 

[![点击查看完整推文列表](https://images.gitee.com/uploads/images/2019/0924/231608_3e0a2204_1522177.png)](https://mp.weixin.qq.com/s/RwkuPpLS7bI5C5OhRqjkOw)

---
[![欢迎加入Stata连享会(公众号: StataChina)](https://images.gitee.com/uploads/images/2019/0924/231609_c0789ff0_1522177.png)](https://mp.weixin.qq.com/s/RwkuPpLS7bI5C5OhRqjkOw)



