# Stata：《毕业论文大礼包 C》——基于 esttab 框架编写的实证结果输出命令

> 作者：王美庭 (中南民族大学经济学院)  
> 邮箱：2017110097@mail.scuec.edu.cn

## 1. 本文目的
大家在做实证论文时，会经常用到 Stata 进行实证结果的输出。那么选择哪种命令进行更有效的实证结果输出往往成了很多同学头疼的事。基于此，我们的[「Stata：毕业论文大礼包 A——实证结果输出命令大比拼」](https://www.jianshu.com/p/821b2565d0cf)给出了最佳输出命令的评价，以及[「Stata：毕业论文大礼包 B——神速实证结果输出之搜狗短语」](https://www.jianshu.com/p/e6fa286b013f)给出了搜狗输出的快捷方式。

通过以上的推文，我们知道，**`esttab`具有最全的功能，不仅支持 Word 完全体输出，还支持 LaTeX 完全体输出**。然而，**`esttab`命令给出的语句往往也最复杂，虽然也有了搜狗快捷输出方式，但这同样会让很多接触 Stata 不久的同学无从下手**。基于此，本文将根据功能最全的`esttab`的框架，编写语法更为简洁的命令。

经过大量时间和精力，以及无数次的测试与 bug 修正，本文的实证结果系列输出命令已经新鲜出炉，包括：**描述性统计输出（`lxhsum`）**、**分组 T 均值检验输出（`lxhttest`）**、**相关系数矩阵输出（`lxhcorr`）**、**回归结果输出（`lxhreg`）**以及前两篇推文都没有包括的**矩阵输出（`lxhmat`）**。加入矩阵输出的原因在于：除了常规的输出外，有时候我们需要一些定制的输出，而定制的输出往往可以通过矩阵进行构建。**如果以上命令有了更新，同学们可以通过`lxhinstall`命令安装更新后的命令**。

**编写后命令将具有以下特点**：
- 基本上`esttab`能做的事，本组命令都能做。
- 默认状态下将被设置成最符合一般文献的输出标准（消除了`esttab`自带的一些多余的空行、多余的分组）。
- 语法简洁，秉持能少写就少写的原则。
- 很多命令在输出结果至 Word 或 LaTeX 时，Stata 界面并不会有相应的结果呈现，这使得我们如果要查看相应的结果，还需要来回的切换不同的软件，甚是麻烦，而本次编写的命令，将解决这个问题。
- 以上命令均可以通过`append`命令将结果输出至一个文件中（Word 或 LaTeX）。
- 这里导出到 LaTeX 的表格均采用三线表模式，并自动添加了`booktabs`宏包。
- 只需查看本推文，就能知道编写的这些命令的使用。

> 如果命令仍然有 bug 或者能完善的地方，欢迎大家留言，本文作者将实时更新。

## 2. 实例所用数据
```stata
sysuse nlsw88.dta, clear
tab race, gen(race_num)
drop race_num1

*构建回归结果
reg wage age married occupation
est store m1
reg wage age married collgrad occupation
est store m2
reg wage age married collgrad occupation race_num*
est store m3
reg wage age married collgrad occupation hours race_num*
est store m4

*构建矩阵
set seed 123456
mat A = 2563*matuniform(6,5)
mat list A
```



## 3. 命令的安装（`lxhinstall`）
大家可以在下方的附件中下载本文会用到的用到的命令，对于以后命令的更新，大家可以通过`lxhinstall`进行安装。另外使用该命令进行安装时有个好处就是：它可以将命令自动安装在 Stata 的 plus 文件夹对应首字母下的子文件夹中，就如同`ssc install ...`的功能一样。该命令的语法如下：

```stata
lxhinstall a_command_name [, replace]
```

**选项说明：**
- `a_command_name`：目前可以输入的命令名包括`itself`、`lxhsum`、`lxhttest`、`lxhcorr`、`lxhreg`、`lxhmat`。`itself`表示更新命令`lxhinstall`本身。
- `replace`：如果 Stata 的 plus 文件夹对应首字母下的子文件夹中已经存在要安装的命令，则需添加该选项才能更新。

> 方括号内的选项代表可选项（下同）。

**实例：**

```stata
lxhinstall itself  //安装命令本身
lxhinstall lxhsum //安装命令lxhsum
lxhinstall lxhsum, replace //如果命令lxhsum已经存在，则替换之
```


## 4. 描述性统计输出（`lxhsum`）
**命令语法**：

```stata
lxhsum [varlist] [if] [in] [using/] [, ///
        replace append Statistics(string) TItle(string) Alignment(string) PAGE(string)]
```

> 选项里面的大写代表可以缩写至大写部分（下同）。

**选项说明：**
- `varlist`：仅可输出数值型变量，若为空，则自动导入所有数值型变量。
- `using`：可以将结果输出至 Word（ .rtf 文件）和 LaTeX（ .tex 文件）中（下同）。
- `replace`：若已存在相同文件名，则替换之。
- `append`：若已存在相同文件名，则附加之。
- `statistics()`：可以输入的全部统计量有：`N mean sd min median max p1 p5 p10 p25 p75 p90 p95 p99`。若为空，则默认输入`N mean sd min median max`。Stata 界面和 Word 输出格式默认为`%11.0f`（`N`）和`%11.3f`（除`N`以外的统计量），LaTeX 输出格式默认为`%11.0fc`（`N`）和`%11.3fc`（除`N`以外的统计量）（下同）。当然，我们也可以自行设置，如输入`N sd(3) min(%9.2f) p99(%9.3fc)`。
- `title()`：设置表格标题，若为空，则默认为`Summary statistics`。
- `alignment()`：LaTeX 输出专属选项，用于设置列格式，可输入`math`或`dot`。`math`表示设置列格式为传统的数学格式（`>{$}c<{$}`），并自动添加宏包`booktabs`和`array`。`dot`表示设置列格式为小数点对齐的数学模式（`D{.}{.}{-1}`），并自动添加添加宏包`booktabs`、`array`和`dcolumn`。若为空，则默认为`dot`。
- `page()`：LaTeX 输出专属选项，除了`alignment()`选项自动添加的宏包，该选项还可以添加额外的宏包。

**实例：**
```stata
lxhsum //输出所有数值型变量的描述性统计
lxhsum wage age race hours //报告所输入变量的描述性统计
lxhsum wage age race hours, s(N sd(3) min(%9.2f) p99(%9.3fc)) //报告指定统计量和指定数值格式的描述性统计
lxhsum wage age race hours, s(N sd(3) min(%9.2f) p99(%9.3fc)) ti(this is a title) //为表格添加自定义标题
lxhsum wage age race hours using Myfile.rtf, replace s(N sd(3) min(%9.2f) p99(%9.3fc)) ti(this is a title) //将结果导入到Word文件中
lxhsum wage age race hours using Myfile.tex, replace s(N sd(3) min(%9.2f) p99(%9.3fc)) ti(this is a title) a(math) //将结果导入到LaTeX中，并设置列格式为传统数学模式
```

**默认输出效果：**
- Stata
```stata
Summary statistics
----------------------------------------------------------------------
                   N      mean        sd       min    median       max
----------------------------------------------------------------------
wage            2246     7.767     5.756     1.005     6.272    40.747
age             2246    39.153     3.060    34.000    39.000    46.000
race            2246     1.283     0.475     1.000     1.000     3.000
hours           2242    37.218    10.509     1.000    40.000    80.000
----------------------------------------------------------------------
```

- Word

![lxhsum—Word](https://images.gitee.com/uploads/images/2020/0106/122639_6f9a4810_1985121.png)

- LaTeX

![lxhsum—LaTeX](https://images.gitee.com/uploads/images/2020/0106/122638_8ce69ac5_1985121.png)


## 5. 分组 T 均值检验输出（`lxhttest`）
**命令语法：**
```stata
lxhttest varlist [if] [in] [using/] , by(string) [ ///
        replace append Statistics(string) TItle(string) Alignment(string) PAGE(string)]
```

**选项说明：**
- `varlist`：需至少输入一个变量，不能为空，且输入变量需要数值型变量。
- `using`：含义同上。
- `by()`：设定要基于分组的变量。
- `replace`：含义同上。
- `append`：含义同上。
- `statistics()`：所有可输入的统计量有`N N1 N2 mean1 mean2 mean_diff se t p`。若为空，则默认输入`N1 mean1 N2 mean2 mean_diff(star)`。数值默认格式同`lxhsum`。也可以自定义统计量和输出格式，如`N N1(%9.0fc) mean1 mean2(%9.3fc) mean_diff(%11.3f star)`，里面的`star`表示以`* p < 0.10, ** p < 0.05, *** p < 0.01`的形式进行标注星号。
- `title()`：设置表格标题，默认为`Grouping t-means test`。
- `alignment()`：含义同上。
- `page()`：含义同上。

**实例：**
```stata
lxhttest wage age race hours, by(south) //依据south对变量进行分组T均值检验
lxhttest wage age race hours, by(south) s(mean_diff(%9.2f) p(star 4)) //自定义数值格式和star要显示的位置
lxhttest wage age race hours, by(south) s(mean_diff(%9.2f) p(star 4)) ti(this is a title) //自定义表格标题
lxhttest wage age race hours using Myfile.rtf, replace by(south) s(mean_diff(%9.2f) p(star 4)) ti(this is a title) //导出结果至Word
lxhttest wage age race hours using Myfile.tex, replace by(south) s(mean_diff(%9.2f) p(star 4)) ti(this is a title) a(math) //导出结果至LaTeX，并设置列格式为传统数学模式
```

**默认输出效果：**
- Stata
```stata
Grouping t-means test
---------------------------------------------------------------
                  N1     mean1        N2     mean2 mean_diff   
---------------------------------------------------------------
wage            1304     8.402       942     6.887     1.515***
age             1304    39.135       942    39.178    -0.043   
race            1304     1.196       942     1.402    -0.206***
hours           1304    36.518       938    38.192    -1.674***
---------------------------------------------------------------
* p < 0.10, ** p < 0.05, *** p < 0.01
```

- Word

![lxhttest—Word](https://images.gitee.com/uploads/images/2020/0106/122639_f9bfd422_1985121.png)

- LaTeX

![lxhttest—LaTeX](https://images.gitee.com/uploads/images/2020/0106/122639_bcafc561_1985121.png)


## 6. 相关系数矩阵输出（`lxhcorr`）
**命令语法：**
```stata
lxhcorr [varlist] [if] [in] [using/] [, ///
	replace append B(string) P(string) STARAUX NOSTAR ///
	CORR PWCORR TItle(string) Alignment(string) PAGE(string) ]
```

**选项说明：**
- `varlist`：输入需要计算相关系数矩阵的数值变量，若为空，则默认导入所有数值型变量。
- `using`：含义同上。
- `replace`：含义同上。
- `append`：含义同上。
- `b()`：设置输出相关系数以及相关系数的数值格式，若为空，则默认为`b(%11.3f)`。
- `p()`：设置在相关系数下方输出`p`值，若为空，则不输出；若只写了`p`，则默认数值格式为`%11.3f`。
- `staraux`和`nostar`：若两者均为空，则默认在相关系数上以`* p < 0.10, ** p < 0.05, *** p < 0.01`的方式报告星号；若`staraux`和`p`存在，则在`p`值上报告星号；若`nostar`存在，则不报告星号。
- `corr`和`pwcorr`：选项`corr`表示报告的相关系数与`corr`默认状态下一致（在计算相关系数前会先去除包含缺漏值的观察值）；选项`pwcorr`表示报告的相关系数与`pwcorr`默认状态下一致（在计算相关系数前不会先删除包含缺漏值）；若两者均为空，则默认导入`corr`。
- `title()`：设置表格标题，默认为`Correlation coefficient matrix`。
- `alignment()`：含义同上。
- `page()`：含义同上。

**实例：**
```stata
lxhcorr //输出所有数值型变量的相关系数矩阵
lxhcorr wage age race hours //输出制定变量的相关系数矩阵
lxhcorr wage age race hours, b(2) //设置相关系数的数值格式
lxhcorr wage age race hours, b(2) p(%9.3f) //报告p值并设定p值的数值格式
lxhcorr wage age race hours, b(2) p(%9.3f) staraux //将星号标注在p值上
lxhcorr wage age race hours, b(2) p(%9.3f) nostar //不标注星号
lxhcorr wage age race hours, b(2) p(%9.3f) corr //以corr默认方式计算相关系数
lxhcorr wage age race hours, b(2) p(%9.3f) ti(this is a title) //设置表格标题
lxhcorr wage age race hours using Myfile.rtf, replace b(2) p(%9.3f) ti(this is a title) //将结果导出至Word中
lxhcorr wage age race hours using Myfile.tex, replace b(2) p(%9.3f) ti(this is a title) a(math) //将结果导出至LaTeX中，并设置列格式为传统的数学模式
```

**默认输出效果：**
- Stata
```stata
Correlation coefficient matrix
--------------------------------------------------------------
                wage          age         race        hours   
--------------------------------------------------------------
wage           1.000                                          
age           -0.036*       1.000                             
race          -0.080***    -0.058***     1.000                
hours          0.159***    -0.028        0.045**      1.000   
--------------------------------------------------------------
* p<0.1, ** p<0.05, *** p<0.01
```

- Word

![lxhcorr—Word](https://images.gitee.com/uploads/images/2020/0106/122639_4b3c9836_1985121.png)

- LaTeX

![lxhcorr—LaTeX](https://images.gitee.com/uploads/images/2020/0106/122639_6dcb4e4d_1985121.png)

## 7. 回归结果输出（`lxhreg`）
**命令语法：**
```stata
lxhreg [est_store_names] [using/] [, ///
	replace append Drop(string) Keep(string) Order(string) VARLabels(string asis) ///
	Bfmt(string) BETAfmt(string) SEfmt(string) Tfmt(string) Pfmt(string) ONLYB ///
	STARAUX NOSTAR INDicate(string asis) Scalars(string) NONUMbers NOMTitles ///
	MTitles(string asis) TItle(string) MGroups(string asis) Alignment(string) PAGE(string)]
```

**选项说明：**
- `est_store_names`：输入已经储存好的回归结果名称，支持通配符(*?)。若为空，则默认导入所有储存好的回归结果。
- `using`：含义同上。
- `replace`：含义同上。
- `append`：含义同上。
- `drop()`：不报告输入变量的系数；`drop(_cons)`表示不需要报告常数项。
- `keep()`：只报告输入变量的系数
- `varlabels()`: 更改要报告的变量名的显示名称
- `b()`和`beta()`：设置输出回归结果系数类型及其数值格式；`b()`表示输出常规的回归系数；`beta()`表示输出标准化的回归系数；若两者均为空，则默认为`b(%11.3f)`（针对 Stata 界面和 Word）和`b(%11.3fc)`（针对 LaTeX ）。
- `se()`、`t()`、`p()`和`onlyb`：设置回归系数下方报告标准误、T 值、 P 值还是什么都不报告，这四个选项至多有一个存在；若都为空，则默认报告标准误。
- `staraux`和`nostar`：若两者均为空，则默认在回归系数上以`* p < 0.10, ** p < 0.05, *** p < 0.01`的方式报告星号；若`staraux`和`se类`（含`se t p`）存在，则在`se类`值上报告星号；若`nostar`存在，则不报告星号。
- `indicate()`：不报告指定变量的回归系数，而代之以  Yes 或 No（表示指定变量是否在回归中出现）。
- `scalars()`：可以输入的全部标量为`r2 ar2 pr2 aic bic F ll N`，若为空，则默认导入`r2 N`。
- **以上带括号的统计量和标量都可以设置对应的数值格式**。
- `nonumbers`：不报告每个回归模型上方的序号。
- `nomtitles`和`mtitles()`：这两者不能同时存在；`nomtitles`表示不报告每个模型的名称；`mtitles()`可设定每个回归模型的名称，另外如果设定的是`depvars`，则以每个回归的因变量作为每个回归模型的名称，如果设定的是`esn`，则以每个回归结果的储存名称作为每个回归模型的名称；若这两个选项均为空，则默认为`mtitles(depvars)`。
- `title()`：设置表格标题，默认为`Regression result`。
- `mgroups()`：可设置分组，如`mgroups(A B 2 2)`表示将要输出的四个回归分为两组，前两个回归归为组别 A，后两个回归归为组别 B；默认下没有分组。
- `alignment()`：含义同上。
- `page()`：含义同上。

**实例：**
```stata
lxhreg //展示所有已经储存的回归结果
lxhreg m1 m2 m3 m4 //展示指定的系列回归结果
lxhreg m1 m2 m3 m4, drop(_cons hours) //不报告常数项和hours变量
lxhreg m1 m2 m3 m4, keep(age married) //只报告age和married变量
lxhreg m1 m2 m3 m4, order(married hours) //将变量married和hours置于报告变量的最上方
lxhreg m1 m2 m3 m4, b(%9.2f) //设定回归系数的格式，且默认下报告系数的标准误
lxhreg m1 m2 m3 m4, b(%9.2f) onlyb //只报告系数值
lxhreg m1 m2 m3 m4, b(%9.2f) t(3) //不报告默认下的se值，换而报告t值
lxhreg m1 m2 m3 m4, b(%9.2f) p(%9.3f) //不报告默认下的se值，换而报告p值
lxhreg m1 m2 m3 m4, b se(%9.3f) staraux //在标准误上标注星号
lxhreg m1 m2 m3 m4, b se(%9.3f) nostar //不在任何地方标注星号
lxhreg m1 m2 m3 m4, b se(%9.3f) ind(race=race_num*) //不报告race_num*系列变量，换之以Yes或No的形式展示race变量是否在回归中出现
lxhreg m1 m2 m3 m4, b se(%9.3f) s(r2(2) ar2(%11.4f) aic F ll N(%9.0fc)) //报告特定的scalars，以及设定它们的数值格式
lxhreg m1 m2 m3 m4, b se(%9.3f) nonum //不报告回归所在序号
lxhreg m1 m2 m3 m4, b se(%9.3f) nomt //不报告每个回归模型的名称
lxhreg m1 m2 m3 m4, b se(%9.3f) mt(model1 model2 model3 model4) //自定义每个回归模型的名称
lxhreg m1 m2 m3 m4, b se(%9.3f) ti(this is a title) //自定义表格标题
lxhreg m1 m2 m3 m4, b se(%9.3f) mg(A B 2 2) //设置前两个回归为组别A，后两个回归为组别B
lxhreg m1 m2 m3 m4, b se(%9.3f) mg("Urban people" B 2 2) //设置前两个回归为组别Urban people，后两个回归为组别B
lxhreg m1 m2 m3 m4 using Myfile.rtf, replace b se(%9.3f) //将回归结果导入至Word
lxhreg m1 m2 m3 m4 using Myfile.tex, replace b se(%9.3f) a(math) //将回归结果导入至LaTeX，并设定列格式为传统数学模式
```

**默认输出效果：**
- Stata
```stata
Regression result
--------------------------------------------------------------
                 (1)          (2)          (3)          (4)   
                wage         wage         wage         wage   
--------------------------------------------------------------
age           -0.064       -0.059       -0.067*      -0.060   
             (0.039)      (0.037)      (0.037)      (0.037)   
married       -0.469*      -0.472**     -0.629**     -0.445*  
             (0.250)      (0.238)      (0.244)      (0.245)   
occupation    -0.284***    -0.384***    -0.370***    -0.358***
             (0.035)      (0.034)      (0.034)      (0.034)   
collgrad                    4.220***     4.133***     3.969***
                          (0.273)      (0.275)      (0.274)   
race_num2                               -0.784***    -0.839***
                                       (0.271)      (0.269)   
race_num3                               -0.224       -0.194   
                                       (1.066)      (1.057)   
hours                                                 0.067***
                                                    (0.011)   
_cons         11.910***    11.168***    11.753***     8.842***
             (1.556)      (1.480)      (1.492)      (1.554)   
--------------------------------------------------------------
R-sq           0.031        0.125        0.128        0.142   
N               2237         2237         2237         2233   
--------------------------------------------------------------
Standard errors in parentheses
* p<0.1, ** p<0.05, *** p<0.01
```

- Word

![lxhreg—Word](https://images.gitee.com/uploads/images/2020/0106/122639_21e4c38b_1985121.png)

- LaTeX

![lxhreg—LaTeX](https://images.gitee.com/uploads/images/2020/0106/122639_7bfed181_1985121.png)

- 分组别后的 LaTeX

![lxhreg—LaTeX(by groups)](https://images.gitee.com/uploads/images/2020/0106/122639_8823e841_1985121.png)

## 8. 矩阵输出（`lxhmat`）
**命令语法：**
```stata
lxhmat a_matrix_name [using/] [, ///
	replace append FMT(string) ROWFMT(string) COLFMT(string) ///
	TItle(string) Alignment(string) PAGE(string)]
```

**选项说明：**
- `a_matrix_name`：需输入一个矩阵的名称。
- `using`：含义同上。
- `replace`：含义同上。
- `append`：含义同上。
- `fmt()`、`rowfmt()`和`colfmt()`：这三个选项至多能有一个存在；`fmt()`设置整体矩阵的数值格式，里面格式的数目只能为一个；`rowfmt()`设置矩阵每一行的数值格式，里面格式的数目要与矩阵的行数相等；`colfmt()`设置矩阵每一列的数值格式，里面格式的数目要与矩阵的列数相等；若三者均为空，则默认为`fmt(%11.3f)`（Stata 界面和 Word）和`fmt(%11.3fc)`（LaTeX）。
- `title()`：设置表格标题，默认为`Matrix 你所输入的矩阵的名称`。
- `alignment()`：含义同上。
- `page()`：含义同上


**实例：**
```stata
lxhmat A //Stata界面输出矩阵A的内容
lxhmat A, fmt(4) //设置整体矩阵的数值格式为小数点后4位
lxhmat A, rowsfmt(1 2 3 4 5 6) //分别设置矩阵每一行数值的格式
lxhmat A, colsfmt(5 4 3 2 1) //分别设置矩阵每一列数值的格式
lxhmat A, ti(this is a title) //自定义表格标题
lxhmat A using Myfile.rtf, replace //将矩阵输出至Word
lxhmat A using Myfile.tex, replace a(math) //将矩阵输出至LaTeX，并设定列格式为传统的数学模式
```

**默认输出效果：**
- Stata
```stata
Matrix A
------------------------------------------------------------
                  c1        c2        c3        c4        c5
------------------------------------------------------------
r1           461.234  1986.842  2246.919  1683.936   443.186
r2          1467.748   915.633  2314.974   430.901  1172.806
r3          1866.594  2377.370  1717.462   206.150  2160.344
r4           833.922   671.686   908.482   688.025  1762.466
r5          1390.449  1458.971  1962.186  2473.799  2560.546
r6            42.378  2542.164   941.073  2211.197   676.034
------------------------------------------------------------
```

- Word

![lxhmat—Word](https://images.gitee.com/uploads/images/2020/0106/122639_18c75b4a_1985121.png)

- LaTeX

![lxhmat—LaTeX](https://images.gitee.com/uploads/images/2020/0106/122640_9c2ab4b2_1985121.png)


## 9. 将以上所有结果输出至单个文件中
**输出至单个 Word 文件中：**
```stata
lxhsum wage age married grade using Myfile.rtf, replace
lxhttest wage age married grade using Myfile.rtf, by(south) append 
lxhcorr wage age married grade using Myfile.rtf, append
lxhreg m1 m2 m3 m4 using Myfile.rtf, append
lxhmat A using Myfile.rtf, append
```

**输出至单个 LaTeX 文件中：**
```stata
lxhsum wage age married grade using Myfile.tex, replace
lxhttest wage age married grade using Myfile.tex, by(south) append
lxhcorr wage age married grade using Myfile.tex, append
lxhreg m1 m2 m3 m4 using Myfile.tex, append
lxhmat A using Myfile.tex, append
```


## 10. 附件
[lxhinstall.ado](https://gitee.com/wangmeiting/personal_public_warehouse/raw/master/ado_files/lxhinstall.ado)

[lxhsum.ado](https://gitee.com/wangmeiting/personal_public_warehouse/raw/master/ado_files/lxhsum.ado)

[lxhttest.ado](https://gitee.com/wangmeiting/personal_public_warehouse/raw/master/ado_files/lxhttest.ado)

[lxhcorr.ado](https://gitee.com/wangmeiting/personal_public_warehouse/raw/master/ado_files/lxhcorr.ado)

[lxhreg.ado](https://gitee.com/wangmeiting/personal_public_warehouse/raw/master/ado_files/lxhreg.ado)

[lxhmat.ado](https://gitee.com/wangmeiting/personal_public_warehouse/raw/master/ado_files/lxhmat.ado) 
