&emsp;

> 作者：韩少真(西北大学) || 刘婉青(西北大学)    
>     
> Stata 连享会： [知乎](https://zhuanlan.zhihu.com/arlion) | [简书](http://www.jianshu.com/u/69a30474ef33) | [码云](https://gitee.com/arlionn) | [CSDN](https://blog.csdn.net/arlionn)

  
&emsp;


> #### [2019暑期Stata现场班，7.17-26日，北京，连玉君+刘瑞明 主讲](https://gitee.com/arlionn/Course/blob/master/2019%E6%9A%91%E6%9C%9FStata%E7%8E%B0%E5%9C%BA%E7%8F%AD.md)


&emsp;


## 1. 问题背景

实证分析中，经常需要对比多个变量在两个子样本组中的均值或者中位数是否存在显著差异。

### 1.1 期刊论文示例一

在社会责任领域，研究发布社会责任报告是否会产生经济后果时，我们常常将样本划分为两组，一组为**发布社会责任报告**的发布组，另一组为**未发布社会责任报告**的未发布组。然后，我们需要对比公司特征等多个变量在发布组和未发布组之间是否存在显著差异。

> [1].	Ni, X. and H. Zhang, Mandatory corporate social responsibility disclosure and dividend payouts: evidence from a quasi‐natural experiment. Accounting & Finance, 2019. 58(5): p. 1581-1612. [[PDF]](https://onlinelibrary.wiley.com/doi/pdf/10.1111/acfi.12438)

![](https://upload-images.jianshu.io/upload_images/17516282-ca67ed4d3e935fb8.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

### 1.2 期刊论文示例二

2016年美国SEC对高管薪酬披露规则进行了重大修订，最核心的变化是增加了“薪酬讨论与分析”（CD＆A）的内容。在研究这项披露规则改变的影响时，需要将样本区分为采用新薪酬披露规则的组和没有采用新薪酬披露规则的组，然后对比组间均值或者中位数的差异。

>[2].	Ferri, F., R. Zheng and Y. Zou, Uncertainty about managers’ reporting objectives and investors’ response to earnings reports: Evidence from the 2006 executive compensation disclosures. Journal of Accounting and Economics, 2018. 66(2): p. 339-365. [[PDF]](https://www.sciencedirect.com/science/article/pii/S0165410118300417)

![image](http://upload-images.jianshu.io/upload_images/17516282-b5efe0d2a691728c.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)


### 1.3 期刊论文示例三

在研究国有企业和非国有企业在代理成本和税收计划等方面的差异时，我们需要将样本区分为国有企业组和非国有企业组，然后对比组间均值或者中位数的差异。
> [3].	Bradshaw, M., G. Liao and M.S. Ma, Agency costs and tax planning when the government is a major Shareholder. Journal of Accounting and Economics, 2018.
[[PDF]](https://www.sciencedirect.com/science/article/pii/S0165410118301125#tbl0002)

![image](http://upload-images.jianshu.io/upload_images/17516282-fbb68001cc08dce2.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)


## 2. Stata实现组间均值或中位数差异检验的常见命令

- `ttest`：单个变量组间均值差异检验
- `median`：单个变量组间中位数差异检验
- `ttable2`：多个变量组间均值差异检验
- `ttable3`：多个变量组间均值或者中位数差异检验
- `balancetable`：多个变量在多组之间两两差异检验，以回归分析为基础，可以控制异方差的影响

> **Note：** `ttest` 和 `median` 为官方命令；`ttable2`、`balancetable` 和 `ttable3` 都是外部命令， 需要用如下命令安装。安装方法详情参见 [Stata: 外部命令的搜索、安装与使用。](https://www.jianshu.com/p/9b8ecf8f332e)

```
*-下载/更新 ttable2 和 balancetable 命令
. ssc install ttable2, replace 
. ssc install balancetable, replace 
*-下载/更新 ttable3 命令
. net install ttable3, from("https://raw.githubusercontent.com/statachina/ttable3/master/") replace
```


### 2.1 ttest 命令

我们首先介绍官方的 `ttest` 命令，它可以实现单个变量组间均值差异检验。

基本语法如下：

```stata
ttest varname  [if] [in],  ///
   by(groupvar) unequal  welch level(#) 
```

具体解释如下：
- `varname`：是要检验的数值型变量。具体通过 T 检验比较变量 **varname** 在选项 **by(groupvar)** 确定的两组之间的均值差异。
- `groupvar`：可以是数值型，也可以是字符串型。但在 **[if]** 和 **[in]** 限定的样本范围内，其分组的类别必须为两组。
- `unequal` ：进行非配对样本非同方差的 T 检验。
- `level(#)`：设置置信水平，默认的置信水平是 (95) 。

下面，我们以 Stata 内置的数据集 **nlsw88.dta** 为例进行演示。首先，我们想要检验工资均值在未婚组和已婚组之间的差异是否显著。

```stata
*-调用内置数据集 nlsw88
. sysuse nlsw88.dta, clear
(NLSW, 1988 extract)

*-对married进行频数统计
. tab married

    married |      Freq.     Percent        Cum.
------------+-----------------------------------
     single |        804       35.80       35.80
    married |      1,442       64.20      100.00
------------+-----------------------------------
      Total |      2,246      100.00

*-利用ttest对单个变量wage进行组间均值差异检验
. ttest wage, by(married)

Two-sample t test with equal variances
------------------------------------------------------------------------------
   Group |     Obs        Mean    Std. Err.   Std. Dev.   [95% Conf. Interval]
---------+--------------------------------------------------------------------
  single |     804    8.080765     .223456    6.336071    7.642138    8.519392
 married |   1,442    7.591978    .1421835    5.399229    7.313069    7.870887
---------+--------------------------------------------------------------------
combined |   2,246    7.766949    .1214451    5.755523    7.528793    8.005105
---------+--------------------------------------------------------------------
    diff |            .4887873    .2531718               -.0076882    .9852627
------------------------------------------------------------------------------
    diff = mean(single) - mean(married)                           t =   1.9307
Ho: diff = 0                                     degrees of freedom =     2244

    Ha: diff < 0                 Ha: diff != 0                 Ha: diff > 0
 Pr(T < t) = 0.9732         Pr(|T| > |t|) = 0.0537          Pr(T > t) = 0.0268
```

其次，当我们假定未婚组和已婚组为非配对样本，且两组的样本方差不相等时，组间均值差异的 T 检验需要添加选项 `unequal`。

```stata
*-通过unequal选项进行非配对样本非同方差的检验
. ttest wage, by(married) unequal

Two-sample t test with unequal variances
------------------------------------------------------------------------------
   Group |     Obs        Mean    Std. Err.   Std. Dev.   [95% Conf. Interval]
---------+--------------------------------------------------------------------
  single |     804    8.080765     .223456    6.336071    7.642138    8.519392
 married |   1,442    7.591978    .1421835    5.399229    7.313069    7.870887
---------+--------------------------------------------------------------------
combined |   2,246    7.766949    .1214451    5.755523    7.528793    8.005105
---------+--------------------------------------------------------------------
    diff |            .4887873     .264856               -.0307541    1.008329
------------------------------------------------------------------------------
    diff = mean(single) - mean(married)                           t =   1.8455
Ho: diff = 0                     Satterthwaite's degrees of freedom =   1452.2

    Ha: diff < 0                 Ha: diff != 0                 Ha: diff > 0
 Pr(T < t) = 0.9674         Pr(|T| > |t|) = 0.0652          Pr(T > t) = 0.0326
```

最后，还可以通过设定 **level** 选项为 **level(99)**，将置信水平更改为 99\%。

```stata
*-通过level选项更改置信水平为 99%
. ttest wage, by(married) unequal level(99)

Two-sample t test with unequal variances
------------------------------------------------------------------------------
   Group |     Obs        Mean    Std. Err.   Std. Dev.   [99% Conf. Interval]
---------+--------------------------------------------------------------------
  single |     804    8.080765     .223456    6.336071     7.50381    8.657721
 married |   1,442    7.591978    .1421835    5.399229    7.225252    7.958704
---------+--------------------------------------------------------------------
combined |   2,246    7.766949    .1214451    5.755523    7.453861    8.080037
---------+--------------------------------------------------------------------
    diff |            .4887873     .264856               -.1943345    1.171909
------------------------------------------------------------------------------
    diff = mean(single) - mean(married)                           t =   1.8455
Ho: diff = 0                     Satterthwaite's degrees of freedom =   1452.2

    Ha: diff < 0                 Ha: diff != 0                 Ha: diff > 0
 Pr(T < t) = 0.9674         Pr(|T| > |t|) = 0.0652          Pr(T > t) = 0.0326
```

> **Notes：**
- `ttest`命令每次只能对一个变量进行检验，无法批量对多个变量进行检验。
- `ttest`命令汇报的检验结果过于详细。我们在论文中只需要汇报一个相对精简的结果，如两组各自的均值、均值的差异、T 统计量或者 p 值。
- 当待检验的变量数量增加时，`ttest`命令的使用会大大增加我们复制、粘贴等手工处理的工作量。


### 2.2 median 命令

`median` 同样为 Stata 的官方命令，基本语法如下：
```stata
median varname [if] [in] [weight], ///
       by(groupvar) [median_options] 
```

**具体解释如下：**
- `median`命令的基本语法和使用方法与 `ttest` 命令很相似，它们都是比较变量 **varname** 在选项 **by(groupvar)** 确定的两组之间的差异。
- `exact` 计算 Fisher's exact test 下的显著性水平。
- `medianties(below|above|drop|split)` 可以对等于中位数的样本进行不同的处理。

下面，我们仍以 Stata 内置的数据集 **nlsw88.dta** 为例进行演示。
首先，我们想要检验未婚组和已婚组的工资中位数差异是否显著。
从检验结果可以发现, Pearson chi2 为 0.6277，p 值为 0.428 ，无法拒绝两组工资中位数相同的原假设。

```stata
*-利用median对单个变量wage进行组间中位数差异检验
. median wage, by(married)

Median test

   Greater |
  than the |        married
    median |    single    married |     Total
-----------+----------------------+----------
        no |       393        730 |     1,123 
       yes |       411        712 |     1,123 
-----------+----------------------+----------
     Total |       804      1,442 |     2,246 

          Pearson chi2(1) =   0.6277   Pr = 0.428

   Continuity corrected:
          Pearson chi2(1) =   0.5599   Pr = 0.454
```

我们可以通过添加 **exact**选项来计算 Fisher's exact test 方法下的显著性水平，结果依然是不显著的：

```stata
*-通过exact选项计算Fisher's exact test方法下的显著性水平
. median wage, by(married) exact

Median test

   Greater |
  than the |        married
    median |    single    married |     Total
-----------+----------------------+----------
        no |       393        730 |     1,123 
       yes |       411        712 |     1,123 
-----------+----------------------+----------
     Total |       804      1,442 |     2,246 

          Pearson chi2(1) =   0.6277   Pr = 0.428
           Fisher's exact =                 0.454
   1-sided Fisher's exact =                 0.227

   Continuity corrected:
          Pearson chi2(1) =   0.5599   Pr = 0.454
```

> **Notes：**
- `median`命令虽然进行了中位数差异的检验，但并没有汇报两组各自的中位数及其差异。这与我们在期刊论文中要求的汇报结果有一定差异。
- `median`命令每次只能对一个变量进行检验，无法披露对多个变量进行检验，也面临着复制、粘贴等手工整理结果的困扰。


### 2.3 ttable2 命令

这是由中南财经大学的李春涛和张璇老师编写的外部命令，基本语法和使用方法和 `ttest` 非常相似，好处在于可以批量对多个变量进行组间均值差异检验。

基本语法如下：

```stata
ttable2 varlist [if] [in], by(groupvar) format(%fmt)
```

**解释：**
- `varlist`：是待检验的一组变量名称。
- `groupvar`：可以是数值型，也可以是字符串型。需要注意的是，多数情况下，**groupvar** 只有有两个取值。
当其有多个取值时，需要保证在使用 **[if]** 和 **[in]** 限定样本范围后，**groupvar** 只有两个取值。
- `format`：用以控制检验结果的输出格式。

比如，我们想要对比已婚组和未婚组在 **wage, ttl_exp, tenure, age** 四个变量上的均值差异。具体操作如下：

```stata
*-利用ttable2对多个变量进行组间均值差异检验
. ttable2 wage ttl_exp tenure age, by(married)

--------------------------------------------------------------------------
Variables   G1(single)       Mean1     G2(married)    Mean2      MeanDiff
--------------------------------------------------------------------------
wage          804             8.081       1442         7.592      0.489*
ttl_exp       804            12.920       1442        12.320      0.600***
tenure        799             6.254       1432         5.824      0.431*
age           804            39.219       1442        39.117      0.102
--------------------------------------------------------------------------
```

我们还可以通过 **format** 选项控制检验结果输出的显示格式。比如，将其设置为 `format(%6.2f)`,可以将小数位的显示控制为小数点后 2 位。

```stata
*-通过format选项控制显示格式
. ttable2 wage ttl_exp tenure age, by(married) format(%6.2f)

--------------------------------------------------------------------------
Variables   G1(single)       Mean1     G2(married)    Mean2      MeanDiff
--------------------------------------------------------------------------
wage          804            8.08         1442        7.59       0.49*
ttl_exp       804           12.92         1442       12.32       0.60***
tenure        799            6.25         1432        5.82       0.43*
age           804           39.22         1442       39.12       0.10
--------------------------------------------------------------------------
```

有些时候，我们面对的组别大于 2 类。我们需要将其中一组设置为基准组，然后分别对比其他组和基准组在多个变量上的均值差异。此时，可以用 `[if]`和`[in]`限定样本范围的方式来实现。
比如，我们想要分别对比黑种人(black)和其他有色人种(other)与白种人(white)在多个变量上的均值差异。此时，**race** 的组别包括 **1(white)**、 **2(black)** 和 **3(other)** 三种。而 **by(groupvar)** 
在执行检验的时候只能为两组。我们可以通过 **if race==1 | race==2** 或 **if inlist(race,1,2)** 语句来限定样本范围为指定的 1(white) 和 2(black) 两组。

```stata
*-对race进行频数统计
. tab race

       race |      Freq.     Percent        Cum.
------------+-----------------------------------
      white |      1,637       72.89       72.89
      black |        583       25.96       98.84
      other |         26        1.16      100.00
------------+-----------------------------------
      Total |      2,246      100.00

*-通过if设定样本范围，比较白人和黑人的均值差异
. ttable2 wage ttl_exp tenure age if (race==1|race==2), by(race)

--------------------------------------------------------------------------
Variables   G1(white)        Mean1     G2(black)      Mean2      MeanDiff
--------------------------------------------------------------------------
wage          1637            8.083       583          6.845      1.238***
ttl_exp       1637           12.469       583         12.719     -0.250
tenure        1627            5.808       578          6.502     -0.693***
age           1637           39.272       583         38.811      0.461***
--------------------------------------------------------------------------

*-通过if设定样本范围，比较白人和其他人种的均值差异
. ttable2 wage ttl_exp tenure age if race==1 | race==3, by(race)

--------------------------------------------------------------------------
Variables   G1(white)        Mean1     G2(other)      Mean2      MeanDiff
--------------------------------------------------------------------------
wage          1637            8.083       26           8.551     -0.468
ttl_exp       1637           12.469       26          12.599     -0.130
tenure        1627            5.808       26           4.949      0.860
age           1637           39.272       26          39.308     -0.035
--------------------------------------------------------------------------
```

Note：
- `ttable3`可以批量对多个变量进行检验。
- `ttable3`的输出结果添加了标识显著性水平的*号，显示格式可以通过`format()`选项控制，基本接近期刊论文的要求。
- `ttable3`只能进行标准的 T 检验，无法执行非匹配样本非同方差的 T 检验。
- `ttable3`无法输出进行统计推断的 t 统计量或者 p 值，这与期刊论文的要求有差异。


### 2.4 ttable3 命令

`ttable3` 由中山大学的连玉君老师编写，主要的改进是在 `ttable2` 基础上进一步支持组间**中位数**差异的检验。

基本语法如下:
```stata
ttable3 varlist [if] [in], by(groupvar) ///
  [ format(%fmt) unequal welch median   ///
    rowname notitle nostar tvalue pvalue ]
```

`ttable3`命令的基本语法和使用方法和 `ttable2` 命令基本一样。由于是 `ttable2` 命令的升级版，所以增加了一些新的选项，功能更为强大，使用也更加灵活。这些新的选项主要包括：
- `median`：将默认的组间**均值**差异检验更换为组间**中位**数差异检验。如果不添加 `median` 选项，则默认执行组间均值差异检验。
- `unequel`：将在非配对样本和非同方差的假设下进行均值差异检验。需要注意的是，该选项只能在均值差异的 T 检验中设定。如果是中位数差异检验，该选项是不可以设定。
- `tvalue/pvalue`：控制结果表格中的` t-value`或者`p-value`。需要注意的是，这两个选项不能同时添加。我们只能选择报告` t-value`或者`p-value`中的一个。
- `notitle`：将检验结果表格的标题去除掉。这有助于使用`logout`命令将结果导出到 word 或者 excel 文件中。
- `nostar`：将结果表格中表示显著性水平的符号`*`去除掉。如果不添加 `nostar` 选项，则默认以符号`*`标记显著性水平。

下面，我们还是以 **nlsw88.dta** 数据集为例，演示一下 `ttable3` 命令的使用。

我们首先分别使用 `ttable2` 和 `ttable3` 命令对比已婚组和未婚组在 **wage  ttl_exp   tenure   age** 四个变量上的均值差异，检验的结果是一模一样的。

```stata
*-首先，用ttable2实现组间均值差异检验
. ttable2 wage ttl_exp tenure age, by(married)

--------------------------------------------------------------------------
Variables   G1(single)       Mean1     G2(married)    Mean2      MeanDiff
--------------------------------------------------------------------------
wage          804             8.081       1442         7.592      0.489*
ttl_exp       804            12.920       1442        12.320      0.600***
tenure        799             6.254       1432         5.824      0.431*
age           804            39.219       1442        39.117      0.102
--------------------------------------------------------------------------

*-然后，用ttable3实现组间均值差异检验
. ttable3 wage ttl_exp tenure age, by(married)

Two-sample t test with equal variances
--------------------------------------------------------------------------
Variables   G1(single)       Mean1     G2(married)    Mean2      MeanDiff   
--------------------------------------------------------------------------
wage          804             8.081       1442         7.592      0.489*
ttl_exp       804            12.920       1442        12.320      0.600***
tenure        799             6.254       1432         5.824      0.431*
age           804            39.219       1442        39.117      0.102
--------------------------------------------------------------------------
```

然后，我们在 `ttable3` 命令中添加 **median** 选项，将组间 **均值** 差异检验更换为 **中位数** 差异检验。

```stata
*-添加media选项，进行组间中位数差异检验
. ttable3 wage ttl_exp tenure age, by(married) media

Nonparametric equality-of-medians test
--------------------------------------------------------------------------
Variables   G1(single)       Median1   G2(married)    Median2    Diff       
--------------------------------------------------------------------------
wage          804          6.357         1442       6.216       0.628
ttl_exp       804         13.625         1442      12.833       8.958***
tenure        799          3.833         1432       3.750       0.063
age           804         39.000         1442      39.000       0.357
--------------------------------------------------------------------------
```

最后，我们还可以通过控制其它选项来对结果表格进行调整。

```stata
*-添加tvalue选项，为结果添加t_value
. ttable3 wage ttl_exp tenure age, by(married) tvalue

Two-sample t test with equal variances
------------------------------------------------------------------------------------
Variables   G1(single)       Mean1     G2(married)    Mean2      MeanDiff   t-Value
------------------------------------------------------------------------------------
wage          804             8.081       1442         7.592      0.489      1.931*
ttl_exp       804            12.920       1442        12.320      0.600      2.964***
tenure        799             6.254       1432         5.824      0.431      1.771*
age           804            39.219       1442        39.117      0.102      0.760
------------------------------------------------------------------------------------

*-添加pvalue选项，为结果添加p_value
. ttable3 wage ttl_exp tenure age, by(married) pvalue

Two-sample t test with equal variances
------------------------------------------------------------------------------------
Variables   G1(single)       Mean1     G2(married)    Mean2      MeanDiff   p-Value
------------------------------------------------------------------------------------
wage          804             8.081       1442         7.592      0.489      0.054*
ttl_exp       804            12.920       1442        12.320      0.600      0.003***
tenure        799             6.254       1432         5.824      0.431      0.077*
age           804            39.219       1442        39.117      0.102      0.447
------------------------------------------------------------------------------------

*-添加notitle选项，将标题删除
. ttable3 wage ttl_exp tenure age, by(married) notitle
--------------------------------------------------------------------------
Variables   G1(single)       Mean1     G2(married)    Mean2      MeanDiff   
--------------------------------------------------------------------------
wage          804             8.081       1442         7.592      0.489*
ttl_exp       804            12.920       1442        12.320      0.600***
tenure        799             6.254       1432         5.824      0.431*
age           804            39.219       1442        39.117      0.102
--------------------------------------------------------------------------

*-添加nostar选项，将*号删除
. ttable3 wage ttl_exp tenure age, by(married) nostar

Two-sample t test with equal variances
--------------------------------------------------------------------------
Variables   G1(single)       Mean1     G2(married)    Mean2      MeanDiff   
--------------------------------------------------------------------------
wage          804             8.081       1442         7.592      0.489
ttl_exp       804            12.920       1442        12.320      0.600
tenure        799             6.254       1432         5.824      0.431
age           804            39.219       1442        39.117      0.102
--------------------------------------------------------------------------
```

### 2.5 balancetable 命令
上述命令每次只能比较两个组的均值差异。但有些情况下，我们需要对多个组别进行两两组间均值差异检验。此外，对于回归分析略有了解的读者可能也会想到异方差问题。此时，可以使用 `balancetable` 命令。

该命令与 `ttable2`, `ttable3` 等命令的核心命令存在差异。`ttest`, `ttable3` 等命令都是调用 Stata 官方命令 `ttest` 或 `median` 等非参数检验方法，进而使用循环语句实现一次性处理多个变量的目的。而 `balancetable` 命令则是基于回归分析来检验组间差异的。

例如，对于 **nlsw88.dta** 数据而言，我们可以用前文介绍的 `ttest` 或 `ttable2` 命令检验 **wage** 在已婚 (**married=1**) 和未婚 (**married=0**) 两个样本组间的均值差异：

```stata
. ttable2 wage, by(married)

--------------------------------------------------------------------------
Variables   G1(single)       Mean1     G2(married)    Mean2      MeanDiff
--------------------------------------------------------------------------
wage          804             8.081       1442         7.592      0.489*
--------------------------------------------------------------------------
```

其实，我们也完全可以使用 `regress` 命令，通过 OLS 估计来实现上述目的：

```stata
. regress  wage married, noheader
------------------------------------------------------------------------------
        wage |      Coef.   Std. Err.      t    P>|t|     [95% Conf. Interval]
-------------+----------------------------------------------------------------
     married |     -0.489      0.253    -1.93   0.054       -0.985       0.008
       _cons |      8.081      0.203    39.83   0.000        7.683       8.479
------------------------------------------------------------------------------
```

这里，常数项的估计系数 **_cons = 8.081** 其实就是 *E*[*Wage*|*married*=0]，也就是解释变量 (**married**) 取值为 0 时，被解释变量 (**wage**) 的样本均值；而 **married** 的系数 **-0.489** 则是两个样本组的 **wage** 均值之差，即 *E*[*Wage*|*married*=1] - *E*[*Wage*|*married*=0] = -0.489。

了解了上述背景后，就很容易理解如下范例的含义了：

#### 
```
sysuse "nlsw88.dta", clear

*-待对比变量组
  global xx "wage hours ttl_exp "

*-基本表格
. balancetable union $xx using "tab1.xlsx", replace
  shellout "tab1.xlsx"  //打开输出的文件，在推文中截图呈现
  
*-设定列名
. balancetable union $xx using "tab2.xlsx", replace ///
      ctitles("Union" "Non-Union" "Difference")
  shellout "tab2.xlsx"
  
*-异方差，p-value 
. balancetable union $xx using "tab3.xlsx", replace ///
      vce(robust) pval ///
      ctitles("Union" "Non-Union" "Difference")
  shellout "tab3.xlsx"
	  
*-三个组：两两对比
  *gen white = (race==1) // Control 组
  gen black = (race==2)  // Treat A
  gen other = (race==3)	 // Treat B
  balancetable (mean if race==1) (mean if race==2) (mean if race==3) ///
     (diff black if race!=3) ///  // White v.s. Black
     (diff other if race!=2) ///  // White v.s. other
     (diff black if race!=1) ///  // black v.s. other
     $xx using "tab4.xlsx",  ///
     ctitles("Mean White" "Mean Black" "Mean Other" "Black vs White" "Other vs White" "Black vs Other")
	
shellout "tab4.xlsx"       // 打开自动输出的表格
```
受限于篇幅，这里仅展示上例中 **tab4.xlsx** 的输出效果：
![连享会-balancetable 自动输出的表格-tab4.xlsx](https://upload-images.jianshu.io/upload_images/7692714-c8867f3fe6db5350.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)


&emsp;

## 3. 小结

我们分别介绍了运用 `ttest`、`median`、`ttable2`、`ttable3` 和 `balancetable` 等五个命令进行组间 **均值** 或 **中位数** 差异的检验。他们各有千秋。
- 官方命令`ttest`、`median` 虽然可以采用多种方法进行不同的均值或中位数检验，但无法一次性对多个变量进行批量检验；而且汇报的结果要么过于详细，要么过于精简，往往无法满足我们在论文中的呈现要求。如果我们采用复制、粘贴的方式整理表格，不仅工作量大、容易出错，还无法实现检验结果的自动化输出。如此一来，当研究样本发生微小变动之后，检验的结果也随之发生了变化，我们又要重复上述复制、粘贴的过程，这样做显然是不明智的。
- `ttable2` 命令能够批量对多个变量进行组间均值差异检验，检验结果的输出与大多数期刊论文的要求基本一致。但是，`ttable2` 只能执行标准的均值差异 T 检验，无法执行非配对样本非同方差下的 T 检验。`ttable2`也无法汇报 检验的 t 统计量或者 p 值。此外，`ttable2`也无法进行组间均值中位数差异检验。
- `ttable3` 命令是 `ttable2` 的升级版。它增加了组间中位数差异检验的功能。对于组间均值差异的 T 检验，`ttable3` 还可以执行非配对样本非同方差下的检验。此外，`ttable3`还增加了一些新的选项实现更为灵活的功能。比如，通过`tvalue`或者`pvalue`选项汇报检验的 t 统计量或者 p 值。
- `balancetable` 以基于回归分析来检验组间均值差异，因此，可以很方便地控制异方差，或得到各类形式的稳健性标准误 (如聚类调整后的标准误)。同时，该命令也只支持一次性对多个组别进行两两对比，只是语法上稍显繁复。
- 上述命令的结果都可以使用 `estout`，`logout` 等命令输出到 **Word** 或 **Excel** 中，以便直接插入论文中。


&emsp;

#### 附录：文中涉及的命令集合

```stata
*--------------
*-ttest 命令
*--------------

sysuse "nlsw88.dta", clear  //调用内置数据集nlsw88
*对married进行频数统计
tab married
*利用ttest对单个变量wage进行组间均值差异检验
ttest wage, by(married)
*通过unequal选项进行非配对样本非同方差的检验
ttest wage, by(married) unequal
*通过level选项更改置信水平为99%
ttest wage, by(married) unequal level(99)


*--------------
*-median 命令
*--------------

*利用median对单个变量wage进行组间中位数差异检验
median wage, by(married)
*通过exact选项计算Fisher's exact test方法下的显著性水平
median wage, by(married) exact


*--------------
*-ttable2 命令
*--------------

*利用ttable2对多个变量进行组间均值差异检验
ttable2 wage ttl_exp tenure age, by(married)
*通过format选项控制显示格式
ttable2 wage ttl_exp tenure age, by(married) format(%6.2f)
*对race进行频数统计
tab race
*通过if设定样本范围，比较白人和黑人的均值差异
ttable2 wage ttl_exp tenure age if race==1 | race==2, by(race)
*通过if设定样本范围，比较白人和其他人种的均值差异
ttable2 wage ttl_exp tenure age if race==1 | race==3, by(race)


*--------------
*-ttable3 命令
*--------------

*首先，用ttable2实现组间均值差异检验
ttable2 wage ttl_exp tenure age, by(married)
*然后，用ttable3实现组间均值差异检验
ttable3 wage ttl_exp tenure age, by(married)
*添加media选项，进行组间中位数差异检验
ttable3 wage ttl_exp tenure age, by(married) media
*添加tvalue选项，为结果添加t_value
ttable3 wage ttl_exp tenure age, by(married) tvalue
*添加pvalue选项，为结果添加p_value
ttable3 wage ttl_exp tenure age, by(married) pvalue
*添加notitle选项，将标题删除
ttable3 wage ttl_exp tenure age, by(married) notitle
*添加nostar选项，将*号删除
ttable3 wage ttl_exp tenure age, by(married) nostar


*--------------------
*- balancetalbe 命令
*--------------------

sysuse "nlsw88.dta", clear

*-待对比变量组
  global xx "wage hours ttl_exp "

*-基本表格
. balancetable union $xx using "tab1.xlsx", replace
  shellout "tab1.xlsx"  //打开输出的文件，在推文中截图呈现
  
*-设定列名
. balancetable union $xx using "tab2.xlsx", replace ///
      ctitles("Union" "Non-Union" "Difference")
  shellout "tab2.xlsx"
  
*-异方差，p-value 
. balancetable union $xx using "tab3.xlsx", replace ///
      vce(robust) pval ///
      ctitles("Union" "Non-Union" "Difference")
  shellout "tab3.xlsx"
	  
*-三个组：两两配对检验
  *gen white = (race==1) // Control 组
  gen black = (race==2)  // Treat A
  gen other = (race==3)	 // Treat B
  balancetable (mean if race==1) (mean if race==2) (mean if race==3) ///
     (diff black if race!=3) ///  // White v.s. Black
     (diff other if race!=2) ///  // White v.s. other
     (diff black if race!=1) ///  // black v.s. other
     $xx using "tab4.xlsx", replace ///
     ctitles("Mean White" "Mean Black" "Mean Other" "Black vs White" "Other vs White" "Black vs Other")
	
. shellout "tab4.xlsx"  
```


&emsp;

[![连享会-空间计量现场班 (2019年6月27-30日，西安)](http://upload-images.jianshu.io/upload_images/7692714-26cfc907a456a153.jpeg?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240 "连享会-空间计量现场班 (2019年6月27-30日，西安)")](https://gitee.com/arlionn/Course/blob/master/Spatial.md)

&emsp;

>#### 关于我们

- 【**Stata 连享会(公众号：StataChina)**】由中山大学连玉君老师团队创办，旨在定期与大家分享 Stata 应用的各种经验和技巧。
- 公众号推文同步发布于 [CSDN-Stata连享会](https://blog.csdn.net/arlionn) 、[简书-Stata连享会](http://www.jianshu.com/u/69a30474ef33) 和 [知乎-连玉君Stata专栏](https://www.zhihu.com/people/arlionn)。可以在上述网站中搜索关键词`Stata`或`Stata连享会`后关注我们。
- 点击推文底部【阅读原文】可以查看推文中的链接并下载相关资料。
- Stata连享会 [精品专题](https://gitee.com/arlionn/stata_training/blob/master/README.md)  || [精彩推文](https://github.com/arlionn/stata/blob/master/README.md)

>#### 联系我们

- **欢迎赐稿：** 欢迎将您的文章或笔记投稿至`Stata连享会(公众号: StataChina)`，我们会保留您的署名；录用稿件达`五篇`以上，即可**免费**获得 Stata 现场培训 (初级或高级选其一) 资格。您也可以从 [连享会选题平台](https://gitee.com/Stata002/StataSX2018/wikis/Home) &rarr; [002_备选主题] 中选择感兴趣的题目来撰写推文。
- **意见和资料：** 欢迎您的宝贵意见，您也可以来信索取推文中提及的程序和数据。
- **招募英才：** 欢迎加入我们的团队，一起学习 Stata。合作编辑或撰写稿件五篇以上，即可**免费**获得 Stata 现场培训 (初级或高级选其一) 资格。
- **联系邮件：** StataChina@163.com

>#### 往期精彩推文
- [Stata连享会推文列表](https://www.jianshu.com/p/de82fdc2c18a) 
- Stata连享会 [精品专题](https://gitee.com/arlionn/stata_training/blob/master/README.md)  || [精彩推文](https://github.com/arlionn/stata/blob/master/README.md)

[![点击此处-查看完整推文列表](https://images.gitee.com/uploads/images/2019/0530/105638_1d68b72a_1522177.png "连享会(公众号: StataChina)推文列表")](https://gitee.com/arlionn/Course/blob/master/README.md)





---
[![欢迎加入Stata连享会(公众号: StataChina)](https://upload-images.jianshu.io/upload_images/7692714-fbec0770ffb974d8.jpg?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240 "扫码关注 Stata 连享会")](https://gitee.com/arlionn/Course/blob/master/README.md)

