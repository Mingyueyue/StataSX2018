
> 修改建议:
1. `ttable2` 作者信息需要更正：原作者为：张璇和李春涛；连玉君对其进行了修订和完善。  
2. 补充如下命令的介绍  
  - `balancetable`，外部命令，需要安装。这个命令是基于回归分析进行组间均值差异检验的。优点：
    - (1) 可以一次性进行多组的对比；
    - (2) 可以在控制其他变量的情况下进行组间差异比较；
  - `ttesttable`
  - 其他你们认为对上述命令功能有所补充的命令。

&emsp;


### balancetable 命令

```stata

sysuse "nlsw88.dta", clear


global xx "wage hours ttl_exp "

*-基本表格
. balancetable union $xx using "tab1.xlsx", replace
  shellout "tab1.xlsx"  //打开输出的文件，在推文中截图呈现
  
*-设定列名
. balancetable union $xx using "tab1.xlsx", replace ///
      ctitles("Union" "Non-Union" "Difference")
  shellout "tab1.xlsx"
  
*-异方差，p-value 
. balancetable union $xx using "tab1.xlsx", replace ///
      vce(robust) pval ///
      ctitles("Union" "Non-Union" "Difference")
  shellout "tab1.xlsx"
	  
*-三个组：你们酌情修改一下这个例子，用 industry 或 race 变量作为分组变量
    Complex syntax with 2 treatment arms and 1 control group
        . generate treat_A = (treat==1)
        . generate treat_B = (treat==2)
        . balancetable (mean if treat==0) (mean if treat==1) (mean if treat==2) (diff treat_A if
            treat!=2) (diff treat_B if treat!=1) (diff treat_A if treat!=0) $depvarlist using
            "myfile.xlsx", ///
            ctitles("Mean Control" "Mean treat. A" "Mean treat. B" "Treat. A vs Control" "Treat. B vs
            Control" "Treat. A vs Treat. B")
        . drop treat_A treat_B
	  
```