># 关于在gitee 环境输入LaTeX 公式的部分问题

>## 1 大括号输入问题
1 ```$$\left\\{ \right\\}$$``` $$\left\\{ \right\\}$$
下面这些写法或多或少有问题

1 ```${}$``` ${}$ 大括号未显示

1 ```${y=a+bx}$``` ${y=a+bx}$ 大括号未显示

1 ```$\{y=a+bx\}$``` $\{y=a+bx\}$ 大括号未显示

1 ```$\\{y=a+bx\\}$``` $\\{y=a+bx\\}$ 这时候未显示
 
1 ```$$\left{ \right}$$``` $$\left{ \right}$$

1 ```$$\left\{ \right\}$$``` $$\left\{ \right\}$$

 **所以在使用 {} 时候，如果是公式请使用``\\{ \\}``或者 ``\left\\{ \right\\}``** 


>## 2 并列公式问题

2 ```$$\begin{aligned} &y=1 \\\\ &y = 2\end{aligned}$$``` $$\begin{aligned} &y=1  \\\\ &y = 2\end{aligned}$$
下面这个就不能并列

2 ```$$\begin{aligned} &y=1 \\ &y = 2\end{aligned}$$``` $$\begin{aligned} &y=1 \\ &y = 2\end{aligned}$$

2 ```$$ a = \left\\{ \begin{aligned} &y=1 \\\\ &y = 2\end{aligned} \right.$$```$$ a = \left\\{ \begin{aligned} &y=1 \\\\ &y = 2\end{aligned} \right.$$

**换行请使用 ```\\\\``` 而不是 ```\\```**

>## 5 推测原因

可能是什么 转义字符 之类的

>## 4 其他测试

3 ```$y = a + b x$``` $y = a + b x$

4 ```$$y = a + b x$$```  $$y = a + b x$$
 
5 ```$$\left( \right.$$``` $$\left( \right.$$

5 ```$$\left\( \right.$$``` $$\left\( \right.$$

6 ```$$\left\( \right)$$``` $$\left\( \right)$$

7 ```$$\left[ \right]$$``` $$\left[ \right]$$

8 $$\hat{a} $$

9 $$\lim_{x\to \infty}$$

10 $$\overline{b}$$

