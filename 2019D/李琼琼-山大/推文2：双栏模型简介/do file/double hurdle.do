******double hurdle ******
/* 生成数据进行模型（1） */
* z_i x_i服从均匀分布
clear all
set obs 1000
set seed 123   // 设置种子，为了使重复模拟过程结果相同
gen z_i = uniform()  // （0,1)均匀分布
set seed 1234
gen x_i = uniform() 
set obs 1000
set seed 12345  
gen e_i2 = invnormal(uniform()) // 标准正态分布
set seed 12435
gen n_i = invnormal(uniform())
gen e_i1 = 0.5*e_i2 + sqrt(1-0.5*0.5)*n_i
gen d_i = 0
replace d_i = 1 if -2 + 4*z_i + e_i1 > 0 
gen y_i2 = 0.5 + 0.3*x_i + e_i2
gen y_i1 = 0
replace y_i1 = y_i2 if y_i2 > 0 
gen y_i = d_i*y_i1
save data_process1.dta, replace

/* 对模拟的数据进行展示 */
use data_process1.dta, clear
hist y_i if d_i == 1,title(Conditional on passing first hurdle) scheme(sj)
graph save y_i_1.gph, replace
hist y_i ,title(All Data) scheme(sj)
graph save y_i_2.gph, replace
gr combine  y_i_1.gph  y_i_2.gph
graph save y_i.png, replace

/* dhreg估计，并且和tobit估计进行对比 */
use data_process1.dta, clear
tobit y_i x_i, ll(0)
dhreg y_i x_i, hd(z_i)
dhreg y_i x_i, hd(z_i) millr  //使用millr选项，鉴于篇幅有限结果未在文中展示



/* 生成panel数据模型（2） */

clear all
set obs 2000
set seed 10011979
gen z_i = uniform()
set seed 1111122
gen u_i = rnormal(0, 3)
set seed 1222222
gen n_i = rnormal(0,3)
gen e_i1 = 0.9*u_i + sqrt(1-0.9^2)*n_i
gen d_i = 0
replace d_i = 1 if -2 + 4*z_i + e_i1 > 0 
* 生成面板数据x_it,y_it2,y_it1,y_it
gen id = _n
expand 5 // Set the number of periods (5)
bys id: gen t = _n
xtset id t
bysort id (t): gen x_it = rnormal(0,1) + rnormal(0,1) if _n==1 
bysort id (t): replace x_it = .8 * x_it[_n-1] + rnormal(0,1) if _n!=1
gen e_i2 = rnormal(0,1)
gen y_it2 = 0.5 + 0.3*x_it + u_i + e_i2
corr e_i2 u_i // 检查e_i2和u_i的相关性
gen y_it1 = 0
replace  y_it1 = y_it2 if y_it2 > 0 
gen y_it = y_it1*d_i
save data_process2.dta, replace

help  mdraws // 进行xtdhreg前先安装
xtdhreg y_it x_it, hd(z_i)
bootdhreg y_it x_it, hd(z_i)  cluster(id) capt


















set obs 1000
set seed 123   // 设置种子，为了使重复模拟过程结果相同
gen z_i = uniform()  
set seed 1234
gen x_i = uniform() 
*  e_i2 n_i服从标准正态分布
set obs 1000
set seed 12345   // 设置种子，为了使重复模拟过程结果相同
gen e_i2 = invnormal(uniform())
set seed 12435
gen n_i = invnormal(uniform())
* e_i1
gen e_i1 = 0.5*e_i2 + sqrt(1-0.5*0.5)*n_i
* d_i* 
gen d_i = 0
replace d_i = 1 if -2 + 4*z_i + e_i1 > 0 
* y_i**
gen y_i2 = 0.5 + 0.3*x_i + e_i2
* y_i*
gen y_i1 = 0
replace y_i1 = y_i2 if y_i2 > 0 
* y_i
gen y_i = d_i*y_i1

/* 对模拟的数据进行展示 */
hist y_i if d_i == 1,title(Conditional on passing first hurdle) scheme(sj)
graph save y_i_1.gph, replace
hist y_i ,title(All Data) scheme(sj)
graph save y_i_2.gph, replace
gr combine  y_i_1.gph  y_i_2.gph
graph save y_i.png, replace

/* dhreg估计，并且和tobit估计进行对比 */
tobit y_i x_i, ll(0)
dhreg y_i x_i, hd(z_i)
dhreg y_i x_i, hd(z_i) millr


