
&emsp;

> 作者：李琼琼 (山东大学)    
>     
> Stata 连享会：  [知乎](https://zhuanlan.zhihu.com/arlion) | [简书](http://www.jianshu.com/u/69a30474ef33) | [码云](https://gitee.com/arlionn) | [CSDN](https://blog.csdn.net/arlionn) | [StataChina公众号](https://mp.weixin.qq.com/s/RwkuPpLS7bI5C5OhRqjkOw) 
  

> #### [连享会：内生性问题及估计方法专题](https://mp.weixin.qq.com/s/FWpF5tE68lAtCEvYUOnNgw)

[![连享会-内生性专题现场班-2019.11.14-17](https://images.gitee.com/uploads/images/2019/1014/000523_2f3a75c4_1522177.png)](https://gitee.com/arlionn/Course/blob/master/Done/2019Endog.md)

> #### [连享会#金秋十月 @ 空间计量专题 (2019.10.24-27,成都)](https://zhuanlan.zhihu.com/p/76341418)
&emsp;
[![2019.10-杨海生-空间计量专题-连享会](https://images.gitee.com/uploads/images/2019/1014/000523_905994f2_1522177.png)](https://mp.weixin.qq.com/s/aU6B9HZaf2BSHF7lSLe6LA)



&emsp;







## 1. 为什么使用三重差分法？

双重差分法的重要假设是对照组和实验组的时间趋势一样，而当控制组和实验组的时间趋势不同，则无法得到一致的实验估计量，需要进一步改进双重差分估计量。 平行趋势检验可以看往期推文[多期倍分法(DID)：平行趋势检验图示](https://www.jianshu.com/p/384776721340)。当选择的控制组与实验组时间趋势不同时，可以采用合成控制法(Synthetic Control Method)即对多个对照组加权构造成一个虚拟的对照组和三重差分法 (Difference in Difference in Difference) , 相比较合成控制法，DDD 操作更加简单，本期主要对三重差分法进行介绍。
通过下面具体的例子，我们来说明三重差分法的原理。假设美国 B 州针对 65 岁或以上的老年人 (实验组，old = 1) 引入一项新的医疗保健政策，其他年龄群体不适用。考察此政策对健康状况的影响，选用 B 州 65 岁以下群体 (old = 0) 作为对照组。由于人的健康状况随时间的变化并不是线性的，而不同年年龄组的个体的健康状况变化的时间趋势也存在差异，这会导致传统 DID 方法的前提条件——共同趋势假设 (Common Trend) 无法得到满足。简言之，实验组和对照组人群的健康状况随时间的变化趋势不一致。这种时间趋势差异的影响可以通过计算相邻的 A 州 65 岁及以上老年人和年轻群体相对健康情况变化差异来捕捉 (相当于再用一次 DID)。

$$Y_{ijt} = \alpha_0+ \color\green{\beta_1} \color\red{B_i \times old_j \times time_t} +\beta_2 B_i \times old_j+ \beta_3 B_i \times time_t+  \\   \beta_4old_j \times time_t +\gamma_1 \times B_i+\gamma_2 \times old_j+\gamma_3 \times time_t+\varepsilon_{ijt} \qquad (1) $$ 

$E( \overline{Y_{BO2}}) = \alpha_0 + \beta_1 +\beta_2 +\beta_3+\beta_4 +\gamma_1+\gamma_2+\gamma_3$  
$E( \overline{Y_{BO1}}) = \alpha_0 +\beta_2+ \gamma_1+\gamma_2 $
$E( \overline{Y_{BY2}}) = \alpha_0 + \beta_3+ \gamma_1+\gamma_3$  
$E( \overline{Y_{BY1}}) = \alpha_0 + \gamma_1 $
$E( \overline{Y_{AO2}}) = \alpha_0 +\beta_4+ \gamma_2+\gamma_3$  
$E( \overline{Y_{AO1}}) = \alpha_0+\gamma_2 $
$E( \overline{Y_{AY2}}) = \alpha_0 +\gamma_3$  
$E( \overline{Y_{AY1}}) = \alpha_0 $

经由 **B 州** 的 DID 可以得到 **医疗政策差异** 和 **年龄差异** 对健康的平均影响：
$$ 
[E( \overline{Y_{BO2}}) - E( \overline{Y_{BO1}})] - [E( \overline{Y_{BY2}}) - E( \overline{Y_{BY1}})] =\beta_1+\beta_4   \qquad (2a)
$$

而经由 **A 州** DID 得到的只是 **年龄差异** 对健康的平均影响: 

$$[E( \overline{Y_{AO2}}) - E( \overline{Y_{AO1}})] - [E( \overline{Y_{AY2}}) - E( \overline{Y_{AY1}})] =\beta_4    \qquad (2b)
$$

式 (2a) 减去式 (2b)，即可得到 **医疗政策效果** 的平均效应 $\beta_1$。$\beta_1$ 是我们在建立 DDD 模型时最感兴趣的估计系数。

&emsp;
> #### [连享会计量方法专题……](https://mp.weixin.qq.com/s/hWtncj56PeFNL4yg2-va0Q)


## 2. 三重差分法案例及 Stata 实现

2007 年，中国开始实行 $SO_2$ 碳排放权交易试点政策，先后批复了江苏、天津、浙江、河北、山西等 11 个排放权交易试点省份，但是还有很多省份没有作为试点地区。任胜钢等 (2019) 收集了不同地区不同行业在实施试点政策前后多年全要素增长率的数据，并使用双重差分法 (DID) 估计排污权对上市企业全要素的影响：
$$Y_{ijt} = \beta_0+\beta_1time*treat_2 + \lambda X + \gamma_t + \mu_i + \eta_j+\varepsilon_{ijt}$$   &emsp;&emsp;其中，$Y_{ijt}$ 表示位于省份 $i$ 行业 $j$ 的企业在 $t$ 年的全要素生产率，$time$ 在排污权交易试点后取 1 否则取 0 ，$treat_2$ 表示当试点企业位于试点地区时取 1 否则取 0 ，系数 $\beta_1$ 即为排污权交易试点政策对全要素生产率的影响，$X$ 表示一系列控制变量，$\gamma_t$ 代表时间固定效应, $\eta_j$ 代表行业固定效应，$\mu_i$ 代表地区固定效应，$\varepsilon_{ijt}$ 为随机误差项。
下面我们通过使用 任胜钢等 (2019) 在「中国工业经济」期刊主页上提供的数据 <http://www.ciejournal.org/Magazine/show/?id=61750> 对这一回归过程进行分析。

```stata
*-Notes:
* (1) tt 为试点前后和处理效应的交乘项，
* (2) zcsy-lnzlb 为控制变量，
* (3) SO2 ==1 表明样本均为排放 SO2 的上市企业

. use "https://gitee.com/arlionn/data/raw/master/data01/tfp_DDD.dta", clear
. reg lntfp tt zcsy lf age owner sczy lnaj lnlabor lnzlb ///
      i.year i.area i.ind                                ///
      if so2==1,cluster(area)     
	  
Linear regression                           Number of obs     =      3,479
                                            F(29, 30)         =          .
                                            Prob > F          =          .
                                            R-squared         =     0.3707
                                            Root MSE          =     .79017

                              (Std. Err. adjusted for 31 clusters in area)
--------------------------------------------------------------------------
         |               Robust
   lntfp |      Coef.   Std. Err.      t    P>|t|     [95% Conf. Interval]
---------+----------------------------------------------------------------
      tt |   .2707783   .0660617     4.10   0.000     .1358623    .4056943
    zcsy |   .0128422    .002167     5.93   0.000     .0084166    .0172678
      lf |  -.0068839   .0060244    -1.14   0.262    -.0191875    .0054197
     age |  -.0043661   .0036656    -1.19   0.243    -.0118523    .0031201
   owner |    .098856   .0647004     1.53   0.137    -.0332799    .2309918
    sczy |   .0214466   .0073615     2.91   0.007     .0064125    .0364808
    lnaj |    .059886   .0275764     2.17   0.038     .0035674    .1162046
 lnlabor |    .159245   .0325336     4.89   0.000     .0928026    .2256874
   lnzlb |   .0624774   .0153048     4.08   0.000     .0312208     .093734
```

回归结果显示排污权交易制度对全要素生产率的回归系数为 0.2708 (在 1% 的水平上显著)，表明中国 $SO_2$ 排污权交易试点政策显著提高了上市企业的全要素生产率。考虑到采用了 2004-2005 年的面板数据，下面使用控制个体特征的固定效应模型 (FE) 进行回归，结果依旧显著：

```stata
. use "https://gitee.com/arlionn/data/raw/master/data01/tfp_DDD.dta", clear
. xtreg lntfp tt zcsy lf age owner sczy lnaj lnlabor lnzlb ///
        i.year i.company  ///
        if so2==1, cluster(area)
	  
Random-effects GLS regression               Number of obs     =      3,479
Group variable: company                     Number of groups  =        290

R-sq:                                       Obs per group:
     within  = 0.1656                                     min =         11
     between = 1.0000                                     avg =       12.0
     overall = 0.5634                                     max =         12

                                            Wald chi2(18)     =          .
corr(u_i, X)   = 0 (assumed)                Prob > chi2       =          .

                              (Std. Err. adjusted for 31 clusters in area)
--------------------------------------------------------------------------
         |               Robust
   lntfp |      Coef.   Std. Err.      z    P>|z|     [95% Conf. Interval]
---------+----------------------------------------------------------------
      tt |   .2768365   .0708105     3.91   0.000     .1380505    .4156225
    zcsy |   .0091712   .0017686     5.19   0.000     .0057048    .0126376
      lf |  -.0061996   .0061249    -1.01   0.311    -.0182043    .0058051
     age |    .003685   .0015579     2.37   0.018     .0006316    .0067385
   owner |   .2201731   .2733538     0.81   0.421    -.3155905    .7559367
    sczy |   .0064717   .0025088     2.58   0.010     .0015545     .011389
    lnaj |   .0772762   .0254718     3.03   0.002     .0273524       .1272
 lnlabor |   .0367946   .0538583     0.68   0.494    -.0687657     .142355
   lnzlb |  -.0155303   .0227497    -0.68   0.495    -.0601189    .0290583
--------------------------------------------------------------------------
```
然而，双重差分估计策略存在潜在的问题，因为除了 $SO_2$ 排放权交易试点之外，还可能存在其他政策对试点地区和非试点地区产生不一致影响，从而使估计结果进行偏差。需要用三重差分来克服这一问题，即需要找到另外一对不受 $SO_2$ 排放权交易试点政策影响的“处理组”和“对照组”，因为非 $SO_2$ 排放行业不受 $SO_2$ 排污权交易政策影响，此时第二对处理组和对照组的差异只来源于其他政策的影响，将第一对处理组和对照组的差异(包含 $SO_2$ 排污权交易政策和其他政策的差异)减去第二对处理组和对照组的其他政策的差异，得到 $SO_2$ 排污权交易政策的净效应。基于以上分析，构建三重差分模型 (DDD) ：

$$Y_{ijt} = \beta_0+ \beta_1time*treat_2*group+ \beta_2time*treat_2 + \beta_3time*group  \\
+\beta_4treat_2*group\lambda X + \gamma_t + \mu_i + \eta_j+\varepsilon_{ijt}$$

其中，$group$ 为虚拟变量，当企业属于 $SO_2$ 排放行业时为 1 ，否则为 0，$time*treat_2*group$ 为 1 时表示实施试点政策后处于试点地区的 $SO_2$ 排放企业，估计系数 $\beta_1$ 为“三重差分估计量”代表 $SO_2$ 排放交易试点政策对企业全要素生产率的平均处理效果。回归结果如下：

```stata
*-Notes:
*  ttt 为 time*treat*group 交乘项
*  tt  为 time*treat 交乘项
*  treats 为 treat*group 交乘项
*  times  为 time*group  交乘项
*  so2    代表 group 变量

. use "https://gitee.com/arlionn/data/raw/master/data01/tfp_DDD.dta", clear
. reg lntfp ttt tt treats times so2 zcsy lf owner age sczy lnaj lnlabor  ///
      lnzlb i.year i.area i.ind, cluster(area)   
	  
Linear regression                          Number of obs     =      6,645
                                           F(29, 30)         =          .
                                           Prob > F          =          .
                                           R-squared         =     0.3603
                                           Root MSE          =      .8086

                              (Std. Err. adjusted for 31 clusters in area)
--------------------------------------------------------------------------
         |               Robust
   lntfp |      Coef.   Std. Err.      t    P>|t|     [95% Conf. Interval]
---------+----------------------------------------------------------------
     ttt |    .468231   .1063143     4.40   0.000     .2511083    .6853537
      tt |  -.1972127   .0811973    -2.43   0.021    -.3630397   -.0313858
  treats |  -.2544498   .1167903    -2.18   0.037    -.4929675   -.0159321
   times |  -.3633623   .0631812    -5.75   0.000    -.4923955   -.2343292
     so2 |   .2824561   .0746324     3.78   0.001     .1300363    .4348759
    zcsy |   .0143365   .0018823     7.62   0.000     .0104925    .0181806
      lf |  -.0041467    .003852    -1.08   0.290    -.0120136    .0037202
   owner |   .0381963   .0351867     1.09   0.286    -.0336645    .1100572
     age |  -.0041661   .0029964    -1.39   0.175    -.0102855    .0019533
    sczy |   .0265252   .0077333     3.43   0.002     .0107317    .0423186
    lnaj |   .0377505   .0218525     1.73   0.094    -.0068782    .0823793
 lnlabor |   .1899508   .0222101     8.55   0.000     .1445916    .2353099
   lnzlb |   .0694883   .0101917     6.82   0.000     .0486741    .0903024
--------------------------------------------------------------------------
```

采用固定效应模型，回归结果如下：

```stata
. xtreg lntfp ttt tt treats times so2 zcsy lf owner age sczy lnaj ///  
        lnlabor lnzlb i.year i.company, cluster(area)

Random-effects GLS regression             Number of obs     =      6,645
Group variable: company                   Number of groups  =        554

R-sq:                                     Obs per group:
     within  = 0.2069                                   min =         10
     between = 1.0000                                   avg =       12.0
     overall = 0.5661                                   max =         12

                                          Wald chi2(20)     =          .
corr(u_i, X)   = 0 (assumed)              Prob > chi2       =          .

                             (Std. Err. adjusted for 31 clusters in area)
-------------------------------------------------------------------------
        |               Robust
  lntfp |      Coef.   Std. Err.      z    P>|z|     [95% Conf. Interval]
--------+----------------------------------------------------------------
    ttt |   .4508811   .1175351     3.84   0.000     .2205166    .6812457
     tt |  -.1780228   .0874499    -2.04   0.042    -.3494214   -.0066243
 treats |   .0546155   .0982847     0.56   0.578    -.1380189    .2472499
  times |   -.377584   .0705263    -5.35   0.000     -.515813    -.239355
    so2 |   .4183972    .087502     4.78   0.000     .2468965     .589898
   zcsy |   .0101169   .0015694     6.45   0.000      .007041    .0131929
     lf |  -.0040799   .0041514    -0.98   0.326    -.0122164    .0040567
  owner |   .1508904   .1394822     1.08   0.279    -.1224896    .4242705
    age |  -.0665185   .0064701   -10.28   0.000    -.0791997   -.0538373
   sczy |   .0100423   .0035932     2.79   0.005     .0029998    .0170848
   lnaj |    .049198   .0188378     2.61   0.009     .0122766    .0861194
lnlabor |   .0715992   .0329644     2.17   0.030     .0069902    .1362082
  lnzlb |   .0041533   .0144601     0.29   0.774    -.0241879    .0324946
-------------------------------------------------------------------------
```

无论使用 OLS 估计还是个体固定效应模型估计，回归系数 $\beta_1$ 均为为 0.45 以上(在1%的水平上显著)，表明三重差分估计 $SO_2$ 排污权交易试点政策对企业全要素生产率的平均促进效应要高于双重差分估计结果，说明双重差分估计可能低估了政策对企业生产效率的提高。


## 3. 三重差分法的其他应用 —— 作用机理的检验

法律制度如何影响企业债务成本？如果仅从国家或者地区的法律差异来要考察企业债务成本问题，很可能受到遗漏变量、法律变量测度偏误等问题影响，难以清晰揭示法律环境对债务成本地影响。钱雪松等 (2019) 利用 2007 年中国正式通过了《物权法》作为自然实验，从法律变化引致的冲击的角度构造对照组和实验组，运用双重差分法，研究担保了物权制度改革可以降低企业债务融资成本。在采用双重差分之前，作者进行了平行趋势检验，结果表明在《物权法》出台之前两组企业债务成本变动维持相同的趋势，即平行趋势假设是满足的。这里，我们可能会想似乎三重差分检验是没有必要的。作者在这里为了进一步探索《物权法》降低企业的作用机理，从法律制度环境和市场化程度差异性切入，运用三重差分法检验物权改革对企业债务融资成本的影响是否表现出差异性。下面我们做详细分析。

### DID 基准模型

$$
Debtcost_{it} = \alpha_0+ \beta_1Low_i \times After_t+\beta_2 X_{it}+ \mu_i+ \lambda_t  +\varepsilon_{it}  
$$ 

其中，$Debtcost_{it-1}$ 表示企业 $i$ 在时间 $t$ 的融资成本。$Low_i$ 表示在企业在实验组时取1，$After_t$ 为时间虚拟变量，样本观测值在《物权法》出台之后取 1，否则取 0, $X_{it-1}$ 表示一系列控制变量。


### 加入法律制度环境的 DDD 模型

$$
Debtcost_{it} = \alpha_0+ \beta_1 {\color\red {Low_i \times After_t \times Law_i}}+ \beta_2 Low_i \times After_t+ \\ \beta_3Low_i \times Fin_i+ \beta_4After_i \times Law_i+\beta_2 X_{it}+ \mu_i+ \lambda_t  +\varepsilon_{it}  
$$ 

其中，$Law_i$ 是构造法律制度环境虚拟变量，当企业所在省份“市场中介组织的发育与法律制度环境指数”在 2006年 排名前 10 则 $Law$ 取 1 ,否则取 0。在 $Low*After$ 的仍旧显著为负的基础下，${\color\red {Low_i \times After_t \times Law_i}}$ 的系数显著为正，表明:与法律制度环境较好的地区相比，《物权法》对债务成本的降低作用在法律制度环境较差的地区下影响更大。


### 加入金融市场化程度的 DDD 模型

$$
Debtcost_{it} = \alpha_0+ \beta_1 \color \red {Low_i \times After_t \times Fin_i} + \beta_2 Low_i \times After_t+ \\ \beta_3Low_i \times Fin_i+ \beta_4After_i \times Fin_i+\beta_2 X_{it}+ \mu_i+ \lambda_t  +\varepsilon_{it}  
$$

其中，$Fin_i$ 是构造金融化程度的虚拟变量，当企业所在省份「金融业市场化指数」在 2006 年排名前 10 则 $Fin$ 取 1，否则取 0。在 $Low \times After$ 的仍旧显著为负的情况下，$Low \times After \times Fin$ 的系数显著为正，表明:与金融市场化程度较高的地区相比，《物权法》对债务成本的降低作用在金融市场化程度较低的地区相对更大。

因此，在双重差分基础上，建立三重差分估计量可以进一步研究政策影响的异质性差异，更好地评估政策效应。


## 参考文献

1. 高级计量经济学及 Stata 应用[M].高等教育出版社,陈强, 2014.
2. 任胜钢,郑晶晶,刘东华,陈晓红.排污权交易机制是否提高了企业全要素生产率——来自中国上市公司的证据[J].中国工业经济, 2019(05):5-23.
3. 钱雪松,唐英伦,方胜.担保物权制度改革降低了企业债务融资成本吗?——来自中国《物权法》自然实验的经验证据[J].金融研究, 2019(07):115-134.


&emsp;

>#### 关于我们

- 【**Stata 连享会(公众号：StataChina)**】由中山大学连玉君老师团队创办，旨在定期与大家分享 Stata 应用的各种经验和技巧。
- 公众号推文同步发布于 [CSDN-Stata连享会](https://blog.csdn.net/arlionn) 、[简书-Stata连享会](http://www.jianshu.com/u/69a30474ef33) 和 [知乎-连玉君Stata专栏](https://www.zhihu.com/people/arlionn)。可以在上述网站中搜索关键词`Stata`或`Stata连享会`后关注我们。
- 点击推文底部【阅读原文】可以查看推文中的链接并下载相关资料。
- Stata连享会 [计量专题](https://gitee.com/arlionn/stata_training/blob/master/README.md)  || [精彩推文](https://github.com/arlionn/stata/blob/master/README.md)

>#### 联系我们

- **欢迎赐稿：** 欢迎将您的文章或笔记投稿至 StataChina@163.com，我们会保留您的署名；录用稿件达`五篇`以上，即可**免费**获得 Stata 现场培训 (初级或高级选其一) 资格。您也可以从 [连享会选题平台](https://gitee.com/Stata002/StataSX2018/wikis/Home) &rarr; [002_备选主题] 中选择感兴趣的题目来撰写推文，网址为：[https://gitee.com/Stata002/StataSX2018/wikis/Home](https://gitee.com/Stata002/StataSX2018/wikis/Home)。
- **招募英才：** 欢迎加入我们的团队，一起学习 Stata。合作编辑或撰写稿件五篇以上，即可**免费**获得 Stata 现场培训 (初级或高级选其一) 资格。
- **联系邮件：** StataChina@163.com

>#### 往期精彩推文
> Stata连享会 &emsp; [计量专题](https://gitee.com/arlionn/Course/blob/master/README.md)  || [精品课程](https://mp.weixin.qq.com/s/hWtncj56PeFNL4yg2-va0Q) || [简书推文](https://www.jianshu.com/p/de82fdc2c18a)  || [公众号合集](https://mp.weixin.qq.com/s/RwkuPpLS7bI5C5OhRqjkOw) 

[![点击查看完整推文列表](https://images.gitee.com/uploads/images/2019/1014/000524_972c3295_1522177.png)](https://gitee.com/arlionn/Course/blob/master/README.md)

---
![欢迎加入Stata连享会(公众号: StataChina)](https://images.gitee.com/uploads/images/2019/1014/000523_f6f39de0_1522177.jpeg "扫码关注 Stata 连享会")

