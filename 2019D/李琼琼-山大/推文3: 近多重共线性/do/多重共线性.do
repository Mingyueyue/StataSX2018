************* 近多重共线性 ********

/* 严格多重共线性 */
clear
set obs 1000
set seed 1010101
gen x1 = uniform() 
gen x2 = rnormal()*10
gen x3 = rnormal()*2
gen x4 = 3*x1 
gen y  = 4*x1 + .5*x2 + .8*x3 + x4 + rnormal()
reg y x1 x2 x3 x4
fgtest y x1 x2 x3 x4


/* 近似多重共线性 */
clear
set obs 1000
set seed 1010101
gen x1 = uniform() 
gen x2 = rnormal()*10
gen x3 = x2 + uniform() 
gen x4 = 3*x1 + rnormal()*0.3
gen y  = 4*x1 + .5*x2 + .8*x3 + x4 + rnormal()
reg y x1 x2 x3 x4
estat vif  // 计算VIF

/* Farrar-Glauber 检验 */
clear 
*ssc install fgtest
set obs 1000
set seed 987654321
gen x1 = uniform() 
gen x2 = rnormal()*10
gen x3 = x2 + uniform() 
gen x4 = 3*x1 + rnormal()*0.3
gen x5 = rnormal()*5
gen y  = 4*x1 + .5*x2 + .8*x3 + x4 + 10*x5 +rnormal() 

fgtest y x1 x2 x3 x4 x5

/* 相关系数估计 */
corr x1 x2 x3 x4 x5
 
 /* 逐步回归法 检验 */
clear 
set obs 1000
set seed 987654321
gen x1 = uniform() 
gen x2 = rnormal()*10
gen x3 = x2 + uniform() 
gen x4 = 3*x1 + rnormal()*0.3
gen x5 = rnormal()*5
gen y  = 4*x1 + .5*x2 + .8*x3 + x4 + 10*x5 +rnormal() 
stepwise, pe(0.01): reg y x1 x2 x3 x4 x5


/* 主成分分析 */
clear 
set obs 1000
set seed 987654321
gen x1 = uniform() 
gen x2 = rnormal()*10
gen x3 = x2 + uniform() 
gen x4 = 3*x1 + rnormal()*0.3
gen x5 = rnormal()*5
gen y  = 4*x1 + .5*x2 + .8*x3 + x4 + 10*x5 + rnormal() 
gen id = _n

pca x1 x2 x3 x4 x5
screeplot,mean
graph save picture.svg, replace


/ 固定效应和多重共线性 /

clear 
set obs 266
set seed 101234876
gen county = _n // 设置266个城市
gen x0 = rnormal(0,1)
gen x1 = uniform() 
gen x2 = 0.9*rnormal(0,1)
expand 200  // 一个地方有200个观测值
bys county: gen t = _n

bysort county (t): gen z1 = rnormal(0,1) + rnormal(0,1) if _n==1 
bysort county (t): replace z1 = .8 * [_n-1] + rnormal(0,1) if _n!=1
bysort county (t): gen z2 = uniform()  if _n==1 
bysort county (t): replace z2 = 0.9 * [_n-1] + rnormal(0,1) if _n!=1

gen y_i = 0.5 + 0.3*z1 + 3*z2 + 0.8*x1 + x2 + 0.2*x0 + rnormal(0,1)
reg y_i z1 z2 x0 x1  i.county , r 

