
# 基于地理加权回归模型的空间平稳性检验

## 1. 简介

空间异质性是空间数据的主要特征之一，而地理加权回归模型则是探索空间异质性常用的空间工具，近年来在人口，环境，生态，区域经济和区域政策等许多领域都得到了十分广泛的应用。在实际研究中，空间数据复杂多样，有的具有显著的空间异质性，而有的则呈现出空间平稳性。如果将具有空间异质性的数据作为空间平稳性处理，则可能会损失研究对象的某些特性；反之，则可能会引起模型参数估计精度下降等后果。因此，在使用地理加权回归模型时，我们需要考虑以下问题：
1. 对一个空间数据集，使用地理加权回归模型会比使用一般线性回归模型好吗？
2. 建立的地理加权回归模型中每个自变量所对应的回归系数都具有显著的空间异质性吗？即每个回归系数都是随着空间位置的变化而变化的吗？
3. 建立的地理加权回归模型中哪些回归系数是随着空间位置的变化而变化的，哪些是常数？即是建立地理加权回归模型还是应该建立混合地理加权回归模型？

## 2. 平稳性检验

### 2.1. 模型平稳性检验

原假设 $H_0$：

$$
y_i=\sum_{j=1}^p\beta_jx_{ij}+\varepsilon_i, \ i=1,2,\cdots , n. 
$$

备择假设 $H_1$:

$$y_i=\sum_{j=1}^p\beta_j(u_i,v_i)x_{ij}+\varepsilon_i, \ i=1,2,\cdots,n. $$

在残差平方和的基础上，构造 $F$ 检验统计量，过程如下：
在 $H_0$下, 拟合方程(1)，得到残差平方和为：

$${\rm RSS(H_0)}=\boldsymbol Y^{\rm T}\left(\boldsymbol I-\boldsymbol L_{H_0}\right)^{\rm T}\left(\boldsymbol I-\boldsymbol L_{H_0}\right)\boldsymbol Y,$$

其中，$\boldsymbol L_{H_0}=\boldsymbol X\left(\boldsymbol X^{\rm T}\boldsymbol X\right)^{-1}\boldsymbol X^{\rm T},$ $\boldsymbol X=(\boldsymbol x_1,\boldsymbol x_2,\cdots,\boldsymbol x_p)$, $\boldsymbol{x}_{i}=(x_{1j},x_{2j},\cdots,x_{nj}).$ 

在 $H_1$ 下, 拟合方程(2)，得到残差平方和为：

$${\rm RSS(H_1)}=\boldsymbol Y^{\rm T}\left(\boldsymbol I-\boldsymbol L_{H_1}\right)^{\rm T}\left(\boldsymbol I-\boldsymbol L_{H_1}\right)\boldsymbol Y,$$

其中，$\boldsymbol{\tilde{x}}_{i}^{\rm T}=(x_{i1},x_{i2},\cdots,x_{ip}),$

$$\boldsymbol L_{H_1}=
\left(
\begin{array}{cccc}
 \boldsymbol{  \tilde{x}}_{1}^{\rm T}\left(\boldsymbol X^{\rm T}\boldsymbol W_{h}(u_1,v_1)\boldsymbol X\right)^{-1}\boldsymbol X^{\rm T} \boldsymbol W_{h}(u_1,v_1)\\
 \boldsymbol{  \tilde{x}}_{2}^{\rm T}\left(\boldsymbol X^{\rm T}\boldsymbol W_{h}(u_2,v_2)\boldsymbol X\right)^{-1}\boldsymbol X^{\rm T} \boldsymbol W_{h}(u_2,v_2)\\
 \vdots\\
 \boldsymbol{ \tilde{x}}_{n}^{\rm T}\left(\boldsymbol X^{\rm T}\boldsymbol W_{h}(u_n,v_n)\boldsymbol X\right)^{-1}\boldsymbol X^{\rm T} \boldsymbol W_{h}(u_n,v_n)
\end{array}
\right),$$

以及 $\boldsymbol W_h(u_i,v_i)={\rm Diag}\left(K(d_{i1}/h),K(d_{i2}/h),\cdots,K(d_{in}/h)\right)$。此外， $K(\cdot)$ 为核函数, $d_{ij}$ 为点 $(u_i,v_i)$ 和 $(u_j,v_j)$ 之间的欧式距离, $h$ 为最优窗宽，可以采用 ${\rm AIC_c}$、${\rm CV}$ 和 ${\rm GCV}$ 等准则来选取。

构造 $F$ 检验统计量如下：

$$F=\frac{\left({\rm RSS(H_0)}-{\rm RSS(H_1)}\right)/ \gamma_1}{{\rm RSS(H_0)}/(n-p)}.$$ 

令 $\boldsymbol A=(\boldsymbol I-\boldsymbol L_{H_0})-\left(\boldsymbol I-\boldsymbol L_{H_1}\right)^{\rm T}\left(\boldsymbol I-\boldsymbol L_{H_1}\right),$ 则 
$\gamma_1={\rm tr}(\boldsymbol A).$

Leung et al.(2000) 给出了 $F$ 在 $H_0$ 下的近似分布：

$$F\sim F(\gamma_1^2/\gamma_2,n-p),$$ 

其中，$\gamma_2={\rm tr}(\boldsymbol A^2).$ 检验 $p$ 值为：

$$p=P_{H_0}\left(F\geq f_0 \right ),$$

其中，$f_0$ 是 $F$ 的观测值。


### 2.2. 单个系数平稳性检验

原假设 $H_0$：$\beta_j(u,v)=\beta_j \ (j=1,2,\cdots,p)$；备择假设 $H_1$：$\beta_j(u,v)$ 随空间位置的变化而变化。

Leung et al.(2000) 根据在 $n$ 个样本点的系数估计样本方差构造检验统计量。令

$$\boldsymbol{\hat{\beta}}_k(u,v)=\left(\hat{\beta}_k(u_1,v_1), \hat{\beta}_k(u_2,v_2),\cdots,\hat{\beta}_k(u_n,v_n)\right)^{\rm T},$$ 

$\boldsymbol{\hat{\beta}}_k(u,v)$ 的样本方差：

$$V_k^2=\frac{1}{n}\sum_{i=1}^n\left[\hat{\beta}_k(u_i,v_i)-\frac{1}{n}\sum_{l=1}^n\hat{\beta}_k(u_l,v_l) \right]^2.$$ 

则构造检验统计量如下：

$$T_k=\frac{V_k^2/\gamma_1}{\hat{\sigma}^2}.$$ 

其中，$\gamma_1={\rm tr }\left[\frac{1}{n}\boldsymbol B_k^{\rm T}\left(\boldsymbol I-\frac{1}{n}\boldsymbol J\right)\boldsymbol B_k\right],$  $\hat{\sigma}^2=\frac{\boldsymbol Y^{\rm T}\left(\boldsymbol I-\boldsymbol L_{H_1}\right)^{\rm T}\left(\boldsymbol I-\boldsymbol L_{H_1}\right)\boldsymbol Y}{{\rm tr}\left(\left(\boldsymbol I-\boldsymbol L_{H_1}\right)^{\rm T}\left(\boldsymbol I-\boldsymbol L_{H_1}\right)\right)}$，$\boldsymbol B_{k}=
\left(
\begin{array}{cccc}
 \boldsymbol{ e}_{k}^{\rm T}\left(\boldsymbol X^{\rm T}\boldsymbol W_{h}(u_1,v_1)\boldsymbol X\right)^{-1}\boldsymbol X^{\rm T} \boldsymbol W_{h}(u_1,v_1)\\
 \boldsymbol{  e}_{k}^{\rm T}\left(\boldsymbol X^{\rm T}\boldsymbol W_{h}(u_2,v_2)\boldsymbol X\right)^{-1}\boldsymbol X^{\rm T} \boldsymbol W_{h}(u_2,v_2)\\
 \vdots\\
 \boldsymbol{ e}_{k}^{\rm T}\left(\boldsymbol X^{\rm T}\boldsymbol W_{h}(u_n,v_n)\boldsymbol X\right)^{-1}\boldsymbol X^{\rm T} \boldsymbol W_{h}(u_n,v_n)
\end{array}
\right)$，
$\boldsymbol J$ 是全部元素为 1 的 $n \times n$ 的矩阵，$\boldsymbol{ e}_{k}^{\rm T}$ 是第 $k$ 个元素为1，其余元素为 0 的列向量。

在零假设下，$T_k$ 近似服从$F(\gamma^2_1/\gamma_2,\delta_1^2/\delta_2)$，其中，
$\gamma_2={\rm tr} \left \{\left[\frac{1}{n}\boldsymbol B_k^{\rm T}\left(\boldsymbol I-\frac{1}{n}\boldsymbol J\right)\boldsymbol B_k\right]^2 \right \},$ $\delta_1={\rm tr}\left \{\left(\boldsymbol I-\boldsymbol L_{H_1}\right)^{\rm T}\left(\boldsymbol I-\boldsymbol L_{H_1}\right)\right \},$ 和
$\delta_2={\rm tr}\left \{[ \left(\boldsymbol I-\boldsymbol L_{H_1}\right)^{\rm T}\left(\boldsymbol I-\boldsymbol L_{H_1}\right)]^2\right \}$。

### 2.3. 部分系数平稳性检验

原假 $H_0$：

$$y_i=\sum_{j=1}^q\alpha_jx_{ij}+\sum_{j=q+1}^p\beta_j(u_i,v_i)x_{ij}+\varepsilon_i, \ i=1,2,\cdots,n, $$ 

即 GWR 模型中部分回归系数是随着空间位置的变化而变化的，但是部分系数是常数；
备择假设 $H_1$：所有系数都是随着空间位置的变化而变化的。
检验统计量构造如下：
在 $H_1$ 下, 拟合方程(2)，得到残差平方和为：

$${\rm RSS(H_1)}=\boldsymbol Y^{\rm T}\left(\boldsymbol I-\boldsymbol L_{H_1}\right)^{\rm T}\left(\boldsymbol I-\boldsymbol L_{H_1}\right)\boldsymbol Y.$$ 

在 $H_0$ 下，拟合方程(3)，得到残差平方和为：

$${\rm RSS(H_0)}=\boldsymbol Y^{\rm T}\left(\boldsymbol I-\boldsymbol L_{H_0}\right)^{\rm T}\left(\boldsymbol I-\boldsymbol L_{H_0}\right)\boldsymbol Y.$$ 

其中，$\boldsymbol L_{H_0}=\boldsymbol L+(\boldsymbol I-\boldsymbol L)\boldsymbol X_c\left(\boldsymbol X_c^{\rm T}(\boldsymbol I-\boldsymbol L)^{\rm T}(\boldsymbol I-\boldsymbol L)\boldsymbol X_c\right)^{-1}\boldsymbol X_c^{\rm T}(\boldsymbol I-\boldsymbol L)^{\rm T}(\boldsymbol I-\boldsymbol L),$    

$$\boldsymbol L=
\left(
\begin{array}{cccc}
 \boldsymbol{ \tilde {x_v}}_{1}^{\rm T}\left(\boldsymbol X_v^{\rm T}\boldsymbol W_{h}(u_1,v_1)\boldsymbol X_v\right)^{-1}\boldsymbol X_v^{\rm T} \boldsymbol W_{h}(u_1,v_1)\\
 \boldsymbol{  \tilde {x_v}}_{2}^{\rm T}\left(\boldsymbol X_v^{\rm T}\boldsymbol W_{h}(u_2,v_2)\boldsymbol X_v\right)^{-1}\boldsymbol X_v^{\rm T} \boldsymbol W_{h}(u_2,v_2)\\
 \vdots\\
 \boldsymbol{ \tilde {x_v}}_{n}^{\rm T}\left(\boldsymbol X_v^{\rm T}\boldsymbol W_{h}(u_n,v_n)\boldsymbol X_v\right)^{-1}\boldsymbol X_v^{\rm T} \boldsymbol W_{h}(u_n,v_n)
\end{array}
\right),$$ 

$\boldsymbol X_c$ 和 $\boldsymbol X_v$ 为常系数部分和变系数部分对应的自变量。
构造检验统计量 $T$ 如下：

$$T=\frac{{\rm RSS(H_0)}-{\rm RSS(H_1)}}{{\rm RSS(H_1)}}.$$

修正 $T$ 得到 $T^{\ast}$ 如下：

$$T^{\ast}=\frac{\left({\rm RSS(H_0)}-{\rm RSS(H_1)}\right)/ \zeta_1}{{\rm RSS(H_1)}/ \delta_1};$$ 

其中，$\zeta_1={\rm tr}[\left(\boldsymbol I-\boldsymbol L_{H_0}\right)^{\rm T}\left(\boldsymbol I-\boldsymbol L_{H_0}\right)-\left(\boldsymbol I-\boldsymbol L_{H_1}\right)^{\rm T}\left(\boldsymbol I-\boldsymbol L_{H_1}\right)],$ $\delta_1={\rm tr}[\left(\boldsymbol I-\boldsymbol L_{H_1}\right)^{\rm T}\left(\boldsymbol I-\boldsymbol L_{H_1}\right)].$
Brunsdon et al.(1999) 给出了零假设下 $T^{\ast}$ 的一个近似分布是:

$$T^{\ast} \sim F(\zeta_1^2/\zeta_2,\delta_1^2/\delta_2),$$

 其中，$\zeta_2={\rm tr}\left
\{[\left(\boldsymbol I-\boldsymbol L_{H_0}\right)^{\rm T}\left(\boldsymbol I-\boldsymbol L_{H_0}\right)-\left(\boldsymbol I-\boldsymbol L_{H_1}\right)^{\rm T}\left(\boldsymbol I-\boldsymbol L_{H_1}\right)]^2 \right \},$ 和 $\delta_2={\rm tr}\left
\{[\left(\boldsymbol I-\boldsymbol L_{H_1}\right)^{\rm T}\left(\boldsymbol I-\boldsymbol L_{H_1}\right)]^2\right \}.$
检验 $p$ 值可由下式计算所得：

$$p=P_{H_0}(T^{\ast}>t^{\ast}),$$ 

$t^{\ast}$ 是 $T^{\ast}$ 的一个观测值。



## 3. 实例分析

以都柏林选举数据为例，介绍在 GWR4 软件中如何完成地理加权回归模型的参数估计和相关检验。详细步骤如下：

1. 数据加载：建议首先将所用数据保存为 txt 或者 csv 格式(这两种格式的文件好操作也简单)且数据的第一行或列作为表头。本例中数据为 csv 格式。点击 Browse，选择使用的文件，然后选择 Comma(CSV)，点击 Open，完成数据加载过程。如下图所示。

![Step1.png](https://imgconvert.csdnimg.cn/aHR0cHM6Ly91cGxvYWQtaW1hZ2VzLmppYW5zaHUuaW8vdXBsb2FkX2ltYWdlcy8xOTUwOTIwMS1mNTZjYmNjZTM4ZDJjNWVlLnBuZw?x-oss-process=image/format,png)

2. 点击 View Data 即可以看到加载好的数据。如下所示：

![Step2.png](https://imgconvert.csdnimg.cn/aHR0cHM6Ly91cGxvYWQtaW1hZ2VzLmppYW5zaHUuaW8vdXBsb2FkX2ltYWdlcy8xOTUwOTIwMS0zYTgzZDdlZGFjZDAyY2UyLnBuZw?x-oss-process=image/format,png)

3. 建立模型：选择 Step2: Model，即可以看到数据里所有变量均列在 Variable list 里，然后根据所需，将变量放入对应的位置。如下所示：

![Step3.png](https://imgconvert.csdnimg.cn/aHR0cHM6Ly91cGxvYWQtaW1hZ2VzLmppYW5zaHUuaW8vdXBsb2FkX2ltYWdlcy8xOTUwOTIwMS02NzlhYWRkMGQzZTA5MDNlLnBuZw?x-oss-process=image/format,png) 

需要做如下说明：
(1) 若坐标是 UTM，则选择 Projected；若坐标是经纬度，则选择 Spherical；
(2) 可选择的模型类型有三种，分别是 Gaussian，Poisson 和 Logistic，分别针对连续性型，计数型和二值型数据建模；
(3) 选择 Geographical variability test，即可以检验回归系数的平稳性；
(4) 若建立的是混合地理加权回归模型，则将常系数对应的变量放在 Global(G)，变系数对应的变量放在 Local(L).

4. 完成建模以后，选择 Step 3: Kernel，选择核函数。如下图，

![Step4.png](https://imgconvert.csdnimg.cn/aHR0cHM6Ly91cGxvYWQtaW1hZ2VzLmppYW5zaHUuaW8vdXBsb2FkX2ltYWdlcy8xOTUwOTIwMS1iMWY3MjhlYTM1YjhlMWYwLnBuZw?x-oss-process=image/format,png)

在 GWR4 中，可以选择 Gaussian 和 bi-square 两种类型的核函数。其中 "Fixed" 表示所选择的窗宽是连续型的，一般适合于研究区域规则的情形，如网格点；而 “Adaptive” 表示所选择的窗宽是观测点的邻近个数，一般适合于研究区域不规则的情形。此外，在 GWR4 里面可以自动设置窗宽范围，也可以根据研究问题自行设置窗宽。AICc，AIC，BIC 和 CV均可以作为选择最优窗宽的准则。

5. 点击 Step 4：Output，设置结果的输出，如下所示：

![Step5.png](https://imgconvert.csdnimg.cn/aHR0cHM6Ly91cGxvYWQtaW1hZ2VzLmppYW5zaHUuaW8vdXBsb2FkX2ltYWdlcy8xOTUwOTIwMS04MzA4NzNkMzU0OWFmYTFkLnBuZw?x-oss-process=image/format,png)

在粉色区域内选择结果输出路径，并建立 ctl 格式的文件，如 Dubiliresults.ctl， 则接下来的两个方框会自动填写，分别保存模型拟合报告和局部估计值及其检验。GWR4 也可以预测其他位置处的值，选择 Prediction at non-sample points 即可。

6.点击 Step：Execute 中 Execute this session 运行并输出结果，如下所示，

![Step6.png](https://imgconvert.csdnimg.cn/aHR0cHM6Ly91cGxvYWQtaW1hZ2VzLmppYW5zaHUuaW8vdXBsb2FkX2ltYWdlcy8xOTUwOTIwMS1lZGU3NDFlMjY2NDZhZTc0LnBuZw?x-oss-process=image/format,png)

GWR4 会将所有运行报告以及对应的系数估计值保存在 Step 4：Output 设置的文件中，且对应的格式分别为 txt 和 csv。

本例对都柏林2014年选举数据，建立地理加权回归模型如下：
 
$$
\begin{aligned}
{\rm GenE12004}_i &=\beta_0(u_i,v_i)+\beta_1(u_i,v_i) {\rm DiffAdd}_i+\beta_2(u_i,v_i) {\rm LARent}_i+\beta_3(u_i,v_i) {\rm LowEduc}_i+\beta_4(u_i,v_i){\rm Age1}_i \\
& +\beta_5(u_i,v_i) {\rm Age 2}_i+\beta_6(u_i,v_i) {\rm SC1}_i +\beta_7(u_i,v_i) {\rm Unempl}_i+\beta_8(u_i,v_i) {\rm Age 3}_i +\varepsilon_i.
\end{aligned}
$$ 

按上述操作，得到主要结果如下：
本例模型中核函数及其最优窗宽选择设置如下：

![image.png](https://imgconvert.csdnimg.cn/aHR0cHM6Ly91cGxvYWQtaW1hZ2VzLmppYW5zaHUuaW8vdXBsb2FkX2ltYWdlcy8xOTUwOTIwMS0xNDAwYTBkYzU4ZmRiMWU3LnBuZw?x-oss-process=image/format,png)

 在 GWR4 中，会自动建立与地理加权回归模型对应的线性回归模型，并给出线性回归结果，结果如下：

![image.png](https://imgconvert.csdnimg.cn/aHR0cHM6Ly91cGxvYWQtaW1hZ2VzLmppYW5zaHUuaW8vdXBsb2FkX2ltYWdlcy8xOTUwOTIwMS0yOWM4YjU3MTNmYmNhNGRiLnBuZw?x-oss-process=image/format,png)

 在本例中，采用 AICc 准则选取最优窗宽，其搜索方式选择的是黄金搜索法，得到的最优窗宽为 58。

![image.png](https://imgconvert.csdnimg.cn/aHR0cHM6Ly91cGxvYWQtaW1hZ2VzLmppYW5zaHUuaW8vdXBsb2FkX2ltYWdlcy8xOTUwOTIwMS1jZDYyOTg0NjRlZGVhNWYwLnBuZw?x-oss-process=image/format,png)

模型平稳性检验结果如下：

![image.png](https://imgconvert.csdnimg.cn/aHR0cHM6Ly91cGxvYWQtaW1hZ2VzLmppYW5zaHUuaW8vdXBsb2FkX2ltYWdlcy8xOTUwOTIwMS0wYTdkYTFlMDc3MjQxNzY4LnBuZw?x-oss-process=image/format,png) 

得到的 F 检验值为 3.367766，对应的概率 $p$ 值没有给出，若需要可以查 F 分布表得到其对应的概率 $p$ 值。

单个回归系数平稳性检验，结果如下：

![image.png](https://imgconvert.csdnimg.cn/aHR0cHM6Ly91cGxvYWQtaW1hZ2VzLmppYW5zaHUuaW8vdXBsb2FkX2ltYWdlcy8xOTUwOTIwMS0xN2FlNzRmNmM0ZDgzNmYwLnBuZw?x-oss-process=image/format,png)

在 GWR4 中，只给出了对应的 F 分布值和自由度，没有检验 $p$ 值，但是 $p$ 值可以查 F 分布表得到。另外 GWR4 还给出了 DIFF of Criterion 的值，也可以根据这个值判断系数的平稳性：若该值大于 0，则可以认为该系数是平稳的。如在本例中可以认为 SC1 和 LowEduc 对应的回归系数是常数。 最后，GWR4 也会给出每个回归系数对应的局部估计值和局部t检验(检验局部估计值的显著性)，部分结果如下：

![image.png](https://imgconvert.csdnimg.cn/aHR0cHM6Ly91cGxvYWQtaW1hZ2VzLmppYW5zaHUuaW8vdXBsb2FkX2ltYWdlcy8xOTUwOTIwMS1mNjk2YzFkOWM1MmQzM2NmLnBuZw?x-oss-process=image/format,png)

结合其他软件，如 Arcgis，R 等软件即可以在地图上将所得到的局部估计值及其局部检验值进行展示。


## 参考文献：
- Leung Y, Mei CL, Zhang WX. Statistical tests for spatial nonstationarity based on geographically weighted
regression model. Environment and Planning A, 2000, 32: 9–32.
- Brunsdon C, Fotheringham AS, Charlton M. Some notes on parametric significance tests for geographically weighted regression. Journal of Regional Science, 1999, 39: 497–524.
