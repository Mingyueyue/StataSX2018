
&emsp;

> 作者：严雅雪 (xxx大学)      
>     
> Stata 连享会： [知乎](https://zhuanlan.zhihu.com/arlion) | [简书](http://www.jianshu.com/u/69a30474ef33) | [码云](https://gitee.com/arlionn) | [CSDN](https://blog.csdn.net/arlionn)

&emsp;
  

## 一、动态门槛效应回归模型简介

在线性回归中，所有观测样本回归函数相同，那如果所有观测样本是离散类，那回归函数的差异性将如何体现? 在回归分析中，我们常常关心系数估计值是否稳定，即如果将整个样本分成若干子样本分别进行回归，是否还能得到大致相同的估计系数？

如果样本的变量不是离散型变量，而是连续型变量，就需要给定一个标准，即“门槛值”。传统的做法是，研究者主观的确定一个门槛值，然后将门槛值帮样本进行分割，即不对门槛值进行参数估计，也不对其显著性进行统计估计检验。显然，这样得到的结果并不可靠。

Hansen (1996) 发表于《Econometrica》上的文章《Inference when a nuisance parameter is not identified under the null hypothesis》，提出了时间序列门槛自回归模型（TAR）的估计和检验。之后，他在门槛模型上连续追踪，发表了几篇经典文章，尤其是1999年的《Threshold effects in non-dynamic panels: Estimation, testing and inference》，2000 年的《Sample splitting and threshold estimation》和 2004 年与他人合作的《Instrumental Variable Estimation of a Threshold Model》。Hansen（1999）提出的门槛效应模型是为存在个体固定效应非动态面板所设计的，门槛值的可以利用固定效应转换，通过最小二乘估计来得到回归斜率。利用一个非标准的渐近理论进行推断，并允许构建置信区间来进行假设测试。Hansen（2000）假设样本数据为 

$$
\left\{ y_{it}, q_{it}, x_{it}: 1 \leq i \leq n, 1 \leq t \leq T \right\}
$$

表达式为，

$$
y_{it}=\mu_{i}+\beta_{1} x_{it} I\left(q_{it} \gamma\right)+\beta_{2} x_{it} I\left(q_{it}>\gamma\right)+e_{it}
$$

其中，$q_{it}$  代表划分样本的门槛变量，$\gamma$  为待估计的门槛值，$x_{it}$ 为外生解释变量，与扰动项 $e_{it}$ 不相关。$I(\cdot)$ 为指示函数，即括号中的表达式为真，则取值为 1；反之，取 0。写成分段函数为：                      

$$
y_{it}=\left\{\begin{array}{ll}{\mu_{i}+\beta_{1} x_{it}+e_{it},} & {q_{it} \leqslant \gamma} \\ {\mu_{i}+\beta_{2} x_{it}+e_{it},} & {q_{it}>\gamma}\end{array}\right.
$$

但在这些文章中，均基于的假设是协变量必须为强外生变量，否则会形成有偏估计。而在现实中较难实现这一假设，故无法扩展应用领域。Caner and Hansen（2004）年采用较为间接的方法解决了这个问题。他们研究了带有内生性的解释变量和一个外生门槛变量的面板门槛模型，只要利用简化型对内生变量进行一定的处理，然后用2SLS（两阶段最小二乘法）或者GMM（广义矩估计）对参数进行估计,即放松了对内生解释变量的要求，但对于门槛变量的要求仍是必须强外生变量。本文即采用这个方法来运用动态门槛效应模型进行表征。

## 二、用STATA运行动态门槛效应命令

##  1、命令的安装示例数据导入

在Stata命令窗口执行第一行代码即可完成对`xthreg`然后输入第二行命令下载中国工业经济面板数据http://www.ciejournal.org/Magazine/Show?id=69845 数据.dta, ，并执行第三行命令导入数据。
```
ssc install xthreg, replace //安装命令
net get xthreg  //下载命令附带的数据到当前工作路径下
use "数据.dta", replace
```
##  2. 命令的语法

该命令的语法如下：
```
    xthreg -- Estimate fixed-effect panel threshold model

        xthreg depvar [indepvars] [if] [in], rx(varlist) qx(varname) [thnum(#) grid(#) trim(numlist) bs(numlist) thlevel(#) gen(newvarname) noreg nobslog
           thgiven options]
```
-`depvar`， 必选项，为因变量。 
-`indepvars`， 可选项，为自变量.
-`rx(varlist) ` 必选项，核心解释变量。允许使用时间序列运算符。
-`qx(varname) ` 必选项，是门槛变量。允许使用时间序列运算符。
-`thnum(#)` 是阈值的数量。在当前版本(Stata 13)中，#必须等于或小于3。默认值是thnum(1).
-`grid(#)`是网格点的数量。使用grid()可以避免在计算大样本时花费太多时间。默认值是grid(300)。
-`trim(numlist) `是估计每个阈值的修剪比例。修剪比例的数量必须等于thnum()中指定的阈值的数量。所有阈值的缺省值都是trim(0.01)。例如，为了适应三阈值模型，您可以设置trim(0.01 0.01 0.05)。
-`bs(numlist) `是引导Bootstrap的数量。如果没有设置bs()， xthreg不会使用bootstrap进行阈值效应测试
-`thlevel(#)`指定阈值的置信区间的置信级别(以百分比表示)。默认值是thlevel(95)。

##  3. 命令操作

### 3.1 拟合值估计

首先，本文将基于` xtdpdsys `命令和nlswork.dta数据对因变量` api `和核心解释变量`is`进行拟合值估计，代码如下：
```
gen api1=L. api
gen is1=L.is
xtdpdsys wage1 grade1 age race hours,twostep
```
运行结果如下：
```stata


System dynamic panel-data estimation            Number of obs     =      2,095
Group variable: city                            Number of groups  =        271
Time variable: year
                                                Obs per group:
                                                              min =          1
                                                              avg =   7.730627
                                                              max =          8

Number of instruments =     39                  Wald chi2(4)      =    1690.05
                                                Prob > chi2       =     0.0000
Two-step results
------------------------------------------------------------------------------
        api1 |      Coef.   Std. Err.      z    P>|z|     [95% Conf. Interval]
-------------+----------------------------------------------------------------
        api1 |
         L1. |   .6248146   .0215764    28.96   0.000     .5825256    .6671036
             |
         is1 |   .0046254   .0028238     1.64   0.101    -.0009092      .01016
       lngrp |  -.1840629   .0725119    -2.54   0.011    -.3261836   -.0419422
       lnfdi |   .0001234   .0134864     0.01   0.993    -.0263095    .0265564
       _cons |   1.950786   .4309568     4.53   0.000     1.106127    2.795446
------------------------------------------------------------------------------
Warning: gmm two-step standard errors are biased; robust standard 
         errors are recommended.
Instruments for differenced equation
        GMM-type: L(2/.).api1
        Standard: D.is1 D.lngrp D.lnfdi
Instruments for level equation
        GMM-type: LD.api1
        Standard: _cons```

然后我们进行sargan检验，和获取拟合值，代码如下：
```
estat sargan
predict hapi
```
运行结果如下：
```stata

Sargan test of overidentifying restrictions
        H0: overidentifying restrictions are valid

        chi2(34)     =  83.05857
        Prob > chi2  =    0.0000
```

### 3.2代入拟合值估计门槛值和置信区间

首先，我们将科技强度stei作为门槛变量，考察在科技强度下，产业结构对空气质量的影响程度。代码如下：

```
xthreg api hapi lngrp pd, rx(is) qx(stei) thnum(3) grid(400) trim(0.01 0.01 0.01) bs(300 300 300)
```
运行结果如下：

```stata
Threshold estimator (level = 95):
-----------------------------------------------------
     model |    Threshold         Lower         Upper
-----------+-----------------------------------------
      Th-1 |       0.1315        0.1268        0.1353
     Th-21 |       0.1315        0.1268        0.1353
     Th-22 |       0.4464        0.4419        0.4491
      Th-3 |       1.0693        1.0650        1.0743
-----------------------------------------------------

Threshold effect test (bootstrap = 300 300 300):
-------------------------------------------------------------------------------
 Threshold |       RSS        MSE      Fstat    Prob   Crit10    Crit5    Crit1
-----------+-------------------------------------------------------------------
    Single |  335.0607     0.1655      17.45  0.1533  19.7967  24.1998  30.3118
    Double |  333.8384     0.1649       7.41  0.3567  11.1727  13.0692  17.5784
    Triple |  333.0669     0.1646       4.69  0.8467  14.7811  17.0482  23.5613
-------------------------------------------------------------------------------

Fixed-effects (within) regression               Number of obs      =      2032
Group variable: id                              Number of groups   =       254

R-sq:  within  = 0.1001                         Obs per group: min =         8
       between = 0.5008                                        avg =       8.0
       overall = 0.3400                                        max =         8

                                                F(7,1771)          =     28.14
corr(u_i, Xb)  = 0.4123                         Prob > F           =    0.0000

------------------------------------------------------------------------------
         api |      Coef.   Std. Err.      t    P>|t|     [95% Conf. Interval]
-------------+----------------------------------------------------------------
        hapi |   .3679484   .0368293     9.99   0.000     .2957149    .4401818
       lngrp |   .2459264   .0878522     2.80   0.005     .0736215    .4182313
          pd |  -.0000987   .0002223    -0.44   0.657    -.0005348    .0003373
             |
   _cat#c.is |
          0  |  -.0067473   .0037753    -1.79   0.074    -.0141519    .0006572
          1  |  -.0127042   .0033193    -3.83   0.000    -.0192143    -.006194
          2  |  -.0149214   .0032984    -4.52   0.000    -.0213905   -.0084522
          3  |  -.0135887    .003351    -4.06   0.000     -.020161   -.0070164
             |
       _cons |   .6807577   .6063483     1.12   0.262    -.5084758    1.869991
-------------+----------------------------------------------------------------
     sigma_u |  .53213522
     sigma_e |  .43366387
         rho |  .60090941   (fraction of variance due to u_i)
------------------------------------------------------------------------------
F test that all u_i=0: F(253, 1771) = 3.72                   Prob > F = 0.0000
```

