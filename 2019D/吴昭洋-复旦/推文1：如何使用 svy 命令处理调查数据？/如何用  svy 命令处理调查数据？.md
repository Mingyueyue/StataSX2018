
&emsp;

> 作者：吴昭洋 (复旦大学)      
>     
> Stata 连享会： [知乎](https://zhuanlan.zhihu.com/arlion) | [简书](http://www.jianshu.com/u/69a30474ef33) | [码云](https://gitee.com/arlionn) | [CSDN](https://blog.csdn.net/arlionn)

&emsp;
  

## 1. 微观实证中的调查数据
对于从事社会科学领域研究的大多数人而言，通过调查研究获取感兴趣的一手数据资料无疑是至关重要的。调查研究十分强调对总体做出有效的推断。由于理想的简单随机抽样在现实中经常难以实现，作为其替代，我们经常采用更为复杂的抽样策略以获得更具代表性的样本（例如CGSS数据所采用的多阶分层抽样）这便要求我们进行必要的抽样后调整（post-sampling adjustment）。近年来，随着微观数据库的不断丰富，基于微观调查数据的实证研究越来越多，研究所使用的计量方法也日益丰富。

然而，在通常的计量课程和实证论文中，我们似乎难以获得关于如何处理调查数据的必要介绍，许多计量教科书（有些甚至是经典教科书）对此“避而不谈”或“一笑而过”，直接造成微观数据用户在开展实证分析时，对所用调查数据的抽样过程不甚了解，所跑出的回归分析常常未考虑样本的权重，导致估计结果出现偏误。即便是同一个调查项目，其抽样框、分层变量和抽样阶段在不同年份也不一致（例如中国综合社会调查数据，即CGSS数据）。众所周知，由于标准的统计方法假定样本来源于简单随机抽样，这便需要针对调查数据而设计专门的方法，进而将样本抽样过程的信息考虑进来。

为了更好地理解调查数据，以及在应用 'Stata' 开展实证研究时养成主动了解数据抽样过程的科学习惯，获得更为准确的估计结果，我们特别就处理调查数据方法撰写本推文。


##2. 抽样设计的基本特征
###2.1抽样权重（Sampling weights）

###2.2 初级抽样单位（PSU，primary sampling unit）

###2.3 层（Strata）

###2.3 有限总体修正（FPC， finite population correction）


## 3.  Stata中'svy' 命令及其使用
Stata 对调查数据的分析是基于一种包含分析方法广泛选择的统一方式，这些分析方法均根据样本结构的一个隐含定义来进行工作，该定义包含了对设计或选择性偏误进行调整的概率权数。在 Stata 中，可借助 'svyset' 命令设定基本的设计元素，数据集可连同这一信息被保存，在随后使用 'svy:' 命令的回归分析时将自动对应权数以及其他的调查设计信息。

### 3.1 命令示范
[results insert]

### 3.2 简单例子
下面通过一个名为 'svysmall' 的数据文件简要展示如何使用 Stata 中的 'svy' 命令。

'use https://stats.idre.ucla.edu/stat/stata/faq/svysmall, clear'
list

[results insert]

在这个例子中，house是家庭，eth是种族，wt是人的权重。我们使用 'svyset' 命令告诉 Stata 这些东西，它会记住它们。如果保存数据文件，stata会用数据文件记住它们，下次使用数据文件时甚至不需要输入它们。下面，我们告诉  Stata， 初级抽样单位 (psu, primary sampling unit) 是家庭（house）。此外，抽样方案包括基于种族（eth）的分层抽样（strata）。最后，加权变量（pweight）被称为wt。

'svyset house [pweight = wt], strata(eth)'

请注意，psu的变量 (house) 在方括号中给出的 pweight 之前给出。一旦stata通过 'svyset' 命令了解了调查数据，就可以使用 'svy:' 前缀，其后的语法与命令同非调查数据的使用保持一致。例如，下面的 'svy:regress' 命令看起来就像常规的 'regress' 命令，但此时它已使用了我们提供的有关调查设计的信息，并在执行运算时考虑了上述信息。

'svy: regress y x1 x2 x3'

[results insert]


## 4. 参考资料
- How do I use the Stata survey (svy) commands? | Stata FAQ
https://stats.idre.ucla.edu/stata/faq/how-do-i-use-the-stata-survey-svy-commands/
- Applied Survey Data Analysis in Stata
https://stats.idre.ucla.edu/stata/seminars/applied-svy-stata13/