`2017/11/17 10:04`

> 任务概述：`logout` 命令可以帮我们把各种基本统计表格输出到 Excel 或  Word 文档中。缺憾是，每次只能输出一张表格到一个 Excel 或 Word 文档中。如果想把多个表格式输出到一个 Excel 文档中，即使用 `append` 模式，该如何处理呢？
> 这篇推文就是介绍一些例子，来解决这类问题。

> 思路：写一个循环，使用 `caplog` 命令把准备输出的表格 append 到一个 txt 文档中，然后使用 `logout` 进行转换。

> 参见：http://statalist.1588530.n2.nabble.com/How-can-I-append-tabout-output-in-Excel-using-logout-td4973504.html 

他们提供的Stata 范例：  

Thank you all for responses.   
I find formatting tables easier to do in Excel.   
Roy Wada, the author of -logout- offered the following solution, which 
worked for me: 

```
* drop frivolous rows 
sysuse auto, clear 
local append replace 
foreach y in trunk weight { 
  forv s=0/1 { 
   qui su mpg if foreign==`s' 
   if r(N)!=0{ 
    caplog using logfile, `append': tabstat mpg if foreign==`s' , 
stat(n mean median p25 p75 min max ) 
    local append append 
   } 
  } 
} 
logout, clear use(logfile) 
drop if t1=="variable" & _n~=1 
logout, save(test_together) excel replace 


* keep them 
sysuse auto, clear 
local append replace 
foreach y in trunk weight { 
  forv s=0/1 { 
   qui su mpg if foreign==`s' 
   if r(N)!=0{ 
    caplog using logfile, `append': tabstat mpg if foreign==`s' ,  ///
           stat(n mean median p25 p75 min max ) 
    local append append 
   } 
  } 
} 
logout, use(logfile) save(test_together) excel replace 
```