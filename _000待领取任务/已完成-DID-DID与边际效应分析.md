> **任务：** 参考 [Some Stata notes – Difference-in-Difference models and postestimation commands](https://andrewpwheeler.wordpress.com/2016/05/12/some-stata-notes-difference-in-difference-models-and-postestimation-commands/) 这篇文章，举例说明 DID 的估计方法，尤其是估计完成后如何采用 `margins` 以及 `marginsplot` 命令进行后续分析，采用图形方式展示分析结果。

> **Stata 范例：** 可以采用 `help diff` 命令中使用的 Stata 范例及数据。也可以从我的最新版的 Stata初级班(现场班) 讲义中截取。