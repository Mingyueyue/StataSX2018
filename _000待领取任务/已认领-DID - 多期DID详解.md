**任务：**    
- 参照我在 `A6_DID.do` 中的范例，介绍多期 DID 模型的设定和估计方法；
- 重点放在如何检验 common trend 假设上。有两种方法：
   - 一是采用图形（我的范例绘制了相应的图形）；
   - 二是采用回归法，详见我的讲义。
- 要求：所有回归结果同时呈现两种形式。一是文本形式，即现有推文的呈现方式，适合电脑上查看；另一种是截图形式，适合手机上查看（我通过微信公众平台发布时会采用图片形式呈现估计结果）

> **A6_DID.rar** 下载：   链接：https://pan.baidu.com/s/1minCLny 密码：ligm


### 附：相关代码

```stata
*-----------------------------
*-6.6 多期 DID 与共同趋势假设
*-----------------------------

  *-数据要求: Treat 实施前两年以上的数据，Treat 实施后一年以上的数据;
  
  *-目的: (1) 可以检验 Common Trend 假设是否合理; 
  *       (2) 有助于评估政策的长期效果
  
  *-假设样本区间为 Year[1987-1996], Treat 发生在 1992 年,
  *                                                 
  *        Before[1987-1991]        After[1993-1996]
  *     ----------------------    ----------------------
  *       Pre-treatment years      Post-treatment years
  *                                                 
  *-模型设定：
   *  ----------------------------------------------------------------
   *-  y = a0 + a1*Treat + a2*Years + b*Treat#Years + a3*Controls + e 
   *  ----------------------------------------------------------------
   *-
   *-Notes:
   *
   * [1] Years: 表示一系列 Year dummies--(yr1988, yr1989, ..., yr1996)
   * [2] Treat#Years: 表示 Treat 与 Year dummies 的交乘项
   * [3] 根据 Common Trend 假设，应该预期:
   *     Treat#yr1988, Treat#1989..., Treat#yr1991 的系数都不显著;
   * [4] 若政策的效果是显著的，则 
   *     Treat#yr1992, Treat#yr1993, ...., Treat#yr1996 至少有一个是显著的,
   *     或者联合显著.  
  
  
  
*------------
*-6.7 范例 3: 美国残疾人法案(ADA)对就业和工资的影响

	*           Acemoglu et al.(2001, JPE)  
	*----------------------------------------------------
	* Acemoglu, D., Joshua D. Angrist, 2001,             
	*  Consequences of employment protection?            
	*  The case of the americans with disabilities act,  
	*  Journal of Political Economy, 109(5): 915-957.    
	*----------------------------------------------------
  
  cd "$path\Acemoglu_2001_JPE_DID"     //该范例所在文件夹
  pwd
  
  shellout "Acemoglu_2001_JPE.pdf"     //PDF原文
  shellout "Acemoglu_2001_PPT.ppt"     //PPT (尚需细化)  
  shellout "Burkhauser_2004_argue.pdf" //反驳
  shellout "Thompkins_2013_DID.pdf"    //近期文献
 
  *-作者提供的数据和 SAS 程序文件:
  view browse "http://economics.mit.edu/faculty/acemoglu/data/aa2001"
  view browse "http://economics.mit.edu/faculty/angrist/data1/data/aceang01"

*------
*-6.7.1 背景介绍 
  
  *-目的: 该文研究了美国残疾人法案(ADA)对就业和工资的影响。

  *-ADA 法案简介：https://www.ada.gov/ (ADA 官网)
  *   ADA: 是由美国国会在1992年7月通过的一项法案，经由老布什总统签署生效。
  *   2008年小布什总统又签署了残疾人法案修正案，这些修正案在2009年1月1日生效。
  *   ADA 的初衷: 保护残疾人在求职、解聘和薪水等方面不受歧视.
  * 可能的影响:
  *   部分企业可能会减少雇佣残疾人士;
  *      i. 用工成本上升: 需要为残疾雇工提供住宿;
  *      ii.解雇(残疾)员工的成本上升了;

  *-本文样本区间: 
  *   [1] ADA 于1990年7月写进宪法；1992年7月正式生效;
  *   [2] Males aged 21-39 from the 1988-1997 March CPS (调查年份, 每年3月)
  *   [2] 实际样本区间 1987-1996 年，刚好跨越了 ADA 法案实施前后的年份;

  *-方法: 多期 DID，有助于验证实验之前是否存在共同趋势  

  *-Treat组: disabled(残疾人士)；  Control组: 其他人员;
  
    use "ABA_JPE2001.dta", clear
  
    des2  //数据概况
  
  *-描述性统计, 对比Treat组和Control组在Treat前后的基本特征
  *-------------------------------
  *-Table 1, Panel A, colum(1),(2) Men age 21-39
  *-------------------------------
  
	cap drop EvenYear
	gen EvenYear = mod(year,2)==0  //偶数年取1，否则取0, 列示时节省空间
	tab year Even
	
	local v "age white posths working wkswork1 wkwage"
	logout, save(Out\table1_disabled) excel replace: ///
			tabstat `v' if disabled==1&EvenYear==1,   ///
			format(%6.2f) s(mean N) by(year) nototal
	logout, save(Out\table1_nondisabled) excel replace: ///
			tabstat `v' if disabled==0&EvenYear==1,      ///
			format(%6.2f) s(mean N) by(year) nototal

	*-self-reading		
    *-----------更为精美的呈现方式----------------------- begin -------	
	*
	  local qui "qui"  //执行时不选此行,可以呈现详细结果
	  mat drop _all	
	  local v "age white posths working wkswork1 wkwage"
	  global colnames ""
	  local j=1
	  forvalues t = 1988(2)1996{
	    forvalues i = 1(-1)0{
		  qui tabstat `v' if disabled==`i'&year==`t', s(mean) c(s) save
		  mat a = r(StatTotal)
		  qui sum age if disabled==`i'&year==`t'
		  mat s`j' = (a' \ `r(N)')
		  mat A = (nullmat(A), s`j++')
	    }
	    global colnames "$colnames `t'_Dis `t'_Non"
	    *dis "$colnames"
	  }
	  mat colnames A = $colnames
	  mat rownames A = Age White Post-high-school Working Weeks_worked ///
		  			   Weekly_wage Observations
	  mat list A, format(%4.2f) noheader
    *
    *----------------------------------------------------- over --------
    *
    *-输出到 Excel 表格中
      logout, save(Out\Table1_A_Men_21-39) excel replace fix(5):  ///
              matrix list A, format(%4.2f) noheader
	*-Notes: 
	* (1) option fix(5)	用于保证输出的 Excel 表格时, 
	*     第一行第一列的空格得以保留;
	* (2) 执行完 logout 命令后，矩阵 A 就被自动删除了.
	

*------
*-6.7.2 图形分析：Common Tread

	*-------------------Figure 2-------------------begin------------------
	*
	*-目的: 直观地呈现 1992年(ADA) 前后残疾人员每周工作时数的变化;
	*-预期: 若Common trend假设合理，
	*       则 1992 年之前Treat组和Control组的每周工作时数差异应该比较稳定;
	preserve
	  collapse (mean) wkswork1, by(disabled year)
	  list, sepby(disabled)
	  xtset year disabled
	  list, sepby(year)
	  gen diff_weeksWork = -d.wkswork1  // mean(Nondisab)-mean(disab)
	  list, sepby(year)
	 #delimit ;
	  local text "Weeks worked last year by disability status of men";
	  twoway 
	   (connected  wkswork1       year if disabled==0)  
	   (connected  wkswork1       year if disabled==1)
	   (connected  diff_weeksWork year, lp(longdash))
	   , 
		scheme(s2mono)   /* scheme(s2color) */
		ytitle("Weeks Worked")   
		ylabel(10(5)50, angle(0))         
		xtitle("Year")         
		xlabel(1987(1)1996)  
		xline(1992, lp(dash) lc(red*1.5))
		yline(25,lp(dash) lc(blue))
		text(47   1994 "Non-disabled")
		text(30.5 1994 "Difference")
		text(14   1994 "disabled")
		legend(label(1 "disabled men") 
			   label(2 "nondisabled men") 
			   label(3 "difference")
			   row(1))
		note("FIG.2.- `text' aged 29-31"); 
	 #delitmit cr
	  graph export "Out\Figure2.wmf", replace //输出并保存图形
	restore 
   *-------------------Figure 2-------------------over-------------------

   *-Note: 当共同趋势假设无法满足时，DID 的结果是有偏的，
   *       此时，使用 Synthetic Control Method 可以得到更为可靠的估计结果
   

*------
*-6.7.3 回归分析：多期 DID

*-重现 文中 Table2. Panel A 
*-几个重要的问题:
* (1) CPI 平减问题,  已经解决, 作者仅在去除 wkwage 变量的离群值时用到了平减;
* (2) 估计时的权重问题: 
*   pp.950, All our estimates are weighted by CPS sample weights
*   我们拿到的数据里无法找到权重变量，这可能是导致无法完全复制原文的主要原因


*---------
*-Table 2: col(1) (5)
*---------
                                                              /*  pp.929
Table 2 reports ordinary least squares (OLS) estimates of equation (6). 
被解释变量: weeks worked (wkswork1) and log weekly earnings (lnwkwage).
控制变量:
   dummies for individual disability status, 
   year dummies, 
   two 10-year age groups, 
   three schooling groups, 
   three race groups, 
   nine census region main effects,  
   interaction terms for age#year, schooling#year, race#year, and region#year.

[1] Coefficients of interest are a full set of year#disability interactions, 
    with 1987 as the base period. 
[2] These year#disability interaction terms, that is, the a’s in equation (6), 
    describe the change in relative employment of the disabled. 
[3] We think of 1993–96 as posttreatment years, whereas 1992 is a 
    transition year during which the ADA was only partially in effect. 
[4] The pre-1992 years provide “pretreatment” specification tests, 
    though they could also capture possible anticipation effects of the ADA.
                                                                          */
 
*-定义 disabilityX1988,  disabilityX1989, ..., disabilityX1998
  dropvars  disabX* yr*
  forvalues t = 1988/1996{  // sample range: 1987-1996
     gen yr`t' = (year==`t')
  }
  forvalues t = 1988/1996{
     gen disabX`t' = disabled*yr`t'
	 label var disabX`t' "Disability x `t'"
  }
  
*-最基本的 DID 分析(原文未做这些分析，是连玉君自己加的)
  dropvars *Post
  gen Post = (year>1992)  //ADA 是1992年7月正式生效的
  gen disable_x_Post = disabled*Post
  
  *-DID回归
  reg wkswork1 disabled Post disable_x_Post
  est store DID_2period
  
  *-考虑多期
  reg wkswork1 disabled yr1988-yr1996 disabX*
  est store DID_Mperiod
  *-评论: 这个结果部分支持 Common Trend 假设，
  *       因为 TreatX1988 和 TreatX1989 都不显著; 
  *       考虑到 ADA 在1990年7月就已经写入宪法, 1992年7月正式生效这一背景,
  *       上述结果表明存在[预期效应], 即部分企业在1990-1992期间就已经开始
  *       尽量少雇佣残疾员工了。
  *-扩展: 可以加入更多的控制变量，控制两个组的个体特征差异.
  
  *-加入控制变量
  global controls  "i.age_G  i.edu_G  i.race_G  i.region"
  reg wkswork1 disabled yr1988-yr1996 disabX* $controls
    estadd local Controls    "Yes", replace
	estadd local YrxControls "No" , replace
  est store DID_Mp_control
   
*--------------------------
*-原文 Table 2, Column (1):  Y = weekes worked, Eq(6) in pp.925
  
  global controls  "i.age_G  i.edu_G  i.race_G  i.region" //控制变量
  
  *=====================================================================
    reg  wkswork1  disabled  i.year   disabX*     i.year##($controls)   //cmd
  *      --------  --------  -------  ----------  ---------------------
  * DID: outcome    Treat      Time   Treat*Time    Control Variables
  *=====================================================================
    estadd local Controls    "Yes", replace
	estadd local YrxControls "Yes", replace
    est store Tab2_Col1	
  *-Q: 我们加入了哪些控制变量?
  
  
*----------------
*-原文Column (5):  Y = log(weekly earnings) 作为被解释变量
  reg lnwkwage disabled i.year disabX*  $controls i.year#($controls)
    estadd local Controls    "Yes", replace
	estadd local YrxControls "Yes", replace
  est store Tab2_Col5  

  
*--------------
*-输出到 Excel 

  local s   "using $Out\Table2.csv"
  local m   "DID_2period DID_Mperiod DID_Mp_control Tab2_Col1 Tab2_Col5"
  local mt  "Weeks ln(wage)"
  local opt "nogaps b(%4.2f) star(* 0.1 ** 0.05 *** 0.01)"
  esttab `m' `s', mtitle(Weeks Weeks Weeks Weeks ln(wage)) `opt' ///
      scalars(Controls YrxControls N r2) sfmt(%10.1f %4.3f)  ///
      se keep(*Post disabX*)  replace label
		  	 
*-结论:
* [1] ADA 的实施显著降低了残疾人士的每周工作时数(企业不愿意雇佣残疾人士)
* [2] ADA 的实施对残疾人士的工资(Weekly Wage)没有显著影响
		 

```