
`2017/11/17 10:05`

> 任务概述：实证分析中经常出于各种目的对变量进行对数转换。那么这种处理方法需要注意哪些要点？分析过程中系数含义有何变化？

> 参见：http://home.wlu.edu/~gusej/econ398/notes/logRegressions.pdf

> 其他参考资料：
- 伍德里奇的导论里有介绍，也可以到其他的教科书中查看，把这个问题搞明白，举一些例子。
- Yahoo 中搜索关键词 `stata log transform`，可以查到很多相关的讨论，[--点击打开--](https://search.yahoo.com/search;_ylt=AwrBT71mRA5aB44AZhxXNyoA;_ylc=X1MDMjc2NjY3OQRfcgMyBGZyA3lmcC10BGdwcmlkA1JrbGlvbV9JUThtRC5fdGw2RzhPMkEEbl9yc2x0AzAEbl9zdWdnAzIEb3JpZ2luA3NlYXJjaC55YWhvby5jb20EcG9zAzAEcHFzdHIDBHBxc3RybAMwBHFzdHJsAzIzBHF1ZXJ5A3N0YXRhJTIwbG9nJTIwdHJhbnNmb3JtBHRfc3RtcAMxNTEwODg0NDU5?p=stata+log+transform&fr2=sb-top&fr=yfp-t&fp=1)

> 补充一个例子：弹性系数不受原始变量量纲的影响，变化的效果会被常数项吸收

```stata
. sysuse "auto.dta", clear

. gen lnp  = ln(price)
. gen lnw1 = ln(weight)
. gen lnw2 = ln(weight/10000)

. reg lnp lnw1
. est store m1
. reg lnp lnw2
. est store m2

. esttab m1 m2, nogap s(N r2)

--------------------------------------------
                      (1)             (2)   
                      lnp             lnp   
--------------------------------------------
lnw1                0.737***                
                   (4.91)                   
lnw2                                0.737***
                                   (4.91)   
_cons               2.760*          9.548***
                   (2.30)         (50.47)   
--------------------------------------------
N                      74              74   
r2                  0.251           0.251   
--------------------------------------------
t statistics in parentheses
* p<0.05, ** p<0.01, *** p<0.001
```