## 任务说明和建议
- 介绍一些 Stata 命令和方法，用于检验某个变量是否服从正态分布。
- 可以在下面提供的参考资料的基础上，进一步使用 `findit` 命令搜索最新的检验方法和命令予以补充；
- 也可以在 Google 中搜索 **stata normality test** 之类的关键词，从相关的网页和 Blogs 中抽取可以借鉴的资料。

## 参考资料
- 这篇 [In Stata, how do I test the normality of a variable?](https://kb.iu.edu/d/alug) 对此问题进行了初步介绍，只是列出了主要命令和提纲，可以以此为基础进行细化。


---
## 附：原文 (可以直接翻译后插入推文正文)
[In Stata, how do I test the normality of a variable?](https://kb.iu.edu/d/alug) 


In [Stata](https://kb.iu.edu/d/afly), you can test normality by either graphical or numerical methods. The former include drawing a stem-and-leaf plot, scatterplot, box-plot, histogram, probability-probability (P-P) plot, and quantile-quantile (Q-Q) plot. The latter involve computing the Shapiro-Wilk, Shapiro-Francia, and Skewness/Kurtosis tests.

The examples below are for the variable `score`:

| Graphical methods |  |
| --- | --- |
| **Command** | **Plot drawn** |
| `. stem score` | stem-and-leaf |
| `. dotplot score` | scatterplot |
| `. graph box score` | box-plot |
| `. histogram score` | histogram |
| `. pnorm score` | P-P plot |
| `. qnorm score` | Q-Q plot |

| Numerical methods | |
| --- | --- |
| **Command** | **Test conducted** |
| `. swilk score` | Shapiro-Wilk |
| `. sfrancia score` | Shapiro-Francia |
| `. sktest score` | Skewness/Kurtosis |

Be aware that in these tests, the null hypothesis states that the variable is normally distributed.
