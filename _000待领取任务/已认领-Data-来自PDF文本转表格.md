> ### 任务：

写一个推文，介绍 `tex2col` 命令。该命令可以将以空格分隔的表格 (无法直接粘贴到 Excel 中) 转换成以 **Tab** 分隔的表格。

主要适用于适于将 PDF 贴进来的表格转成变量或表格。

## txt2col  命令简介

```stata
. help tex2col  //Split Text into Columns
```


 
## Stata 范例

### Example 1: 逗号分隔的文本文件 &rarr; 表格
```stata
    clear
    input str60 (data)
        "Chaco Hamedo 16,6 25,1 21,5 881,5 58 73 66"
        "Chaco Seco 16,2 24,3 21,3 736,1 60 79 71"
        "Valles Centrales 14,7 18,3 16,9 721,4 50 74 62"
        "Valles del Sur 12,2 18,1 16 351,3 40 70 53"
    end

    tex2col, data(data) col(7) dpcomma  // dpcomma: 把逗号转换成句点
    list row col*, clean 
```
结果如下：
```stata
. list row col*, clean 

                    row   col_1   col_2   col_3   col_4   col_5   col_6   col_7  
  1.       Chaco Hamedo    16.6    25.1    21.5   881.5      58      73      66  
  2.         Chaco Seco    16.2    24.3    21.3   736.1      60      79      71  
  3.   Valles Centrales    14.7    18.3    16.9   721.4      50      74      62  
  4.     Valles del Sur    12.2    18.1      16   351.3      40      70      53 
```

**注意事项：**
- 使用 `input str##` 输入数据时，由于变量都是字符串类型，所以每一行观察值都要用半角双引号包围。
- `tex2col` 命令中的 `col(#)` 选项是必填项，**#** 需要根据列数自行指定。

### Example 2: 空格分隔的文本文件 &rarr; 表格  
    clear
    input str60 (data)
        "Argentina 2011 18.7%"
        "Bolivia 2011 0.4%"
        "Brasil 2011 3.6%"
        "Chile 2011 1.7%"
        "Colombia 2011 5.2%"
        "Costa Rica 2010 8.0%"
        "Ecuador 2011 3.1%"
        "El Salvador 2010 0.3%"
        "Honduras 2011 3.0%"
        "Mexico 2010 3.1%"
        "Panam 2011 0.6%"
        "Paraguay 2010 0.9%"
        "Per 2011 24.1%"
        "Uruguay 2011 4.3%"
        "Venezuela 2011 10.3%"
    end
    tex2col, data(data) col(2) ignore(%)
    list



### Example 3: 去掉千分位符号  

```stata
clear
input str200 (data)	
"738 0.333 · · · · ·"
"738 0.802 0.802 4,527 0.708 5.33 0.000"
"738 0.560 0.560 4,527 0.398 8.28 0.000"
"738 0.420 0.420 4,527 0.114 22.16 0.000"
"738 0.225 0.225 4,527 0.068 14.10 0.000"
"738 0.234 0.234 4,527 0.101 10.44 0.000"
end
compress

tex2col, data(data) col(7) cname(v) ignore(,)
list row v*, clean noobs	
```  