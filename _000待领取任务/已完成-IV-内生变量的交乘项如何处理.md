已认领-> **任务：** 根据 [Endogenous Variable in Interaction, Simulatenous Equations, Use of IV](https://www.statalist.org/forums/forum/general-stata-discussion/general/1347694-endogenous-variable-in-interaction-simulatenous-equations-use-of-iv) 这个帖子的讨论写一篇推文，介绍问题的背景和具体的实操方法。

### 参考资料：
- [How to do an instrumental variables regression with an instrumented interaction term in Stata?](https://stats.stackexchange.com/questions/41895/how-to-do-an-instrumental-variables-regression-with-an-instrumented-interaction)
- [Multiple endogenous variables – now what?!](http://www.mostlyharmlesseconometrics.com/2010/02/multiple-endogenous-variables-what-now/)

