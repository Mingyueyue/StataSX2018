**运用Stata做长期事件研究**

## 1 长期事件研究法概览

### 1.1 长期事件研究法的定义
- (Buy-and-hold abnormal return approach (**BHAR**);
-  Calendar-time portfolio approach (CTIME)/**Jensen's alpha** approach)
### 1.2 长期事件研究法的理论框架  
### 1.3 长期事件研究法的应用范围（详细介绍研究领域：如企业社会责任水平较高的公司是否有更高的长期股票回报，Deng et al. (2013)JFE）

## 2 长期事件研究法的理论模型

- Market Model
- Market-adjused Model
- Fama-French Three Factor Model
- Fama-French Plus Momentum

## 3 长期事件研究法的理论依据
长期事件研究法可以捕获事件在事件发生后的几个月或几年内是否对股票价格产生了持续影响。与短期效应的大小相比，此方法测量的效应持久性更高。

## 4 长期事件研究法的估计步骤
### 4.1 Buy-and-hold abnormal return approach (BHAR)
### 4.2 Calendar-time portfolio approach (CTIME)/Jensen's alpha approach


## 5 长期事件研究法资源
- 沃顿商学院数据库（付费需购买）帮你一键操作Event Study Tool by WRDS[【U.S long-run event study】](https://wrds-www.wharton.upenn.edu/) 
- EventStudyTools



## 6 Stata 范例


## 7 Stata外部命令





