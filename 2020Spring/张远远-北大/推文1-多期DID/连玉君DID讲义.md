## 简介
在政策评价领域，有多重识别政策效应的计量方法，包括：DID，PSM，RDD，SCM 等。其中，DID 拥有非常广泛的应用领域。

双重差分法（Difference-in-differences，简记DID）无疑是实证研究中最常用的计量方法之一，而交互项则是DID的灵魂。在计量实践中，取决于数据的类型与性质，DID的交互项有着不同的形式。灵活地使用DID的交互项，是实证研究的一项重要技能。为此，本文全面地梳理了文献中关于DID交互项的各种形式，包括（1）传统DID；（2）经典DID；（3）异时DID；（4）广义DID；以及（5）异质性DID。

- 说明政策评价的基本架构：反事实
- Treat 组和 Control 组的界定

## DID 的核心思想：两期 DID

Callaway and 

Difference-in-Differences (DID) has become one of the most popular designs used to evaluate causal effects of policy interventions. In its canonical format, there are two time periods and two groups: in the first period no one is treated, and in the second period some individuals are treated (the treated group), and some individuals are not (the control group). If, in the absence of treatment, the average outcomes for treated and control groups would have followed parallel paths over time (which is the so-called parallel trends assumption), one can estimate the average treatment effect for the treated subpopulation (ATT) by comparing the average change in outcomes experienced by the treated group to the average change in outcomes experienced by the control group. Most methodological extensions of DID methods focus on this standard two periods, two groups setup; see, e.g., Heckman et al. (1997, 1998), Abadie (2005), Athey and Imbens (2006), Qin and Zhang (2008), Bonhomme and Sauder (2011), Botosaru and Gutierrez
(2017), de Chaisemartin and D'Haultfuille (2017), and Callaway et al. (2018); see Section 6 of Athey and Imbens (2006) and Theorem S1 in de Chaisemartin and D'Haultfouille (2017) for notable exceptions that cover multiple periods and multiple groups.



> 提纲

- 广州-佛山房价例子
- DID 的计量模型及其系数含义
- 两期 DID 的局限
  - 引出 Common Trend 假设
  - 提出其他可能存在的情形
> ---

### 模型设定

$$
y_{it} = \alpha + \theta_1 Treat_i+ \theta_2 Post_t  + \gamma\ Treat_i \times Post_t + {x}_{it}'\beta + \varepsilon_{it}
$$


$$
\begin{equation}
y_{i t}=\alpha+\theta_{1} \text { Treat }_{i}+\theta_{2} \text { Post }_{t}+\gamma \text { Treat }_{i} \times \text { Post }_{t}+x_{i t}^{\prime} \beta+\varepsilon_{i t}
\end{equation}
$$

其中，$Treat_i$ 为处理组虚拟变量，用以标记实验组和控制组。若第 $i$ 组属于实验组，则 $Treat_i=1$，否则 $Treat_i=0$。虚拟变量 $Post_t$ 用以标记时间时点，设 $t_0$ 为政策发生的时点，则对于 $t>t_0$ 的观察值 $Post_t=1$，否则 $Post_t=0$。

### 系数含义
为了便于表述，假设 $\beta=0$，即先不考虑其他控制变量 $x_{it}$ 的影响。

> 方式1
- $E(y_{it}|Treat=0, Post=0) = \hat{\alpha} = C_0 $ 
- $E(y_{it}|Treat=1, Post=0) = \hat{\alpha} + \hat{\theta}_1 = Y_0$
- $E(y_{it}|Treat=0, Post=1) = \hat{\alpha} + \hat{\theta}_2 = C_1$
- $E(y_{it}|Treat=1, Post=1) = \hat{\alpha} + \hat{\theta}_1 + \hat{\theta}_2 + \hat{\gamma} = Y_1$

> 方式2

$$
\begin{align}
E(y_{it}|Treat=0, Post=0) &= \hat{\alpha} = C_0                             \tag{2a} \\ 
E(y_{it}|Treat=1, Post=0) &= \hat{\alpha} + \hat{\theta}_1 = Y_0            \tag{2b} \\
E(y_{it}|Treat=0, Post=1) &= \hat{\alpha} + \hat{\theta}_2 = C_1            \tag{2c} \\
E(y_{it}|Treat=1, Post=1) &= \hat{\alpha} + \hat{\theta}_1 + \hat{\theta}_2 + \hat{\gamma} = Y_1  \tag{2d}
\end{align}
$$
我们可以先分析一下 $\theta_1$ 和 $\theta_2$ 的含义：
- $\hat{\theta}_1 = Y_0 - C_0$，个体固定效应，这其实就是「处理组」和「控制组」在政策实施之前的「结果变量」差异，
- $\hat{\theta}_2 = C_1 - C_0$，时间趋势效应。由于控制组并未收到政策的影响，因此其结果变量在事前与事后的变化完全归因为时间趋势。


由此可以得到：

$$
\begin{align}
 \hat{\gamma} & = (Y_1-C_1) - (Y_0-C_0)  \tag{3a}  \\
                       & =  (Y_1-Y_0) - (C_1-C_0)  \tag{3b}
\end{align}
$$
- $(Y_1-C_1) = \hat{\theta}_1 + \hat{\gamma} = \color{blue}{个体效应}   + \color{red}{政策效应}$
- $(Y_1-Y_0) = \hat{\theta}_2 + \hat{\gamma} = \color{green}{时间趋势} + \color{red}{政策效应}$




## 多期 DID
- 适用于所有实验组的个体在同一个时间点受到政策冲击。
- 多期数据有助于在事前检验是否满足「平行趋势假设」，在事后可以进行短期和长期政策评价。
### 模型设定

$$
y_{it} = \alpha + \gamma\ Treat_i \times Post_t + \mu_i + \lambda_t + {x}_{it}'\beta + \varepsilon_{it}
$$



### 系数含义


## 异时 DID
主要是指政策时点不一致的情形，此时可以参照「事件研究法」的基本思想，把事件发生当年定义为 $t=0$，而事件发生之前和之后的时点则分别定义为 $t=-1, -2, \cdots$ 和  $t=+1, +2, \cdots$


$$
y_{it} = \alpha + \gamma\  D_{it}+ \mu_i + \lambda_t + {x}_{it}'\beta + \varepsilon_{it}
$$
其中，$D_{it}$ 为虚拟变量，当第 $i$ 个体在第 $t$ 时点受到政策影响时取值为 1，否则为零。具体而言，假设广州市在 2013 年开始实施某项政策，则其

Dit
is a treatment indicator that is equal to one if an individual i is treated at time t and zero otherwise,


## 参考资料













