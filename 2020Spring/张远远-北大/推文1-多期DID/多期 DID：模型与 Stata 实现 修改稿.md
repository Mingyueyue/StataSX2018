### 背景介绍

经济学家在评估某项政策或事件的处理效应时，常使用反事实框架，将受到处理 (处理组) 的数据 (实际可观测到)，与假如未受到处理的数据 (反事实)，之间的差异称为“处理效应”。由于未受到处理的数据是反事实的，并不能被实际观测到，因此常寻找各个方面都与处理组相似的、未受到处理的控制组，作为处理组的反事实的替代，处理组前后变化与控制组前后变化之差为“处理效应”。

在估计处理效应时，如果处理组仅有一个被处理对象，使用[合成控制法](https://mp.weixin.qq.com/s?__biz=MzAwMzk4ODUzOQ==&mid=2247485898&idx=1&sn=ec39c9a2a29eb307d44d9b7ccf31cc48&chksm=9b33849eac440d8868962d1bbffb1ddf35c2e48ffbcae2a2d27f0c37c6d580cceb3db7c636fb&scene=21#wechat_redirect) (Synthetic Control Method，简记 SCM)。如果处理组有多个被处理对象，使用倍分法 / 双重差分法 (Difference in differences，简记 DID 或 DD) ，如果被处理时间是同一时间点，使用[传统 DID](https://mp.weixin.qq.com/s/9xAerU-nLeVq6tl0ZGqvpA)；如果被处理时间是不同时间点，使用多期 DID。

DID 是估计处理效应中应用最广泛的计量方法。该方法的思想最初是由医学家 John Snow (1855) 年研究伦敦的霍乱流行时提出的，由 Obenauer 和 von der Nienburg (1915) 研究最小工资法的影响效应时引入经济学。为估计处理效果，比较处理后与处理前的差异，利用处理组的前后变化减去控制组的前后变化，就是处理效应，故名“双重差分”。DID 需要满足的前提假设是，处理组与控制组在未受到处理前必须具有相同的变化趋势，这就是“平行趋势”或“共同趋势”假设。

如果检验发现满足共同趋势假设，则可以直接采用 DID 方法；如果检验发现不满足共同趋势假设，需要使用基于倾向得分匹配法的双重差分法 (PSM-DID)。PSM-DID 的基本思想是，在总的控制组中使用 PSM 方法构造出一个与处理组具有共同趋势的控制组，即在总的控制组中选择与处理组具有相同或相似倾向得分值的样本，作为处理组的实际使用的控制组，使得处理组与控制组满足共同趋势假设。

本文主要的关注点是处理时间点不一致的处理效应的估计方法，即多期 DID，主要分三部分内容进行介绍：

-  1 模型介绍

-  2 模型估计及 Stata 实现

-  3 图示处理前后各期的处理效应及检验平行趋势假设


### 1. 模型介绍

#### 1.1 传统 DID 模型

DID使用了面板数据，估计面板数据的最常用的模型是双向固定效应模型，对于面板模型的设定，可以参考连享会之前的推文[Stata: 面板数据模型-一文读懂](https://www.jianshu.com/p/e103270ce674)。在双向固定效应模型的基础上，**传统 DID** 模型加入了处理组虚拟变量 ($treat_i$) 与处理期虚拟变量 ($post_t $) 的交乘项，模型设定如下： 

$$
y_{i, t} = \alpha + \mu_{i} + \lambda_t + \theta treat_i \times post_t + \beta \mathbf{x_{i, t}} + \epsilon_{i, t} (1)
$$

其中，$y_{i, t}$ 为因变量；$i$ ($i$=1, $\cdots$, $N$) 表示个体；$t$ ($t=1, \cdots, T$) 表示时间；$\mu_{i}$ 表示个体固定效应；$\lambda_{t}$ 表示时间固定效应；$\mathbf{x_{i, t}}$ 表示随时间和个体变化的控制变量；$\beta$是控制变量的系数； $\epsilon_{i, t}$ 为模型误差项。

$treat_i$ 为处理组虚拟变量，若个体 $i$ 属于受到政策冲击 (这里以估计政策实施效果为例进行说明) 的“处理组”，则取值为1；若个体 $i$ 属于未受到政策冲击的“控制组”，取值为0。$post_t$ 为处理期虚拟变量，处理组的个体也只有到了处理期才会受到政策冲击 (之前未受到冲击) ，若个体 $i$ 进入处理期取值为1；否则，取值为0。

值得注意的是，模型中不需要加入处理组虚拟变量 $treat_i$，是因为模型中加入了个体固定效应 $\mu_{i}$，$\mu_{i}$ 包含更多信息，是控制了个体层面不随时间变化的特征，而 $treat_i$ 仅控制了组别层面不随时间变化的特征，若二者同时加入会产生多重共线性问题。同样的，模型中也不需要加入处理期虚拟变量 $post_t $，因为模型中加入了时间固定效应 $\lambda_t$，$\lambda_t$ 包含更多的信息，是控制了每一期的时间效应，而 $post_t $ 仅控制了处理期前后的时间效应，若二者同时加入会产生多重共线性问题。

交乘项的系数 $\theta$ 是我们关心的处理效应。系数 $\theta$ 为：

$$
\begin{align}
\theta & = \left\{ E[y_{i, t}| treat_i =1, post_t =1]  - E[y_{i, t}|  treat_i =1, post_t =0] \right\} \\
& -\left\{ E[y_{i, t}|  treat_i =0, post_t =1]  - E[y_{i, t}|  treat_i =0, post_t =0] \right\} \\
& =  (Y_1-Y_0) - (C_1-C_0) =(\theta + \lambda_t ) - \lambda_t \\
& = (Y_1-C_1) - (Y_0-C_0) =(\theta + \mu_i ) - \mu_i
\end{align}
$$

上式的含义是处理组前后变化与控制组前后变化之差，就是处理效应。

下面通过一个包含3个个体，5期的面板数据，展示一下处理组虚拟变量 $treat_i$，处理期虚拟变量 $post_t $ 以及交乘项 $treat_i \times post_t $ 的取值。处理组包括受到政策冲击的个体 1 和 个体 2，政策冲击发生在 2003 年；控制组包括未受到政策冲击的个体 3。

```Stata
id  year treati	postt treati*postt
--------------------------------------
1	2001	1	  0         0
1	2002	1	  0         0
1	2003	1	  1         1
1	2004	1	  1         1
1	2005	1	  1         1
--------------------------------------
2	2001	1	  0         0
2	2002	1	  0         0
2	2003	1	  1         1
2	2004	1	  1         1
2	2005	1	  1         1
--------------------------------------
3	2001	0	  0         0
3	2002	0	  0         0
3	2003	0	  0         0
3	2004	0	  0         0
3	2005	0	  0         0
```

#### 1.2 多期 DID 模型

传统 DID 假定处理组的所有个体开始受到政策冲击的时间点均完全相同，但是会出现处理组个体接受处理时间点不一致的情况，比如美国银行分支机构的放松管制政策在不同的州推出的时间不一致。**多期DID (Time-varying DID)**，也被称为多时点DID或异时DID，就是描述个体的处理期时间点不完全一致的情况，将上述 (1) 式中的 $post_t $ 替换为 $post_{i, t} $，即处理期的时间点因个体  $i$ 而异。模型如下：

$$
y_{i, t} = \alpha + \mu_{i} + \lambda_t + \theta treat_i \times post_{i, t} + \beta \mathbf{x_{i, t}} + \epsilon_{i, t} (2)
$$

交乘项的系数 $\theta$ 是我们关心的整体的平均处理效应。系数 $\theta$ 为：

$$
\begin{align}
\theta &= \left\{ E[y_{i, t}| treat_i =1, post_{i, t} =1]  - E[y_{i, t}|  treat_i =1, post_{i, t} =0] \right\}  \\
 & -\left\{ E[y_{i, t}|  treat_i =0, post_{i, t}  =1]  - E[y_{i, t}|  treat_i =0, post_{i, t}  =0] \right\} \\ 
& =  (Y_1-Y_0) - (C_1-C_0) =(\theta + \lambda_t ) - \lambda_t \\
& = (Y_1-C_1) - (Y_0-C_0) =(\theta + \mu_i ) - \mu_i
\end{align}
$$

上式的含义是处理组前后变化与控制组前后变化之差，就是平均处理效应。

上述模型中的交乘项 $treat_i \times post_{i, t}$，与表示个体 $i$ 在第 $t$ 期接受处理的虚拟变量 $D_{i, t}$ 等价。因此，多期 DID 模型也可以设定如下：

$$ 
y_{i, t} = \alpha + \mu_{i} + \lambda_t + \theta D_{i, t} + \beta \mathbf{x_{i, t}} + \epsilon_{i, t} (3)
$$

其中，$D_{i, t}$ 表示因个体而异的处理期虚拟变量，若个体$i$ 在第 $t$ 期接受处理，代表进入处理期，则此后时期均取值为1；否则，取值为0。

同样的，虚拟变量 $D_{i, t}$ 的系数 $\theta$ 是我们关心的整体的平均处理效应。$D_{i, t}$ 的系数为：

$$
\begin{align}
\theta &= \left\{ E[y_1|D_{i, t}=1]  - E[y_1|D_{i, t}=0] \right\} \\
&-\left\{ E[y_0|D_{i, t}=1]  - E[y_0|D_{i, t}=0] \right\} \\
& =  (Y_{after}-Y_{before}) - (C_{after}-C_{before}) =(\theta + \lambda_t ) - \lambda_t \\
& = (Y_{after}-C_{after}) - (Y_{before}-C_{before}) =(\theta + \mu_i ) - \mu_i
\end{align}
$$

下面仍然通过一个包含3个个体，5期的面板数据，展示一下处理组虚拟变量 $treat_i$，处理期虚拟变量 $post_{i, t} $ ，交乘项 $treat_i \times post_ {i, t} $ 以及个体异质性的处理期虚拟变量 $D_{i, t}$ 的取值。个体 1 的政策冲击发生在 2003 年，个体 2 的政策冲击发生在 2004 年，个体 3 始终没有受到政策冲击。

```Stata
id	year treati postit treati*postit Dit
----------------------------------------------
1	2001	1	  0          0        0
1	2002	1	  0          0        0
1	2003	1	  1          1        1
1	2004	1	  1          1        1
1	2005	1	  1          1        1
----------------------------------------------
2	2001	1	  0          0        0
2	2002	1	  0          0        0
2	2003	1	  0          0        0
2	2004	1	  1          1        1
2	2005	1	  1          1        1
----------------------------------------------
3	2001	0	  0          0        0
3	2002	0	  0          0        0
3	2003	0	  0          0        0
3	2004	0	  0          0        0
3	2005	0	  0          0        0
```
#### 1.3 包含处理前后各期效应的多期 DID 模型

上述模型 (2) 和模型 (3) 中的系数 $\theta$ 是处理后整体的平均处理效应。多期 DID 通常涉及处理前后多期，为考察处理期前后各期的处理效应，需要看处理前后各期的情况；为检验多期DID是否满足共同趋势假设，往往需要看处理前的情况。模型设定如下：

$$ 
y_{i, t} = \alpha + \mu_{i} + \lambda_t + \sum_{\tau=1}^{m} \theta_{-\tau}  D_{i, t-\tau} + \theta D_{i, t} + \sum_{\tau=1}^{q} \theta_{+\tau}  D_{i, t+\tau} + \beta \mathbf{x_{i, t}} + \epsilon_{i, t} (4)
$$

其中，$\theta_{+\tau}$ 表示处理之前的 $\tau$ 期产生的影响，$\theta_{+\tau}$ 表示处理之后的 $\tau$ 期产生的影响。$\theta$ 表示处理当期产生的影响，因此当年份为处理当期时，$D_{i, t}$ 取值为1，否则取值为0。

假设对于每个个体来说，在时间范围 $[t-m, t+q]$，处理只发生一次，那么可能的处理情况表示如下：

$$w_{j}=\left\{D_{it-m}, \ldots, D_{it-1}, D_{it}, D_{it+1}, \ldots, D_{it+q} \right\}= \left[ \begin{array}{c}{w_{c}} \\ {w_{-m}} \\ {\vdots} \\ {w_{-1}} \\ {w_{0}} \\ {w_{+1}} \\ {\vdots} \\ {w_{+q}}\end{array}\right] =\left[ \begin{array}{cccc}{0} & {\cdots} & {0}  & {0}  & {0}  & {\cdots} & {0} \\ {1} & {\cdots} & {0}  & {0}  & {0}  & {\cdots} & {0} \\ {\vdots} &  & {\vdots}  & {\vdots}  & {\vdots}  & & {\vdots} \\ {0} & {\cdots} & {1}  & {0}  & {0}  & {\cdots} & {0} \\ {0} & {\cdots} & {0}  & {1}  & {0}  & {\cdots} & {0} \\ {0} & {\cdots} & {0}  & {0}  & {1}  & {\cdots} & {0} \\  {\vdots} &  & {\vdots}  & {\vdots}  & {\vdots}  & & {\vdots} \\ {0} & {\cdots} & {0}  & {0}  & {0}  & {\cdots} & {1} \end{array}\right]$$

在两个可能的结果值 $y_{it}(w_{j})$ 和 $y_{it}(w_{k})$ 之间的平均处理效应可以表示为：

$$ATE_{jk} = E\left\{y_{it}(w_{j})- y_{it}(w_{k})\right\} = E\left(y_{it} |w_{j} \right) - E\left(y_{it} |w_{k} \right)$$

那么，虚拟变量 $D_{it+h}$ 的系数为：$\theta_{h} = ATE_{hc}=E\left(y_{it} |w_{h} \right) - E\left(y_{it} |w_{c} \right)= (Y_{h}-C_{h}) - (Y_{before}-C_{before})$

$\theta_{-m} = ATE_{-m, c}= (Y_{-m}-C_{-m}) - (Y_{before}-C_{before})$
$ {\vdots} $
$\theta_{-2}= ATE_{-2, c}= (Y_{-2}-C_{-2}) - (Y_{before}-C_{before})$

$\theta_{-1}= ATE_{-1, c}= (Y_{-1}-C_{-1}) - (Y_{before}-C_{before})$

$\theta_{0} = ATE_{0, c}= (Y_{0}-C_{0}) - (Y_{before}-C_{before})$

$\theta_{+1} = ATE_{+1, c}= (Y_{+1}-C_{+1}) - (Y_{before}-C_{before})$

$\theta_{+2} = ATE_{+2, c}= (Y_{+2}-C_{+2}) - (Y_{before}-C_{before})$
$ {\vdots} $
$\theta_{+q} = ATE_{+q, c}= (Y_{+q}-C_{+q}) - (Y_{before}-C_{before})$

而我们关注的平均处理效应 ATT，并不是各期虚拟变量的系数，而是：

$ATT_{-m} = (Y_{-m}-C_{-m}) - (Y_{0}-C_{0})=\theta_{-m}-\theta_{0}$
$ {\vdots} $
$ATT_{-2} = (Y_{-2}-C_{-2}) - (Y_{0}-C_{0})=\theta_{-2}-\theta_{0}$
$ATT_{-1} = (Y_{-1}-C_{-1}) - (Y_{0}-C_{0})=\theta_{-1}-\theta_{0}$
$ATT_{+1} = (Y_{+1}-C_{+1}) - (Y_{0}-C_{0})=\theta_{+1}-\theta_{0}$
$ATT_{+2} = (Y_{+2}-C_{+2}) - (Y_{0}-C_{0})=\theta_{+2}-\theta_{0}$
$ {\vdots} $
$ATT_{+q} = (Y_{+q}-C_{+q}) - (Y_{0}-C_{0})=\theta_{+q}-\theta_{0}$

而由于政策冲击前总的处理效应为0，因此有： $ATT_{-m} + {\cdots} +ATT_{-2}+ATT_{-1}=0$

因此， $(\theta_{-m}-\theta_{0}) + {\cdots} +(\theta_{-2}-\theta_{0})+(\theta_{-1}-\theta_{0})=0$

那么， $\theta_{0}=(\theta_{-m}+ {\cdots} +\theta_{-2}+\theta_{-1})/m$

**综上，各期的平均处理效应 ATT，等于各期虚拟变量的系数减去处理前各期系数均值。**

下面仍然通过一个包含3个个体，5期的面板数据，展示一下包含处理前两期的虚拟变量 $D_{it-2}$、$D_{it-1}$ ，处理当期的虚拟变量 $D_{it}$ 和处理后两期的虚拟变量 $D_{it+1}$、$D_{it+2}$ 的取值。个体 1 的政策冲击发生在 2003 年，个体 2 的政策冲击发生在 2004 年，个体 3 始终没有受到政策冲击。

```Stata
id	year  Dit-2 Dit-1  Dit  Dit+1 Dit+2
--------------------------------------------------
1	2001	1	  0	    0	  0	    0
1	2002	0	  1	    0	  0	    0
1	2003	0	  0	    1	  0	    0
1	2004	0	  0	    0	  1	    0
1	2005	0	  0	    0	  0	    1
--------------------------------------------------
2	2001	0	  0	    0	  0	    0
2	2002	1	  0	    0	  0	    0
2	2003	0	  1	    0	  0	    0
2	2004	0	  0	    1	  0	    0
2	2005	0	  0	    0	  1	    0
--------------------------------------------------
3	2001	0	  0	    0	  0	    0
3	2002	0	  0	    0	  0	    0
3	2003	0	  0	    0	  0	    0
3	2004	0	  0	    0	  0	    0
3	2005	0	  0	    0	  0	    0
```

### 2. 模型估计及Stata实现

#### 2.1 `xtreg` 命令

关于面板模型，在推文中[Stata: 面板数据模型-一文读懂](https://www.jianshu.com/p/e103270ce674) 对面板数据模型设定、模型分类和选择以及 Stata 实现均做了详细的介绍。本文的多期 DID 模型是建立在双向固定效应模型基础上的，对于双向固定效应模型的估计，在连享会之前的推文[倍分法DID详解 (二)：多时点 DID (渐进DID)]([https://mp.weixin.qq.com/s/bba2lRo3QAwyMLPNkPhb6w](https://mp.weixin.qq.com/s/bba2lRo3QAwyMLPNkPhb6w)
) 和 [倍分法DID详解 (三)：多时点 DID (渐进DID) 的进一步分析]([https://mp.weixin.qq.com/s/nHX2KTU5vhwRXlrqr_CqGw](https://mp.weixin.qq.com/s/nHX2KTU5vhwRXlrqr_CqGw)
)中，通过模拟的方法，分别对比分析了政策效果不随时间变化，以及政策效果随时间变化两种情况下的多期 DID 的双向固定效应模型，采用 `reg`，`xtreg`，`areg`，`reghdfe` 这四种命令估计的优缺点以及估计结果。结果发现这四个命令在估计双向固定效应模型上是等价的，估计系数大小是一致的，唯一区别是估计系数的标准误有所变化。

为演示模型估计结果，本文采用 Stata 中用来估计固定效应模型的官方命令 `xtreg` 来估计多期DID模型，语法如下：

```stata
  xtreg depvar [indepvars] [if] [in] [weight] , fe [FE_options]
```
其中，
`depvar` 是被解释变量；
`indepvars` 是解释变量，包括主要的自变量，时间固定效应和其他控制变量。

`FE_options`中的选项有：
`fe` 是选用固定效用模型估计，控制个体固定效应；
`vce(vcetype)` 是指稳健型标准误估计的类别选项，可以是异方差稳健型标准误 `robust`，聚类调整的稳健型标准误 `cluster`，自体抽样法稳健型标准误 `bootstrap` 或 刀切法稳健型标准误 `jackknife`。

#### 2.2 论文实例演示

Beck(2010) 是一篇发表在金融学顶级期刊 Journal of Finance 上的利用多期 DID 模型的经典文献。美国各州在 1960-1999 年期间的不同时间点放松了对银行分支机构的管制，该文章研究银行分支机构放松管制对收入分配不平等的影响。样本包括美国的 49 个州，31年 (1976-2006年) 的平衡面板数据，共1519个观测值。

论文选取基尼系数等四类指标作为因变量收入分配不平等的代理变量，本文只选取基尼系数这一变量演示政策时点不一致的多期 DID 的处理效应，基尼系数取值在 0 和 1 之间，其含义是，值越大，表示收入分配越不平等；值越小，表示收入分配越平等。处理期虚拟变量在某个州银行分支机构放松管制后取值为1，意味着此后该州处于处理组中。

该多期 DID 模型设定为双向固定效应模型，因此在模型中控制个体固定效应和时间固定效应，分别产生地区虚拟变量和时间虚拟变量。首先估计的模型是不加入其他控制变量的，数据处理和估计命令如下：

```Stata
  use "macro_workfile.dta", clear //读取数据

  label var _intra "Bank deregulation" //处理期虚拟变量
  
  xtset statefip wrkyr //设置面板数据格式
  
  tabulate wrkyr, gen(wrkyr_dumm) //生成时间固定效应虚拟变量
  tabulate statefip, gen(state_dumm) //生成个体固定效应虚拟变量

  replace p10 = 1 if p10==0
  
  generate log_gini = log(gini) //因变量

  *未加入控制变量
  xtreg log_gini _intra  wrkyr_dumm*, fe robust // 对应模型 (3)
```

在估计模型中未加入控制变量的结果如下所示：

```Stata
Fixed-effects (within) regression               Number of obs     =      1,519
Group variable: statefip                        Number of groups  =         49

R-sq:                                           Obs per group:
     within  = 0.3490                                         min =         31
     between = 0.0075                                         avg =       31.0
     overall = 0.2490                                         max =         31

                                                F(31,48)          =      45.99
corr(u_i, Xb)  = -0.0117                        Prob > F          =     0.0000

                              (Std. Err. adjusted for 49 clusters in statefip)
------------------------------------------------------------------------------
             |               Robust
    log_gini |      Coef.   Std. Err.      t    P>|t|     [95% Conf. Interval]
-------------+----------------------------------------------------------------
      _intra |  -.0219874   .0075254    -2.92   0.005    -.0371183   -.0068565
 
       _cons |  -.8090041   .0110005   -73.54   0.000    -.8311221   -.7868861
-------------+----------------------------------------------------------------
     sigma_u |  .02915639
     sigma_e |  .03757708
         rho |  .37579375   (fraction of variance due to u_i)
------------------------------------------------------------------------------
```

上表的估计结果汇报了系数估计值、稳健性标准误、t统计量、p值、置信区间、F统计量等。由于报告年份虚拟变量 `wrkyr_dumm*` 的系数回归结果会使得篇幅显得冗长，因此在上表的结果窗口中省略报告 虚拟变量 `wrkyr_dumm*` 的系数，下同。

我们重点关注的是系数估计值及其符号，以及t统计量或p值。上表结果显示，美国银行分支机构放松管制对基尼系数有显著负的影响，意味着银行分支机构放松管制导致收入分配不平等的降低。

由于收入分配会受到其他因素的影响，如人均州生产总值增长率、黑人所占比例、辍学率、户主为女性的家庭所占的比例和失业率等。加入上述控制变量，模型估计代码如下：

```Stata
*控制变量 
  global Xs "gsp_pc_growth prop_blacks prop_dropouts prop_female_headed unemploymentrate"
  
*加入控制变量
  xtreg log_gini _intra $Xs wrkyr_dumm*, fe robust //对应模型 (3)
```
估计模型中加入控制变量的结果显示如下：

```Stata
Fixed-effects (within) regression               Number of obs     =      1,519
Group variable: statefip                        Number of groups  =         49

R-sq:                                           Obs per group:
     within  = 0.3876                                         min =         31
     between = 0.0396                                         avg =       31.0
     overall = 0.2339                                         max =         31

                                                F(36,48)          =      98.01
corr(u_i, Xb)  = -0.2892                        Prob > F          =     0.0000

                                    (Std. Err. adjusted for 49 clusters in statefip)
------------------------------------------------------------------------------------
                   |               Robust
          log_gini |      Coef.   Std. Err.      t    P>|t|     [95% Conf. Interval]
-------------------+----------------------------------------------------------------
            _intra |  -.0177239   .0064033    -2.77   0.008    -.0305986   -.0048493
     gsp_pc_growth |  -.0288419   .0412498    -0.70   0.488    -.1117802    .0540965
       prop_blacks |  -.2128497   .1584962    -1.34   0.186    -.5315277    .1058284
     prop_dropouts |   .1641627   .0713148     2.30   0.026     .0207747    .3075507
prop_female_headed |   .0190403    .056497     0.34   0.738    -.0945545    .1326352
  unemploymentrate |   .0063327   .0012649     5.01   0.000     .0037895    .0088759
             _cons |  -.8407245   .0347162   -24.22   0.000    -.9105261   -.7709229
-------------------+----------------------------------------------------------------
           sigma_u |  .03262543
           sigma_e |  .03651163
               rho |    .443967   (fraction of variance due to u_i)
------------------------------------------------------------------------------------

```

上表结果显示，在控制了一些影响收入分配的其他因素后，主效应依然稳健，美国银行分支机构放松管制对基尼系数依然有显著负的影响，说明银行分支机构放松管制会减弱收入分配的不平等。

### 3. 图示处理前后各期的处理效应及检验平行趋势假设

#### 3.1 Stata实现：`tvdiff` 命令

本文介绍的 `tvdiff` 命令，能够实现仅运行一条命令就可以同时实现图示处理期前后处理效应，以及呈现平行趋势检验统计结果。

`tvdiff` 是 Giovanni Cerulli 和 Marco Ventura 开发的外部命令，可以实现对多期 DID 前后各期处理效应的分析。

**命令安装**

在Stata命令窗口中输入如下命令即可自动安装 `tvdiff` 命令：

```stata
ssc install tvdiff, replace //安装命令
```
在使用 `tvdiff` 命令前，要保证 Stata 已安装 `coefplot` 命令，因为 `tvdiff` 命令的画图功能是根据 Jann (2014) 的 `coefplot` 命令实现的。`coefplot` 命令的安装可以输入如下命令：

```stata
ssc install coefplot, replace //安装命令
```

**语法格式**

该命令的基本语法如下：

```stata
tvdiff outcome treatment [varlist] [if] [in] [weight], ///
model(modeltype) pre(#) post(#) [test_tt graph ///
save_graph(graphname) vce(vcetype)]
```
其中，
`outcome` 是被解释变量；
`treatment` 是处理效应虚拟变量，处理当期取值为1，否则取值为0。该变量就是上述模型 (4) 中的 $D_{it}$；
`varlist` 是其他解释变量，包括固定效应虚拟变量、时间效应虚拟变量或控制变量；
`model(modeltype)`指的是估计模型，模型类型 modeltype 必须是 `fe` (fixed effects) 或 `ols` (OLS)，该选项必填；
`pre(#)` 是指处理之前的时期的数目`(#)`，该选项必填；
`post(#)` 是指处理之后的时期的数目`(#)`，该选项必填；
`test_tt` 实现平行趋势检验，默认使用 leads；
`graph` 实现把结果呈现在图形中，绘制该图使用的是 `coefplot` 命令；
`save_graph(graphname)` 允许把图保存为名字 graphname；
`vce(vcetype)` 允许模型估计中实现异方差稳健型标准误 `robust`，或聚类调整的稳健型标准误 `cluster`。

运行`tvdiff` 命令会产生下列变量：

**_D_L1, _D_L2, ..., _D_Lq**：代表处理后的时期的虚拟变量，q 等于 `post(#)`中填写的数字 `(#)`。
**_D_F1, _D_F2, ..., _D_Fm**：代表处理前的时期的虚拟变量，m 等于 `pre(#)`中填写的数字 `(#)`。

#### 3.2 一个模拟数据的例子

首先，运用数据生成过程 (GDP) 产生数据以及设定多期 DID 模型，然后使用 `tvdiff` 命令进行估计。具体代码如下：

```Stata
//数据生成过程
. clear
. set obs 5 //设置5个个体
. set seed 10101
. gen id=_n //生成个体id
. expand 50 //观察值扩展50倍
. drop in 1/5 //删除前五个观察值
. bysort id: gen time=_n+1999 //时间从2000-2048年，共49期
. gen D=rbinomial(1,0.4) 
. gen x1=rnormal(1,7) 
. tsset id time
//设定多期 DID 模型
  forvalues i=1/6{ gen L`i'_x=L`i'.x }
. bys id: gen y0=5+1*x+ rnormal()
. bys id: gen y1=100+5*x+90*L1_x+90*L2_x+120*L3_x+100*L4_x+90*L5_x +90*L6_x + rnormal()
. gen A=6*x+rnormal()
. replace D=1 if A>=15
. replace D=0 if A<15
. gen y=y0+D*(y1-y0)
//模型估计
. tvdiff y D x , model(fe) pre(6) post(6) vce(robust) test_tt graph save_graph(mygraph) //对应模型 (4)
```
结果如下图所示：
![](https://images.gitee.com/uploads/images/2019/1226/175459_9504e9cc_5479945.png)

从上图可以看出，在处理前的6期，每个时期的虚拟变量的系数均与 0 无显著差异，说明满足平行趋势假设。在处理后的6期，每个时期的虚拟变量的系数均大于0，且均在 1% 或 5% 的水平上显著，表明具有显著正的处理效应。

平时趋势假设检验结果如下所示：

```Stata
******************************************************************************
**************** Test for 'parallel trend' using the 'leads' *****************
******************************************************************************

 ( 1)  _D_F6 = 0
 ( 2)  _D_F5 = 0
 ( 3)  _D_F4 = 0
 ( 4)  _D_F3 = 0
 ( 5)  _D_F2 = 0
 ( 6)  _D_F1 = 0
       Constraint 2 dropped
       Constraint 6 dropped

       F(  4,     4) =    0.78
            Prob > F =    0.5920

RESULT: 'Parallel-trend' passed

******************************************************************************
```

结果显示，满足平行趋势假设。

#### 3.3 论文实例演示 - 使用 `tvdiff` 命令

该部分同样是选用 Beck(2010) 论文的例子进行演示。数据处理及模型估计命令如下：

```Stata
  use "macro_workfile.dta", clear

  xtset statefip wrkyr
  
  generate D = (wrkyr - branch_reform == 0)

  generate y = ln(gini)
 
  global X "gsp_pc_growth prop_blacks prop_dropouts"
 
  tvdiff y D $X, model(fe) pre(5) post(10) vce(robust) test_tt graph save_graph(mygraph) //对应模型 (4)
```
结果如下图所示：
![](https://images.gitee.com/uploads/images/2019/1226/175459_22066f2b_5479945.png)

从上图可以看出，在处理前的5期，每个时期的虚拟变量的系数均与 0 无显著差异，说明满足平行趋势假设。在处理后的10期，每个时期的虚拟变量的系数均小于0，表明放松银行分支机构管制会在管制放松后的几期仍然对收入不平等具有减弱效应，处理后持续到第8期，减弱效应增强，第8期后，减弱效应降低。

平时趋势假设检验结果如下所示：

```Stata
******************************************************************************
**************** Test for 'parallel trend' using the 'leads' *****************
******************************************************************************

 ( 1)  _D_F5 = 0
 ( 2)  _D_F4 = 0
 ( 3)  _D_F3 = 0
 ( 4)  _D_F2 = 0
 ( 5)  _D_F1 = 0

       F(  5,    48) =    1.41
            Prob > F =    0.2363

RESULT: 'Parallel-trend' passed

******************************************************************************
```
结果显示，满足平行趋势假设。

#### 3.4 论文实例演示 - 使用 `coefplot` 命令手动画图

对于平行趋势检验的图示，可以通过手动生成各期虚拟变量，估计回归系数，然后使用 `coefplot` 命令画图的方式。`tvdiff` 命令的画图功能 `graph` 已经封装在代码中，运行 `tvdiff` 命令得到的图示如 3.5.1 部分的图所示。使用 `coefplot` 命令手动画图，可以使得图示更加灵活，根据需要调整各种参数，可以得到我们想要的图的效果。

Beck(2010) 论文中的图3，可以使用 `coefplot` 命令手动画出比较相似的图。为保证与上述使用 `tvdiff` 命令画出的图具有可比性，处理前后的时期分别选定为 5 期和 10 期。数据处理、模型估计和画图代码如下：

``` Stata
use "macro_workfile.dta",replace
xtset statefip wrkyr

gen policy = wrkyr - branch_reform
replace policy = -5 if policy <= -5
replace policy = 10 if policy >= 10

gen policy_d = policy + 5
gen y = log(gini)

xtreg y ib5.policy_d i.wrkyr, fe r 

///生成前五期系数均值
forvalues i = 0/4{
	gen b_`i' = _b[`i'.policy_d]
}

gen avg_coef = (b_0+b_4+b_3+b_2+b_1)/5
su avg_coef 

coefplot, baselevels ///
   drop(*.wrkyr _cons policy_d) ///
   coeflabels(0.policy_d = "t-5" ///
   1.policy_d = "t-4" ///
   2.policy_d = "t-3" ///
   3.policy_d = "t-2" ///
   4.policy_d = "t-1" ///
   5.policy_d = "t" ///
   6.policy_d = "t+1" ///
   7.policy_d = "t+2" ///
   8.policy_d = "t+3" ///
   9.policy_d = "t+4" ///
   10.policy_d = "t+5" ///
   11.policy_d = "t+6" ///
   12.policy_d = "t+7" ///
   13.policy_d = "t+8" ///
   14.policy_d = "t+9" ///
   15.policy_d = "t+10") ///更改系数的label
   vertical ///转置图形
   yline(0, lwidth(vthin) lpattern(dash) lcolor(teal)) ///加入y=0这条虚线
   ylabel(-0.06(0.02)0.06) ///
   xline(6, lwidth(vthin) lpattern(dash) lcolor(teal)) ///
   ytitle("Percentage Changes", size(small)) ///加入Y轴标题,大小small
   xtitle("Years relative to branch deregulation", size(small)) ///加入X轴标题，大小small
   transform(*=@-r(mean)) ///去除前五期的系数均值 
   addplot(line @b @at) ///增加点之间的连线
   ciopts(lpattern(dash) recast(rcap) msize(medium)) ///CI为虚线上下封口
   msymbol(circle_hollow) ///plot空心格式
   scheme(s1mono)  
```
运行结果如下图所示：

![](https://images.gitee.com/uploads/images/2019/1226/175500_f78c2c07_5479945.png)


### 参考文献

1. Abadie A. Semiparametric difference-in-differences estimators[J]. The Review of Economic Studies, 2005, 72(1): 1-19.[[pdf]](https://sci-hub.se/10.1111/0034-6527.00321)
2. Angrist J D, Pischke J S. Mostly harmless econometrics: An empiricist's companion[M]. Princeton university press, 2009.[[pdf]](https://sci-hub.se/10.1057/be.2009.37)
3. Beck T, Levine R, Levkov A. Big bad banks? The winners and losers from bank deregulation in the United States[J]. The Journal of Finance, 2010, 65(5): 1637-1667.[[pdf]](https://www.jstor.org/stable/pdf/40864982.pdf)
4. Cerulli G, Ventura M. Estimation of pre-and posttreatment average treatment effects with binary time-varying treatment using Stata[J]. The Stata Journal, 2019, 19(3): 551-565.[[pdf]](https://sci-hub.se/10.1177/1536867X19874224)
5. Cerulli G. Extending the difference-in-differences (DID) to settings with many treated units and same intervention time: Model and Stata implementation[C]//2019 Stata Conference. Stata Users Group, 2019 (26).[[pdf]](http://fmwww.bc.edu/repec/scon2019/chicago19_Cerulli.pdf)



