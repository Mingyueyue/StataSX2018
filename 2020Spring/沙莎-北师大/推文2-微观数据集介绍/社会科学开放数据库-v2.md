> 沙莎 (北京师范大学)，shasha_1212@163.com


# 背景介绍

数据是科研工作的重要基础。

本文对国内社会科学开放研究数据库进行梳理，对比了使用注意事项，希望为大家的科研工作提供便利。


## 社会科学开放研究数据库整理

### 1. [CLHLS](https://opendata.pku.edu.cn/dataset.xhtml?persistentId=doi:10.18170/DVN/XRV2WN)：中国老年健康影响因素跟踪调查
- 原名：中国老人健康长寿影响因素跟踪调查
- 网站链接：https://opendata.pku.edu.cn
- 数据来源：北京大学健康老龄与发展研究中心/国家发展研究院组织
- 调查对象：65 岁及以上老年人和 35-64 岁成年子女
- 调查时间：基线调查始于 1998 年，此后分别于 2000 年、2002年、2005年、2008-2009 年、2011-2012 年和 2014-2015 年进行了 6 次跟踪调查
- 调查内容：调查问卷分为存活被访者问卷和死亡老人家属问卷两种：存活被访者问卷的调查内容包括老人及家庭基本状况、社会经济背景及家庭结构、经济来源和经济状况、健康和生活质量自评、认知功能、性格心理特征、日常活动能力、生活方式、生活照料、疾病治疗和医疗费承担等；死亡老人家属问卷的调查内容包括 死亡时间、死因等内容。在问卷调查同时，课题组还对老人进行了最基本的健康体能测试，并在几次调查中采集了生物样本
- 研究领域：老年学；健康老龄政策等
- 获取方式：可通过北京大学开放数据研究平台注册申请，申请通过后免费获取 (https://opendata.pku.edu.cn/)
- 特点：CLHLS 数据最初设计是以高龄老人为主的调查，最新公开老人数据为2014年调查，数据清晰，便与研究；曾进行过老人 - 子女配对调查，配对数据仅公开到 2002-2005 年份调查

#### 参考文献：
- **数据介绍文献：**
   - 曾毅．中国老年健康影响因素跟踪调查 (1998-2012) 及相关政策研究综述(上) [J]．老龄科学研究，2013，1(01)：65-72．[[PDF]](https://quqi.gblhgk.com/s/896184/MAUcV67l0fpzEZo5)
  - 曾毅．中国老年健康影响因素跟踪调查 (1998-2012) 及相关政策研究综述(下) [J]．老龄科学研究，2013，1(02)：63-71．[[PDF]](https://quqi.gblhgk.com/s/896184/YyDL9XBlUecPQh4m)  
  - *更多数据介绍可参考 https://opendata.pku.edu.cn/dataset.xhtml?persistentId=doi:10.18170/DVN/XRV2WN*
- **数据使用文献：**
  - Li, T., et al. (2018). "All-cause mortality risk associated with long-term exposure to ambient PM2.5 in China: a cohort study." Lancet Public Health 3 (10): E470-E477. [[PDF]](https://www.thelancet.com/action/showPdf?pii=S2468-2667%2818%2930144-0)
  - Lv, X., et al. (2019). "Cognitive decline and mortality among community-dwelling Chinese older people." Bmc Medicine 17. 
[[PDF]](https://bmcmedicine.biomedcentral.com/track/pdf/10.1186/s12916-019-1295-8)
  - Lv, Y.-B., et al. (2017). "A U-shaped Association Between Blood Pressure and Cognitive Impairment in Chinese Elderly." Journal of the American Medical Directors Association 18(2). [[PDF]](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC5294228/pdf/nihms840989.pdf)

### 2. [CHARLS](http://charls.pku.edu.cn/)：中国健康与养老追踪调查
- 网站链接：http://charls.pku.edu.cn/
- 数据来源：北京大学国家发展研究院主持、北京大学中国社会科学调查中心与北京大学团委共同执行
- 调查对象：中国 45 岁及以上中老年人家庭和个人
- 调查时间：基线调查于 2011 年开展，覆盖 150 个县级单位， 450 个村级单位，于 2011、2013、2015 和 2018 年分别在全国 28 个省 (自治区、直辖市) 的 150 个县、450 个社区 (村) 开展调查访问，目前公开数据到 2015 年。CHARLS 还曾在 2014 年组织并实施了“中国居民生命历程调查”、 2016 年开展“共和国初期基层经济历史调查”两项全国性专项访问，亦完全覆盖上述样本地区
- 调查内容：个人基本信息，家庭结构和经济支持，健康状况，体格测量，医疗服务利用和医疗保险，工作、退休和养老金、收入、消费、资产，以及社区基本情况等
- 研究领域：老年学；健康政策等
- 获取方式：在每次调查研究结束 1 年后，向大众公开数据，可通过注册申请获取 (http://charls.pku.edu.cn/pages/data/111/zh-cn.html)
- 特点：数据成果丰富；数据分为多个子问卷，包需要事前合并和清理，部分变量包含缺失值较多；收集受访对象生物学信息，例如血检体格测量等
#### 参考文献：
- **数据介绍文献：**
  - Zhao, Y., et al. (2014). "Cohort Profile: The China Health and Retirement Longitudinal Study (CHARLS)." International Journal of Epidemiology 43(1): 61-68. [[PDF]](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC3937970/pdf/dys203.pdf)
   - *更多数据介绍可参考 http://charls.pku.edu.cn/*
- **数据使用文献：**
   - 张川川，陈斌开．“社会养老”能否替代“家庭养老”？——来自中国新型农村社会养老保险的证据 [J]．经济研究，2014，49(11)：102-115．[[PDF]](https://quqi.gblhgk.com/s/896184/NDZLunex8WARobXb)
  - Khera, R., et al. (2018). "Impact of 2017 ACC/AHA guidelines on prevalence of hypertension and eligibility for antihypertensive treatment in United States and China: nationally representative cross sectional study." Bmj-British Medical Journal 362. [[PDF]](https://www.bmj.com/content/bmj/362/bmj.k2357.full.pdf)

### 3. [CFPS](http://www.isss.pku.edu.cn/cfps/index.htm)：中国家庭追踪调查
- 网站链接：http://www.isss.pku.edu.cn/cfps/index.htm
- 数据来源：北京大学中国社会科学调查中心
- 调查对象：社区、家庭和个体，个体包括成人和少儿
- 调查时间：2010 年基线调查界定出所有基线家庭成员及其今后的血缘/领养子女，成为 CFPS 基因成员和永久追踪对象。基线样本覆盖 25 个省/市/自治区，代表了中国 95% 的人口，此后又分别于 2012、2014、2016 年开展了三轮全样本的追踪调查。
- 调查内容：共有社区问卷、家庭问卷、成人问卷和少儿问卷四种主体问卷类型，并在此基础上不断发展出针对不同性质家庭成员的长问卷、短问卷、代答问卷、电访问卷等多种问卷类型。在社区层面，CFPS 通过村居问卷对各样本村/居进行一个整体的访问，主要了解该村/居的基础设施、人口结构、政策实施、经济情况、社会服务等信息；在家庭层面，由一位家庭成员回答一份关于家庭成员信息与成员间关系的家庭成员问卷以及一份反映家庭整体情况的家庭问卷；在个人层面，对于符合资格的个人，16 岁以下者回答少儿问卷，16 岁及以上者回答成人问卷
- 研究领域：重点关注中国居民的经济与非经济福利，以及包括经济活动、教育获得、家庭关系与家庭动态、人口迁移、身心健康等在内的诸多研究主题
- 获取方式：注册申请，免费获取 (http://www.isss.pku.edu.cn/cfps/cjwt/xzwt/index.htm)
- 特点：提供详细的数据使用手册供参考，研究儿童问题、家庭问题、贫困问题等使用的研究使用很多
#### 参考文献：
- **数据介绍文献：**
  - 谢宇，胡婧炜，张春泥．中国家庭追踪调查：理念与实践 [J]．社会，2014，34(02)：1-32．[[PDF]](https://quqi.gblhgk.com/s/896184/GcOAcQSrp0CoNaVd)
  - *更多数据介绍可参考 http://www.isss.pku.edu.cn/cfps/index.htm*
- **数据使用文献：**
  - 李忠路，邱泽奇．家庭背景如何影响儿童学业成就？——义务教育阶段家庭社会经济地位影响差异分析 [J]．社会学研究，2016，31(04)：121-144+244-245．[[PDF]](https://quqi.gblhgk.com/s/896184/q717hyP2jZ5c2Jk5)
  - 马光荣，周广肃．新型农村养老保险对家庭储蓄的影响：基于 CFPS 数据的研究 [J]．经济研究，2014，49(11)：116-129．[[PDF]](https://quqi.gblhgk.com/s/896184/zgHksYdRpGh56O52)
  - Qi, D. and Y. Wu (2018). "Does welfare stigma exist in China? Policy evaluation of the Minimum Living Security System on recipients' psychological health and wellbeing." Social Science & Medicine 205: 26-36. [[PDF]](https://pdf.sciencedirectassets.com/271821/1-s2.0-S0277953618X00082/1-s2.0-S027795361830159X/main.pdf?X-Amz-Security-Token=IQoJb3JpZ2luX2VjEAkaCXVzLWVhc3QtMSJHMEUCIDoAacgnSp%2F9s1wtPO7Uzc7YqZ57UnfvTS9RIHdjlVryAiEAq3IvgrkT2nrZNXhXbN698I%2Bra3DF721kSZWlOmcMBrwqzwIIUhACGgwwNTkwMDM1NDY4NjUiDPFo1eJC1WVYtq2HDiqsAmNX3LPC0BqLhMdzsVlBzuI50r7r%2B4rIqEOV%2Ff%2Fo%2BUBb7%2Fw9GUzRM09t8iPDUwy6aSpp5PMkPaW%2Fp3nQFOEC4LqfBvFZ7%2BOVysR%2FPOBTdJPKr5tWJduO0ishTUJ9DQhrkS6glKYyslv3HiAF2vTxw3mT0ZqApSZwjjq8JYorr5HRP3h6L%2BXxiZX97%2BVLgW0YdGXJaCW1vqzAmfIjp0CJ6ztiCSxJFc0lGkAxueMoFwOWMLXUZp5hR9YeCOIPNbKGwTc%2FCJAI6bWkoogSQmln0G8i6QfOQSjPsZ0JhL4jzRLnqL25Qfzou0JKon73uZ2%2By5afrw7RG5jvPg%2Bsjq6ovjACVIrKcPWxOb9Zsx8%2BejI0x5BSSvaFfJiory2BPSkizS7LaFuFakqh%2B8rajjDAqqHvBTrQApxLXWJSHCeuJSIIN34Bp6xMj19A%2BD201xNEcfnCEKO1iwysYhmsMIacy7XSNE5bd8FU9s9%2FbMbOEVbCPKZE91UIvMq6MjGScW9jeh9VnDt9tEwvUf3%2BVimjTKfXyaFNJo531KJKrqrl8eYn1SCFVeI7E5tXH2cmUeTiLzH6BiHETYkFcwNzCYf1DDa5vRysb3ONTnSn3t2O6UzWDw1VN%2F0Zy47soUQsM9VcdxHEo7u1Fk%2BWzpYegxo7okSV62ccROAVTG4Ml42FBmMFrD0QU8DD9OzJshRKWFFw1tkLCZFX06V%2FYc4qotr4fjTlJhg3NROCilIUmgdnvfEMQ7cyow3sJ9KKkTKzBo5SRMifcM3Ay%2B742X5EiuSJ74DakgKR0zb2U0uGaDv5GHeGEVyYYY66fUadDfMbcAmfcKBqEyXh3qMCVIoFpZG2ntpRDDEYFg%3D%3D&X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Date=20191205T023126Z&X-Amz-SignedHeaders=host&X-Amz-Expires=300&X-Amz-Credential=ASIAQ3PHCVTY2FMIRKPN%2F20191205%2Fus-east-1%2Fs3%2Faws4_request&X-Amz-Signature=275d4ccd3d2ea3d7de263c0e3653dd85adc0b60ff74eb9cfa70eda9d8bca8e80&hash=ac161e224b4369b44083a12c45e0fb8377865b31a1c7116c47cabb416dfccf72&host=68042c943591013ac2b2430a89b270f6af2c76d8dfd086a07176afe7c76c2c61&pii=S027795361830159X&tid=spdf-87dc877c-b0c0-4f27-a7bc-18c85cf89b3a&sid=9e2b6d652ea3734dcf38fb162738717df756gxrqa&type=client)

### 4. [CHIP](http://www.ciidbnu.org/chip/)：中国家庭收入调查
- 网站链接：http://www.ciidbnu.org/chip/
- 数据来源：北京师范大学中国收入分配研究院
- 调查对象：所有的 CHIP 数据均包含针对城镇和农村住户的调查。2002 年和 2007 年增加了对流动人口的关注，分为城镇住户调查、农村住户调查和流动人口调查
- 调查时间：CHIP 始于 1989 年，后续分别于 1996 年、2003 年、2008 年和 2014 年进行了四次入户调查
- 调查内容：包括住户个人层面的基本信息、就业信息，以及家庭层面的基本信息、主要收支信息和一些专题性问题，包括收入、支出、住户成员个人情况、劳动时间安排、就业情况、住户资产、拆迁征地情况、农业经营等内容
- 研究领域：关注经济问题、收入分配、消费、贫困等问题
- 获取方式：注册申请后获取 (http://www.ciidbnu.org/chip/about.asp)
#### 参考文献：
- **数据介绍文献：**
  - *更多详细介绍参考 http://www.ciidbnu.org/chip/*
- **数据使用文献：**
  - 罗楚亮，李实．中国住户调查数据收入变量的比较 [J]．管理世界，2019，35(01)：24-35+226．[[PDF]](https://quqi.gblhgk.com/s/896184/9WnyYp3BXbb4EVyl)
  - 李实，朱梦冰．中国经济转型40年中居民收入差距的变动 [J]．管理世界，2018，34(12)：19-28．[[PDF]](https://quqi.gblhgk.com/s/896184/HuHBTegtuhj9Gn22)
  - 岳希明，张斌，徐静．中国税制的收入分配效应测度 [J]．中国社会科学，2014(06)：96-117+208．[[PDF]](https://quqi.gblhgk.com/s/896184/WGnUogA9fdjwHo2u)
  - 罗楚亮．经济增长、收入差距与农村贫困 [J]．经济研究，2012，47(02)：15-27．[[PDF]](https://quqi.gblhgk.com/s/896184/7NlsKnEqrMnUXirX)

### 5. [CHFS](https://chfs.swufe.edu.cn/researchcenter/intro.html)：中国家庭金融调查
- 网址链接：https://chfs.swufe.edu.cn/researchcenter/intro.html
- 数据来源：西南财经大学中国家庭金融调查与研究中心
- 调查对象：所有的 CHFS 数据均包含针对城镇和农村住户的调查。2002 年和 2007 年增加了对流动人口的关注，分为城镇住户调查、农村住户调查和流动人口调查
- 调查时间：CHFS 基线调查始于 2009 年，每两年进行一次中国家庭金融调查，现已经在 2011 年、2013 年、2015 年和 2017 年四次成功实施全国范围内的家庭随机抽样调查。目前对外开放申请 2011 年、2013 年和 2015 年家庭金融调查数据库，2017 年的家庭金融调查数据库暂只对 2017 年有过合作的高校联盟用户开放申请
- 调查内容：住房资产与金融财富、负债与信贷约束、收入与消费、社会保障与保险、代际转移支付、人口特征与就业以及支付习惯等相关信息
- 研究领域：关注家庭经济、金融行为；房地产市场调控、收入分配与经济转型、城镇化问题研究都有所使用
- 获取方式：注册申请后获取 (http://chfs.swufe.edu.cn/datas/](http://chfs.swufe.edu.cn/datas/)
#### 参考文献：
- **数据介绍文献：**
  - Li Gan, Zhichao Yin, Nan Jia, Shu Xu, Shuang Ma, 2013, Data you need to know about China, Springer. (书籍需购买：https://link.springer.com/book/10.1007%2F978-3-642-38151-5#about) 
  - *更多详细介绍参考 https://chfs.swufe.edu.cn/researchcenter/intro.html*
- **数据使用文献：**
  - 甘犁，尹志超，贾男，徐舒，马双．中国家庭资产状况及住房需求分析 [J]．金融研究，2013(04)：1-14．[[PDF]](https://quqi.gblhgk.com/s/896184/RQTXnnO4I8HJPE9p)
  - 甘犁，赵乃宝，孙永智．收入不平等、流动性约束与中国家庭储蓄率 [J]．经济研究，2018，53(12)：34-50．[[PDF]](https://quqi.gblhgk.com/s/896184/3IyyR24UgEbwKzBI)
  - 李凤，罗建东，路晓蒙，邓博夫，甘犁．中国家庭资产状况、变动趋势及其影响因素 [J]．管理世界，2016(02)：45-56+187．[[PDF]](https://quqi.gblhgk.com/s/896184/iV5jRyC34Bs6QnWb)
  - Clark, W. A. V., et al. (2019). "Subjective well-being in China's changing society." Proceedings of the National Academy of Sciences of the United States of America 116(34): 16799-16804. [[PDF]](https://www.pnas.org/content/pnas/116/34/16799.full.pdf)

### 6. [CGSS](http://cgss.ruc.edu.cn/)：中国综合社会调查
- 网站链接：http://cgss.ruc.edu.cn/
- 数据来源：中国人民大学社会学系、香港科技大学社会科学部联合建立
- 调查对象：社会、社区、家庭、个人
- 调查时间：自 2003 年起，每年一次，进行连续性横截面调查
- 调查内容：调查问卷由三部分构成：核心模块：调查全部样本，年度调查，固定不变；主题模块：调查全部样本，5 年重复一次，两次调查内容重合率超过 80% 。其中，核心模块与主题模块主要服务于描述与解释社会变迁的宗旨，扩展模块则主要服务于跨国比较研究的目的，每年具体调查内容可参考 (http://cgss.ruc.edu.cn/index.php?r=index/questionnaire)
- 研究领域：家庭问题，社会认同、社会变迁、社会资本等
- 获取方式：公开部分年份数据，最新公开数据为 2015 年，注册后免费获取 (http://www.cnsda.org/)
#### 参考文献：
- **数据介绍文献：**
  - *更多数据介绍请参考 http://cgss.ruc.edu.cn/index.php?r=index/publication*
- **数据使用文献：**
  - 吴愈晓．中国城乡居民的教育机会不平等及其演变 (1978—2008) [J]．中国社会科学，2013(03)：4-21+203．[[PDF]](https://quqi.gblhgk.com/s/896184/mxWuMcLTjfoLx0QL)
  - 刘军强，熊谋林，苏阳．经济增长时期的国民幸福感——基于CGSS数据的追踪研究 [J]．中国社会科学，2012(12)：82-102+207-208．[[PDF]](https://quqi.gblhgk.com/s/896184/BasS3Nl5yHzVm72K)
  - 李颖晖．教育程度与分配公平感:结构地位与相对剥夺视角下的双重考察 [J]．社会，2015，35(01)：143-160．[[PDF]](https://quqi.gblhgk.com/s/896184/idDLb3SmxV2CN3i5)
  - Li, S., et al. (2018). "Laugh and grow fat: Happiness affects body mass index among Urban Chinese adults." Social Science & Medicine 208: 55-63. [[PDF]](https://reader.elsevier.com/reader/sd/pii/S0277953618302430?token=8E6B3B99FD78292860A997B5A7C09C0C6AEE84092270F0FBF617B7BF81FCEBAAE5DE85BF3DF168A4835FFC1DCB9353E3)

### 7. [CHNS](https://www.cpc.unc.edu/projects/china)：中国营养与健康调查
- 网站链接：https://www.cpc.unc.edu/projects/china
- 数据来源：由北卡罗来纳大学人口研究中心﹑美国国家营养与食物安全研究所和中国疾病与预防控制中心合作开展
- 调查对象：社区、家庭和个人
- 调查时间：项目始于 1989 年，分别于 1989 年、1991 年、1993 年、1997 年、2000 年、2004 年、2006 年、2009 年、2011 年、2015 年进行调查。2018 年 CHNS 官方网站新发布 2009 年的生物标志物数据和 2015 年的大部分调查数据，并发布 1989-2015 面板数据
- 调查内容：包括社区组织、家庭和个人经济、人口和社会因素等的现状及变化。其中个人及家庭调查数据包括基本人口学、健康状况、营养膳食状况和健康指标、医疗保险等；社区调查数据包括食品市场、医疗服务及其他社会基础设施建设情况等
- 研究领域：营养摄入；收入，贫困；健康等
- 获取方式：家庭数据及个人数据通在 CHNS 官网注册申请后可免费获取，社区数据数据的获取则需通过和北卡罗来纳大学人口中心签订数据使用协议，并在线完成相关流程付费获取，费用标准为 330 美金
- 特点：英文数据库，涉及的变量较多，处理较为复杂处理，追踪年份的数据存在较多缺失值
#### 参考文献：
- **数据介绍文献：**
  - Popkin, B. M., et al. (2010). "Cohort Profile: The China Health and Nutrition Survey-monitoring and understanding socio-economic and health change in China, 1989-2011." International Journal of Epidemiology 39(6): 1435-1440. [[PDF]](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC2992625/pdf/dyp322.pdf)
  - Zhang, B., et al. (2014). "The China Health and Nutrition Survey, 1989-2011." Obesity Reviews 15: 2-7. [[PDF]](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC3869031/pdf/nihms533090.pdf)
  - 杨晓光，孔灵芝，翟凤英，马冠生，金水高．中国居民营养与健康状况调查技术执行组．中国居民营养与健康状况调查的总体方案 [J]．中华流行病学杂志，2005(07)：471-474．[[PDF]](https://quqi.gblhgk.com/s/896184/pIgj9aZvWvTxpzZo)
  - 常虹，田国祥，柳青青，任晓东，冀彬，吕军．CHNS 数据库架构及数据申请提取方法与流程 [J]．中国循证心血管医学杂志，2018，10(09)：1043-1047．[[PDF]](https://quqi.gblhgk.com/s/896184/9jdV20g3k3TdFkHe)
- **数据使用文献：**
  - 王小林，Sabina Alkire．中国多维贫困测量：估计和政策含义 [J]．中国农村经济，2009(12)：4-10+23．[[PDF]](https://quqi.gblhgk.com/s/896184/zzaXPbeVWwnNSfAW)
  - Huang, M., et al. (2019). "Potato consumption is prospectively associated with risk of hypertension: An 11.3-year longitudinal cohort study." Clinical Nutrition 38(4): 1936-1944. [[PDF]](https://pdf.sciencedirectassets.com/272493/1-s2.0-S0261561419X00060/1-s2.0-S0261561418311920/main.pdf?X-Amz-Security-Token=IQoJb3JpZ2luX2VjEA0aCXVzLWVhc3QtMSJIMEYCIQCerfbD1wLafRv6UeRoOazxy4lrVBPD4TURszVAUXo6NAIhAP1fh3MroYI4aWO%2FKQrq2TAFRicZ39f0KUPSkAJ2mTkgKtACCFYQAhoMMDU5MDAzNTQ2ODY1Igy%2BHsfEFb2swfKnGQUqrQL%2BT6%2FrrRkPGqzF9XpRuL24vMWUMzjuGdIamIlIK5S8SzrVvd86a2c84D%2FghnjkwfAWZjsdVHwccG4M7P6TQU6q7JE4nMd6ocfGPTNjgYtm0LJxwjbKNkAcBvR%2BbGYVNWOOng7v9fNDz%2BOdWIFyHpJJ584s%2Fe5RKxCLCHaweJS5LLFOlul3vcFoJ38zNLkgjB2oVKdpoufEDkD411%2Fnfpd9hy1Upb0sVu7Nay3smdID0uXSdXDzecn9vwvWJSd%2Bm0MdveX0ufz3sBNn5%2FtSfU9vtyGGsy40zD3K1Xr%2BBUx%2F5iZI8iThwDqbFguaevQU1%2FMMTEAOON1C5Tcd1qQtPqsFZR0Lah6DANSqEJf911BdwIaI3FwbXVfAHizaDSeq2MolvJWDv5jHA3sS2BKbMP2Uou8FOs4CE8XTYIZTnzt2Kl14ak%2FAZTi%2BEsFfvfZOCaPqmKUxkaGdyqghQAmuqx0gGztjeGIfkSWl%2FfKSUriMHGtaZk4vQGEZ5nCvzRRXB3OdHvF5eE36XBd8nfewQvJ%2FsQzR6PFQNomM9ZZkdaXwe%2FQLEQY4oSJgJdDkS3w35dTI69rUXxks4XUy2Gnc2y0JcvEqKTYt9PXEywsBkVhJy5HNdFQoaAYgM2E6gixL9ZGPXlLOv92Oi%2BXCYBevO1NClBnxXGa3cpU6KESNd8IBhm5LwiDF2ItLi0we3fANB%2F6FZJptu6P%2FLVfPySupWVEz0%2BP1jB4N2pflOyHCY2WP49GUh1ywAHcLtuinMOv6Sw4kzZMnM5LmijE4skQFGSuNDoCcBjbY6EbH01942cFM43j6MaPqe4iezXAEpb5EKmzs%2FebpVzR56mc1MmLH%2B2LtPWxgfw%3D%3D&X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Date=20191205T053408Z&X-Amz-SignedHeaders=host&X-Amz-Expires=300&X-Amz-Credential=ASIAQ3PHCVTY7TT3RRTK%2F20191205%2Fus-east-1%2Fs3%2Faws4_request&X-Amz-Signature=e4894fdd5bba93d8da377d6174838ba5f716e9deba6d0faf99d63bd87a50ca70&hash=ed0e87acad1ba5c29146a547bdea812c1ce3eaebb22b82a0b906832a76db2339&host=68042c943591013ac2b2430a89b270f6af2c76d8dfd086a07176afe7c76c2c61&pii=S0261561418311920&tid=spdf-e46a5d8f-ca0a-4105-9258-8d6cdb270858&sid=9e2b6d652ea3734dcf38fb162738717df756gxrqa&type=client)
   - Spracklen, C. N., et al. (2018). "Identification and functional analysis of glycemic trait loci in the China Health and Nutrition Survey." Plos Genetics 14(4). [[PDF]](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC5886383/pdf/pgen.1007275.pdf)
  - Zhuang, P., et al. (2019). "Polyunsaturated fatty acids intake, omega-6/omega-3 ratio and mortality: Findings from two independent nationwide cohorts." Clinical Nutrition 38(2): 848-855. [[PDF]](https://pdf.sciencedirectassets.com/272493/1-s2.0-S0261561419X00035/1-s2.0-S0261561418300827/main.pdf?X-Amz-Security-Token=IQoJb3JpZ2luX2VjEA0aCXVzLWVhc3QtMSJHMEUCIQDLh%2BB58IMC1xcnuczfh9UmCltnGhQrYA01aqkPEnGWBwIgH47vXRw7o3fw0El4%2FxE6ZzRH%2BrvnzSVQpMG4CCaHhN0qzwIIVhACGgwwNTkwMDM1NDY4NjUiDLnAog%2FbsSpenbkO4CqsAlWCfOZJ0laAGmXwaLGoe2VfCm%2BjMvG3j1RkdR21dWtSsxS5hodpNTGPFH8RedSZ2ZEFaZAOoCxPkrXSfXmoDX2E4kfBjjFrPFWK4ADbzvAu5wJFz4oxsmaweR5MtFAT0GcfBSoRTMUuXqe1gA0trdMKDriNR3ql2uIG%2FeJ7Y3M8HPMF5gHrEX%2Bab9%2BSOD6zi9CrGNmQ8amEfK3LsOVJrjb8kG6p3AbpdvXCPFPmKcH7%2BvjODER0%2FzoPUOVOgjk4m9jZAkM%2BkWjjBpLoUd0Ga7GvfzhWruv2mTUQracCLRMsTq5qn5OZCoyPGidtmuWOMLyjctuWWUSBYJGlugEuTDVoDFKrJJAfRLT%2BkyXYu4xFtR80iVaW4IjTqBi08gl5Bscimiy63%2BQ8i0QipTCnk6LvBTrQAtR0gdkX0c6G46Iar%2FpUosjWaISN%2B2c8lzfgSqkNwavhouDyGj9kdgOaMtiS2GdgCMROJsMl%2Fx53FAVieTZsughh5Q%2FInr8APymxf9pFh9o3UNB1Z%2B2ZC%2Bf5fUoWd4juvlA6SfJetM24hlST8t9rlM1Oc8hK1%2FkC2D7JoXgd5oZpDzz4d%2B6DOZsdJfVgwixTHrlcnzVYTOxo0vMbi0JaIcPWy1%2B6dBb3oxKYOPWTOYJ2lXT1HUwOyyQH1VeLzMHreGtsMDdw4BcAQ57hqdLxWxcft12ehPH8Z3g%2FT27yuZ5tWwgRehdaxNIlWLI%2Bbd02wFGXubnvFUaJN0X%2BaL15YTUK%2FrrbMfAeDnYrAaL2T%2FX5AB%2BKwofFjGF9ciAr4CfoNQ%2B4TGZ4%2BLnb6JqMgXVp%2Fx3mu0Z9GnIDdGc4DULfva4stCW7Ssq4vC7WbAJNxtKQxA%3D%3D&X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Date=20191205T053809Z&X-Amz-SignedHeaders=host&X-Amz-Expires=300&X-Amz-Credential=ASIAQ3PHCVTYRJCLTMSL%2F20191205%2Fus-east-1%2Fs3%2Faws4_request&X-Amz-Signature=8a6c450054f56587fabb2730567e7cea1ead98efd979956b4af16bf3cc607c42&hash=270cfccdfafd119eef3cf45f6f791eddb137ae14ee7cc823e850db3c60e6f149&host=68042c943591013ac2b2430a89b270f6af2c76d8dfd086a07176afe7c76c2c61&pii=S0261561418300827&tid=spdf-11fcb58a-ded4-4673-92c4-298b6c55d930&sid=9e2b6d652ea3734dcf38fb162738717df756gxrqa&type=client)

### 8. [SUNS](http://www.sociology.shu.edu.cn/default.aspx?tabid=22123&ctl=detail&mid=41137&id=240404&skinsrc=[l]skins/shxy_2fen/shxy_2fen)：上海都市社区调查
- 网站链接：http://www.sociology.shu.edu.cn/default.aspx?tabid=22123&ctl=detail&mid=41137&id=240404&skinsrc=[l]skins/shxy_2fen/shxy_2fen
- 数据来源：上海大学数据科学与都市研究中心和香港科技大学应用社会经济研究中心联合进行
- 调查对象：分为三个调查，居村调查 (上海市居委会或村委会)、住户调查 (家庭户及其所有成员)，社区观察 (居委会及其下属的小区 (不包括村委会) )
- 调查时间：2015 年开始调查，2017 年结束
- 调查内容：社区层次搜集的信息包括社区空间与设施、社区人口构成、社区组织数量与构成、社区资源特征、社区治理方式与绩效、社区集体效能感等方面；家庭层次的信息包括家庭人口构成、家庭社会经济地位、家庭关系、家庭生活方式等；个人层次的信息包括个人基本社会经济特征、工作与生活、健康、社会态度与评价、社区参与等
- 研究领域：大都市；社区参与；社会适应等
- 获取方式：数据仍处于梳理阶段，尚未开始网上公开申请，感兴趣的话可以持续关注
#### 参考文献：
- **数据介绍文献：**
  - 孙秀林等著．中国城市社区脉动：上海调查(2017) [Ｍ]．北京：社会科学文献出版社，2018．
  - 王建平，叶锦涛．大都市老漂族生存和社会适应现状初探——一项来自上海的实证研究 [J]．华中科技大学学报 (社会科学版)，2018，32(02)：8-15．[[PDF]](https://quqi.gblhgk.com/s/896184/xWptjHbtjUgokEKQ)
- **数据使用文献：**
  - Miao, J., et al. (2019). "Neighborhood, social cohesion, and the Elderly's depression in Shanghai." Social Science & Medicine 229: 134-143. [[PDF]](https://pdf.sciencedirectassets.com/271821/1-s2.0-S0277953619X00088/1-s2.0-S0277953618304465/main.pdf?X-Amz-Security-Token=IQoJb3JpZ2luX2VjEA0aCXVzLWVhc3QtMSJGMEQCIAx6NrdkEcc6TWAvHk%2FrrkR65bC5La1fIYoBZYSj%2Fr%2FyAiBnN5pu02UpgSKd4rgYlXxnMcafUoySSXm4NvoHJ%2BgGMCrPAghWEAIaDDA1OTAwMzU0Njg2NSIMUldz9g2iNh4qLtIOKqwCFQkVGFl%2BFlTwzRNYSNTtoaT6bKcekTryVJyO8Nuidida6NHDuHPnnOjW0RsfYxdCyxvvSCBeY8twyb2qsdTOzW48VX0tI%2BgUx6P6ynQO0xNOSy6dsHkPTdoqkf%2BnwrH65g9e8wXcSJnYzzJpePxDttNVG6LloHMlSE%2F0tJYVKRA9FR%2BgfxaWAX4cx3SQsppaearqj2rwRJWWPyKA05NAQU6BdxifqH6A1VsTNt0knhlJgoNfWBDxv4CsFtGBkoyu%2FLZKMlFadIKpC9tiHAaZzbUVzO3OcEAWYUphnl7hzL1fRL6DkKpi2amknSgtDZboo%2F3TtOx3mM%2FH79O%2BkqubMcdIIKS7WwbZChhDeCDnjvzXg01JXyIkCw73Sy4OY%2BoK1U5%2BzZ%2B7GOH%2BHygGMIWbou8FOtECN4xU06N5x9sqpgjYO36zbHfLfCJ1BKguvjaIBULv3lX2rZ09ILHQv3b80mh%2FKQWne8hLLo3bKNQ3mObjsTCjB%2FBFtfZAoMdl10rKJFUwcGTqyxT3MFoKs1C9E7cotY0A3aMyGG2zfXBqEoM7hKzvNH7obB%2FaEd8%2FiQhRrUnqFml4sxAP%2BpjYCU1%2FJOSarZD165C8JdXMLBW7zVu3GqHtaNb5Tjmc6bIr70rn5tHVjqawn0Euj%2BFUlNfDyKdJn5as6yesaUZrTnT8lLLW%2FSTqcqQ2IQRIltaBYk0Mvdoi0vhx%2BxL3ziRcPePTb1O2oNYQTg5AjLSU63BOTIyTLzsBzXB32kTV3rF5ONZgQ6976xNQ%2BDURp1EJJjNced0Ma7u3Sgjh4GAM9uJGvlw%2BG0wmMhHtf6eLheRnVW5O%2BnDjjxIYW3YF0dWFl5ZMCp8EYzSt5w%3D%3D&X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Date=20191205T060701Z&X-Amz-SignedHeaders=host&X-Amz-Expires=300&X-Amz-Credential=ASIAQ3PHCVTYRE3CQHXZ%2F20191205%2Fus-east-1%2Fs3%2Faws4_request&X-Amz-Signature=508b47f064cd4b4dd94ee046a72555130678b8a3ba97f29cd61da7ac5c1fccec&hash=fd24131369b06c73ac9eec14929428624e2f7ae05131ca9c9844c5d856572466&host=68042c943591013ac2b2430a89b270f6af2c76d8dfd086a07176afe7c76c2c61&pii=S0277953618304465&tid=spdf-c5698c91-c245-4d3e-a316-f2367f5a3bce&sid=9e2b6d652ea3734dcf38fb162738717df756gxrqa&type=client)


