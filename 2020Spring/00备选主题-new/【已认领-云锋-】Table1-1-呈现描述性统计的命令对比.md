
## 任务说明
- 这篇推文的主要目的是为了汇总、对比一下各种各样制作论文中第一张表的命令。有点类似于我们在对比的基础上出一份用户体验报告。
- 主要的是如下三个命令：`tabstat`， `fsum`， `tabulate summarize`。我列举了其他命令，主要是在一些特殊情形下使用，那有助于制作比较复杂的基本统计量的表格，这个可以结合目前期刊里边列出的第一张表格的典型的状况，来做一些范例。
- 下面是我目前想到的一些命令，当然你可以使用 `findit` 命令，进一步的在 Stata 里边搜索，同时，也可以登录 `Github`，在里面搜索看有没有更合适的命令。
- 需要说明的是，我列举的这些命令不一定全部都要介绍，你自己首先需要对这些命令都操作一下，进而根据你自己的使用经验作为一个筛选。你先感受一下，然后列出一个提纲，把你计划写到推文里边的命令，列举出来给我看一下，等我们双方商议完，确定最终的提纲以后再正式的开始写这个推文，在此之前，你主要是在 Stata 里编写 do 文件，完成各种范例。
 

```stata

*-计算基本统计量的命令汇总

  help sum 
  
  help tabstat
  
  help tabulate summarize
  
  help collapse //Make dataset of summary statistics
  
  help fsum  // sum with variable labels
  
  *-比较一下下面三个，选一个好用的，如果功能不能完全替代，则保留
  help gsum
  help gdsum
  help sumup  // sum by group
  
  help sumtable //SJ 15(3):775--783

  help datesum
  

*-分组统计量


  help orth_out  //虽然语法复杂一些，但功能非常强大
  sysuse "auto.dta", clear
  orth_out price mpg, by(foreign) se compare test count stars 
  
  
  
*-列表  
  help surtbl //produce various tables for putdocx
  
  
*-高级

  help ameans  
  help moments //compute moment-based statistics
  help matrixtools //
```
  
  
  
 
  

```stata
  help fsum
. sysuse auto, clear
. sumtable foreign, vartype(headerrow) vartext("CAR DETAILS") first(1)
. sumtable price foreign, vartype(contmed) dp1(0) dp2(0)
. sumtable weight foreign, vartype(contmean) dp1(0) dp2(0)
. sumtable length foreign, vartype(contmean) dp1(1) dp2(1)

. sumtable rep78 foreign, vartype(categorical)  ///
           vartext("Repairs since 1978") dp1(0) dp2(1) ///
		   last(1) exportname("Details by car groups")
```