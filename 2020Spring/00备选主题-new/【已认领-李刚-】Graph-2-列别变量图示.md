#### 任务概况

- 介绍几个图形化呈现分组统计量的命令，以便在毕业答辩和学术报告的 PPT 中使用。
- 下面的内容供参考，可以根据自己查阅的资料优化和细化提纲。
- 写法和结构参见 [刘杨 - Stata可视化：让他看懂我的结果！](https://www.jianshu.com/p/43fe2339c90c), 
可以直接复制里面的 Markdown 文档。

#### 主要命令
  
##### catplot 命令

- `help cat/分类变量图示


##### grmeanby 命令：图形呈现类别变量的均值和中位数

- `help grmeanby` //Graph means and medians by categorical variables
  
```stata
  sysuse "nlsw88", clear
*-Graph means of mpg according to values of foreign, rep78, and turn
  grmeanby race married collgrad south c_city union, sum(wage)
  grmeanby race married collgrad south c_city union, sum(hours)
*-graph medians rather than means
  grmeanby race married collgrad south c_city union, sum(wage) median
```