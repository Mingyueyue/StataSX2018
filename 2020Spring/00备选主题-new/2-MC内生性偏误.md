
   
通过 MC 来说明 IV 估计的性质，以及弱 IV 导致的后果。

数据的 DGP 过程参考 Cameron(2009) Microeconometrics Using Stata，[[PDF]](https://quqi.gblhgk.com/s/880197/1701YWJQm0QRk5ha)

如果能进一步将 Clarke, D., & Matta, Benjamín. Practical considerations for questionable IVs. Stata Journal, 2019, 18 (3), 663-691. [[PDF]](https://sci-hub.tw/10.1177/1536867X1801800308) 文中使用的方法应用其中，这个问题就算是比较完整了。

下面是我的课件里 codes，供参考。你也可以用 `postfile` 命令来实现如下 MC 过程。这样一来，从行文上就和上一篇保持一致了。

```stata

*--------------------
*-6.4.2  MC 应用实例: 内生偏误的后果
 
 
  *--------
  *-6.4.2.1  产生模拟数据(DGP)
  
    *-Ref: Cameron(2009), pp.143  Section 4.6.5
	
	*--------------DGP----------------
	*
	*   y = a + b*x + u;   u~N(0,1)
	*   
	*   x = z + 0.5*u;     z~N(0,1)
	*
	*   a=10;  b=2;  N=150
	*
	*---------------------------------
	
	*-Q1: OLS 估计的性质如何？
	*
	*-Q2: IV  估计的性质如何？
	
	
	*-------------------endogreg_OLS.ado----------------------
	 cap program drop endogreg_OLS
	 program define endogreg_OLS, rclass
	    version 11.0
		syntax [, obs(int 150)]
		drop _all
		set obs `obs'
		gen u = rnormal()  
		gen z = rnormal()     //corr(u,z) =0, 因为二者是分别生成的 
		gen x = z + 0.5*u     //corr(x,u)!=0(内生), corr(x,z)!=0(IV)
		gen y = 10 + 2*x + u
		regress y x
		return scalar b  = _b[x]
		return scalar se = _se[x]
		return scalar t  = (_b[x]-2)/_se[x]  //Ho: b(x)=2
		local alpha = invttail(`obs'-2, 0.025)       
		                        // t 分布 5% 显著水平临界值
		return scalar over_rej= (abs(return(t))>`alpha') //过度拒绝取 1
		return scalar p  = 2*ttail($N-2, abs(return(t))) // p ֵ
	 end
	*---------------------------------------------------------
	*-Note: 执行后续命令前，请先选中上述程序，按快捷键 Ctrl+R
	
		
  *--------
  *-6.4.2.2  OLS 估计量的性质
	
	  *-Simulation
	    simulate b=r(b) se=r(se) t=r(t) over_rej=r(over_rej), ///
	         reps(1000) seed(135): endogreg_OLS, obs(500)
	
	  *-统计量的性质
	    tabstat b se t over_rej,  ///
	      s(mean sd min p5 p50 p95 max) f(%6.3f) c(s)
/*
       variable |   mean     sd    min     p5    p50     p95     max
      ----------+---------------------------------------------------
              b |  2.400  0.037  2.281  2.338  2.398   2.463   2.504
             se |  0.036  0.002  0.031  0.033  0.036   0.039   0.043
              t | 11.162  1.145  7.543  9.364 11.146  13.027  15.026
       over_rej |  1.000  0.000  1.000  1.000  1.000   1.000   1.000
      --------------------------------------------------------------
*/   

	  *-OLS estimator is biased  true(b)=2
	    kdensity b, xscale(range(1.9 2.6))  ///
	                xlabel(1.9(0.1)2.6) xline(2,lp(dash))
		graph export "$Out\MC_OLS01.wmf", replace
	    *-Question: 修改为 endogreg_OLS, obs(5000), 看结果有何变化？
	
	  *-S.E. is 32 times too small, and 
	  * the null Ho: b2=2 is over-rejected
	    mean b se over_rej  // mean(reject) should be 5%
/*
     Mean estimation                   Number of obs   =      1,000
     
     --------------------------------------------------------------
                  |       Mean   Std. Err.     [95% Conf. Interval]
     -------------+------------------------------------------------
                b |      2.400      0.001         2.397       2.402
               se |      0.036      0.000         0.036       0.036
         over_rej |      1.000      0.000             .           .
     --------------------------------------------------------------
*/
  
  
  *--------
  *-6.4.2.3  IV 估计量的性质    
	
	*-扩展上述程序
	*----------------------endogreg.ado-----------------------
	 cap program drop endogreg
	 program define endogreg, rclass
	    version 11.0
		syntax [, obs(int 150) iv Rho(real 1)]  // new! 
		drop _all
		set obs `obs'
		gen u = rnormal()
		gen z = rnormal()
		gen x = `rho'*z + 0.5*u        // new!
		gen y = 10 + 2*x + u
		if "`iv'" != ""{               // new!
		  ivregress 2sls y (x=z)       // new!
		}                              // new!
		else{                          // new!
		  regress y x
		}                              // new!
		return scalar b  = _b[x]
		return scalar se = _se[x]
		return scalar t  = (_b[x]-2)/_se[x]  //Ho: b(x)=2
		local alpha = invttail(`obs'-2, 0.025)       
		                        // t 分布 5% 显著水平临界值
		dis in red "alpha = " in y `alpha'						
		return scalar over_rej= (abs(return(t))>`alpha') //过度拒绝取 1
		return scalar p  = 2*ttail($N-2, abs(return(t)))
	 end
	*---------------------------------------------------------
	*-Note: 执行后续命令前，请先选中上述程序，按快捷键 Ctrl+R	
	
	
	*--------                        -------------------   
	*-Case I: use a valid instrument  x = rho*z + 0.5*u   rho=1
	*--------                        ------------------- 
	
	  *-Simulation
	    simulate b=r(b) se=r(se) t=r(t) over_rej=r(over_rej), ///
	         reps(500) seed(135): endogreg, obs(200) iv rho(1.0)
	
	  *-统计量的性质
	    tabstat b se t over_rej,  ///
	      s(mean sd min p5 p50 p95 max) f(%6.3f) c(s)
/*
      variable |   mean     sd     min      p5    p50    p95    max
     ----------+---------------------------------------------------
             b |  1.996  0.069   1.796   1.874  1.998  2.110  2.185
            se |  0.071  0.007   0.053   0.060  0.071  0.084  0.098
             t |  0.009  0.969  -2.487  -1.605 -0.034  1.743  3.489
      over_rej |  0.050  0.218   0.000   0.000  0.000  0.500  1.000
     --------------------------------------------------------------
*/
	   
	  *-OLS estimator is unbiased  true(b)=2
	    kdensity b, xscale(range(1.6 2.2))  ///
	                xlabel(1.6(0.1)2.2) xline(2)
	    graph export "$Out\MC_OLS02.wmf", replace
	  *-S.E. is slightly small, and 
	  * the null Ho: b=2 is rejected about 5% times
	    mean b se over_rej	
/*
      -----------------------------------------------------------
                |       Mean   Std. Err.     [95% Conf. Interval]
      ----------+------------------------------------------------
              b |      1.996      0.003         1.990       2.002
             se |      0.071      0.000         0.071       0.072
       over_rej |      0.050      0.010         0.031       0.069
      -----------------------------------------------------------
*/
	

	*--------                        -------------------   
	*-Case II: use a weak instrument  x = rho*z + 0.5*u	  rho=0.1
	*--------                        ------------------- 	
	
	  *-Simulation
	    simulate b=r(b) se=r(se) t=r(t) over_rej=r(over_rej), ///
	         reps(500) seed(135): endogreg, obs(200) iv rho(0.1)
	
	  *-统计量的性质
	    tabstat b se t over_rej,  ///
	      s(mean sd min p5 p50 p95 max) f(%6.3f) c(s)
/*
      variable |   mean     sd      min      p5     p50    p95     max
     ----------+------------------------------------------------------
             b |  1.490  2.174  -22.198  -0.888   1.975  2.737   3.011
            se |  1.941  7.796    0.158   0.279   0.718  4.188 125.063
             t |  0.307  1.040   -0.784  -0.691  -0.034  2.578   6.400
      over_rej |  0.090  0.286    0.000   0.000   0.000  1.000   1.000
     -----------------------------------------------------------------
*/
	   
	  *-OLS estimator is downward biased  true(b)=2
	    kdensity b, xline(2)
	    graph export "$Out\MC_OLS03.wmf", replace
		
	  *-S.E. is slightly small, and 
	  * the null Ho: b=2 is rejected about 5% times
	    mean b se over_rej		
/*
      -----------------------------------------------------------
                |       Mean   Std. Err.     [95% Conf. Interval]
      ----------+------------------------------------------------
              b |      1.490      0.097         1.299       1.681
             se |      1.941      0.349         1.256       2.626
       over_rej |      0.090      0.013         0.065       0.115
      -----------------------------------------------------------
*/
		
	  *-Exercises:
	  * (1) rho(0.01) 
	  * (2) obs(1000)
	  * (3) gen u = 3*rnormal()	
	  * (4) extend to multiple IVs, and estimate the model using GMM
	  *     hint: ivregress gmm y (x=z1 z2)
		
	  *-Another DGP:  (Cameron, 2005, pp.102)
        *  y = 0 + 0.5*x + u
	    *  x = 0 + z + v
	    *  (u,v) ~ N(0, 1)  // Joint Nomral	
		
  *--------
  *-6.4.2.4 弱 IV 下的估计方法
  *
  * 参见 Clarke and Matta (2018, Stata Journal)

    需要勇吏来补充  

```

## 参考文献

- Cameron A C, Trivedi P K. Microeconometrics Using Stata[M]. 2009. [[PDF]](https://quqi.gblhgk.com/s/880197/1701YWJQm0QRk5ha)
- Clarke, D., & Matta, Benjamín. Practical considerations for questionable IVs. Stata Journal, 2019, 18 (3), 663-691. [[PDF]](https://sci-hub.tw/10.1177/1536867X1801800308)