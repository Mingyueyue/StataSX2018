> **任务：** 根据连享会已经发布的推文，与连老师商议后，写一篇完整的对文，对交乘项的使用进行一个系统性的介绍。相当于把前期的推文做一个汇总，但需要做适当的调整和精简。必要时，可能需要再增加一些内容。考虑到这篇推文的工作量比较大，可以视为两篇推文。

#### 交乘项 (调节效应\中介效应)
- [如何用 Stata 做调节中介效应检验？](http://www.jianshu.com/p/9f29f5f5380f)
- [Stata: 交叉项\交乘项该这么分析！](http://www.jianshu.com/p/b5ea12da7f36)
- [交乘项: 主效应项可以忽略吗?](http://www.jianshu.com/p/eef92dc7d101)
- [Stata：交乘项该如何使用？](http://www.jianshu.com/p/f7222672fe89)
- [Stata：边际效应分析](http://www.jianshu.com/p/012d8a6159cf)
- [Stata：图示连续变量的边际效应(交乘项)](http://www.jianshu.com/p/7af58033dc24)
- [Stata：图示交互效应\调节效应](http://www.jianshu.com/p/fa6778828354)
- [加入交乘项后符号变了!?](http://www.jianshu.com/p/953f30f39195)
- [Stata: 手动计算和图示边际效应](http://www.jianshu.com/p/c6137a56b68a)
- [[转载]Stata: 使用 SEM 估计调节-中介效应](http://www.jianshu.com/p/ca914fb1f296)


&emsp;



1. 基本原理
2. 边际效应及图形呈现
3. 实操问题
  - 主效应是否可以忽略?
  - 中心化问题 [加入交乘项后符号变了!?](http://www.jianshu.com/p/953f30f39195)
4. 内生性问题
   x 是内生变量，模型中加入 x#w 后，交乘项也是内生变量，如何选择工具变量？




## 参考资料
- Balli H O, Sørensen B E. Interaction effects in econometrics[J]. Empirical Economics, 2013, 45(1): 583-603. [[PDF]](https://mpra.ub.uni-muenchen.de/38608/1/interactionApril10_2012.pdf) 这篇讲的非常清楚，还做了 MC 分析。
- Bun M J G, Harrison T D. OLS and IV estimation of regression models including endogenous interaction terms[J]. Econometric Reviews, 2018: 1-14. [-PDF-](https://editorialexpress.com/cgi-bin/conference/download.cgi?db_name=IAAE2015&paper_id=140)
- Ebbes, P., D. Papies, H. van Heerde. 2016, Dealing with endogeneity: A nontechnical guide for marketing researchers[C], Handbook of market research  1-37. 从 IV 讲起，很细致，进一步讨论了交乘项内生以及多个内生变量的情形。[[PDF]]()
- Papies, D., P. Ebbes, H. J. Van Heerde. 2017, Addressing endogeneity in marketing models[C], Advanced methods for modeling markets (Springer,  581-627. [[PDF]](https://www.researchgate.net/publication/319360558_Addressing_Endogeneity_in_Marketing_Models), [[web]](https://link.springer.com/chapter/10.1007/978-3-319-53469-5_18)
- [交乘项 - 这一篇就够了，说得无比清楚！](http://www.quantpsy.org/interact/interactions.htm)
- [交乘项 - 一系列文章，写的很清楚](https://cfss.uchicago.edu/persp013_interaction_terms.html)
- [Interpreting coefficients from interaction (Part 1)](http://rpubs.com/hughes/15353)
- [Interpreting three-way interactions in R - 模拟分析](https://datascienceplus.com/interpreting-three-way-interactions-in-r/)
wode