## 任务：
- 介绍 `rddsga` 命令的原理和使用方法
  - 原理介绍：[subgroup analysis](https://gitlab.com/acarril/rddsga/wikis/subgroup-analysis)
  - Stata 范例：[stata application](https://gitlab.com/acarril/rddsga/wikis/stata-application)
- 学会这个方法，也可以用在你的论文中


## 参考资料：
- 原理介绍和Stata 范例，Carril, A., A. Cazor, M. P. Gerardino, S. Litschig,D. Pomeranz, 2018, Subgroup analysis in regression discontinuity designs, Working Paper. [[PDF]](https://pdfs.semanticscholar.org/d8c6/9a0260a0af43d893cdb1747fba73fe1bffad.pdf)
- [rddsga 的 Github 仓库](https://github.com/acarril/rddsga)，内附程序和 [相关数据](https://github.com/acarril/rddsga/tree/master/data)，以及 [dofiles](https://github.com/acarril/rddsga/tree/master/do)