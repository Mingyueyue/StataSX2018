## 2020 寒假班-论文班-扩展论文 (1.16日)

> 2020-1-16 江老师课上提到的论文

#### 1. Faber, B. (2014). Trade integration, market size, and industrialization: Evidence from China’s national trunk highway system. Review of Economic Studies, 81(3), 1046–1070.
[课上讲的工具变量论文下载地址](https://quqi.gblhgk.com/s/3829824/dUXahIa4LbG20OTw)

#### 2. Dale, S. B., & Krueger, A. B. (2002). Estimating the payoff to attending a more selective college: An application of selection on observables and unobservables. Quarterly Journal of Economics, 117(4), 1491–1527.
[课上讲的匹配论文下载地址](https://quqi.gblhgk.com/s/3829824/EMj2wdXUWv4I45OD)

#### 3. Financial Dependence and Growth
[课后答疑提到的交互项论文下载地址](https://quqi.gblhgk.com/s/3829824/QFSKWV9Tyord2HQp)