
>作者：王宇桐 || 连玉君 ||  ([知乎](https://zhuanlan.zhihu.com/arlion) | [简书](http://www.jianshu.com/u/69a30474ef33) | [码云](https://gitee.com/arlionn))    
>

&nbsp;


### **一. 为什么需要Mata？**
结合Mata的特点与优势，介绍Mata学习的必要性。
#### 1.1 Mata简介
- 定义
- 功能

#### 1.2 Mata运算功能
- 举一个例子


### **二. 回归问题分析**
在本章中，对回归问题进行分析（即程序需要实现什么样的效果）  

基础问题：输入数据后，得到我们想要的**估计量、R²、残差**  

计量基础：我们希望得到的参数如何计算。

> 有了以上分析，我们才能够知道我们到底想要实现什么

### **三. Mata知识准备**
对程序中用到的Mata知识进行简要的介绍。   
> 实际上这个是最后完成的一部分，从程序反推到底需要具备什么样的知识


### **四. 代码构建**
主要参考《The Mata Book: A Book for Serious Programmers and Those Who Want to Be（2018）》中的代码程序