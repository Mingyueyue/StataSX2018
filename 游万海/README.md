### 游万海的项目主页

**Note：** 写作之前请务必先查看 [「Wiki」](https://gitee.com/Stata002/StataSX2018/wikis/Home) 页面中的相关建议和要求，尤其是 [「Stata连享会推文格式要求」](https://gitee.com/Stata002/StataSX2018/wikis/00_Stata%E8%BF%9E%E4%BA%AB%E4%BC%9A%E6%8E%A8%E6%96%87%E6%A0%BC%E5%BC%8F%E8%A6%81%E6%B1%82.md?sort_id=953475)

### 日志
- `2018/11/9 8:28` 开始写作内生性检验推文，期望完成时间：2018.11.15
- `2018/11/7 11:10` 万海，请把外部命令那篇推文的相关文档放入对应的文件夹，以保证项目主页清爽。
- `2018/11/5 10:50` 已完成 [「 Stata: 外部命令的搜索、安装和使用」](https://gitee.com/Stata002/StataSX2018/blob/master/%E6%B8%B8%E4%B8%87%E6%B5%B7/Stata:%20%E5%A4%96%E9%83%A8%E5%91%BD%E4%BB%A4%E7%9A%84%E6%90%9C%E7%B4%A2%E3%80%81%E5%AE%89%E8%A3%85%E5%92%8C%E4%BD%BF%E7%94%A8.md)