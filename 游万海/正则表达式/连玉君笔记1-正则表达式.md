# Stata 中的正则表达式
[toc]

## 简介

参考资料：
- [正则表达式一览表](https://krijnhoetmer.nl/stuff/regex/cheat-sheet/)
- [Stata常用字符串数据处理函数](http://chuansong.me/n/1608664452738)
- [Stata常用字符函数（一）](https://www.wxzhi.com/archives/316/fubrxpa4r0abvaks/)
- [彭文威-Stata常用字符串数据处理函数](https://mp.weixin.qq.com/s?__biz=MzA5NTM4NDE3MQ==&mid=2650691510&idx=1&sn=6cc62bcd980e66f7509e3fa2fc392640&scene=1&srcid=0902RH4Df7tWZChs44oqNOze#wechat_redirect)
- [Stata: What are regular expressions and how can I use them in Stata?](http://www.stata.com/support/faqs/data-management/regular-expressions/)
- [UCLA: How can I extract a portion of a string variable using regular expressions?](http://www.ats.ucla.edu/stat/stata/faq/regex.htm)
-  [Rose Anne Medeiros: Using regular expressions for data management in Stata](http://repec.org/wcsug2007/medeiros_reg_ex.pdf)      

在处理字符串或文本过程中，使用正则表达式 (Regular expressions) 可以快捷、灵活地查询字符。我们可以用正则表达式查询变量、暂元甚至是一个文档中中包含的字符。

**Stata 中的正则表达式规则：**
  In Stata, regular expression syntax is based on **Henry Spencer's NFA algorithm**, and this is nearly identical to the **POSIX.2 standard**

## 正则表达函数
Stata 中的正则表达函数都包含 `regex` 关键词，这是 **++reg++ular ++ex++pressions** 的简写，随后会包含 `m` (**++m++atch**) ，`r` (**++r++eplace**)，或 `f` (**++f++ind**) 等字符以标明该函数的用途。

### regex函数和ustrregex函数
Stata 提供了两大类正则表达函数：一类是以 `regex` 开头的函数，仅能处理简单的 ASCII 字符，包括：
  - `regexm(s, re)`
  - `regexr(s1, re, s2)`
  - `regex(n)`  
  
另一类则是 Stata 14 以后提供的扩展函数，在上述命令基础上附加 `ustr` 前缀，意为 **++u++nicode ++str++ing**，可以处理较为复杂的字符，包括：
  - `ustrregexm(s,re[,noc])`
  - `ustrregexrf(s1,re,s2[,noc])` 
  - `ustrregexra(s1,re,s2[,noc])`
  - `ustrregexs(n)`     

ASCII 字符和 unicode 字符的区别可以简单的理解为，ASCII 字符只能用于英语表达，而运用在中文表达中容易出现乱码，而 unicode 则是适用于多种语言的字符。

### 应用实例

#### regex 函数
我们这里使用 Stata 自带的美国 1978 年汽车数据 **auto.dta** 作为例子，在该数据集中，变量 make 是字符变量(string)，是样本的汽车制造商以及型号。

* **regexm函数**      

    **regexm(s, re) 函数**                用于对字符串 **s** 中的字符进行匹配，如果满足正则表达式 **re**，就赋值为 1，否则赋值为 0。例如，如果我们想要产生一个虚拟变量 Ford，用于描述样本的制造商是否为福特 (Ford), 那么我们可以输入以下命令：
    ```
    sysuse auto.dta
    gen Ford=regexm(make,"Ford")
    ```
    其中make为你需要做匹配的字符变量，即范围，“Ford”则是正则表达，即匹配条件。本例中，正则表达式 "Ford" 意为字符中含有 Ford 的才符合匹配条件，且区分大小写。那么我们可以得到以下效果：
			
    make|	Ford|   
	------|:-----
    Dodge Magnum|	0|	
    Dodge St. Regis|	0	
      Ford Fiesta|	1	
    Ford Mustang|	1	
    Linc. Continental|	0	
    Linc. Mark V|	0	
    Linc. Versailles|	0	
    29.	Merc. Bobcat|	0	
			
* **regexr函数**    

  **regexr(s1,re,s2) 函数** 的作用在于，如果字符串 **s1** 符合正则表达式 **re**，则用字符串**s2**替换掉字符串 **s1** 中符合正则表达式的部分。
  沿用上面的例子，如果我们想生成一个新的字符串 **make2**，而新的字符串仅仅是将 **make** 中的 “Ford” 替换成 “福特”，那么我们可以借助 **regexr函数** :
    ```
    gen make2=regexr(make,"Ford","福特")
    ```
    那么我们可以得到以下结果：
     | make      |                      make2 |
     |------------|------------------------|
     | Dodge Magnum       |      Dodge Magnum |
     | Dodge St. Regis     |  Dodge St. Regis |
     | Ford Fiesta        |       福特 Fiesta |
     | Ford Mustang        |     福特 Mustang |
     | Linc. Continental  | Linc. Continental |
     | Linc. Mark V      |       Linc. Mark V |
     | Linc. Versailles   |  Linc. Versailles |
     | Merc. Bobcat        |     Merc. Bobcat |
    
    值得注意的是，如果在字符变量 **make** 不存在符合正则表达式的字符，那么字符变量**make2**将恢复原状，即等于**make**，即使你之前使用过 **regexr(s1,re,s2)函数** 对其做过替换，例如在上面的基础上再输入命令：
    ```
    replace make2=regexr(make,"BYD","比亚迪")
    list make make2 in 22/29  
    ```
    那么，由于**make**没有包含字符“BYD”，所以**make2**将恢复为**make**:
      
     | make            |                make2 |
     |:------------------|:--------------------|
     | Dodge Magnum      |       Dodge Magnum |
    | Dodge St. Regis     |  Dodge St. Regis |
    | Ford Fiesta          |     Ford Fiesta |
    | Ford Mustang          |   Ford Mustang |
    | Linc. Continental   |Linc. Continental |
    | Linc. Mark V          |   Linc. Mark V |
    | Linc. Versailles     |Linc. Versailles |
    | Merc. Bobcat          |   Merc. Bobcat |
* **regexs函数**   

    **regexs(n)函数** 必须与 **regexm函数** 结合使用,用于返回上一个 **regexm(s,re)函数** 第 n 个子表达式所匹配的字符，其中，0 &lt; n &lt; 10。如果 n 取值为 0，那么 `regexs(n)` 函数将返回整个符合正则表达的字符串。例如，在上文的美国 1978 年汽车数据中的字符串变量 **make** 是包含样本的制造商以及具体的汽车型号：        
     | make            |        
     |:------------------|          
     | Dodge Magnum      |              
     | Dodge St. Regis     |  
     | Ford Fiesta          |     
     | Ford Mustang          |  
    | Linc. Continental   |
     | Linc. Mark V          |  
     | Linc. Versailles     |
     | Merc. Bobcat          | 
    可以看出，制造商与具体的汽车型号之间会有一个空格进行区分，并且型号名中间可能也有空格那么如果我们想要利用这个特征将样本的制造商以及具体的型号名称分别用不同的字符串变量表示，那么我们可以利用**regexs(n)函数**以及**regexm(s,re)函数**：
    ```
    gen manufacturer=regexs(1) if regexm(make,"([A-Za-z.]+[^ ])(.+)")        
    gen model=regexs(2) if regexm(make,"([A-Za-z.]+[^ ])(.+)")
    ```
    其中第一条命令的 **regexs(1)** 表示返回 **if** 后面的 **regexm()** 中的第一个子字符串，即第一个括号表达的内容—— `([A-Za-z.]+[^ ])`。其中，「`([A-Za-z.]+[^ ])(.+)`」是字符串 **make** 的正则表达式。
    
    这里，我们根据 **make** 的特征将其分为两个子字符串，分别用括号区分。各个正则表达式的含义详解如下：
    - "[]"：只匹配方括号内的任意一个字符
    - "+"：将加号前的字符串至少匹配一次
    - "[\^]"：不匹配方括号内的字符串
    - "."：任意一个字符串
    
    由此可知，“`([A-Za-z.]+[^ ])`” 表示匹配所有大小写字母以及英语句号 **“.”** 组成的字符串，该字符串后不接空格，即 **make** 里所有样本第一个单词，而 **“(.+)”** 则表示了其余所有字符串。那么，通过这两条命令，我们可以得到：
     | make            |   manufacturer     |   model |
     |:---------------|:--------------|:-----------|
     | Dodge Magnum    |       Dodge    |    Magnum |
   | Dodge St. Regis  |      Dodge  |   St. Regis |
     | Ford Fiesta         |    Ford    |    Fiesta |
    | Ford Mustang  |     Ford    |   Mustang |
   | Linc. Continental   |   Linc.  | Continental |
    | Linc. Mark V        |   Linc.|        Mark V |
    | Linc. Versailles      | Linc.  |  Versailles |
     | Merc. Bobcat         |  Merc.    |    Bobcat |
### ustrregex 函数
`ustrregex` 函数的用法与 `regex` 函数相似，只是所针对的字符串类型不同 (ASCII 和 unicode)，在实际使用中更为好用，这里就不再赘述。

## 正则表达式元字符

正则表达式基于字符串以及元字符来实现对字符串的识别和匹配，而其中元字符的使用最为关键。正则表达式的元字符及其用法如下所示：

### 1. "^"
"**^**"用于表示所需匹配的字符串以元字符"\^"后的字符串开头。例如,"\^Buick"就是匹配所有以"Buick"开头的字符串。沿用上文美国1978年汽车数据的例子，如果我们想生成一个虚拟变量fbegin，用于描述样本的制造商的英文首字母是否为F开头，其正则表达式就是"\^F":
```
gen fbegin=regexm(make,"^F")
```
那么输出的结果是：

 | Fbegin   |make              |
 |-----------|-----------------|
 |      0   |Dodge Magnum      |
|      0   |Dodge St. Regis   |
 |      1  | Ford Fiesta       |
|      1  |Ford Mustang      |
|      0   |Linc. Continental |
|      0  | Linc. Mark V      |
|      0   |Linc. Versailles  |
|      0   |Merc. Bobcat      |
  
### 2. "$"
"**$**"表示字符串以美元符 **$** 前的字符结尾。例如，"市$"表示匹配所有以“市”结尾的字符串。沿用美国1978年汽车数据的例子，如果我们想要在字符串**make**中匹配出以数字结尾的样本，其正则表达就是"**[0-9]$**"(其中[0-9]意思是匹配任意0到9的字符串)：

```
gen numend=regexm(make,"[0-9]$")
```
所输出的结果是：

| make        |   numend |
|-------------|---------|
| Datsun 210   |       1 |
 | Datsun 510   |       1 |
 |Datsun 810     |     1 |
| Fiat Strada     |    0 |
 | Honda Accord    |    0 |
| Honda Civic       |  0 |
| Mazda GLC          | 0 |
| Peugeot 604         |1 |

### 3. "."
"**.**" 英语的句号在正则表达式中代指任意的字符，无论是数字、字母或者文字，甚至空格，都可以用"**.**"代指。例如，正则表达式"cat."会匹配到字符串中任意含有"cat+任意数量字符串"，即既会匹配到"catt"，也会匹配到"cat2"和"catty"，但不会匹配到"cat"：
```
gen cat=regexm(make,"cat.")
```

 | cat  | make     |
|--------|--------|
 |   0   |213cat   |
|   1   |4646caty |
|   1   |546catty |

### 4. "[ ]"
"**[ ]**"表示匹配任意在方括号内的字符。例如，"gr[ae]y"所匹配既可以是"gray"也可以是"grey"，而不会匹配到"greay"(方括号内字符顺序不影响结果)：
```
gen gry=regexm(make,"gr[ea]y")
```
| make |   gry |
 |------|-------|
 | grey  |    1 |
 | greay  |   0 |
 | gray    |  1 |

### 5. "[^]"
"**[\^]**"则表示匹配的时候忽视"**[\^]**"里面的字符。例如，"1[^02]"可以匹配到"13"、"11"等，但不会匹配到"10"和"12",也可以匹配到"112"，因为存在“1”后面没有跟着“0”或“2”：
```
gen no12=regexm(make,"1[^20]")
```
| make |  no20 |
|-------|------|
| 13     |   1 |
| 12      |  0 |
| 10       | 0 |
 | 11       | 1 |
 | 123      | 0 |
| 132       |1 |
| 112       |1 |

### 6. "[-]"
"**[-]**"表示匹配一个范围内的字符。例如，"[0-5]"表示匹配任意0到5之间的数，"[a-z]"则表示匹配任意a到z之间的字母(区分大小写)，也可以结合使用："[0-5B-Z]"表示匹配任意0到5之间的数字以及B到Z之间字母（大写）。
```
gen range=regexm(make,"^[0-5B-Z]")
```
| make |    range |
|-------|---------|
| 1zla2  |      1 |
| z10055  |     0 |
| 6z1215   |    0 |
| A13215    |   0 |
| B13215     |  1 |

### 7. "?"
"**?**"表示问号前的一个或一组字符匹配0或一次。例如："colou?r"表示匹配"u"0次，即匹配"color"，或者匹配"u"1次，即匹配"colour"，而不会匹配到"colouur"：
```
gen color=regexm(make,"colou?r")
```
| make   |   color |
|---------|--------|
| color    |     1 |
| colour    |    1 |
| colouur    |   0 |

### 8. "+"
"**+**"表示加号+前的一个或一组字符串至少匹配一次。承上例，"colou?r"会匹配到"color"而不会匹配到“colouur”，而"colou+r"则恰好相反：
```
gen color=regexm(make,"colou+r")
```
| make  |    colour |
|-------|-----------|
| color |         0 |
| colour|         1 |
| colouur|        1 |

### 9. "*"
"**\***"则是上述两种符号的并集，表示星号\*前的一个或一组字符串匹配0次或以上。承上例，"colou*r"既会匹配到"color"，也会匹配到"colour"、"colouur"、"colouuuur"等：
```
gen colouur=regexm(make,"colou*r")
```
| make   |   colouur |
|---------|----------|
| color    |       1 |
| colour    |      1 |
| colouur    |     1 |

### 10. "()"
"**()**"表示括号内的字符串为子字符串或者被其它元字符整体运用的一组字符串。例如，"a(bcd)?e"表示匹配"abcde"或者"ae"，即括号内的字符串"bcd"作为一个整体适用于元字符“**?**”：
```
gen Parentheses=regexm(make,"a(bcd)?e")
```
| make     |  Parentheses |
|---------|------------|
| abcde     |        1 |
| ae         |       1 |
| abdde       |      0 |
| abcdbcde     |     0 |
 
子字符串的运用实例参见上文**regexs(n)函数**一节。

### 11. "|"
"**|**"表示选择性匹配，即“或”。例如，"October (First|1st|1)"表示匹配"Obtober First"或"October 1st"或"October 1"：
```
gen Oct1=regexm(make."October (First|1st|1)")
```
| make     |       Oct1 |
|-----------|-----------|
| October 1st|        1 |
| October First|      1 |
| October 1     |     1 |
| October 2     |     0 |
| October 2nd    |    0 |

### 12. "\\"
"**\\**"在Stata里面充当转义符，如果所需要匹配的字符串里面包含了正则表达式元字符，那么在这些字符前键入"\"即可表示后面的字符表示原本的意思，而不是充当元字符。例如，"**\\.**"表示匹配字符串中含有"**.**"的字符，如果不在"**.**"前键入"**\\**"，则Stata会理解为匹配任意字符，即所有非空字符串都会匹配到。以美国1978年汽车数据为例：
```
gen Period=regexm(make,"\.")
```
| make          |      noPeriod |
|----------------|--------------|
| Dodge Magnum    |           0 |
| Dodge St. Regis  |          1 |
| Ford Fiesta       |         0 |
| Ford Mustang       |        0 |
| Linc. Continental   |       1 |
| Linc. Mark V        |       1 |
| Linc. Versailles     |      1 |
| Merc. Bobcat          |     1 |
如果我们去掉转义符"**\\**"，那么我们可以得到：
```
gen noPeriod=regexm(make,".")
```
| make          |      noPeriod |
|----------------|--------------|
| Dodge Magnum   |            1 |
| Dodge St. Regis|            1 |
| Ford Fiesta    |            1 |
| Ford Mustang   |            1 |
| Linc. Continental|          1 |
| Linc. Mark V      |         1 |
 | Linc. Versailles  |        1 |
| Merc. Bobcat        |       1 |


**********
以下正则表达式元字符目前不适用于Stata
### 13. "{n}"
"**{n}**"表示大括号前的字符串匹配正好n次，而不会像"?"、"*"以及"+"那样匹配次数不确定。例如，"[0-9]{3}"则表示匹配任意三个数字组成的字符串：012、446等。

### 14. "{n,}"
"**{n,}**"表示元字符前的字符串匹配至少n次以上，即设定匹配次数的下限。例如，"[0-9]{3,}"则表示匹配任意由三个以上数字组成的字符串，如020，13113等，但不能匹配到"11"这些两个数字组成字符串。

### 15. "{n,m}"
"**{n,m}**"表示元字符前的字符串匹配至少n次，至多m次，即设定匹配次数的上下限。例如，"[0-9]{3,5}"表示匹配由3到5个数字组成的字符串：222、2324、44565，而不会匹配到22或123456。

## 其他正则表达式语法规则
其他比较流行的正则表达式语法包括POSIX Standard以及Perl's Standard，但Stata目前不支持这两种语法规则。
### POSIX标准
* "**[:alnum:]**"表示文字数字式字符，即"[0-9a-zA-Z]"。 "[[:alnum:]]{3}"表示匹配三个文字数字式字符，如"t7S"，但不会匹配到"X-7"。
* "**[:alpha:]**"表示文字字符，不区分大小写，即"[a-zA-Z]"。"[[:alpha:]]{5}"匹配任意5个文字字符组成的字符串，如"AbCdE"等。
* "**[:blank:]**"表示水平空白符——空格和制表符(Tabs)。"[[:blank:]]{3,5}"表示匹配3个到5个空格或制表符。
* "**[:digit:]**"表示数字字符，即"[0-9]"。"[[:digit:]]{3,5}"表示匹配3位数、4位数或5位数。
* "**[:lower:]**"表示小写字母字符，即"[a-z]"。"[[:lower:]]"会匹配到a而不会匹配到A。
* "**[:upper:]**"表示大写字母字符，即"[A-Z]"。"[[:upper:]]"会匹配到A而不会匹配到a。
* "**[:punct:]**"表示任意标点符号。"[[:punct:]]"会匹配到"." 、"," 、"-"等，而不会匹配到7或者a这些数字字符或字母字符。
* "**[:space:]**"表示空白——换行、回车、制表符、空格、垂直制表符。"[[:space:]]"会匹配到任意换行、回车、制表符、空格、垂直制表符。

### Perl's Style正则表达式
* "**//**"表示对斜杠内的字符串进行匹配，如"/colou?r/"表示匹配"colour"或'color"。
* "**i**"表示匹配时忽略大小写，如"/colou?r/i"既可以匹配"colour"，也可以匹配到"COlOUR"。
* "**\b**"表示一个单词边界，即单词与非单词之间的位置。如"/\bfred\b/i"表示所匹配的"fred"是一个独立出来的字符串，前后不存在其他字符，即不会匹配到"Frederick"或者"Alfred"；而"/fred\b/"则可以匹配到"Alfred"而匹配不到"Frederick"。
* "**\B**"表示一个非单词边界。如"/fred\B/"可以匹配到"Frederick"，而匹配不到"Alfred"。
* "**\d**"表示一个数字字符。如"/a\db/"表示匹配"a+数字+b"这种格式的字符串。
* "**\D**"表示一个非数字字符。如"/a\Db/"表示匹配"a+非数字+b"这种格式的字符串。
* "**\n**"表示匹配一个换行符。
* "**\r**"表示匹配一个回车符。
* "**\s**"表示匹配一个空格符。如"/a\sb"匹配的是"a b"而不是"ab"。
* "**\S**"表示匹配一个非空格符。如"a\Sb/"匹配的是"a2b"而不是"a b"。
* "**\t**"表示匹配制表符(Tabs)。如"/\t/"表示匹配一个制表符，即 "    "。
* "**\w**"表示匹配一个包括下划线以内的任意字母和数字，等价于"[a-zA-Z0-9_]"。如"/\w/"可以匹配到"1"和"_"，但匹配不到"?"。
* "**\W**"表示匹配一个非单词字符，等价于"[^0-9A-Za-z_]"。"/a\Wb/i" 可以匹配到"a!b"但匹配不到"a2b"。

# Stata字符串变量处理函数
## 1.文本合并和扩展
### 1.1文本合并：+
Stata的文本合并可以直接用"+"进行运算，例如我们上文中将美国1978年汽车数据中的字符串变量**make**拆分成了manufacturer以及model两个新的字符串变量，如果我们想合并这两个新的字符串变量，那么我们可以直接用"+"进行运算：
```
gen make3=manufacturer+model
```
| make        |        manufa~r    |      model     |          make3 |
|--------------|--------------------|----------------|---------------|
| Dodge Magnum  |         Dodge     |    Magnum      |  Dodge Magnum |
 | Dodge St. Regis|        Dodge    |  St. Regis     |Dodge St. Regis |
| Ford Fiesta      |       Ford     |    Fiesta      |   Ford Fiesta |
| Ford Mustang      |      Ford     |   Mustang      |  Ford Mustang |
| Linc. Continental  |    Linc.    |Continental   |Linc. Continental |
| Linc. Mark V        |   Linc.     |    Mark V    |    Linc. Mark V |
| Linc. Versailles     |  Linc. |    Versailles    |Linc. Versailles |
 | Merc. Bobcat         |  Merc. |        Bobcat    |    Merc. Bobcat |
我们可以看到，字符串变量**manufacturer**和**model**文字合并后与拆分前完全一致。

### 1.2 文本扩展：*
如果文本合并+是字符串变量的加号，那么文本扩展*就是字符串变量的乘号，我们直接用例子说明：
```
gen model2=model*2
```
 |        model|                     model2 |
|---------------|--------------------------|
|       Magnum   |           Magnum Magnum |
|    St. Regis    |    St. Regis St. Regis |
|       Fiesta     |         Fiesta Fiesta |
 |      Mustang     |       Mustang Mustang |
 |  Continental |   Continental Continental |
|       Mark V   |           Mark V Mark V |
|   Versailles    |  Versailles Versailles |
 |       Bobcat    |          Bobcat Bobcat |
 
 ## 2.文本修改和替换
 ### 







* * *

[The do file for this document](../regular_expressions.do)

Last update: 2017-06-03, Stata version 14.2


### 常用元字符、限定符和反义词

常用元字符 |  &emsp;
--- | ---
代码 |	说明
.	| 匹配除换行符以外的任意字符
\w	| 匹配字母或数字或下划线
\s	| 匹配任意的空白符
\d	| 匹配数字
\b	| 匹配单词的开始或结束
^	| 匹配字符串的开始
$	| 匹配字符串的结束

常用限定符 |  &emsp;
--- | ---
代码 | 说明
*	   | 重复零次或更多次
+	   | 重复一次或更多次
?	   | 重复零次或一次
{n}	   | 重复n次
{n,}   | 重复n次或更多次
{n,m}  | 重复n到m次

常用反义词 |  &emsp;
--- | ---
代码 | 说明
\W	   | 匹配任意不是字母，数字，下划线，汉字的字符
\S	   | 匹配任意不是空白符的字符
\D	   | 匹配任意非数字的字符
\B	   | 匹配不是单词开头或结束的位置
[^x]   | 匹配除了x以外的任意字符
[^aei] | 匹配除了aei 这几个字母以外的任意字符

### 常用正则表达式(匹配数字)

> Source: [比较常用的几个正则表达式(匹配数字)](https://blog.csdn.net/wangjia55/article/details/7877915)



- 匹配特定数字：
  - `^-[1-9]\d*$` 　 //匹配负整数
  - `^-?[1-9]\d*$`　　 //匹配整数
  - `^[1-9]\d*|0$`　 //匹配非负整数 (正整数 + 0)
  - `^-[1-9]\d*|0$`　　 //匹配非正整数 (负整数 + 0)
  - `^[1-9]\d*\.\d*|0\.\d*[1-9]\d*$`　　 //匹配正浮点数
  - `^-([1-9]\d*\.\d*|0\.\d*[1-9]\d*)$`　 //匹配负浮点数
  - `^-?([1-9]\d*\.\d*|0\.\d*[1-9]\d*|0?\.0+|0)$`　 //匹配浮点数
  - `^[1-9]\d*\.\d*|0\.\d*[1-9]\d*|0?\.0+|0$`　　 //匹配非负浮点数 (正浮点数 + 0)
  - `^(-([1-9]\d*\.\d*|0\.\d*[1-9]\d*))|0?\.0+|0$`　　//匹配非正浮点数 (负浮点数 + 0)     
  - 验证n位的数字：`^\d{n}$` 
  - 验证至少n位数字：`^\d{n,}$` 
  - 验证m-n位的数字：`^\d{m,n}$` 
  - 验证零和非零开头的数字：`^(0|[1-9][0-9]*)$` 
  - 验证有两位小数的正实数：`^[0-9]+(.[0-9]{2})?$` 
  - 验证有1-3位小数的正实数：`^[0-9]+(.[0-9]{1,3})?$` 
  - 验证非零的正整数：`^\+?[1-9][0-9]*$` 
  - 验证非零的负整数：`^\-[1-9][0-9]*$` 
  - 验证非负整数（正整数 + 0） `^\d+$` 
  - 验证非正整数（负整数 + 0） `^((-\d+)|(0+))$` 
  - 验证长度为3的字符：`^.{3}$` 
- 匹配特定字符串：
  - `^[A-Za-z]+$`     //匹配由 26 个英文字母组成的字符串
  - `^[A-Z]+$`　　   //匹配由 26 个英文字母的大写组成的字符串
  - `^[a-z]+$`　　   //匹配由 26 个英文字母的小写组成的字符串
  - `^[A-Za-z0-9]+$` //匹配由数字和 26 个英文字母组成的字符串
  - `^\w+$`　　      //匹配由数字、26 个英文字母或者下划线组成的字符串
   
- 匹配中文字符的正则表达式： `[\u4e00-\u9fa5]`    
评注：匹配中文还真是个头疼的事，有了这个表达式就好办了

- 匹配双字节字符(包括汉字在内)：`[^\x00-\xff]`    
评注：可以用来计算字符串的长度（一个双字节字符长度计2，ASCII字符计1）

- 匹配空白行的正则表达式：`\n\s*\r`   
评注：可以用来删除空白行

- 匹配 HTML 标记的正则表达式：`<(\S*?)[^>]*>.*?</\1>|<.*? />`    
评注：网上流传的版本太糟糕，上面这个也仅仅能匹配部分，对于复杂的嵌套标记依旧无能为力

- 匹配首尾空白字符的正则表达式：`^\s*|\s*$`    
评注：可以用来删除行首行尾的空白字符(包括空格、制表符、换页符等等)

- 匹配 Email 地址的正则表达式：`\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*`    
评注：表单验证时很实用

- 匹配网址 URL 的正则表达式：`[a-zA-z]+://[^\s]*`    
评注：网上流传的版本功能很有限，上面这个基本可以满足需求

- 匹配帐号是否合法(字母开头，允许 5-16 字节，允许字母数字下划线)：     
`^[a-zA-Z][a-zA-Z0-9_]{4,15}$`    
评注：表单验证时很实用

- 匹配国内电话号码：`\d{3}-\d{8}|\d{4}-\d{7}`    
评注：匹配形式如 0511-4405222 或 021-87888822

- 匹配腾讯 QQ 号：`[1-9][0-9]{4,}`    
评注：腾讯 QQ 号从10000开始

- 匹配中国邮政编码：`[1-9]\d{5}(?!\d)`     
评注：中国邮政编码为 6 位数字

- 匹配身份证：`\d{15}|\d{18}`    
评注：中国的身份证为 15 位或 18 位

- 匹配 ip 地址：`\d+\.\d+\.\d+\.\d+`     
评注：提取ip地址时有用


