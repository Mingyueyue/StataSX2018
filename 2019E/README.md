# 连享会推文工作平台 2019E-南京|内生性专题


---
### 推荐推文选题 

> `2019/10/21 8:33`

1. 翻译并扩充：[What exactly is the confidence interval?](https://acarril.github.io/posts/confidence-interval)。可以查阅一些其他资料，让推文的可读性和 Stata 范例都更加丰富一些。建议提纲如下：
  - 何谓置信区间？
  - 回归结果中的置信区间
  - 使用 bootstrap 获取置信区间
  - Stata 范例
2. 翻译并优化 [How do instrumental variables work?](https://acarril.github.io/posts/IV-intro)。需要增加一些Stata 范例，具体可以和连老师商议，连老师可以提供一些例子。【已认领-杨柳】
3. 翻译并添加 Stata 例子。[multimport: automatic import and append of non-Stata data](https://acarril.github.io/posts/multimport)
4. 翻译并扩充，添加 Stata 范例：[levelsof 命令的妙用](https://acarril.github.io/posts/looping-over-values)
5. 翻译：[执行循环语句时显示进度 - Loops with progress bars ](https://acarril.github.io/posts/progess-bar)【已认领】
6. 翻译并扩充，增加对 `reshape`，`xpose` 等命令的应用。[Preserve labels when collapsing](https://acarril.github.io/posts/collapse-preserving-labels)
7. 翻译并扩充，[Running sections of do-files](https://acarril.github.io/posts/run-dofile-section)【已认领】
8. 翻译并扩充，[Adding column percentages of sums to tabstat](https://acarril.github.io/posts/column-percentages-tabstat)


下面几篇 Stata Journal 上的文章可以看看：

1.  Bruun, N. H. (2019). Visualizing effect modification on contrasts. The Stata Journal, 19(3), 566–580. https://doi.org/10.1177/1536867X19874226， PDF：https://sci-hub.tw/10.1177/1536867X19874226
2.   Cerulli, G., & Ventura, M. (2019). Estimation of pre- and posttreatment average treatment effects with binary time-varying treatment using Stata. The Stata Journal, 19(3), 551–565. https://doi.org/10.1177/1536867X19874224，[[PDF]](https://sci-hub.tw/10.1177/1536867X19874224)
3.   Xu, R., Frank, K. A., Maroulis, S. J., & Rosenberg, J. M. (2019). konfound: Command to quantify robustness of causal inferences. The Stata Journal, 19(3), 523–550. https://doi.org/10.1177/1536867X19874223, [[PDF]](https://sci-hub.tw/10.1177/1536867X19874223) 
    任务：写个评述性的推文，简要介绍文章的基本思想和应用领域。重点在于这篇文章末尾附带的各个领域的应用情况。【已认领-吴思锐】
4. 统计学生的成绩分布和学生考试质量分析，这篇文章很有意思，估计很多老师会喜欢。  Gallup, J. L. (2019). Grade functions. The Stata Journal, 19(2), 459–476. https://doi.org/10.1177/1536867X19854020，
[[PDF]](https://sci-hub.tw/10.1177/1536867X19854020) 【已认领-关月琴-中大】

> `2019/10/19 11:32`

- 有关交乘项的一个汇总说明，参见 [Matt Golder - Interactions](http://mattgolder.com/interactions#code)。可以将其翻译并作进一步细化和深化。可以切割成多篇推文。需要注意的是，最好能预先看看连享会此前发表的有关交乘项的推文，融合到本文中。
  - [William Berry](http://polisci.fsu.edu/people/faculty/berry.htm), Matt Golder & [Daniel Milton](https://danieljmilton.wordpress.com/). 2012\. [“Improving Tests of Theories Positing Interaction.”](http://mattgolder.com/files/research/jop2.pdf)  *Journal of Politics* 74: 653-671.
[abstract] [[online appendix](http://mattgolder.com/files/research/online_appendix.pdf)] [[replication files](http://mattgolder.com/files/research/jop2.zip)] [[webpage](http://mattgolder.com/interactions)] [bibtex] 【已认领-唐跃军】
- 论文重现 (如下多篇论文都涉及交乘项以及边际效应的图形呈现，作者提供了完整的 Stata 程序，我们的任务是简要介绍这篇文章的核心思想，继而把文章里的模型设定和交乘项以及边际效应分析作为重点来展示，便于读者掌握规范的边际效应分析方法)
  - [Ben Gaskins](https://college.lclark.edu/live/profiles/3764-benjamin-gaskins), Matt Golder & [David Siegel](http://people.duke.edu/~das76/). 2013\. [“Religious Participation, Social Conservatism, and Human Development.” ](http://mattgolder.com/files/research/jop3.pdf) *Journal of Politics* 75: 1125-1141. [[online appendix](http://mattgolder.com/files/research/appendix_jop.pdf)] [[replication files](http://mattgolder.com/files/research/jop3.zip)] 【已认领 -李萧宇-中山大学】
  - [Ben Gaskins](https://college.lclark.edu/live/profiles/3764-benjamin-gaskins), Matt Golder & [David Siegel](http://people.duke.edu/~das76/). 2013\. [“Religious Participation and Economic Conservatism.”](http://mattgolder.com/files/research/ajps4.pdf) *American Journal of Political Science* 57: 823-840. [[online appendix](http://mattgolder.com/files/research/ajps4_appendix.pdf)] [[replication files](http://mattgolder.com/files/research/ajps4.zip)]
- 论文重现：Mixed Logit model
  - **Mixed Logit Model** [Garrett Glasgow](http://www.polsci.ucsb.edu/faculty/glasgow/), Matt Golder & [Sona N. Golder](http://sonagolder.com/). 2011\. [“Who “Wins”? Determining the Party of the Prime Minister.”](http://mattgolder.com/files/research/ajps3.pdf)  *American Journal of Political Science* 55: 937-954. [[online appendix](http://mattgolder.com/files/research/appendix_ajps.pdf)] [[replication files](http://mattgolder.com/files/research/ajps3.zip)] 


---

### 2019.10.11 任务


1. 申请注册[码云](https://gitee.com/)账号：https://gitee.com/ 。有关账号设置和码云使用说明，请查看 [-连玉君-码云使用指南-](https://gitee.com/Stata002/StataSX2018/wikis/码云使用指南.md?sort_id=937861)
2. 请点击 [- Stata公众号团队成员邀请码 -](https://gitee.com/Stata002/StataSX2018/invite_link?invite=99d36473e26408ba0ecd1bdada4a9726862a018c6d8a1955b7993663152babc5ba8855ba9e0d87b5828b69667c84aeba) 加入本项目工作小组，以便获得在小组中编辑文档的权限。

---


&emsp;

> 连享会图书馆： [计量教材在线浏览](https://quqi.gblhgk.com/s/880197/wqZT9wv6IKpd4Wh1) || [Stata Journal下载](https://blog.csdn.net/arlionn/article/details/89915318) || [Stata Journal 在线浏览](https://quqi.gblhgk.com/s/880197/Uv7IUeASWJnvyjUd)

> [工作日志](https://gitee.com/Stata002/StataSX2018/blob/master/2019E/工作日志.md) || [FAQs-助教答疑资料](https://gitee.com/Stata002/StataSX2018/tree/master/2019D/FAQs-%E7%AD%94%E7%96%91%E8%B5%84%E6%96%99)


&emsp;

> **特别注意：** 转写推文时的两个最基本的格式要求： (1) 中英文混排时，英文字符和数字两侧要各留一个空格；(2) 认真阅读本文 **第 4 小节** 中的格式要求。


&emsp;


### 1. 项目介绍

大家好，欢迎各位加入推文团队，一起分享实证分析与 Stata 应用中的点点滴滴。

每一篇推文都有一个明确的主题，深入浅出地介绍某一个方法、模型或命令。每人完成 3-5 篇推文 (具体数量取决于推文的难度和质量)。

这些推文都是采用 Markdown 语法写的，非常适合在网页上展示。我平时使用的 [知乎](https://zhuanlan.zhihu.com/arlion) | [简书](http://www.jianshu.com/u/69a30474ef33) | [码云](https://gitee.com/arlionn)，以及有道云笔记，都充分借助了 Markdown 语法的简洁和高效。

> **特别提示：** 建议申请一个 [简书](http://www.jianshu.com/) 账号，然后在右上角图标的下拉菜单中选择 【设置】&rarr; 【常用编辑器】&rarr; 【Markdown 编辑器】。推文请在简书中写，可以实时预览，实时保存，支持图片拖拽和批量上传，以及 LaTeX 格式的数学公式。全部推文完成后贴入码云中你的项目主页下即可。

我为每一位成员设立了一个文件夹，后续完成推文过程中的相关资料都存放在这个文件夹里。项目完成中过程中的讨论在下方的留言区里面完成。

由于在连享会的工作平台里多数的项目都是公开项目，所以我鼓励大家相互串门，了解一下其他的老师和同学们的项目的进展，互相学习。

- 建议大家每人申请如下账号各一个，以便协作，提高工作效率：
  * 一个 【[码云](https://gitee.com)】 账号，这是 [连玉君的码云账号](https://gitee.com/arlionn)
  * 一个 【[有道云笔记](http://note.youdao.com/)】 账号 
  * 一个 【[简书](http://www.jianshu.com)】账号，我的账号是 [连玉君-简书](http://www.jianshu.com/u/69a30474ef33)。


**重要提示：** 
- 建议在 [简书](http://www.jianshu.com) 中使用 Markdown 完成推文的写作，然后贴入你的码云文件夹下即可。简书会实时保存你写的东西，而码云则没有这个功能，这很危险！
- 有些数学公式在码云中无法正常显示，不用太纠结，只要保证在简书的 Markdown 编辑器中能正常显示就可以。

### 2. 注册和工作流程

1. 申请注册[码云](https://gitee.com/)账号：https://gitee.com/ 。有关账号设置和码云使用说明，请查看 [-连玉君-码云使用指南-](https://gitee.com/Stata002/Hello/tree/master/%E7%A0%81%E4%BA%91%E4%BD%BF%E7%94%A8%E6%8C%87%E5%8D%97)
2. 请点击 [- Stata公众号团队成员邀请码 -](https://gitee.com/Stata002/StataSX2018/invite_link?invite=99d36473e26408ba0ecd1bdada4a9726862a018c6d8a1955b7993663152babc5ba8855ba9e0d87b5828b69667c84aeba) 加入本项目工作小组，以便获得在小组中编辑文档的权限。
3. 请阅读 [Stata连享会成员手册-Wikis](https://gitee.com/Stata002/StataSX2018/wikis/%E7%A0%81%E4%BA%91%E4%BD%BF%E7%94%A8%E6%8C%87%E5%8D%97?sort_id=937861)，了解 Markdown 和码云的使用方法等内容。
  - 关于 Markdown，不需要了解太多的内容，只需要花几分钟看看这个就会了：[五分钟 Markdown 教程](https://gitee.com/Stata002/StataSX2018/wikis/%E4%BA%94%E5%88%86%E9%92%9F%20Markdown%20%E6%95%99%E7%A8%8B.md?sort_id=961850)。如果想了解更多，可以看 [[连玉君 Markdown 笔记]](https://www.jianshu.com/p/db1d26af109d)
  - 关于码云，只需要注意如下几点即可：(1) 可以在自己的文件夹下新建文件夹；(2) 新建的推文文件务必要加上后缀 `.md`，如 `推文1：如何使用stata.md`，否则对应的文件不支持 Markdown 语法。(3) 如果这篇推文中包含数据文件、Stata程序或 PDF 文件附件，可以在项目文件夹下新建名为 【Refs】、【Data】、【prog】之类的文件夹，用于存放这些文件。总体的原则是：没篇推文一个文件夹，可以根据需要设定子文件夹，保证每个项目的文件存储结构清晰、易读。

### 3. 推文选题

选题有三个来源：(主要以 1 和 3 为主)
- **来源1：** StataSX2018 >> [【Wiki - 002_备选专题】](https://gitee.com/Stata002/StataSX2018/wikis/Home)
- **来源2：** StataSX2018 >> [【_000待领取任务】](https://gitee.com/Stata002/StataSX2018/tree/master/_000%E5%BE%85%E9%A2%86%E5%8F%96%E4%BB%BB%E5%8A%A1) 
- **来源3：** 你也可以自行提出选题(最好是你正在学习或研究的内容，一举两得)，我们做进一步讨论，以便确认是否合适。这段时间可能主要以这种方式来选题了。

#### 3.1 参考项目
如下几位的推文项目主页都做的非常好，供大家参考：
- 参考项目1：[Stata连享会-游万海](https://gitee.com/Stata002/StataSX2018/tree/master/%E6%B8%B8%E4%B8%87%E6%B5%B7)
- 参考项目2：[Stata连享会-许梦洁](https://gitee.com/Stata002/StataSX2018/tree/master/%E8%AE%B8%E6%A2%A6%E6%B4%81)
- 参考项目3：[Stata连享会-胡雨霄](https://gitee.com/Stata002/StataSX2018/tree/master/2019B/%E8%83%A1%E9%9B%A8%E9%9C%84)

#### 3.2 特别说明    
- 选定主题后，请微信通知我确认一下，自拟提纲开始写作 (如有疑问可以随时联系我讨论)
- 每篇推文是一个项目，每个项目下新建一个以 `.md` 为后缀的 Markdown 文档，用于推文的撰写；如有需要，可以新建子文件夹用于存放数据，程序等附件资料。
- 写初稿时，建议使用 [简书](http://www.jianshu.com/) 或 [CSDN 博客](https://blog.csdn.net/)，这些编辑器都支持实时自动保存。完成后贴入 码云 即可。主要能保证在 上述系统中正常显示即可

---

### 4. 推文写作格式要求

> **特别注意：** 除非万不得已，尽量不要用图片形式展示 Stata 结果。推文中的 Stata 结果尽量采用结果窗口中呈现的文本信息来呈现，也就是采用代码块来呈现估计结果。这样就不会过度依赖图床，图床一旦出问题，就会导致推文中的所有图片都无法正常显示。另外，使用文本块显示结果可以获得更好的跨平台展示效果。

1. **署名**： 请在推文首行填入你的真实姓名 (单位信息)，格式为 `> 张三丰 (xxx大学)，邮箱`
1. **标题和层级：** 推文的各个小节需要顺序编号，一级标题用 **「## 1. 一级标题」** (说明：`##` 后要添加一个空格)，二级标题用 **「### 1.1 二级标题」**，最多用到三级标题，即「#### 1.1.1 三级标题」。如果后续还需细分，可以用 **条目(item)** 或独占一行的粗体文字代替。这主要是因为，多数 Markdown 编辑器的一级标题字号都太大了。
1. **段落：** 段落之间要空一行，这样网页上的显示效果比较好；每个段落不要太长 (最好不好超过 200 字，4 行以内比较好)，否则网页或手机阅读时会比较累；
1. **中英文混排**：英文字符和数字两侧要空一格，否则中英文混排时字符间距过小；例如：「**我用Stata已经15年了(Cox,2019)，但Arlion(2018)说他刚用了14年。**」要修改为：「**我用 Stata 已经 15 年了 (Cox, 2019)，但 Arlion (2018) 说他刚用了 14 年。**」。注意，文中所有圆括号都要在半角模式下输入，`(` 左侧和 `)` 右侧各添加一个空格。
1. **Stata 相关**
  - Stata 要写为 「Stata」(首字母大写，无需加粗)，不要写成 「stata」或「STATA」。
  - 变量名用粗体 (如 \*\*varname\*\* &rarr; **varname**)；
  - Stata 命令用高亮显示 (如 \`regress\` &rarr; `regress`)；
  - 多行 Stata 命令和结果展示使用 **代码块样式**，即使用首尾 **\`\`\`** 包围，首行为 「\`\`\`stata」，尾行为「\`\`\`」。
  - **特别注意：** 除非万不得已，尽量不要用图片形式展示 Stata 结果。推文中的 Stata 结果尽量采用结果窗口中呈现的文本信息来呈现，也就是采用代码块来呈现估计结果。这样就不会过度依赖图床，图床一旦出问题，就会导致推文中的所有图片都无法正常显示。另外，使用文本块显示结果可以获得更好的跨平台展示效果。
5. **数学公式：** (1) 单行公式：用 **\$\$**math**\$\$** 包围的单行数学公式上下各空一行，以保证公式和正文之间的段落间距合理。(2) 行内公式：可以使用 **\$**math**\$** 包围。为了保证在知乎，简书和码云中都能正常显示公式，请把 $y=x\beta$ 写成 `$y=x\beta$`，而不要写成 `$ y=x\beta $` (内侧多加了两个空格)。&emsp; **惊喜：** 无论是网页还是 PDF 中的数学公式，都可以使用 mathpix (https://mathpix.com/) 软件扫描后识别成 LaTeX 数学公式，非常快捷。参见 [神器-数学公式识别工具 mathpix](https://www.jianshu.com/p/1f0506163694)
6. **图片**： 参见 [如何在 Markdown 中插入图片](https://gitee.com/Stata002/StataSX2018/wikis/%E5%A6%82%E4%BD%95%E5%9C%A8%20Markdown%20%E4%B8%AD%E6%B7%BB%E5%8A%A0%E5%9B%BE%E7%89%87?sort_id=1422837)
1. **版权和引用：** 推文若为英文翻译稿，请务必在文首注明来源(独占一行)。Markdown 格式为：「\**Source：**\[原英文标题](网址)」，显示效果为「**Source：**[原英文标题](网址)」。文中若有从别处复制粘贴而来的内容，要标明出处。
1. **底部文本：** 推文底部的文字介绍无需添加，在修改完后会由总编统一添加。